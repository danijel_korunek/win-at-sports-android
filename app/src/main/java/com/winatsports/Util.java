package com.winatsports;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.winatsports.activities.MainActivity;
import com.winatsports.data.Constants;
import com.winatsports.data.MyBet;
import com.winatsports.data.MyParlay;
import com.winatsports.data.adapters.RealBetTutorialAdapter;
import com.winatsports.data.elements.Odd;
import com.winatsports.data.header.HeaderMainClass;
import com.winatsports.data.elements.BetType;
import com.winatsports.data.elements.Game;
import com.winatsports.data.elements.League;
import com.winatsports.data.elements.Sport;
import com.winatsports.data.elements.SportIcon;
import com.winatsports.data.elements.Team;
import com.winatsports.data.parlaytotal.ParlayOdd;
import com.winatsports.data.parsers.XmlAddUserHelper;
import com.winatsports.data.parsers.XmlHistoryWagersHelper;
import com.winatsports.data.stats.StatsCollection;
import com.winatsports.data.stats.UserStats;
import com.winatsports.data.upload.AsyncAddUser;
import com.winatsports.dialogs.RealMoneyWarningDialog;
import com.winatsports.settings.Settings;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.zip.GZIPInputStream;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by dkorunek on 18/01/16.
 */

public class Util {

    public static boolean isNetworkConnected(Context context){
        boolean isNetworkAvailable = false;
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connManager.getActiveNetworkInfo();

        if( netInfo != null ) {
            if (netInfo.isConnectedOrConnecting() == true ) // I think this is better to write then ===> netInfo.isConnected()
                isNetworkAvailable = true;
        }
        else
            isNetworkAvailable = false;
        return isNetworkAvailable;
    }

    public static ArrayList<String> createValuesForCarousel() {

        ArrayList<String> moneyValues = new ArrayList<String>();

        moneyValues.add("$5");
        moneyValues.add("$10");
        moneyValues.add("$20");
        moneyValues.add("$50");
        moneyValues.add("$100");

        return  moneyValues;
    }

    public static String getBetSlipTitle() {
        double totalOdds = Util.calculateTotalOdds();

        StringBuffer sb = new StringBuffer();
        sb.append(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_PARLAY));

        if (MyApplication.getInstance().getParlayArray().size() > 0) {
            sb.append(" (" + MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_TOTAL) + " ");
            sb.append(Util.formatDecimalOdds(MyApplication.getInstance(), totalOdds) + ")");
        }

        return sb.toString();
    }


    public static double convertUSOddsToDecimalOdds(Integer USOdds) {
        double betOdd = (double) USOdds;
        double toReturnOdd = 0.0;

        if( betOdd > 0.0 )
            toReturnOdd = (betOdd / 100.0) + 1.0;
        else if (betOdd < 0.0)
            toReturnOdd = (100.0 / Math.abs(betOdd)) + 1.0;

        return fixDecimal(toReturnOdd);
    }


    public static double fixDecimal(double decimalOdds) {

        int iDec = (int)Math.round(decimalOdds * 100.0);
        return (double)iDec / 100.0;
    }

    public static String formatMoney(double money, boolean forTitle) {
        NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.US);
        if (forTitle)
            formatter.setMaximumFractionDigits(0);
        return formatter.format(money);
    }

    public static double calculateTotalOdds() {

        double totalOdds = 1.0;
        for (ParlayOdd po : MyApplication.getInstance().getParlayArray()) {
            totalOdds *= Util.convertUSOddsToDecimalOdds(po.getOdd().getBetOdds());
        }
        return totalOdds;
    }

    public static String formatDecimalOdds(Context context, double decimal) {

        int settingsOdds = Settings.getOddType(context);
        switch (settingsOdds)  {
            case 0: {
                if (decimal <= 1.0) return "+100";
                if (decimal < 2.0) return "-" + String.valueOf(Math.round(100.0 / (decimal - 1.0)));
                if (decimal > 2.0) {
                    String returnString = "+" + Math.round(100.0 * (decimal - 1.0));
                    return returnString;
                }

                return  "+100";
            }
            case 2: {

                decimal -= 1.0;

                int part1 = (int) Math.round(decimal * 100.0);
                int part2 = 100;

                int divider = findGreatestCommonDivider(part1, part2);

                part1 = part1 / divider;
                part2 /= divider;

                return String.valueOf( part1 + "/" + part2);
            }
        }
        String returnString = String.format("%.2f", decimal);
        return returnString; // this is FOR case 1, FOR US ODDS
    }

    private static int findGreatestCommonDivider(int a, int b) {
        if (b==0) return a;
        return findGreatestCommonDivider(b, a % b);
    }

    /**
     *
     * @return true if a game is an Futures entry
     */
    public static boolean isFutures(Game g) {
        // hardcoded because this code was transcoded from iOS version
        if (g != null && Util.isPopulated(g.getFuturesDesc()) ) {
            if( g.getEventType() == 2 )
                return true;
        }

        return false;
    }

    public static String formatHomeVsAway( Game g, Context c ) {
        if (Util.isFutures(g)) return g.getGameDesc();

        if( g != null && g.getTeamArrayList().size() == 2 ) {
            Team team1 = g.getTeamArrayList().get(0);
            Team team2 = g.getTeamArrayList().get(1);
            String team1Name = team1.getName() == null ? "" : team1.getName();
            String team2Name = team2.getName() == null ? "" : team2.getName();

            if (!Util.isPopulated(team1Name) && !Util.isPopulated(team2Name))
                return "";

            if (canSwapHomeAndAway(g.getSportID()) && Settings.getTeamOrder(c) == Constants.C_SETTINGS_AWAY_VS_HOME) {
                return team2Name + " " + MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_VS) + " " + team1Name;
            }

            return team1Name + " " + MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_VS) + " " + team2Name;
        }

        return "";
    }

    public static String getOddName(ParlayOdd odd) {
        for(BetType betType : odd.getSport().getBetTypes()) {
            if( betType.getSportID() == odd.getGame().getSportID() ) {
                int betTypeID = betType.getTypeID();

                if (betTypeID == odd.getOdd().getBetTypeId())
                    return betType.getTypeName();
            }
        }
        return "";
    }

    // function to check if some sport can swap, change ===> 1) home vs away and 2) away vs home
    public static boolean canSwapHomeAndAway( int sportID ) {
        for (Sport sport : MyApplication.getInstance().getAllSports()) {
            if (sport.getSportID() == sportID) {
                if (sport.getChangeOrder() == Constants.C_SPORT_CHANGE_ORDER)
                    return true;
                else
                    return false;
            }
        }

        return false;
    }

    /**
     * @param context ui context (Activity)
     * @param message If I want to display some special message(for example like exception)
     * @param cancelListener
     */
    public static void showMessageDialog(Context context, String message, DialogInterface.OnCancelListener cancelListener) {

        final Dialog dialog = new Dialog(context);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_download_error);

        TextView tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
        tvMessage.setText(message);

        Button btnClose = (Button) dialog.findViewById(R.id.btnClose);
        String closeStr = Util.getString(Constants.MSG_KEY_REALBETTINGLOGINPOPUPCANCELBUTTON);
        if (!Util.isPopulated(closeStr))
            closeStr = context.getString(R.string.button_close);
        btnClose.setText(closeStr);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.setOnCancelListener(cancelListener);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = (int) (context.getResources().getDisplayMetrics().widthPixels * Util.getMultiplierForDialogWidth((Activity) context));
        dialog.getWindow().setBackgroundDrawable(null);
        dialog.show();
        dialog.getWindow().setAttributes(lp);

    }

    public static void showUpdateDialog(final Context context) {

        final Dialog dialog = new Dialog(context);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_update);

        TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        tvTitle.setText(Util.getString(Constants.MSG_KEY_UPDATE_TITLE));

        TextView tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
        tvMessage.setText(Util.getString(Constants.MSG_KEY_UPDATE_POPUP_DESC));

        Button btnClose = (Button) dialog.findViewById(R.id.btnClose);
        btnClose.setText(Util.getString(Constants.MSG_KEY_UPDATE_OK_BUTTON));
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                // open google play store
                final String appPackageName = MyApplication.getInstance().getPackageName(); // getPackageName() from Context or Activity object
                ((Activity) context).finish();

                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    MyApplication.getInstance().startActivity(intent);
                } catch (android.content.ActivityNotFoundException anfe) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    MyApplication.getInstance().startActivity(intent);
                }
                MyApplication.getInstance().quit();
            }
        });

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = (int) (context.getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
        dialog.getWindow().setBackgroundDrawable(null);
        dialog.show();
        dialog.getWindow().setAttributes(lp);

    }

    public static boolean isPopulated(String s) {
        if (s != null && s.length() > 0 && !s.equalsIgnoreCase("null"))
            return true;
        return false;
    }

    /**
     *
     * @param s
     * @return Empty string if s == null or the string itself.
     */
    public static String fixNull(String s) {
        return s == null ? "" : s;
    }

    public static String[] explode(final String s) {
        // this function was transcoded from the iOS version of the app
        if (s == null || (s != null && s.length() == 0))
            return null;

        String[] lines1 = s.split("\r\n");
        String[] lines2 = s.split("\r");
        String[] lines3 = s.split("\n");

        if (lines1 != null && lines1.length > 1)
            return lines1;
        else if (lines2 != null && lines2.length > 1)
            return lines2;
        else if (lines3 != null && lines3.length > 1)
            return lines3;

        return null;
    }

    public static boolean isApplicationSentToBackground(final Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (tasks != null && !tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    public static String getTopActivity(final Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (tasks != null && !tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (topActivity != null)
                return topActivity.getClassName();
        }
        return null;
    }

    public static String getString(String messageKey) {

        String returnValue = "";
        if( MyApplication.getInstance().getAllMessages().size() > 0 ) {
            try {
                returnValue = MyApplication.getInstance().getAllMessages().get(messageKey);
            } catch (Exception e) {
                returnValue = "";
            }
        }

        if (!Util.isPopulated(returnValue))
            return "";

        returnValue = returnValue.replace("**", "\n");
        returnValue = returnValue.replace("##one_time_bonus##", Util.formatMoney( (double) MyApplication.getInstance().getUserData().getSpecialBonus(), false));
        returnValue = returnValue.replace("##wallet_amount##",  Util.formatMoney( (double) MyApplication.getInstance().getUserData().getWalletAmount(), false));
        returnValue = returnValue.replace("##parlay_count##", String.valueOf(MyApplication.getInstance().getHeaderMainClass().getParlayLimit()) );
        returnValue = returnValue.replace("##welcome_bonus##", Util.formatMoney( (double)MyApplication.getInstance().getHeaderMainClass().getRegisterBonus(), false));
        returnValue = returnValue.replace("##tm##", "(TM)" );
        returnValue = returnValue.replace("##device_name##", Build.MANUFACTURER + android.os.Build.MODEL );

        return returnValue;

    }

    public static void showAnimationForWalletTitle(final TextView textView) {
        final ScaleAnimation flipYPart1 = new ScaleAnimation(1.0f, 1.0f, 100.0f, 0.01f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        flipYPart1.setStartOffset(3600);
        flipYPart1.setInterpolator(new AccelerateDecelerateInterpolator());
        flipYPart1.setDuration(400);
        flipYPart1.setFillAfter(true);

        final ScaleAnimation flipYPart2 = new ScaleAnimation(1.0f, 1.0f, 0.01f, 100.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        flipYPart2.setStartOffset(4000);
        flipYPart2.setInterpolator(new AccelerateDecelerateInterpolator());
        flipYPart2.setDuration(400);
        flipYPart2.setFillAfter(true);

        final Animation out = new AlphaAnimation(1.0f, 1.0f);
        out.setDuration(3600);
        out.setFillAfter(true);
        out.setStartOffset(4400);
        out.setInterpolator(new AccelerateDecelerateInterpolator());

        final ScaleAnimation flipYPart3 = new ScaleAnimation(1.0f, 1.0f, 100.0f, 0.01f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        flipYPart3.setStartOffset(8000);
        flipYPart3.setInterpolator(new AccelerateDecelerateInterpolator());
        flipYPart3.setDuration(400);
        flipYPart3.setFillAfter(true);

        final ScaleAnimation flipYPart4 = new ScaleAnimation(1.0f, 1.0f, 0.01f, 100.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        flipYPart4.setStartOffset(8400);
        flipYPart4.setInterpolator(new AccelerateDecelerateInterpolator());
        flipYPart4.setDuration(400);
        flipYPart4.setFillAfter(true);

        final AnimationSet set = new AnimationSet(true);

        set.addAnimation(flipYPart1);
        set.addAnimation(flipYPart2);
        set.addAnimation(out);
        set.addAnimation(flipYPart3);
        set.addAnimation(flipYPart4);

        textView.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_IN_PLAY)
                + ": " + Util.formatMoney(MyApplication.getInstance().getUserData().getInPlayAmount(), true)); // should be In game: $total_bet_amount

        flipYPart1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                textView.setText(Util.generateWalletTitle());
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        flipYPart3.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                textView.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_IN_PLAY)
                        + ": " + Util.formatMoney(MyApplication.getInstance().getUserData().getInPlayAmount(), true)); // should be In game: $total_bet_amount
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        set.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //Util.showAnimationForWalletTitle(textView);
                textView.startAnimation(set);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        textView.startAnimation(set);

    }

    public static String generateWalletTitle() {
        return MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_WALLET) + ": " + Util.formatMoney(MyApplication.getInstance().getUserData().getWalletAmount(), true);
    }

    public static void showQuitDialog(final Activity uiContext) {
        final Dialog dialog = new Dialog(uiContext);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_quit);

        TextView tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
        tvMessage.setText(uiContext.getString(R.string.quit_message));

        Button btnYes = (Button) dialog.findViewById(R.id.btnYes);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().setQuitting(true);
                dialog.dismiss();
                uiContext.finish();
            }
        });

        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = (int) (uiContext.getResources().getDisplayMetrics().widthPixels * Util.getMultiplierForDialogWidth(uiContext));
        dialog.getWindow().setBackgroundDrawable(null);
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public synchronized static League getLeague(long leagueId, long leagueType) {
        synchronized (MyApplication.lock) {
            ArrayList<League> leagues = MyApplication.getInstance().getAllLeagues();
            for (League l : leagues) {
                if (l.getLeagueID() == leagueId && l.getLeagueType() == leagueType)
                    return l;
            }
        }

        return null;
    }

    public synchronized static League getLeague(long leagueId) {
        synchronized (MyApplication.lock) {
            for (League l : MyApplication.getInstance().getAllLeagues()) {
                if (l.getLeagueID() == leagueId)
                    return l;
            }
        }

        return null;
    }

    public synchronized static Sport getSport(long id) {
        synchronized (MyApplication.lock) {
            ArrayList<Sport> sports = MyApplication.getInstance().getAllSports();
            for (Sport s : sports) {
                if (s.getSportID() == id)
                    return s;
            }
        }
        return null;
    }

    public synchronized static Game getGame(long eventId) {
        synchronized (MyApplication.lock) {
            ArrayList<Game> games = MyApplication.getInstance().getAllGames();
            for (Game game : games) {
                if (game.getEventID() == eventId)
                    return game;
            }
        }

        return null;
    }

    public static String getBetResult(MyParlay parlay) {
        switch ((int) parlay.getBetResult()) {
            case 1: return Util.getString(Constants.MSG_KEY_OTHER_WON);
            case 2: return Util.getString(Constants.MSG_KEY_OTHER_LOST);
            case 3: return Util.getString(Constants.MSG_KEY_OTHER_PUSH3);
            case 4: return Util.getString(Constants.MSG_KEY_OTHER_PUSH4);
            case 5: return Util.getString(Constants.MSG_KEY_OTHER_PUSH5);
            case 6: return Util.getString(Constants.MSG_KEY_OTHER_PUSH6);
        }
        return Util.getString(Constants.MSG_KEY_OTHER_PUSH);
    }

    public static String formatOdds(Context c, int usOdd) {
        int oddType = Settings.getOddType(c);
        if (oddType == Constants.C_DECIMAL_ODDS) {
            double decimal = Util.convertUSOddsToDecimalOdds(usOdd);
            return String.format("%.02f", decimal);
        } else if (oddType == Constants.C_FRACTIONAL_ODDS) {
            double decimal = Util.convertUSOddsToDecimalOdds(usOdd);
            decimal -= 1.0;

            decimal *= 100;

            int part1 = (int) Math.rint(decimal);
            int part2 = 100;
            int divider = Util.findGreatestCommonDivider(part1, part2);
            part1 /= divider;
            part2 /= divider;

            return String.format("%d/%d", part1, part2);
        }

        if (usOdd < 0) return "" + usOdd;

        return "+" + usOdd;
    }

    // This code is taken from Sports Betting iOS and adjusted for Java
    public static int determineSortId(int betTypeId) {
        switch (betTypeId) {
            case 3:
                return 1;
            case 1:
                return 2;
            case 7:
                return 3;
            case 5:
                return 4;
            case 4:
                return 5;
            case 2:
                return 6;
            case 8:
                return 7;
            case 6:
                return 8;
            case 10:
                return 9;
            case 9:
                return 10;
            case 11:
                return 11;
            case 13:
                return 12;
            case 16:
                return 13;
            case 19:
                return 14;
            case 22:
                return 15;
            case 12:
                return 16;
            case 15:
                return 17;
            case 18:
                return 18;
            case 21:
                return 19;
            case 14:
                return 20;
            case 17:
                return 21;
            case 20:
                return 22;
            case 23:
                return 23;
        }
        return 0;
    }

    public static String formatDate(final String dateAndTime) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat ndf = new SimpleDateFormat("EEEE, MMMM d, yyyy 'at' HH:mm");

        Date parsedDate = null;
        try {
            parsedDate = ndf.parse(dateAndTime);
            return dateAndTime;
        } catch (Exception ignored) {
            // ignored
        }

        try {
            parsedDate = df.parse(dateAndTime);
            String dayOfTheWeek = (String) android.text.format.DateFormat.format("EEEE", parsedDate);
            String day = (String) android.text.format.DateFormat.format("dd", parsedDate);

            Calendar c = Calendar.getInstance(Locale.US);
            c.setTime(parsedDate);
            String fullNameOfMonth = c.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.US);
            // That way I need this function to get full name of month
            String year = (String) android.text.format.DateFormat.format("yyyy", parsedDate);
            CharSequence getOnlyHoursAndMinute = DateFormat.format("HH:mm", parsedDate.getTime());

            // Sunday, April 9, 2005 at 11:00
            return dayOfTheWeek + ", " + fullNameOfMonth + " " + day + ", " + year + " at " + getOnlyHoursAndMinute;
        } catch (Exception e) {
            Log.e(Util.class.getSimpleName(), "", e);
        }

        return dateAndTime;
    }

    public static String formatWMIDate(final String dateAndTime) {
        SimpleDateFormat inputDF = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date parsedDate = inputDF.parse(dateAndTime);
            Calendar cal = Calendar.getInstance(Locale.US);
            cal.setTime(parsedDate);

            String fullMonth = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.US);
            String day = "" + cal.get(Calendar.DAY_OF_MONTH);
            String year = "" + cal.get(Calendar.YEAR);

            return fullMonth + ", " + day + " " + year;
        } catch (Exception e) {
            Log.e(Util.class.getSimpleName(), "", e);
        }

        return dateAndTime;
    }

    public static String addWeekToDate(final String date) {
        SimpleDateFormat inputDF = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date tmp = inputDF.parse(date);
            Calendar cal = Calendar.getInstance();
            cal.setTime(tmp);
            cal.add(Calendar.WEEK_OF_YEAR, 1);

            String fullMonth = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.US);
            String day = "" + cal.get(Calendar.DAY_OF_MONTH);
            String year = "" + cal.get(Calendar.YEAR);

            return fullMonth + ", " + day + " " + year;
        } catch (Exception e) {
            Log.e(Util.class.getSimpleName(), "", e);
        }

        return date;
    }

    public static void executeAddUser(Context context, boolean logout, boolean redownload) throws Exception {
        Log.d("ADD_USER", "Executing it... logout = " + logout);
        long now = System.currentTimeMillis();

        long loginTime = Settings.getLoginTime(context);
        long fgTimer = Settings.getTimeWhenEnteredFg(context);
        long timeSpentInAppUntilNow = 0;
        long stats_timer = Settings.getStatsTimer(context);

        if (loginTime == 0) {
            Settings.setLoginTime(context, now);
            loginTime = now;
        }

        if (stats_timer == 0) {
            Settings.setStatsTimer(context, 0);
        }

        if (loginTime > fgTimer)
            timeSpentInAppUntilNow = (now - loginTime) / 1000;
        else
            timeSpentInAppUntilNow = (now - fgTimer) / 1000;

        stats_timer += timeSpentInAppUntilNow;

        HttpClient client = HttpClientBuilder.create()
                .setMaxConnTotal(1)
                .setUserAgent(System.getProperty("http.agent", "Unknown"))
                .build();

        String versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        int versionCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;

        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
/*
        String tmDevice = "" + tm.getDeviceId();
        String tmSerial = "" + tm.getSimSerialNumber();
        String androidID = "" + android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
*/
        AdvertisingIdClient.Info idInfo = null;
        try {
            idInfo = AdvertisingIdClient.getAdvertisingIdInfo(getApplicationContext());
        } catch (Exception e) {
            Log.e(AsyncAddUser.class.getSimpleName(), "", e);
        }

        String advertisingId = null;
        try {
            advertisingId = idInfo.getId();
        } catch (NullPointerException e) {
            Log.e(AsyncAddUser.class.getSimpleName(), "", e);
        }
        if (advertisingId == null)
            advertisingId = "";

        /*UUID deviceUuid = new UUID(androidID.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
        String imei = deviceUuid.toString();*/
        String deviceName = Build.MANUFACTURER + android.os.Build.MODEL;
        String baseOs = android.os.Build.VERSION.RELEASE;

        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                .setRedirectsEnabled(false)
                .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                .setCircularRedirectsAllowed(false).build();

        HttpPost post = new HttpPost(Uri.parse(Constants.URL_ADD_USER).toString());
        post.setConfig(config);

        List<NameValuePair> pairs = new ArrayList<NameValuePair>();

        pairs.add(new BasicNameValuePair("key", Constants.ADD_USER_REQUEST_HEADER_KEY));
        pairs.add(new BasicNameValuePair("udid", advertisingId ));
        pairs.add(new BasicNameValuePair("device_id", Settings.getDeviceId(context)));
        pairs.add(new BasicNameValuePair("dtype", Constants.C_DTYPE));

        pairs.add(new BasicNameValuePair("user_d", Settings.getUserD(context)));

        pairs.add(new BasicNameValuePair("user_r", Settings.getUserR(context) ));
        pairs.add(new BasicNameValuePair("device", deviceName ));
        pairs.add(new BasicNameValuePair("os", baseOs ));
        Log.d(Util.class.getSimpleName(), "Logout: " + logout + " Seconds sent: " + stats_timer);
        pairs.add(new BasicNameValuePair("sec", "" + stats_timer));

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf.format(new Date(System.currentTimeMillis()));

        Log.d("ADD_USER", "Before: user_r = " + Settings.getUserR(context) + " user_d = " + Settings.getUserD(context));

        pairs.add(new BasicNameValuePair("datum", date ) );
        pairs.add(new BasicNameValuePair("fver", versionName  ) );
        pairs.add(new BasicNameValuePair("ver", String.valueOf(versionCode) ) );

        pairs.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded charset=utf-8"));

        post.setEntity(new UrlEncodedFormEntity(pairs));

        HttpResponse response = client.execute(post);

        BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuffer returnData = new StringBuffer();
        int character;

        while ( (character = br.read()) != -1 ) {
            returnData.append((char) character);
        }

        br.close();

        XmlAddUserHelper xmlAddUserHelper = new XmlAddUserHelper(context);
        xmlAddUserHelper.parseXMLFile(returnData.toString());

    }

    public static String getUserId(Context context) {
        return !Settings.getUserR(context).equals(Settings.DEFAULT_USER_R) ? Settings.getUserR(context) : Settings.getUserD(context);
    }

    public static boolean isRealBettingEnabled(final Context context) {


        //Log.e(Util.class.getSimpleName(), "Times run: " + Settings.getTimesRun(context));

        synchronized (MyApplication.lock) {
            HeaderMainClass config = MyApplication.getInstance().getHeaderMainClass();
            if (config == null)
                return false;
            try {
                if (!Settings.getUserR(MyApplication.getInstance()).equals(Settings.DEFAULT_USER_R)) {
                    // logged in
                    if (config.getRealBetSettings().isRealbetOn()) {
                        // compare dates first if possible
                        if (Util.isPopulated(config.getRealBetSettings().getRealbetFromRegDate()) &&
                                Util.isPopulated(MyApplication.getInstance().getUserData().getFirstRun())) {

                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                            try {
                                Date realBetDate = sdf.parse(config.getRealBetSettings().getRealbetFromRegDate());
                                Date firstRunDate = sdf.parse(MyApplication.getInstance().getUserData().getFirstRun());
                                if (!realBetDate.after(firstRunDate)) {
                                    Log.e(Util.class.getSimpleName(), "Cannot show real betting");
                                    return false;
                                }
                            } catch (Exception e) {
                                Log.e(Util.class.getSimpleName(), "Error comparing real bet dates", e);
                            }
                        }

                        if (Settings.getTimesRun(context) > config.getRealBetSettings().getRealbetRuns())
                            return true;

                    }
                } else {
                    if (config.getRealBetSettings().isRealbetGuestsOn()) {
                        // user is not logged in, check if real betting for guests is on
                        if (Settings.getTimesRun(context) > config.getRealBetSettings().getRealbetGuestsRuns())
                            return true;
                    }
                }

            } catch (Exception e) {
                Log.e(Util.class.getSimpleName(), "", e);
            }
        }
        return false;
    }

    public static void showRealBettingDialog(final Activity uiContext) {
        if (!isRealBettingEnabled(uiContext)) {
            Log.e(Util.class.getSimpleName(), "Real betting not enabled (yet)");
            return;
        }

        if (MyApplication.getInstance().rbDialog != null) {
            MyApplication.getInstance().rbDialog.dismiss();
            MyApplication.getInstance().rbDialog = null;
        }

        MyApplication.getInstance().rbDialog = new Dialog(uiContext);

        MyApplication.getInstance().rbDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        MyApplication.getInstance().rbDialog.setCancelable(true);
        MyApplication.getInstance().rbDialog.setContentView(R.layout.dialog_real_betting);

        TextView tvTitle = (TextView) MyApplication.getInstance().rbDialog.findViewById(R.id.tvTitle);
        tvTitle.setText(Util.getString(Constants.MSG_KEY_IPOP_TITLE));

        TextView tvMessage = (TextView) MyApplication.getInstance().rbDialog.findViewById(R.id.tvMessage);
        tvMessage.setText(Util.getString(Constants.MSG_KEY_IPOP_TEXT));

        Button btnRealMoney = (Button) MyApplication.getInstance().rbDialog.findViewById(R.id.btnRealMoney);
        btnRealMoney.setText(Util.getString(Constants.MSG_KEY_REALBETTINGTAB2));
        btnRealMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().rbDialog.dismiss();

                if (!Settings.wasRealBettingTutorialShown(uiContext)) {
                    if( !Settings.getUserR(uiContext).equals(Settings.DEFAULT_USER_R) ) {
                        if (MyApplication.getInstance().getHeaderMainClass().isShowingRealBettingTutorial()) {
                            Settings.setRealBettingTutorialShown(uiContext, true);
                            Util.showRealBettingTutorial(uiContext, 0);
                        } else {
                            MyApplication.getInstance().startRealBetting(false, uiContext, 0);
                        }
                    } else {
                        RealMoneyWarningDialog realMoney = new RealMoneyWarningDialog(uiContext, null);
                        realMoney.show();
                        return;
                    }

                } else {
                    if (Util.isRealBettingEnabled(uiContext)) {
                        if( Settings.getUserR(uiContext).equals(Settings.DEFAULT_USER_R) ) {
                            RealMoneyWarningDialog realMoney = new RealMoneyWarningDialog(uiContext, null);
                            realMoney.show();
                            return;
                        } else
                            MyApplication.getInstance().startRealBetting(false, uiContext, 0);
                    }
                }
            }
        });

        Button btnGameMode = (Button) MyApplication.getInstance().rbDialog.findViewById(R.id.btnGameMode);
        btnGameMode.setText(Util.getString(Constants.MSG_KEY_REALBETTINGTAB1));
        btnGameMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (uiContext instanceof MainActivity)
                    ((MainActivity) uiContext).refreshViews();
                MyApplication.getInstance().rbDialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(MyApplication.getInstance().rbDialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = (int) (uiContext.getResources().getDisplayMetrics().widthPixels * Util.getMultiplierForDialogWidth(uiContext));
        MyApplication.getInstance().rbDialog.getWindow().setBackgroundDrawable(null);
        MyApplication.getInstance().rbDialog.show();
        MyApplication.getInstance().rbDialog.getWindow().setAttributes(lp);
    }

    public static void showLogoutDialog(final Activity uiContext) {
        final Dialog dialog = new Dialog(uiContext);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_logout);

        TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        tvTitle.setText(Util.getString(Constants.MSG_KEY_PROFILELOGOUTTITLE));

        TextView tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
        tvMessage.setText(Util.getString(Constants.MSG_KEY_PROFILELOGOUTMESSAGE));

        Button btnYes = (Button) dialog.findViewById(R.id.btnYes);
        btnYes.setText(Util.getString(Constants.MSG_KEY_PROFILELOGOUTCONFIRMBUTTON));
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new AsyncAddUser(uiContext, true).execute();
            }
        });

        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        btnNo.setText(Util.getString(Constants.MSG_KEY_PROFILELOGOUTCANCELBUTTON));
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                if (uiContext instanceof MainActivity)
                    ((MainActivity) uiContext).setupProfileView();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = (int) (uiContext.getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
        dialog.getWindow().setBackgroundDrawable(null);
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public static void showUrlInBrowser(String url) {
        try {
            Uri uri = Uri.parse(url);
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);
            browserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            MyApplication.getInstance().startActivity(browserIntent);
        } catch (Exception e) {
            Log.e(Util.class.getSimpleName(), "", e);
        }
    }

    public static void showRealBettingTutorial(final Activity uiContext, int eventId) {
        final Dialog dialog = new Dialog(uiContext);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_real_bet_tutorial);

        ViewPager viewPager = (ViewPager) dialog.findViewById(R.id.viewPager);
        final RealBetTutorialAdapter adapter = new RealBetTutorialAdapter(uiContext, dialog, eventId);
        viewPager.setAdapter(adapter);

        final LinearLayout llDots = (LinearLayout) dialog.findViewById(R.id.llDots);

        for ( int i = 0; i < adapter.getCount(); i++ ) {
            ImageButton imgDot = new ImageButton(uiContext);
            imgDot.setTag(i);
            imgDot.setImageResource(R.drawable.view_pager_tutorial_dot_selector);
            imgDot.setBackgroundResource(0);
            // Here I set last dot to be aplha 0.0f, because I don't want to see last dot on screen
            // And just for any case I have set backgroundColor ===> Transparent
            // Otherwise I will have here 5 dots
            if( i == 3) {
                imgDot.setAlpha(0.0f);
                imgDot.setBackgroundColor(Color.TRANSPARENT);
            }
            imgDot.setPadding(5, 5, 5, 5);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(25, 25);
            imgDot.setLayoutParams(params);
            if(i == 0)
                imgDot.setSelected(true);

            llDots.addView(imgDot);
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                try {
                    for (int i = 0; i < llDots.getChildCount(); i++) {
                        ImageButton imgDot = (ImageButton) llDots.findViewWithTag(i);
                        if (position == i)
                            imgDot.setSelected(true);
                        else
                            imgDot.setSelected(false);
                    }
                } catch (Exception e) {
                    Log.e(Util.class.getSimpleName(), "Error in onPageSelected()", e);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //float percentage = uiContext.getResources().getDimension(R.dimen.tutorial_dialog_screen_percentage) / 100;
        //lp.height = (int) (uiContext.getResources().getDisplayMetrics().heightPixels * percentage);
        //lp.width = (int) (uiContext.getResources().getDisplayMetrics().widthPixels * percentage);
        lp.width = (int) (uiContext.getResources().getDisplayMetrics().widthPixels * 0.95f);
        lp.height = (int) (uiContext.getResources().getDisplayMetrics().heightPixels * 0.95f);
        dialog.getWindow().setBackgroundDrawable(null);
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public static String convertDateWithTimezone(String date) {
        try {
            // ex. input date is changed into "Sunday, April 9, 2005 at 11:00"
            // with the device timezone
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            SimpleDateFormat ndf = new SimpleDateFormat("EEEE, MMMM d, yyyy 'at' HH:mm");
            ndf.setTimeZone(TimeZone.getDefault());

            return ndf.format(df.parse(date));
        } catch (Exception e) {
            //Log.e(Util.class.getSimpleName(), "Error in date conversion: " + date, e);
            return date;
        }
    }

    public static Date convertDateWithTimezoneToDate(String date) {
        try {
            //timezone change
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat outDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            outDF.setTimeZone(TimeZone.getDefault());
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            outDF.setTimeZone(Calendar.getInstance().getTimeZone());
            String changed = outDF.format(df.parse(date));
            return outDF.parse(changed);
        } catch (Exception e) {
            Log.e(Util.class.getSimpleName(), "Error in date conversion: " + date);
            return null;
        }
    }

    public static String getOnlyDayAndTime(String dateString) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE, MMMM d, yyyy 'at' HH:mm");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date dateObject = sdf.parse(dateString);
            SimpleDateFormat outFormat = new SimpleDateFormat("EEEE, HH:mm");
            outFormat.setTimeZone(Calendar.getInstance().getTimeZone());
            return outFormat.format(dateObject);
        } catch (Exception e) {
            Log.e(Util.class.getSimpleName(), "Error in date conversion: " + dateString);
            return null;
        }
    }

    public static boolean onSameDay(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);

        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    }

    public static Team getTeam(int sportId, int teamId) {
        Sport sport = Util.getSport(sportId);
        if (sport == null)
            return null;

        synchronized (MyApplication.lock) {
            for (Team team : sport.getTeams()) {
                if (team.getTeamID() == teamId)
                    return team;
            }
        }

        return null;
    }

    public static String generateCrashReport(Throwable ex) {
        if (ex == null)
            return "null";

        StringBuffer sbReport = new StringBuffer();

        sbReport.append("Crash report\n\n");
        sbReport.append("Phone specs:\n");
        sbReport.append("Device: " + Build.DEVICE + "\n");
        sbReport.append("Board: " + Build.BOARD + "\n");
        sbReport.append("BOOTLOADER: " + Build.BOOTLOADER + "\n");
        sbReport.append("BRAND: " + Build.BRAND + "\n");
        sbReport.append("DISPLAY: " + Build.DISPLAY + "\n");
        sbReport.append("FINGERPRINT: " + Build.FINGERPRINT + "\n");
        sbReport.append("HARDWARE: " + Build.HARDWARE + "\n");
        sbReport.append("ID: " + Build.ID + "\n");
        sbReport.append("MANUFACTURER: " + Build.MANUFACTURER + "\n");
        sbReport.append("MODEL: " + Build.MODEL + "\n");
        sbReport.append("PRODUCT: " + Build.PRODUCT + "\n");
        sbReport.append("SERIAL: " + Build.SERIAL + "\n");
        sbReport.append("TAGS: " + Build.TAGS + "\n");
        sbReport.append("TYPE: " + Build.TYPE + "\n");
        sbReport.append("BASE_OS: " + Build.VERSION.BASE_OS + "\n");
        sbReport.append("CODENAME: " + Build.VERSION.CODENAME + "\n");
        sbReport.append("RELEASE: " + Build.VERSION.RELEASE + "\n");
        sbReport.append("INCREMENTAL: " + Build.VERSION.INCREMENTAL + "\n");
        sbReport.append("SECURITY_PATCH: " + Build.VERSION.SECURITY_PATCH + "\n\n");

        sbReport.append("Message: " + ex.getMessage() + "\n");
        sbReport.append("Throwable cause:\n");
        sbReport.append(getStacktrace(ex.getCause()));
        sbReport.append("\nException:\n");
        sbReport.append(getStacktrace(ex));

        return  sbReport.toString();
    }

    private static String getStacktrace(Throwable ex) {
        if (ex != null && ex.getStackTrace() != null) {
            StringBuffer sb = new StringBuffer();

            for (StackTraceElement element : ex.getStackTrace()) {
                sb.append(element.getFileName() + " " + element.getClassName() + "." + element.getMethodName() + "(): " + element.getLineNumber() + "\n");
            }
            return sb.toString();
        }

        return "no stacktrace available";
    }

    public static ArrayList<MyParlay> downloadHistoryForWagersView(int start) throws Exception {
        StringBuffer buffer = new StringBuffer();

        HttpClient client = HttpClientBuilder.create()
                .setMaxConnTotal(5)
                .setUserAgent(System.getProperty("http.agent", "Unknown"))
                .build();

        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                .setRedirectsEnabled(false)
                .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                .setCircularRedirectsAllowed(false).build();

        HttpPost post = new HttpPost(Uri.parse(Constants.URL_USER_COMMON).toString());
        post.setConfig(config);

        List<NameValuePair> pairs = new ArrayList<NameValuePair>();

        pairs.add(new BasicNameValuePair("key", Constants.USER_COMMON_REQUEST_HEADER_KEY));
        pairs.add(new BasicNameValuePair("p", Constants.USER_COMMON_PARAMS_HISTORY));
        pairs.add(new BasicNameValuePair("lang", Settings.getLanguage(MyApplication.getInstance())));
        pairs.add(new BasicNameValuePair("user_id", Util.getUserId(MyApplication.getInstance())));
        pairs.add(new BasicNameValuePair("start", "" + start));
        pairs.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded charset=utf-8"));

        post.setEntity(new UrlEncodedFormEntity(pairs));

        HttpResponse response = client.execute(post);

        BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        int character;

        while ((character = br.read()) != -1) {
            buffer.append((char) character);
        }

        br.close();

        XmlHistoryWagersHelper historyWagersHelper = new XmlHistoryWagersHelper();
        return historyWagersHelper.parseXMLFile(buffer.toString());


    }

    public static StatsCollection downloadStatsOverall() throws Exception {
        StringBuffer buffer = new StringBuffer();

        HttpClient client = HttpClientBuilder.create()
                .setMaxConnTotal(5)
                .setUserAgent(System.getProperty("http.agent", "Unknown"))
                .build();

        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                .setRedirectsEnabled(false)
                .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                .setCircularRedirectsAllowed(false).build();

        HttpGet get = new HttpGet(Uri.parse(Constants.URL_STATS_OVERALL).toString());
        get.setConfig(config);

        HttpResponse response = client.execute(get);

        BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(response.getEntity().getContent())));

        int character;

        while ((character = br.read()) != -1) {
            buffer.append((char) character);
        }

        br.close();

        StatsCollection stats = new StatsCollection(buffer);
        stats.setUserStats(downloadUserStats());

        return stats;
    }

    public static UserStats downloadUserStats() throws Exception {
        StringBuffer buffer = new StringBuffer();

        HttpClient client = HttpClientBuilder.create()
                .setMaxConnTotal(5)
                .setUserAgent(System.getProperty("http.agent", "Unknown"))
                .build();

        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                .setRedirectsEnabled(false)
                .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                .setCircularRedirectsAllowed(false).build();

        HttpPost post = new HttpPost(Uri.parse(Constants.URL_USER_COMMON).toString());
        post.setConfig(config);

        List<NameValuePair> pairs = new ArrayList<NameValuePair>();

        pairs.add(new BasicNameValuePair("key", Constants.USER_COMMON_REQUEST_HEADER_KEY));
        pairs.add(new BasicNameValuePair("p", Constants.USER_PARAM_STATS));
        pairs.add(new BasicNameValuePair("lang", Settings.getLanguage(MyApplication.getInstance())));
        pairs.add(new BasicNameValuePair("user_id", Util.getUserId(MyApplication.getInstance())));

        post.setEntity(new UrlEncodedFormEntity(pairs));

        HttpResponse response = client.execute(post);

        BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        int character;

        while ((character = br.read()) != -1) {
            buffer.append((char) character);
        }

        br.close();

//        this is only for printing the whole XML into the log
//        int read = 0;
//        while (read < buffer.length()) {
//            if (read != 0)
//                read++;
//            Log.d(Util.class.getSimpleName(), "XML: " + buffer.substring(read));
//            read += 512;
//        }

        return new UserStats(buffer);
    }

    public static boolean isNotificationEnabled(Context context) {
        final String CHECK_OP_NO_THROW = "checkOpNoThrow";
        final String OP_POST_NOTIFICATION = "OP_POST_NOTIFICATION";

        if(Build.VERSION.SDK_INT>=19) {
            AppOpsManager mAppOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);

            ApplicationInfo appInfo = context.getApplicationInfo();

            String pkg = context.getApplicationContext().getPackageName();

            int uid = appInfo.uid;

            Class appOpsClass = null; /// Context.APP_OPS_MANAGER /

            try {

                appOpsClass = Class.forName(AppOpsManager.class.getName());

                Method checkOpNoThrowMethod = appOpsClass.getMethod(CHECK_OP_NO_THROW, Integer.TYPE, Integer.TYPE, String.class);

                Field opPostNotificationValue = appOpsClass.getDeclaredField(OP_POST_NOTIFICATION);
                int value = (int) opPostNotificationValue.get(Integer.class);

                return ((int) checkOpNoThrowMethod.invoke(mAppOps, value, uid, pkg) == AppOpsManager.MODE_ALLOWED);

            } catch (ClassNotFoundException e) {
                //Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
            } catch (NoSuchMethodException e) {
                //Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
            } catch (NoSuchFieldException e) {
                //Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
            } catch (InvocationTargetException e) {
                //Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
            } catch (IllegalAccessException e) {
                //Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
            } catch (Exception e) {
                //Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
            }
        }

        return false;
    }

    public static Bitmap getIconForSportId(int sportId) {
        synchronized (MyApplication.lock) {
            for(SportIcon si : MyApplication.getInstance().getSportIcons()) {
                if (si.getSportId() == sportId)
                    return si.getBitmap();
            }

            return null;
        }
    }

    public static double getDisplayDiagonalInInches(Activity uiContext) {
        DisplayMetrics dm = new DisplayMetrics();
        uiContext.getWindowManager().getDefaultDisplay().getMetrics(dm);
        double x = Math.pow(dm.widthPixels / dm.xdpi, 2);
        double y = Math.pow(dm.heightPixels / dm.ydpi, 2);
        double screenInches = Math.sqrt(x + y);
        //Log.d("debug", "Screen inches : " + screenInches);
        return screenInches;
    }

    public static double getMultiplierForDialogWidth(Activity uiContext) {
        return getDisplayDiagonalInInches(uiContext) > 6.0 ? 0.5f : Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG;
    }
}
