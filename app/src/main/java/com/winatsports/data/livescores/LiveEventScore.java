package com.winatsports.data.livescores;

import com.winatsports.data.elements.Team;

import java.util.ArrayList;

public class LiveEventScore {

    private int eventID;
    private int sportID;
    private int leagueID;
    private String startDate;
    private int homeScore;
    private int awayScore;
    private int homeTeamID;
    private int awayTeamID;

    private Team homeTeam;
    private Team awayTeam;

    private String extraEventInfo1;
    private String extraEventInfo2;

    private String currentLeft;
    private String currentRight;

    ArrayList<Period> periods;

    public LiveEventScore() {

        eventID = -1;
        sportID = -1;
        leagueID = -1;
        startDate = null;
        homeScore = -1;
        awayScore = -1;
        homeTeamID = -1;
        awayTeamID = -1;

        extraEventInfo1 = null;
        extraEventInfo2 = null;

        currentLeft = null;
        currentRight = null;
    }


    public int getSportID() {
        return sportID;
    }

    public void setSportID(int sportID) {
        this.sportID = sportID;
    }

    public int getLeagueID() {
        return leagueID;
    }

    public void setLeagueID(int leagueID) {
        this.leagueID = leagueID;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public int getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(int homeScore) {
        this.homeScore = homeScore;
    }

    public int getHomeTeamID() {
        return homeTeamID;
    }

    public void setHomeTeamID(int homeTeamID) {
        this.homeTeamID = homeTeamID;
    }

    public int getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(int awayScore) {
        this.awayScore = awayScore;
    }

    public int getAwayTeamID() {
        return awayTeamID;
    }

    public void setAwayTeamID(int awayTeamID) {
        this.awayTeamID = awayTeamID;
    }

    public String getExtraEventInfo2() {
        return extraEventInfo2;
    }

    public void setExtraEventInfo2(String extraEventInfo2) {
        this.extraEventInfo2 = extraEventInfo2;
    }

    public String getExtraEventInfo1() {
        return extraEventInfo1;
    }

    public void setExtraEventInfo1(String extraEventInfo1) {
        this.extraEventInfo1 = extraEventInfo1;
    }

    public ArrayList<Period> getPeriods() {
        return periods;
    }

    public void setPeriods(ArrayList<Period> periods) {
        this.periods = periods;
    }

    public String getCurrentLeft() {
        return currentLeft;
    }

    public void setCurrentLeft(String currentLeft) {
        this.currentLeft = currentLeft;
    }

    public String getCurrentRight() {
        return currentRight;
    }

    public void setCurrentRight(String currentRight) {
        this.currentRight = currentRight;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }
}
