package com.winatsports.data.livescores;

/**
 * Created by broda on 02/12/2015.
 */
public class Period {


    private String periodName;
    private int homeScore;
    private int awayScore;


    public Period() {
        periodName = null;
        homeScore = -1;
        awayScore = -1;
    }


    public String getPeriodName() {
        return periodName;
    }

    public void setPeriodName(String periodName) {
        this.periodName = periodName;
    }

    public int getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(int homeScore) {
        this.homeScore = homeScore;
    }

    public int getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(int awayScore) {
        this.awayScore = awayScore;
    }





}
