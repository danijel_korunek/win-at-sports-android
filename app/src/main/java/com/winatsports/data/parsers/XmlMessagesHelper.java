package com.winatsports.data.parsers;


/**
 * Created by broda on 28/10/2015.
 */




import android.util.Log;

import com.winatsports.MyApplication;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;




public class XmlMessagesHelper extends DefaultHandler {


    String TAG = "XMLMessagesHelper";

    private final String TAG_MESSAGE = "message";
    private final String ID_ATTRIBUTE = "id";
    private final String TEXT_ATTRIBUTE = "text";

    private boolean reDownload;

    public void parseXMLFile( String dataToParse, boolean reDownload ) throws Exception {
        this.reDownload = reDownload;
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser mSaxParser = factory.newSAXParser();
            XMLReader mXmlReader = mSaxParser.getXMLReader();
            mXmlReader.setContentHandler(this);
            InputSource is = new InputSource(new StringReader( dataToParse ) );
            mXmlReader.parse(is);
            // after parser read this line, parser will read, parse InputSource "is" and all data inside this "is"
            // that means, that parse will execute first start element, characters and after that end elements for EACH TAG INSIDE "is"
            // after that parser will execute this bellow LINES OF CODE ==>  MyApplication.getInstance().getAllMessages().addAll(historyEventsArrayList);

        } catch (Exception e) {
            // Exceptions can be handled for different types
            // But, this is about XML Parsing not about Exception Handling
            Log.e(TAG, "Exception: " + e.getMessage(), e);
        }

    }

    // This method receives notification character data inside an element
    // for now in that file, this method will never execute, because all data from elements I get in startElement() method ==> from attributes
    // AND not ONE ELEMENT has character inside an element.. All data from elements attributes
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {

    }

    // Receives notification of end of an element
    // e.g. </message>
    // It will be called when TAG </message > is found, discovered, encountered
    // for now in that file, this method will never execute, because all data from elements I get in startElement() method ==> from attributes
    // AND not ONE ELEMENT has character inside an element.. All data from elements attributes
    @Override
    public void endElement( String uri, String localName, String qName ) throws SAXException {

    }

    // Receives notification of start of an element
    // e.g. <message>
    // It will be called when <message> is found, discovered, encountered
    // here with a help from attributes I will get all data
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (localName.equals(TAG_MESSAGE)) {
            String id = attributes.getValue(ID_ATTRIBUTE);
            String text = attributes.getValue(TEXT_ATTRIBUTE);
            if (!reDownload)
                MyApplication.getInstance().getAllMessages().put(id,text);
            else
                MyApplication.getInstance().getAllMessagesCopy().put(id,text);
        }

    }

}