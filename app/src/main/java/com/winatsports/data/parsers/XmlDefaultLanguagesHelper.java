package com.winatsports.data.parsers;


import com.winatsports.MyApplication;
import com.winatsports.data.CountryLang;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;




public class XmlDefaultLanguagesHelper extends DefaultHandler {

	 /**
      *  Jedan primjer kako radi za 1 section taj XMLHELPER.java class
	 *  Prvo program ide u metodu parseXMLFIle() ==> koristim SAXParser da obradim XML dokument
	 *  Taj SAXParser obradi cijeli XML dokument od pocetka do kraja.
	 *  Zatim kompajler cita redak po redak iz XML dokumenta. Mene zanimaju podaci samo pod elementom "Section" i njih cu ispisati
	 *  Kad je prvi put dosao na element "Section" kreira se novi object od classe "Section"
	 *  I u taj novi object "Section" cu spremati: 1)name i 2) sportsBookID . Tako sam i definirao konstruktora kod classe Section
	 *  Posto taj element "Section" ima jos neke elementa, onda kompajler ne ide u characters() ili endELemente() metode, nego preskace te dvije metode
	 *  Zatim dolazi do elementa Name. Taj elemenet ima podatke i nakon toga kompjaler ide u characters()
	 *  U njoj spremam podatak("Top SportsBook") ==> od elementa("Name") u string varijablu.
	 *  Nakon tog podatka kompajler vidi da dolazi zatvoreni tag i kompajler ide u metodu endElement() metodu
	 *  U metodi endElement() spremam vrijednost od string varijable("Top SportsBook") u objekt Section koristeci setName() metodu
	 *  Zatim dolazi novi element("SportBookID") i pokrece se metoda startElement().
	 *  Nakon njega ide characters() metoda gdje uzimam vrijednost od prvog "SportBookID". Tu vrijednost spremam u varijablu currentTagValueFromXML
	 *  Nakon tog podatka kompajler vidi da dolazi zatvoreni tag i kompajler ide u metodu endElement() metodu
	 *  U metodi endElement() spremam vrijednost od string varijable("19") u objekt Section koristeci setSportBookIDs() metodu
	 *  U tu metodu setSportBookIDs() zapravo dodajem vrijednost (19,20,32,30 i 24) u arrayListu  "sportBookIDs"
	 *  Tako radi sad za svih ostalih 4 elementa koji se zovu("SportBookID")
	 *  BITNO ========= >>>  Nakon tog ZADNJEG ZATVORENOG TAGA("SportBookID") kompajler vidi da dolazi zatvoreni tag od SECTION,, i kompajler ide u metodu endElement() metodu
	 *  U toj metodi onda dodajem novi object Section sa podacima: 1) name, 2) SportBookID,, u arrayList-u "historyEventsArrayList"
	 *  Tada sam dobio 1 objekt od Section u ArrayList-i "historyEventsArrayList" sa svim potrebnim podacima
	 *  Isti postupak se ponavlja za drugi "Section". Kad je gotov drugi "Section" takoder se objekt "Section" dodaje u arrayList-u "historyEventsArrayList"
	 *  ZATIM BITNO ======= >> NA KRAJU UVJET U metodi startElement() ==>  if ( || historyEventsArrayList.size() == VARIABLE_FOR_EXERCISE_CREATE_ONLY_TWO_OBJECT_FROM_CLASS_SECTION)
	 *  Sada znamo unaprijed da arrayLista "historyEventsArrayList" mora biti velicina 2(imati dva objekta od "Section")
	 *  i s tim sprijecavamo da bi mi se ostali elementi("SportBookID") dodali u drugi objekt od "Section" i zatim naravno ti podaci prikazali u arrayList "historyEventsArrayList"
	 *
	 * */
	
    String TAG = "XMLDefaultHelper";

    private final String TAG_COUNTRY_LANG = "CountryLang";
    private final String COUNTRY_ATTRIBUTE = "country";
    private final String LANGUAGE_ATTRIBUTE = "language";
    private final String LOCALE_ATTRIBUTE = "locale";

    public ArrayList<CountryLang> completedArrayList = new ArrayList<CountryLang>();

    public void parseXMLFile(String dataToParse, boolean redownload ) throws Exception {

        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser mSaxParser = factory.newSAXParser();
        XMLReader mXmlReader = mSaxParser.getXMLReader();
        mXmlReader.setContentHandler(this);
        InputSource is = new InputSource(new StringReader( dataToParse  ) );
        mXmlReader.parse(is);

        if (!redownload)
            MyApplication.getInstance().setAllDefaultLanguages(completedArrayList);
        else
            MyApplication.getInstance().setAllDefaultLanguagesCopy(completedArrayList);
    }

    // This method receives notification character data inside an element
    // for now in that file, this method will never execute, because all data from elements I get in startElement() method ==> from attributes
    // AND not ONE ELEMENT has character inside an element.. All data from elements attributes
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {

    }

    // Receives notification of end of an element
    // e.g. <LanguageDefaults>
    // for now in that file, this method will never execute, because all data from elements I get in startElement() method ==> from attributes
    // AND not ONE ELEMENT has character inside an element.. All data from elements attributes
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

    }

    // Receives notification of start of an element
    // e.g. <LanguageDefaults>
    // It will be called when <LanguageDefaults> is found, discovered, encountered
    // here with a help from attributes I will get all data
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        //Log.i(TAG, "TAG: " + localName);

        if (localName.equals(TAG_COUNTRY_LANG)) {
            CountryLang countryLang = new CountryLang();
            countryLang.setCountry(attributes.getValue(COUNTRY_ATTRIBUTE));
            countryLang.setLanguage(attributes.getValue(LANGUAGE_ATTRIBUTE));
            countryLang.setLocale(attributes.getValue(LOCALE_ATTRIBUTE));
            completedArrayList.add(countryLang);
        }

    }
    
    
    
}