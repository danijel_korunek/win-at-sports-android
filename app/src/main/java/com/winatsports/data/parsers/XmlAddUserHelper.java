package com.winatsports.data.parsers;


import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.Util;
import com.winatsports.activities.MainActivity;
import com.winatsports.data.Constants;
import com.winatsports.data.MyBet;
import com.winatsports.data.MyParlay;
import com.winatsports.data.SupportLang;
import com.winatsports.data.UserData;
import com.winatsports.data.firstpage.HomeNotification;
import com.winatsports.data.elements.BetResult;
import com.winatsports.data.upload.AsyncRealBetting;
import com.winatsports.settings.Settings;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.message.BasicNameValuePair;


/**
 * Created by broda on 18/12/2015.
 */

public class XmlAddUserHelper extends DefaultHandler {

    private String TAG = "XmlAddUserHelper";
    StringBuilder sb = null;

    private Context context;

    private final  String TAG_MERITUM = "meritum";

    // all variable required for UserInfo tag in XML FILE that I get from Server
    private final String TAG_RESPONSE = "response";
    private final String TAG_DEVICE_ID = "device_id";
    private final String TAG_USER_D = "user_d";
    private final String TAG_COUNTRY = "country";
    private final String TAG_WALLET_AMOUNT = "wallet_amount";
    private final String TAG_APP_VERSION_PROTECT = "DroidAppVersionProtect";
    private final String TAG_SPECIAL_BONUS = "special_bonus";
    private final String TAG_FIRST_RUN = "first_run";

    // all variable required for UserSettings tag in XML FILE that I get from Server
    private final String TAG_START_PUSH = "start_push";
    private final String TAG_END_PUSH = "end_push";
    private final String TAG_BET_SLIDER = "bet_slider";
    private final String TAG_CONFIRM_BET = "confirm_bet";
    private final String TAG_TEAM_ORDER = "team_order";
    private final String TAG_ODD_TYPE = "odd_type";
    private final String TAG_DAILY_BONUS = "daily_bonus";
    private final String TAG_SHOW_INACTIVE = "show_inactive";
    private final String TAG_LANG = "lang";
    private final String TAG_WIN_TYPE = "win_type";
    private final String TAG_SUPPORT_LANG = "supportLang";
    private final String TAG_USER_BETS = "userBets";
    private final String TAG_PARLAY = "parlay";
    private final String TAG_CONFIRMED_USER = "confirmed_user";
    private final String TAG_BOOKER_LOGOUT = "booker_logout";

    // all variable required for HomeNotificatons tag in XML FILE that I get from Server
    private final String TAG_NOTIFICATIONS = "Notification";

    private final String TAG_DAILY_WIN = "dailyWin";

    private UserData userData = null;

    boolean insideUserBetsTag = false;
    private boolean insideDailyWin = false;
    MyParlay myParlay;
    BetResult betResult;

    public XmlAddUserHelper(Context c) {
        this.context = c;
    }

    public void parseXMLFile(String dataToParse) throws Exception {

        Log.d("add_user", dataToParse + "\n-----------\n");

        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser mSaxParser = factory.newSAXParser();
        XMLReader mXmlReader = mSaxParser.getXMLReader();
        mXmlReader.setContentHandler(this);
        InputSource is = new InputSource(new StringReader( new String(dataToParse.getBytes("UTF-8"), "UTF-8") ));
        mXmlReader.parse(is);

        Settings.setDeviceId(context, userData.getDeviceId());
        Settings.setUserD(context, userData.getUserD());
        // I need to put here -1, because "TEAM ORDER FROM SERVER have INDEX 1,2,3" and here in "Settings.getTeamOrder HAVE INDEX 0,1,2"
        Settings.setTeamOrder(context, (userData.getTeamOrder() - 1));
        // I need to put here -1, because "ODD TYPE FROM SERVER have INDEX 1,2,3" and here in "Settings.getOddType HAVE INDEX 0,1,2"
        Settings.setOddType(context, (userData.getOddType() - 1));

        Settings.setLanguage(context, userData.getLang().toLowerCase());


        Log.d("ADD_USER", "after: user r je: " + Settings.getUserR(context) + " user id je:" + Settings.getUserD(context));

        Settings.setStartGameNotification(context, userData.getStartPush() == 0 ? false : true);
        Settings.setEndGameNotification(context, userData.getEndPush() == 0 ? false : true);
        Settings.setBetConfirmation(context, userData.getConfirmBet() == 0 ? false : true);
        Settings.setPredefinedWagerAmount(context, userData.getBetSlider() == 0 ? false : true);
        Settings.setShowEmptySchedule(context, userData.getShowInactive() == 0 ? false : true);
        Settings.setWinAmount(context, userData.getWinType() == 0 ? false : true);

        Settings.setStatsTimer(context, 0);
        Settings.setLoginTime(context, System.currentTimeMillis());

        MyApplication.getInstance().setUserData(userData);

        Log.d("ADD_USER", "Data set. Wallet amount: " + MyApplication.getInstance().getUserData().getWalletAmount());

        // send to server not to send again daily wins
        notifyServerAboutDailyWins();
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (sb!=null ) {
            for (int i=start; i<start+length; i++) {
                sb.append(ch[i]);
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

        if( localName.equals(TAG_RESPONSE) )
            userData.setResponse(sb.toString().trim());
        else if( localName.equals(TAG_DEVICE_ID) )
            userData.setDeviceId(sb.toString().trim());
        else if( localName.equals(TAG_USER_D) )
            userData.setUserD(sb.toString().trim());
        else if( localName.equals(TAG_COUNTRY) )
            userData.setCountry(sb.toString().trim());
        else if( localName.equals(TAG_WALLET_AMOUNT) ) {
            userData.setWalletAmount(Double.parseDouble(sb.toString().trim()));
            Log.d(XmlAddUserHelper.class.getSimpleName(), "add_user wallet amount parsed: " + userData.getWalletAmount() + " " + sb.toString().trim());
        } else if( localName.equals(TAG_APP_VERSION_PROTECT) )
            userData.setAppVersionProtect(Integer.parseInt(sb.toString().trim()));
        else if( localName.equals(TAG_SPECIAL_BONUS) )
            userData.setSpecialBonus(Integer.parseInt(sb.toString().trim()));
        else if( localName.equals(TAG_FIRST_RUN) )
            userData.setFirstRun(sb.toString().trim());

        else if( localName.equals(TAG_START_PUSH) )
            userData.setStartPush(Integer.parseInt(sb.toString().trim()));
        else if( localName.equals(TAG_END_PUSH) )
            userData.setEndPush(Integer.parseInt(sb.toString().trim()));
        else if( localName.equals(TAG_BET_SLIDER) ) {
            Log.d(XmlAddUserHelper.class.getSimpleName(), "Show bet wager amount: " + sb.toString());
            userData.setBetSlider(Integer.parseInt(sb.toString().trim()));
        } else if( localName.equals(TAG_CONFIRM_BET) )
            userData.setConfirmBet(Integer.parseInt(sb.toString().trim()));
        else if( localName.equals(TAG_TEAM_ORDER) )
            userData.setTeamOrder(Integer.parseInt(sb.toString().trim()));
        else if( localName.equals(TAG_ODD_TYPE) )
            userData.setOddType(Integer.parseInt(sb.toString().trim()));
        else if( localName.equals(TAG_DAILY_BONUS) )
            userData.setDailyBonusUserSettings(Integer.parseInt(sb.toString().trim()));
        else if( localName.equals(TAG_SHOW_INACTIVE) )
            userData.setShowInactive(Integer.parseInt(sb.toString().trim()));
        else if( localName.equals(TAG_LANG) )
            userData.setLang(sb.toString().trim().toLowerCase(Locale.US));
        else if( localName.equals(TAG_WIN_TYPE) )
            userData.setWinType(Integer.parseInt(sb.toString().trim()));
        else if (localName.equals(TAG_CONFIRMED_USER))
            userData.setConfirmedUser(Integer.parseInt(sb.toString().trim()) == 1);
        else if (localName.equals(TAG_BOOKER_LOGOUT))
            userData.setBookerLogoutUrl(sb.toString().trim());

        else if( localName.equals(TAG_PARLAY) && insideUserBetsTag == true )
            userData.addParlay(myParlay);
        else if( localName.equals(TAG_USER_BETS) )
            insideUserBetsTag = false;
        else if (insideDailyWin && localName.equals(TAG_DAILY_WIN)) {
            insideDailyWin = false;
        } else if (localName.equals(TAG_PARLAY) && insideDailyWin) {
            userData.getDailyWins().add(betResult);
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        //Log.i(TAG, "TAG: " + localName);
        sb = new StringBuilder();

        if( localName.equals(TAG_MERITUM) ) {
            userData = new UserData();
        }
        else if( localName.equals(TAG_SUPPORT_LANG) ) {
            SupportLang supportLang = new SupportLang();
            supportLang.setLangID(attributes.getValue("langID"));
            supportLang.setLangName(attributes.getValue("langName"));
            userData.getSupportLangs().add( supportLang );
        }
        else if( localName.equals(TAG_USER_BETS) ) {
            insideUserBetsTag = true;
        } else if (insideDailyWin && localName.equals(TAG_PARLAY)) {
            betResult = new BetResult();
            betResult.setParlayId(attributes.getValue("parlay_id"));
            betResult.setBetAmount(Double.parseDouble(attributes.getValue("bet_amount")));
            betResult.setBetWin(Double.parseDouble(attributes.getValue("bet_win")));
            betResult.setResult(Integer.parseInt(attributes.getValue("bet_result")));

        } else if( insideUserBetsTag == true && localName.equals(TAG_PARLAY) ) {
            myParlay = new MyParlay();
            myParlay.setID(Integer.parseInt(attributes.getValue("ID")));

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            Date date = null;
            try {
                date = format.parse(attributes.getValue("time"));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            myParlay.setTime(date);
            myParlay.setAmount(Double.parseDouble(attributes.getValue("amount")));
            if (attributes.getValue("teaser_odd") != null) {
                if (attributes.getValue("teaser_odd").contains("+"))
                    myParlay.setTeaserOdd(Double.parseDouble(attributes.getValue("teaser_odd")));
                else
                    myParlay.setTeaserOdd((double) Integer.parseInt(attributes.getValue("teaser_odd")));
            }

            if (attributes.getValue("teaser_points") != null) {
                if (attributes.getValue("teaser_points").contains("+"))
                    myParlay.setTeaserPoints(Double.parseDouble(attributes.getValue("teaser_points")));
                else
                    myParlay.setTeaserPoints((double) Integer.parseInt(attributes.getValue("teaser_points")));
            }
            //myParlay.setTeaserPoints(Double.parseDouble(attributes.getValue("teaser_points")));
        }
        else if( insideUserBetsTag == true && localName.equals("myBet") ) {
            MyBet myBet = new MyBet();
            myBet.setEventID(Long.parseLong(attributes.getValue("EventID")));
            myBet.setStartDate(attributes.getValue("StartDate"));
            myBet.setSportID(Long.parseLong(attributes.getValue("SportID")));
            myBet.setLeagueID(Long.parseLong(attributes.getValue("LeagueID")));
            myBet.setEventDesc(attributes.getValue("event_desc"));
            myBet.setBetTypeId(Long.parseLong(attributes.getValue("bet_type_id")));
            myBet.setBetValue(Long.parseLong(attributes.getValue("bet_value")));
            myBet.setBetOdd(Double.parseDouble(attributes.getValue("bet_odd")));
            if( attributes.getValue("bet_line").equals("") != true )
                myBet.setBetLine(Double.parseDouble(attributes.getValue("bet_line")));
            myBet.setTypeLangValue(attributes.getValue("type_lang_value"));
            myBet.setTypeLangLine(attributes.getValue("type_lang_line"));
            myBet.setTypeName2ndHalf(attributes.getValue("type_lang_line"));
            myBet.setHomeID(Integer.parseInt(attributes.getValue("home_id")));
            myBet.setAwayID(Integer.parseInt(attributes.getValue("away_id")));
            myBet.setTeamName(attributes.getValue("team_name"));
            myBet.setTeamId(Integer.parseInt(attributes.getValue("team_id")));
            myParlay.getMyBets().add(myBet);
        }
        else if( localName.equals(TAG_NOTIFICATIONS) ) {

            HomeNotification homeNotification = new HomeNotification();
            homeNotification.setTitle(attributes.getValue("Title"));
            homeNotification.setSubTitle(attributes.getValue("Message"));
            String dismissibleStr = attributes.getValue("Dismissable");
            boolean isDismissible = dismissibleStr != null && dismissibleStr.equalsIgnoreCase("1") ? true : false;
            homeNotification.setDismissible(isDismissible);
            String url = attributes.getValue("URL");
            if (url != null && url.equalsIgnoreCase(AsyncRealBetting.URL_REAL)) {
                homeNotification.setOpensRealBetting(true);
                homeNotification.setUrl(null);
            } else
                homeNotification.setUrl(attributes.getValue("URL"));
            homeNotification.setSportID(Integer.parseInt(attributes.getValue("SportID")));
            homeNotification.setNoteID(Integer.parseInt(attributes.getValue("NoteID")));
            homeNotification.setWinLoseReport(false);
            userData.getHomeNotifications().add(homeNotification);
        } else if (localName.equals(TAG_DAILY_WIN)) {
            insideDailyWin = true;
        }
    }

    private void notifyServerAboutDailyWins() {
        HttpClient client = null;
        StringBuffer xmlBuffer = new StringBuffer();

        xmlBuffer.append("<meritum>");
        xmlBuffer.append("<dailyWin>");

        for (BetResult result : MyApplication.getInstance().getUserData().getDailyWins()) {
            xmlBuffer.append("<parlay_id>" + result.getParlayId() + "</parlay_id>");
        }
        xmlBuffer.append("</dailyWin>");
        xmlBuffer.append("</meritum>");

        try {

            final String URL_USER_COMMON = Constants.URL_USER_COMMON;
            final String REQUEST_HEADER_KEY = Constants.USER_COMMON_REQUEST_HEADER_KEY;

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                    .setRedirectsEnabled(false)
                    .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                    .setCircularRedirectsAllowed(false).build();

            HttpPost post = new HttpPost(Uri.parse(URL_USER_COMMON).toString());
            post.setConfig(config);

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("key", REQUEST_HEADER_KEY));
            pairs.add(new BasicNameValuePair("lang", Settings.getLanguage(context)));
            pairs.add(new BasicNameValuePair("p", "confirmpopup"));
            pairs.add(new BasicNameValuePair("xml", xmlBuffer.toString()));
            pairs.add(new BasicNameValuePair("user_id", Util.getUserId(context)));
            pairs.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded charset=utf-8"));

            post.setEntity(new UrlEncodedFormEntity(pairs));

            client = HttpClientBuilder.create()
                    .setMaxConnTotal(1)
                    .build();

            HttpResponse response = client.execute(post);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer returnData = new StringBuffer();
            int character;

            while ( (character = br.read()) != -1 ) {
                returnData.append((char) character);
            }

            br.close();

        } catch ( Exception e ) {
            Log.e(MainActivity.class.getSimpleName(), e.getLocalizedMessage(), e);
        } finally {
            if( client != null )
                client = null;
        }
    }

}