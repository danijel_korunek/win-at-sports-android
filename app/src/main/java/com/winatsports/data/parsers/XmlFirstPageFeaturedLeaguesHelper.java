package com.winatsports.data.parsers;



import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.data.firstpage.Feature;
import com.winatsports.data.firstpage.FirstPageLeagueSection;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by broda on 10/12/2015.
 */


public class XmlFirstPageFeaturedLeaguesHelper extends DefaultHandler {

    String TAG = "XMLAboutToStartHelper";

    private final String TAG_PAGE_SECTION = "PageSection";
    private final String TITLE_ID_ATTRIBUTE = "TitleID";
    private final String POSITION_ATTRIBUTE = "Position";
    private final String TAG_FEATURE = "Feature";

    // attribute for FEATURED LEAGUES IN HOME SCREEN and for that xml file ==>  https://www.eclecticasoft.com/betting3/app_data/first_page/leagues_us.gz
    private final String LEAGUE_TYPE_ATTRIBUTE = "LeagueType";
    private final String LEAGUE_ID_ATTRIBUTE = "LeagueID";
    private final String ORIGINAL_LEAGUE_ATTRIBUTE = "OriginalLeague";

    FirstPageLeagueSection leagueSection = new FirstPageLeagueSection();

    Feature feature = null;

    public void parseXMLFile(String dataToParse, boolean redownload) throws Exception {

        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser mSaxParser = factory.newSAXParser();
            XMLReader mXmlReader = mSaxParser.getXMLReader();
            mXmlReader.setContentHandler(this);
            InputSource is = new InputSource(new StringReader( dataToParse ) );
            mXmlReader.parse(is);

            MyApplication myApplication = MyApplication.getInstance();

            if (!redownload)
                myApplication.getFirstPage().setFeaturedLeagues(leagueSection);
            else
                myApplication.getFirstPageCopy().setFeaturedLeagues(leagueSection);

        } catch (Exception e) {
            // Exceptions can be handled for different types
            // But, this is about XML Parsing not about Exception Handling
            Log.e(TAG, "Exception: " + e.getMessage(), e);
        }

    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {

    }

    @Override
    public void endElement( String uri, String localName, String qName ) throws SAXException {

    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        if (localName.equals(TAG_PAGE_SECTION)) {
            leagueSection.setTitleId(attributes.getValue(TITLE_ID_ATTRIBUTE));
            leagueSection.setPosition(Integer.parseInt(attributes.getValue(POSITION_ATTRIBUTE)));
        }

        else if( localName.equals(TAG_FEATURE) && attributes.getIndex(LEAGUE_TYPE_ATTRIBUTE) != -1 &&  attributes.getIndex(LEAGUE_ID_ATTRIBUTE) != -1 ) {
            feature = new Feature();
            feature.setLeagueType( Integer.parseInt(attributes.getValue(LEAGUE_TYPE_ATTRIBUTE)) );
            feature.setLeagueID( Integer.parseInt(attributes.getValue(LEAGUE_ID_ATTRIBUTE)) );
            feature.setOriginalLeague( Integer.parseInt(attributes.getValue(ORIGINAL_LEAGUE_ATTRIBUTE)) );
            leagueSection.getFeatures().add(feature);
        }

    }

}