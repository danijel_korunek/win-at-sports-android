package com.winatsports.data.parsers;


/**
 * Created by broda on 30/10/2015.
 */


import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.data.elements.BetType;
import com.winatsports.data.elements.League;
import com.winatsports.data.elements.Sport;
import com.winatsports.data.elements.Team;
import com.winatsports.data.elements.Teaser;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;



public class XmlLanguageCodesHelper extends DefaultHandler {

    String TAG_XML_LANGUAGE_CODES = "XML_LANGUAGE_CODES_HELPER";
    StringBuilder sb = null;

    private int tempSportID = -1;
    private boolean reDownload;

    public void parseXMLFile( String dataToParse, boolean reDownload ) throws Exception {
        try {
            this.reDownload = reDownload;

            if (!reDownload)
                MyApplication.getInstance().getAllLeagues().clear();
            else
                MyApplication.getInstance().getAllLeaguesCopy().clear();

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser mSaxParser = factory.newSAXParser();
            XMLReader mXmlReader = mSaxParser.getXMLReader();
            mXmlReader.setContentHandler(this);
            InputSource is = new InputSource(new StringReader( dataToParse ) );
            mXmlReader.parse(is);
        } catch (Exception e) {
            // Exceptions can be handled for different types
            // But, this is about XML Parsing not about Exception Handling
            Log.e(TAG_XML_LANGUAGE_CODES, "Exception: " + e.getMessage(), e);
        }
    }

    // This method receives notification character data inside an element
    // e.g. <LanguageName>English</LanguageName>

    // It will be called when TAG_XML_LANGUAGE_CODES </LanguageName> is found, discovered, encountered
    // It will be called to read "English"
    // ONLY FROM THAT ONE ELEMENT <LanguageName> I WILL BECOME DATA ==> English
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (sb!=null ) {
            for (int i=start; i<start+length; i++) {
                sb.append(ch[i]);
            }
        }
    }

    // Receives notification of end of an element
    // e.g. <LanguageName>English</LanguageName>
    // It will be called when TAG_XML_LANGUAGE_CODES </LanguageName> is found, discovered, encountered
    // ONLY FROM THAT ONE ELEMENT <LanguageName> I WILL BECOME DATA ==> English
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

        if( localName.equals("LanguageName") )
            ;// nothing - ignore it! MyApplication.getInstance().setLanguageName(sb.toString().trim());
    }

    // Receives notification of start of an element
    // e.g. <Sport>
    // It will be called when <Sport> is found, discovered, encountered
    // here with a help from attributes I will get ALMOST ALL DATA, only not first ELEMENT ==> <LanguageName>English</LanguageName>
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        //Log.i(TAG_XML_LANGUAGE_CODES, "TAG: " + localName);

        sb = new StringBuilder();

         if ( localName.equals("Sport")  ) {

             int sportID = Integer.parseInt(attributes.getValue("SportID"));
             String sportName = attributes.getValue("SportName");
             int status = Integer.parseInt(attributes.getValue("Status")); // we won't update status, because we already have status set

             // After I have read all values from top, I want to set "SPORT NAME"
             boolean found = false;
             for( int i=0; i<MyApplication.getInstance().getAllSports().size() && !found ;  i++ ) {
                 Sport sport = MyApplication.getInstance().getAllSports().get(i);
                 if( sport.getSportID() == sportID )  {
                     sport.setSportName(sportName);
                     found = true;
                     break;
                 }
             }
         }
         else if( localName.equals("League") ) {

             League league = new League();

             league.setLeagueID(Integer.parseInt(attributes.getValue("LeagueID")));
             league.setLeagueType(Integer.parseInt(attributes.getValue("LeagueType")));
             league.setSportID(Integer.parseInt(attributes.getValue("SportID")));
             league.setLeagueName(attributes.getValue("LeagueName"));
             league.setStatus(Integer.parseInt(attributes.getValue("Status")));
             if (!reDownload)
                MyApplication.getInstance().getAllLeagues().add(league);
             else
                MyApplication.getInstance().getAllLeaguesCopy().add(league);
             // Later in class "AsyncDownloadData.java", I will link, connect "LEAGUES" with "SPORT"
         }
         else if( localName.equals("BetTypeSport") ) {
             tempSportID =  Integer.parseInt(attributes.getValue("SportID"));
         }
         else if( localName.equals("BetType") ) {

             BetType betType = new BetType();

             betType.setSportID(tempSportID);
             betType.setTypeID(Integer.parseInt(attributes.getValue("TypeID")));
             betType.setTypeName(attributes.getValue("TypeName"));


             for( int i=0; i< (!reDownload ? MyApplication.getInstance().getAllSports().size() : MyApplication.getInstance().getAllSportsCopy().size()); i++ ) {

                 Sport sport = (!reDownload ? MyApplication.getInstance().getAllSports().get(i) : MyApplication.getInstance().getAllSportsCopy().get(i));
                 if( sport.getSportID() == betType.getSportID() ) {
                     sport.getBetTypes().add(betType);
                     break;
                 }
             }
         }
         else if( localName.equals("Team") ) {

             Team team = new Team();

             team.setTeamID(Integer.parseInt(attributes.getValue("TeamID")));
             team.setSportID(Integer.parseInt(attributes.getValue("SportID")));
             team.setName(attributes.getValue("Name"));

             for( int i=0; i<(!reDownload ? MyApplication.getInstance().getAllSports().size() : MyApplication.getInstance().getAllSportsCopy().size()); i++ ) {

                 Sport sport = (!reDownload ? MyApplication.getInstance().getAllSports().get(i) : MyApplication.getInstance().getAllSportsCopy().get(i));
                 if( sport.getSportID() == team.getSportID() ) {
                     sport.getTeams().add(team);
                     break;
                 }
             }
         }
         else if( localName.equals("Teaser") ) {

             Teaser teaser = new Teaser();
             teaser.setSportID(Integer.parseInt(attributes.getValue("sport_id")));
             teaser.setTeams(Integer.parseInt(attributes.getValue("teams")));
             teaser.setPoints(Double.parseDouble(attributes.getValue("points")));
             teaser.setOdd(Double.parseDouble(attributes.getValue("odd")));

             for( int i=0; i<(!reDownload ? MyApplication.getInstance().getAllSports().size() : MyApplication.getInstance().getAllSportsCopy().size()); i++ ) {

                 Sport sport = (!reDownload ? MyApplication.getInstance().getAllSports().get(i) : MyApplication.getInstance().getAllSportsCopy().get(i));
                 if( sport.getSportID() == teaser.getSportID() ) {
                     sport.getTeasers().add(teaser);
                     break;
                 }
             }
         }
         else if( localName.equals("Term") ) {

             String termKey =  attributes.getValue("term_from");
             String termValue =  attributes.getValue("term_to");

             if (!reDownload)
                 MyApplication.getInstance().getAllLiveTerms().put(termKey, termValue);
             else
                 MyApplication.getInstance().getAllLiveTermsCopy().put(termKey, termValue);
         }

    }
}