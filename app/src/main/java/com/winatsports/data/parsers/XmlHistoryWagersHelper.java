package com.winatsports.data.parsers;

import com.winatsports.MyApplication;
import com.winatsports.Util;
import com.winatsports.data.MyBet;
import com.winatsports.data.MyParlay;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by broda on 19/02/2016.
 *
 * modified by Danijel
 */


public class XmlHistoryWagersHelper extends DefaultHandler {

    StringBuilder sb = null;

    private final String TAG_PARLAY = "parlay";

    MyParlay myParlay;
    private ArrayList<MyParlay> wagerHistory;

    public XmlHistoryWagersHelper() {
        wagerHistory = new ArrayList<MyParlay>();
    }

    public ArrayList<MyParlay> parseXMLFile( String dataToParse) throws Exception {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser mSaxParser = factory.newSAXParser();
        XMLReader mXmlReader = mSaxParser.getXMLReader();
        mXmlReader.setContentHandler(this);
        InputSource is = new InputSource(new StringReader( new String(dataToParse.getBytes("UTF-8"), "UTF-8") ));
        mXmlReader.parse(is);

        return wagerHistory;
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (sb != null ) {
            for (int i=start; i<start+length; i++) {
                sb.append(ch[i]);
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

        if( localName.equals(TAG_PARLAY)) {
            wagerHistory.add(myParlay);
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        //Log.i(TAG, "TAG: " + localName);
        sb = new StringBuilder();

       if(localName.equals(TAG_PARLAY) ) {
            myParlay = new MyParlay();
            myParlay.setID(Integer.parseInt(attributes.getValue("ID")));

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            Date date = null;
            try {
                date = format.parse(attributes.getValue("time"));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            myParlay.setTime(Util.convertDateWithTimezoneToDate(attributes.getValue("time")));
            myParlay.setAmount(Double.parseDouble(attributes.getValue("amount")));
            if (attributes.getValue("teaser_odd") != null) {
                if (attributes.getValue("teaser_odd").contains("+"))
                    myParlay.setTeaserOdd(Double.parseDouble(attributes.getValue("teaser_odd")));
                else
                    myParlay.setTeaserOdd((double) Integer.parseInt(attributes.getValue("teaser_odd")));
            }

            if (attributes.getValue("teaser_points") != null) {
                if (attributes.getValue("teaser_points").contains("+"))
                    myParlay.setTeaserPoints(Double.parseDouble(attributes.getValue("teaser_points")));
                else
                    myParlay.setTeaserPoints((double) Integer.parseInt(attributes.getValue("teaser_points")));
            }
            myParlay.setBetWin( Double.parseDouble(attributes.getValue("bet_win")) );
            myParlay.setBetResult( Long.parseLong(attributes.getValue("bet_result")));
            //myParlay.setTeaserPoints(Double.parseDouble(attributes.getValue("teaser_points")));
        }
        else if( localName.equals("myBet") ) {
            MyBet myBet = new MyBet();
            myBet.setEventID(Long.parseLong(attributes.getValue("EventID")));
            myBet.setStartDate(attributes.getValue("StartDate"));
            myBet.setSportID(Long.parseLong(attributes.getValue("SportID")));
            myBet.setLeagueID(Long.parseLong(attributes.getValue("LeagueID")));
            myBet.setEventDesc(attributes.getValue("event_desc"));
            myBet.setBetTypeId(Long.parseLong(attributes.getValue("bet_type_id")));
            myBet.setBetValue(Long.parseLong(attributes.getValue("bet_value")));
            myBet.setBetOdd(Double.parseDouble(attributes.getValue("bet_odd")));
            if( attributes.getValue("bet_line").equals("") != true )
                myBet.setBetLine(Double.parseDouble(attributes.getValue("bet_line")));
            myBet.setTypeLangValue(attributes.getValue("type_lang_value"));
            myBet.setTypeLangLine(attributes.getValue("type_lang_line"));
            myBet.setHomeID(Integer.parseInt(attributes.getValue("home_id")));
            myBet.setAwayID(Integer.parseInt(attributes.getValue("away_id")));
            myBet.setTeamName(attributes.getValue("team_name"));
            myBet.setTeamId(Integer.parseInt(attributes.getValue("team_id")));
            myParlay.getMyBets().add(myBet);
        }

    }


}
