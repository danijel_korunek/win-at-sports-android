package com.winatsports.data.parsers;



import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.data.firstpage.FirstPageGameSection;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;



/**
 * Created by broda on 09/12/2015.
 */


public class XmlFirstPageFeaturedGamesHelper extends DefaultHandler {

    String TAG = "XMLAboutToStartHelper";

    private final String TAG_PAGE_SECTION = "PageSection";
    private final String TITLE_ID_ATTRIBUTE = "TitleID";
    private final String POSITION_ATTRIBUTE = "Position";
    private final String TAG_FEATURE = "Feature";

    // attribute for FEATURED GAMES IN HOME SCREEN and for that xml file ==>  https://www.eclecticasoft.com/betting3/app_data/first_page/events_us.gz
    private final String EVENT_ID_ATTRIBUTE = "EventID";

    FirstPageGameSection gameSection = new FirstPageGameSection();

    public void parseXMLFile( String dataToParse, boolean redownload ) throws Exception {

        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser mSaxParser = factory.newSAXParser();
            XMLReader mXmlReader = mSaxParser.getXMLReader();
            mXmlReader.setContentHandler(this);
            InputSource is = new InputSource(new StringReader( dataToParse ) );
            mXmlReader.parse(is);

            if (!redownload) {
                MyApplication.getInstance().getFirstPage().setFeaturedGames(gameSection);
            } else {
                MyApplication.getInstance().getFirstPageCopy().setFeaturedGames(gameSection);
            }
        } catch (Exception e) {
            // Exceptions can be handled for different types
            // But, this is about XML Parsing not about Exception Handling
            Log.e(TAG, "Exception: " + e.getMessage(), e);
        }



    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {

    }

    @Override
    public void endElement( String uri, String localName, String qName ) throws SAXException {
        if (localName.equals(TAG_PAGE_SECTION))
            ; // nothing
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        if (localName.equals(TAG_PAGE_SECTION)) {
            gameSection.setTitleId(attributes.getValue(TITLE_ID_ATTRIBUTE));
            gameSection.setPosition(Integer.parseInt(attributes.getValue(POSITION_ATTRIBUTE)));
        }

        else if( localName.equals(TAG_FEATURE) && attributes.getIndex(EVENT_ID_ATTRIBUTE) != -1 ) {
            gameSection.getEventIds().add(Integer.parseInt(attributes.getValue(EVENT_ID_ATTRIBUTE)));
        }

    }

}
