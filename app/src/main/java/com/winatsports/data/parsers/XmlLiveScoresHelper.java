package com.winatsports.data.parsers;


import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.Util;
import com.winatsports.data.livescores.LiveEventScore;
import com.winatsports.data.livescores.Period;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


/**
 * Created by broda on 02/12/2015.
 */


// That is the only one XmlHelper where I use stringBuilder instead of normal STRING variable..
// And it is better working with this STRINGBUILDER
// In this file I have also deleted boolean variable = currentTagElementFromXML
// If I left this variable, then is not working good this ==> XmlLiveScoresHelper
public class XmlLiveScoresHelper extends DefaultHandler {


    StringBuilder sb = null;

    Boolean checkIfInsideFinishedTag = false;
    Boolean checkIfOutsideFinishedTag = false;

    // we are here CHECKING if "Events" TAG IS ACTIVE
    // If is not ACTIVE, THEN ACTIVE TAG IS "HistoryEvents"
    // I need this boolean variable, so that I can parse correctly live_scores.xml FILE
    Boolean checkIfEventsTagIsActive = false;

    ArrayList<LiveEventScore> newFinishedLiveScores;
    ArrayList<LiveEventScore> newActiveLiveScores;

    LiveEventScore tempFinishedLiveScore;
    LiveEventScore tempActiveLiveScore;

    ArrayList<Period> periodsArrayList;
    Period period = null ;

    public void parseXMLFile(String dataToParse) {
        newFinishedLiveScores = new ArrayList<LiveEventScore>();
        newActiveLiveScores = new ArrayList<LiveEventScore>();

        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser mSaxParser = factory.newSAXParser();
            XMLReader mXmlReader = mSaxParser.getXMLReader();
            mXmlReader.setContentHandler( this );
            InputSource is = new InputSource( new StringReader( dataToParse ) );
            mXmlReader.parse(is);

            MyApplication.getInstance().setAllActiveLiveScores(newActiveLiveScores);
            MyApplication.getInstance().setAllFinishedLiveScores(newFinishedLiveScores);
        } catch (Exception e) {
            // Exceptions can be handled for different types
            // But, this is about XML Parsing not about Exception Handling
            Log.e(XmlLiveScoresHelper.class.getSimpleName(), "Exception: " + e.getMessage(), e);
            if (Util.isNetworkConnected(MyApplication.getInstance())) {

            }
        }

    }

    // This method receives notification character data inside an element
    // for now in that file, this method will never execute, because all data from elements I get in startElement() method ==> from attributes
    // AND not ONE ELEMENT has character inside an element.. All data from elements attributes
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {

        if (sb!=null ) {
            for (int i=start; i<start+length; i++) {
                sb.append(ch[i]);
            }
        }

    }

    // Receives notification of end of an element
    // e.g. </message>
    // It will be called when TAG </message > is found, discovered, encountered
    // for now in that file, this method will never execute, because all data from elements I get in startElement() method ==> from attributes
    // AND not ONE ELEMENT has character inside an element.. All data from elements attributes
    @Override
    public void endElement( String uri, String localName, String qName ) throws SAXException {

        try {
            String value = sb.toString().trim();

            if( checkIfEventsTagIsActive == true ) {
                if (localName.equals("EventID") && Util.isPopulated(value))
                    tempActiveLiveScore.setEventID(Integer.parseInt(value));
                else if (localName.equals("SportID") && Util.isPopulated(value))
                    tempActiveLiveScore.setSportID(Integer.parseInt(value));
                else if (localName.equals("LeagueID") && Util.isPopulated(value))
                    tempActiveLiveScore.setLeagueID(Integer.parseInt(value));
                else if (localName.equals("StartDate") && Util.isPopulated(value))
                    tempActiveLiveScore.setStartDate(value);
                else if (localName.equals("HomeScore") && checkIfOutsideFinishedTag == true && Util.isPopulated(value))
                    tempActiveLiveScore.setHomeScore(Integer.parseInt(value));
                else if (localName.equals("AwayScore") && checkIfOutsideFinishedTag == true && Util.isPopulated(value))
                    tempActiveLiveScore.setAwayScore(Integer.parseInt(value));
                else if (localName.equals("HomeTeamID") && Util.isPopulated(value))
                    tempActiveLiveScore.setHomeTeamID(Integer.parseInt(value));
                else if (localName.equals("AwayTeamID") && Util.isPopulated(value))
                    tempActiveLiveScore.setAwayTeamID(Integer.parseInt(value));
                else if (localName.equals("CurrentLeft") && Util.isPopulated(value))
                    tempActiveLiveScore.setCurrentLeft(value);
                else if (localName.equals("CurrentRight") && Util.isPopulated(value))
                    tempActiveLiveScore.setCurrentRight(value);
                else if (localName.equals("PeriodName") && Util.isPopulated(value))
                    period.setPeriodName(value);
                else if (localName.equals("HomeScore") && checkIfInsideFinishedTag == true && Util.isPopulated(value))
                    period.setHomeScore(Integer.parseInt(value));
                else if (localName.equals("AwayScore") && checkIfInsideFinishedTag == true && Util.isPopulated(value))
                    period.setAwayScore(Integer.parseInt(value));

                else if (localName.equals("Period")) {
                    periodsArrayList.add(period);
                } else if (localName.equals("Finished")) {
                    tempActiveLiveScore.setPeriods(periodsArrayList);
                    checkIfInsideFinishedTag = false;
                } else if (localName.equals("Events")) {
                    newActiveLiveScores.add(tempActiveLiveScore);
                }
            }
            else {
                if (localName.equals("EventID") && Util.isPopulated(value))
                    tempFinishedLiveScore.setEventID(Integer.parseInt(value));
                else if (localName.equals("SportID") && Util.isPopulated(value))
                    tempFinishedLiveScore.setSportID(Integer.parseInt(value));
                else if (localName.equals("LeagueID") && Util.isPopulated(value))
                    tempFinishedLiveScore.setLeagueID(Integer.parseInt(value));
                else if (localName.equals("StartDate") && Util.isPopulated(value))
                    tempFinishedLiveScore.setStartDate(value);
                else if (localName.equals("HomeScore") && checkIfOutsideFinishedTag == true && Util.isPopulated(value))
                    tempFinishedLiveScore.setHomeScore(Integer.parseInt(value));
                else if (localName.equals("AwayScore") && checkIfOutsideFinishedTag == true && Util.isPopulated(value))
                    tempFinishedLiveScore.setAwayScore(Integer.parseInt(value));
                else if (localName.equals("HomeTeamID") && Util.isPopulated(value))
                    tempFinishedLiveScore.setHomeTeamID(Integer.parseInt(value));
                else if (localName.equals("AwayTeamID") && Util.isPopulated(value))
                    tempFinishedLiveScore.setAwayTeamID(Integer.parseInt(value));
                else if (localName.equals("CurrentLeft") && Util.isPopulated(value))
                    tempActiveLiveScore.setCurrentLeft(value);
                else if (localName.equals("CurrentRight") && Util.isPopulated(value))
                    tempActiveLiveScore.setCurrentRight(value);
                else if (localName.equals("PeriodName") && Util.isPopulated(value))
                    period.setPeriodName(value);
                else if (localName.equals("HomeScore") && checkIfInsideFinishedTag == true && Util.isPopulated(value))
                    period.setHomeScore(Integer.parseInt(value));
                else if (localName.equals("AwayScore") && checkIfInsideFinishedTag == true && Util.isPopulated(value))
                    period.setAwayScore(Integer.parseInt(value));

                else if (localName.equals("Period")) {
                    periodsArrayList.add(period);
                } else if (localName.equals("Finished")) {
                    tempFinishedLiveScore.setPeriods(periodsArrayList);
                    checkIfInsideFinishedTag = false;
                }  else if (localName.equals("HistoryEvents")) {
                    newFinishedLiveScores.add(tempFinishedLiveScore);
                }
            }
            /*else if( localName.equals("Games") ) {

                if( onlyForTesting.size() > 0 )
                    myApplication.getAllActiveLiveScore().addAll(onlyForTesting);
                myApplication.setAllActiveLiveScore(new ArrayList<LiveEventScore>());
            }*/

        }
        catch (Exception e) {
            Log.e(XmlLiveScoresHelper.class.getSimpleName(),  "Something went wrong. PLease try again to start application. ", e);
        }

    }

    // Receives notification of start of an element
    // e.g. <message>
    // It will be called when <message> is found, discovered, encountered
    // here with a help from attributes I will get all data
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        //Log.i(TAG, "TAG: " + localName);

        sb = new StringBuilder();

        if (localName.equals("Period")) {
            period = new Period();
        }
        else if( localName.equals("Finished") ) {
            periodsArrayList = new ArrayList<Period>();
            checkIfInsideFinishedTag = true;
            checkIfOutsideFinishedTag = false;
        }
        else if( localName.equals("HistoryEvents") ) {
            tempFinishedLiveScore = new LiveEventScore();
            checkIfOutsideFinishedTag = true;

            checkIfEventsTagIsActive = false;
        }
        // for now I'm only using this tag to test if there is any active game
        else if( localName.equals("Events") ) {

            tempActiveLiveScore = new LiveEventScore();
            checkIfOutsideFinishedTag = true;

            checkIfEventsTagIsActive = true;
        }

    }
}
