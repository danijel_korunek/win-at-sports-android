package com.winatsports.data.parsers;

/**
 * Created by broda on 29/10/2015.
 */


import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.Util;
import com.winatsports.data.MainGameData;
import com.winatsports.data.header.HeaderAppTutorial;
import com.winatsports.data.header.HeaderMainClass;
import com.winatsports.data.header.HeaderMarchMadness;
import com.winatsports.data.header.HeaderRealBetSettings;
import com.winatsports.data.header.HeaderRealBettingTutorial;
import com.winatsports.data.elements.Game;
import com.winatsports.data.elements.Odd;
import com.winatsports.data.elements.Sport;
import com.winatsports.data.elements.Team;
import com.winatsports.settings.Settings;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class XmlGameDataHelper extends DefaultHandler {

    String TAG = "XMLgameDataHelper";
    StringBuilder sb = null;

    public static MainGameData mainGameData = new MainGameData();

    public static HeaderMainClass headerMainClass = null;

    HeaderRealBetSettings headerRealBetSettings = new HeaderRealBetSettings();
    HeaderMarchMadness headerMarchMadness = new HeaderMarchMadness();
    HeaderAppTutorial headerAppTutorial = new HeaderAppTutorial();
    HeaderRealBettingTutorial headerRealBettingTutorial = new HeaderRealBettingTutorial();

    Sport sport = null;

    Game game = null;
    Team team = null;
    Odd odd = null;

    public void parseXMLFile( String dataToParse, boolean reDownload ) throws Exception {
        try {

            mainGameData.getGamesArrayList().clear();
            mainGameData.getSportsArrayList().clear();
            headerMainClass = new HeaderMainClass();

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser mSaxParser = factory.newSAXParser();
            XMLReader mXmlReader = mSaxParser.getXMLReader();
            mXmlReader.setContentHandler(this);
            InputSource is = new InputSource( new StringReader( dataToParse ) );
            mXmlReader.parse(is);

            if (!reDownload) {
                MyApplication.getInstance().setAllSports(mainGameData.getSportsArrayList());
                MyApplication.getInstance().setAllGames(mainGameData.getGamesArrayList());
                MyApplication.getInstance().setHeaderMainClass(headerMainClass);
            } else {
                MyApplication.getInstance().setAllSportsCopy(mainGameData.getSportsArrayList());
                MyApplication.getInstance().setAllGamesCopy(mainGameData.getGamesArrayList());
                MyApplication.getInstance().setHeaderMainClassCopy(headerMainClass);
            }
        } catch (Exception e) {
            // Exceptions can be handled for different types
            // But, this is about XML Parsing not about Exception Handling
            Log.e(TAG, "Exception: " + e.getMessage(), e);
        }
    }

    // This method receives notification character data inside an element
    /// e.g. <RegisterBonus>0</RegisterBonus>
    // It will be called when </RegisterBonus> is found, discovered, encountered
    // It will be called to read "0"
    // here I will call a lot's of time this method ==> characters()
    @Override
    public void characters( char[] ch, int start, int length ) throws SAXException {
        if (sb!=null ) {
            for (int i=start; i<start+length; i++) {
                sb.append(ch[i]);
            }
        }
    }

    // Receives notification of end of an element
    // e.g. <RegisterBonus>0</RegisterBonus>
    // It will be called when </RegisterBonus> is found, discovered, encountered
    // here I will call a lot's of time this method ==> endELement()
    @Override
    public void endElement( String uri, String localName, String qName ) throws SAXException {
        if (localName.equals("showWelcomeTutor")) {
            int value = Integer.parseInt(sb.toString().trim());
            headerMainClass.setShowingAppTutorial(value == 1);
        }

        if (localName.equals("showRealbetTutor")) {
            int value = Integer.parseInt(sb.toString().trim());
            headerMainClass.setShowingRealBettingTutorial(value == 1);
        }

        if( localName.equals("RegisterBonus") ) {
            headerMainClass.setRegisterBonus(Integer.parseInt(sb.toString().trim()));
        }
        else if( localName.equals("DroidAppVersionProtect") ) {
            headerMainClass.setAppVersionProtect(Integer.parseInt(sb.toString().trim()));
        }
        else if( localName.equals("DroidAppMaintenance") ) {
            headerMainClass.setAppMaintenance(Integer.parseInt(sb.toString().trim()));
        }
        else if( localName.equals("ParlayLimit") ) {
            headerMainClass.setParlayLimit(Integer.parseInt(sb.toString().trim()));
        }

        else if( localName.equals("EventID") ) {
            game.setEventID(Integer.parseInt(sb.toString().trim()));
        }
        else if( localName.equals("SportID") ) {
            game.setSportID(Integer.parseInt(sb.toString().trim()));
        }
        else if( localName.equals("OriginalLeague") ) {
            game.setOriginalLeague(Integer.parseInt(sb.toString().trim()));
        }
        else if( localName.equals("LeagueID") ) {
            game.setLeagueID(Integer.parseInt(sb.toString().trim()));
        }
        else if( localName.equals("LeagueType") ) {
            game.setLeagueType(Integer.parseInt(sb.toString().trim()));
        }
        else if( localName.equals("StartDate") ) {
            String startDate = sb.toString().trim();
            game.setStartDate(Util.convertDateWithTimezone(startDate));
        }
        else if( localName.equals("EventType") ) {
            game.setEventType( Integer.parseInt(sb.toString().trim()) );
        }
        else if( localName.equals("Status") ) {
            game.setStatus( Integer.parseInt(sb.toString().trim()) );
        }
        else if( localName.equals("GameDesc") ) {
            game.setGameDesc(Util.fixNull(sb.toString().trim()));
        }
        else if( localName.equals("FuturesDesc") ) {
            game.setFuturesDesc(Util.fixNull(sb.toString().trim()));
        }

        else if( localName.equals("Game") ) {
            mainGameData.getGamesArrayList().add(game);

        }

    }

    // Receives notification of start of an element
    // e.g. <RegisterBonus>0</RegisterBonus>
    // It will be called when <RegisterBonus> is found, discovered, encountered
    // here I will call a lot's of time this method ==> startELement()
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        //Log.i(TAG, "TAG: " + localName);
        sb = new StringBuilder();

        if ( localName.equals("Header") ) {
            headerMainClass = MyApplication.getInstance().getHeaderMainClass();
        }
        else if( localName.equals("SportInfo") ) {

            sport = new Sport();
            sport.setSportID(Integer.parseInt(attributes.getValue("SportID")));
            sport.setSportIconUrl(attributes.getValue("SportURL"));
            sport.setSportIconTime(attributes.getValue("SportIconTime"));
            sport.setChangeOrder(Integer.parseInt(attributes.getValue("ChangeOrder")));
            sport.setStatus(Integer.parseInt(attributes.getValue("Status")));
            sport.setHaveTeaser(Integer.parseInt(attributes.getValue("haveTeaser")));

            mainGameData.getSportsArrayList().add(sport);
        }
        else if( localName.equals("Game") ) {
            game = new Game();
        }
        else if( localName.equals("Team")) {

            team = new Team();
            team.setTeamID(Integer.parseInt(attributes.getValue("TeamID")));
            team.setTeamStatus(Integer.parseInt(attributes.getValue("TeamStatus")));
            team.setName(attributes.getValue("Name"));
            game.getTeamArrayList().add(team);
        }
        else if( localName.equals("Odd") ) {

            odd = new Odd();
            odd.setBetTypeId(Integer.parseInt(attributes.getValue("bet_type_id")));

            odd.setSortId(Util.determineSortId(odd.getBetTypeId()));
            odd.setTeamId(Integer.parseInt(attributes.getValue("team_id")));

            odd.setBetOdds(Double.parseDouble(attributes.getValue("bet_odd")));

            odd.setBetLine(attributes.getValue("bet_line"));

            odd.setOutValue(attributes.getValue("out_value"));
            odd.setOutBetLine(attributes.getValue("out_bet_line"));

            odd.setBetValue(Integer.parseInt(attributes.getValue("bet_value")));

            game.getOddArrayList().add(odd);
        }
        else if( localName.equals("RealBetSettings") ) {
            // first way how can I get value from RealBetSettings ==> RealbetInSafari which values is 1
            headerRealBetSettings.setRealbetFromRegDate(attributes.getValue("DroidRealbetFromRegDate"));
            Log.d(XmlGameDataHelper.class.getSimpleName(), "DroidRealbetFromRegDate: " + headerRealBetSettings.getRealbetFromRegDate());
            Settings.setRealBettingDate(MyApplication.getInstance(), attributes.getValue("DroidRealbetFromRegDate"));

            headerRealBetSettings.setRealbetInSafari(Integer.parseInt(attributes.getValue("DroidRealbetInSafari")) == 1);
            Settings.setRealBettingInSafari(MyApplication.getInstance(), headerRealBetSettings.isRealbetInSafari());
            Log.d(XmlGameDataHelper.class.getSimpleName(), "Droidrealbetinsafari: " + headerRealBetSettings.isRealbetInSafari());

            // second way how can I get value from RealBetSettings ==> RealbetOn which values is 0
            headerRealBetSettings.setRealbetOn(Integer.parseInt(attributes.getValue("DroidRealbetOn")) == 1);
            Settings.setRealBettingOn(MyApplication.getInstance(), headerRealBetSettings.isRealbetOn());
            Log.d(XmlGameDataHelper.class.getSimpleName(), "Droidrealbeton: " + headerRealBetSettings.isRealbetOn());

            headerRealBetSettings.setRealbetRuns(Integer.parseInt(attributes.getValue("DroidRealbetRuns")));
            Settings.setRealBettingNumOfRuns(MyApplication.getInstance(), headerRealBetSettings.getRealbetRuns());
            Log.d(XmlGameDataHelper.class.getSimpleName(), "DroidRealbetRuns: " + headerRealBetSettings.getRealbetRuns());

            headerRealBetSettings.setRealbetGuestsOn(Integer.parseInt(attributes.getValue("DroidRealbetGuestsOn")) == 1);
            Settings.setRealBettingGuestsOn(MyApplication.getInstance(), headerRealBetSettings.isRealbetGuestsOn());
            Log.d(XmlGameDataHelper.class.getSimpleName(), "DroidRealbetGuestsOn: " + headerRealBetSettings.isRealbetGuestsOn() + " " + attributes.getValue("DroidRealbetGuestsOn"));

            headerRealBetSettings.setRealbetGuestsRuns(Integer.parseInt(attributes.getValue("DroidRealbetGuestsRuns")));
            Settings.setRealBettingNumOfRuns(MyApplication.getInstance(), headerRealBetSettings.getRealbetGuestsRuns());
            Log.d(XmlGameDataHelper.class.getSimpleName(), "DroidRealbetGuestsRuns: " + headerRealBetSettings.getRealbetGuestsRuns());

            headerMainClass.setRealBetSettings(headerRealBetSettings);

        }
        else if( localName.equals("MarchMadness") ) {
            // first way how can I get value from RealBetSettings ==> Status which values is 1

            sb.append(attributes.getValue("Status"));
            headerMarchMadness.setStatus(Integer.parseInt(sb.toString().trim()));

            // second way how can I get value from RealBetSettings ==> MarchTitle which values is March Madness
            headerMarchMadness.setMarchTitle(attributes.getValue("MarchTitle"));
            headerMarchMadness.setMarchText(attributes.getValue("MarchText"));
            headerMarchMadness.setMarchURL(attributes.getValue("MarchURL"));
            headerMarchMadness.setSportID(Integer.parseInt(attributes.getValue("sportID")));
            headerMarchMadness.setZipDate(attributes.getValue("zipDate"));

            headerMainClass.setMarchMadness(headerMarchMadness);
        }
        else if( localName.equals("appTutorial") ) {
            headerAppTutorial.setLastUpdate(attributes.getValue("lastUpdate"));
            headerAppTutorial.setPageNumber(Integer.parseInt(attributes.getValue("pageNumber")));

            headerMainClass.setAppTutorial(headerAppTutorial);
        }
        else if( localName.equals("realBettingTutorial") ) {
            headerRealBettingTutorial.setLastUpdate(attributes.getValue("lastUpdate"));
            headerRealBettingTutorial.setPageNumber(Integer.parseInt(attributes.getValue("pageNumber")));

            headerMainClass.setRealBettingTutorial(headerRealBettingTutorial);
        }

    }



}