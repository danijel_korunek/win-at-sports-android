package com.winatsports.data.stats;

import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.Util;
import com.winatsports.data.elements.BetType;
import com.winatsports.data.elements.League;
import com.winatsports.data.elements.Sport;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by dkorunek on 29/08/16.
 */
public class StatsCollection extends DefaultHandler {
    private ArrayList<SportEvent> eventsPerDay;
    private float eventsPerDayTotal;
    private ArrayList<SportEvent> eventsPerWeek;
    private float eventsPerWeekTotal;
    private ArrayList<SportEvent> eventsPerMonth;
    private float eventsPerMonthTotal;

    private ArrayList<SportEvent> popularSportsPerDay;
    private float sportsPerDayTotal;
    private ArrayList<SportEvent> popularSportsPerWeek;
    private float sportsPerWeekTotal;
    private ArrayList<SportEvent> popularSportsPerMonth;
    private float sportsPerMonthTotal;

    private ArrayList<SportEvent> popularLeaguesPerDay;
    private float leaguesPerDayTotal;
    private ArrayList<SportEvent> popularLeaguesPerWeek;
    private float leaguesPerWeekTotal;
    private ArrayList<SportEvent> popularLeaguesPerMonth;
    private float leaguesPerMonthTotal;

    private ArrayList<SportEvent> popularBetTypesPerDay;
    private float betTypesPerDayTotal;
    private ArrayList<SportEvent> popularBetTypesPerWeek;
    private float betTypesPerWeekTotal;
    private ArrayList<SportEvent> popularBetTypesPerMonth;
    private float betTypesPerMonthTotal;

    private final String TAG_EVENTS_NUMBER = "EventsNumber";
    private final String TAG_EVENTS_PER_DAY = "EventsPer1";
    private final String TAG_EVENT = "Events";
    private final String TAG_EVENTS_PER_WEEK = "EventsPer7";
    private final String TAG_EVENTS_PER_MONTH = "EventsPer30";

    private final String TAG_POPULARITY = "Popularity";
    private final String TAG_POPULARITY_SPORTS = "PopularitySports";
    private final String TAG_POPULARITY_LEAGUES = "PopularityLeagues";
    private final String TAG_POPULARITY_PER_DAY = "Popularity1";
    private final String TAG_POPULARITY_PER_WEEK = "Popularity7";
    private final String TAG_POPULARITY_PER_MONTH = "Popularity30";
    private final String TAG_POPULAR_SPORT = "PopularSports";
    private final String TAG_POPULAR_LEAGUE = "PopularLeagues";

    private final String TAG_POPULARITY_BET_TYPES = "PopularityBetTypes";
    private final String TAG_POPULAR_BET_TYPE = "PopularBetTypes";

    private boolean isEventsNumber;
    private boolean isEventsPerDay;
    private boolean isEventsPerWeek;
    private boolean isEventsPerMonth;

    private boolean isPopularity;
    private boolean isPopularitySports;
    private boolean isPopularityLeagues;
    private boolean isPopularityPerDay;
    private boolean isPopularityPerWeek;
    private boolean isPopularityPerMonth;

    private boolean isPopularityBetTypes;

    private UserStats userStats;

    public static class SportEvent {
        public String name;
        public int value;
    }

    public StatsCollection(StringBuffer buffer) {
        // init arrays
        eventsPerDay = new ArrayList<SportEvent>();
        eventsPerDayTotal = 0f;
        eventsPerWeek = new ArrayList<SportEvent>();
        eventsPerWeekTotal = 0f;
        eventsPerMonth = new ArrayList<SportEvent>();
        eventsPerMonthTotal = 0f;

        popularSportsPerDay = new ArrayList<SportEvent>();
        sportsPerDayTotal = 0f;
        popularSportsPerWeek = new ArrayList<SportEvent>();
        sportsPerWeekTotal = 0f;
        popularSportsPerMonth = new ArrayList<SportEvent>();
        sportsPerMonthTotal = 0f;

        popularLeaguesPerDay = new ArrayList<SportEvent>();
        leaguesPerDayTotal = 0f;
        popularLeaguesPerWeek = new ArrayList<SportEvent>();
        leaguesPerWeekTotal = 0f;
        popularLeaguesPerMonth = new ArrayList<SportEvent>();
        leaguesPerMonthTotal = 0f;

        popularBetTypesPerDay = new ArrayList<SportEvent>();
        popularBetTypesPerWeek = new ArrayList<SportEvent>();
        popularBetTypesPerMonth = new ArrayList<SportEvent>();

        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser mSaxParser = factory.newSAXParser();
            XMLReader mXmlReader = mSaxParser.getXMLReader();
            mXmlReader.setContentHandler(this);
            InputSource is = new InputSource(new StringReader(buffer.toString()));
            mXmlReader.parse(is);
        } catch (Exception e) {
            Log.e(StatsCollection.class.getSimpleName(), "Error instating", e);
        }

        userStats = null;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        if (localName.equalsIgnoreCase(TAG_EVENTS_NUMBER))
            isEventsNumber = true;
        if (localName.equalsIgnoreCase(TAG_EVENTS_PER_DAY))
            isEventsPerDay = true;
        if (localName.equalsIgnoreCase(TAG_EVENTS_PER_WEEK))
            isEventsPerWeek = true;
        if (localName.equalsIgnoreCase(TAG_EVENTS_PER_MONTH))
            isEventsPerMonth = true;

        if (localName.equalsIgnoreCase(TAG_POPULARITY))
            isPopularity = true;
        if (localName.equalsIgnoreCase(TAG_POPULARITY_SPORTS))
            isPopularitySports = true;
        if (localName.equalsIgnoreCase(TAG_POPULARITY_PER_DAY))
            isPopularityPerDay = true;
        if (localName.equalsIgnoreCase(TAG_POPULARITY_PER_WEEK))
            isPopularityPerWeek = true;
        if (localName.equalsIgnoreCase(TAG_POPULARITY_PER_MONTH))
            isPopularityPerMonth = true;
        if (localName.equalsIgnoreCase(TAG_POPULARITY_LEAGUES))
            isPopularityLeagues = true;
        if (localName.equalsIgnoreCase(TAG_POPULARITY_BET_TYPES))
            isPopularityBetTypes = true;

        if (localName.equalsIgnoreCase(TAG_EVENT) || localName.equalsIgnoreCase(TAG_POPULAR_SPORT)) {
            String sportId = attributes.getValue("SportID");
            String eventId = null;
            if (isEventsNumber)
                eventId = attributes.getValue("EventNumber");
            else if (isPopularity)
                eventId = attributes.getValue("Value");

            Sport sport = Util.getSport(Long.parseLong(sportId));

            SportEvent se = new SportEvent();
            se.name = sport.getSportName();
            se.value = Integer.parseInt(eventId);

            if (isEventsNumber) {
                if (isEventsPerDay) {
                    getEventsPerDay().add(se);
                    eventsPerDayTotal += se.value;
                } else if (isEventsPerWeek) {
                    getEventsPerWeek().add(se);
                    eventsPerWeekTotal += se.value;
                } else if (isEventsPerMonth) {
                    getEventsPerMonth().add(se);
                    eventsPerMonthTotal += se.value;
                }
            } else if (isPopularity) {
                if (isPopularitySports) {
                    if (isPopularityPerDay) {
                        getPopularSportsPerDay().add(se);
                        sportsPerDayTotal += se.value;
                    } else if (isPopularityPerWeek) {
                        getPopularSportsPerWeek().add(se);
                        sportsPerWeekTotal += se.value;
                    } else if (isPopularityPerMonth) {
                        getPopularSportsPerMonth().add(se);
                        sportsPerMonthTotal += se.value;
                    }
                }
            }
        } else if (localName.equalsIgnoreCase(TAG_POPULAR_LEAGUE)) {
            String leagueId = attributes.getValue("LeagueID");
            String leagueType = attributes.getValue("LeagueType");
            String value = attributes.getValue("Value");
            League league = Util.getLeague(Long.parseLong(leagueId), Long.parseLong(leagueType));
            String leagueName = league == null ? "" : league.getLeagueName();

            SportEvent se = new SportEvent();
            se.name = leagueName;
            se.value = Integer.parseInt(value);

            if (isPopularity && isPopularityLeagues) {
                if (isPopularityPerDay) {
                    getPopularLeaguesPerDay().add(se);
                    leaguesPerDayTotal += se.value;
                } else if (isPopularityPerWeek) {
                    getPopularLeaguesPerWeek().add(se);
                    leaguesPerWeekTotal += se.value;
                } else if (isPopularityPerMonth) {
                    getPopularLeaguesPerMonth().add(se);
                    leaguesPerMonthTotal += se.value;
                }
            }
        } else if (localName.equalsIgnoreCase(TAG_POPULAR_BET_TYPE)) {
            int betTypeId = Integer.parseInt(attributes.getValue("BetTypeID"));
            String value = attributes.getValue("Value");
            String name = "";

            ArrayList<Sport> allSports = MyApplication.getInstance().getAllSports();
            synchronized (allSports) {
                boolean found = false;
                for (Sport sport : allSports) {
                    for (BetType betType : sport.getBetTypes()) {
                        if (betType.getTypeID() == betTypeId) {
                            found = true;
                            name = betType.getTypeName();
                            break;
                        }

                        if (found)
                            break;
                    }
                }
            }

            SportEvent se = new SportEvent();
            se.name = name;
            se.value = Integer.parseInt(value);

            if (isPopularityBetTypes) {
                if (isPopularityPerDay) {
                    getPopularBetTypesPerDay().add(se);
                    betTypesPerDayTotal += se.value;
                } else if (isPopularityPerWeek) {
                    getPopularBetTypesPerWeek().add(se);
                    betTypesPerWeekTotal += se.value;
                } else if (isPopularityPerMonth) {
                    getPopularBetTypesPerMonth().add(se);
                    betTypesPerMonthTotal += se.value;
                }
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (localName.equalsIgnoreCase(TAG_EVENTS_NUMBER))
            isEventsNumber = false;
        if (localName.equalsIgnoreCase(TAG_EVENTS_PER_DAY))
            isEventsPerDay = false;
        if (localName.equalsIgnoreCase(TAG_EVENTS_PER_WEEK))
            isEventsPerWeek = false;
        if (localName.equalsIgnoreCase(TAG_EVENTS_PER_MONTH))
            isEventsPerMonth = false;

        if (localName.equalsIgnoreCase(TAG_POPULARITY))
            isPopularity = false;
        if (localName.equalsIgnoreCase(TAG_POPULARITY_SPORTS))
            isPopularitySports = false;
        if (localName.equalsIgnoreCase(TAG_POPULARITY_PER_DAY))
            isPopularityPerDay = false;
        if (localName.equalsIgnoreCase(TAG_POPULARITY_PER_WEEK))
            isPopularityPerWeek = false;
        if (localName.equalsIgnoreCase(TAG_POPULARITY_PER_MONTH))
            isPopularityPerMonth = false;
        if (localName.equalsIgnoreCase(TAG_POPULARITY_LEAGUES))
            isPopularityLeagues = false;
        if (localName.equalsIgnoreCase(TAG_POPULARITY_BET_TYPES))
            isPopularityBetTypes = false;
    }

    public ArrayList getEventsPerDay() {
        return eventsPerDay;
    }

    public float getEventsPerDayTotal() {
        return eventsPerDayTotal;
    }

    public ArrayList getEventsPerWeek() {
        return eventsPerWeek;
    }

    public float getEventsPerWeekTotal() { return eventsPerWeekTotal; }

    public ArrayList getEventsPerMonth() {
        return eventsPerMonth;
    }

    public float getEventsPerMonthTotal() { return eventsPerMonthTotal; }

    public ArrayList getPopularSportsPerDay() {
        return popularSportsPerDay;
    }

    public float getSportsPerDayTotal() { return sportsPerDayTotal; }

    public ArrayList getPopularSportsPerWeek() {
        return popularSportsPerWeek;
    }

    public float getSportsPerWeekTotal() { return sportsPerWeekTotal; }

    public ArrayList getPopularSportsPerMonth() {
        return popularSportsPerMonth;
    }

    public float getSportsPerMonthTotal() { return sportsPerMonthTotal; }

    public ArrayList getPopularLeaguesPerDay() {
        return popularLeaguesPerDay;
    }

    public float getLeaguesPerDayTotal() { return  leaguesPerDayTotal; }

    public ArrayList getPopularLeaguesPerWeek() {
        return popularLeaguesPerWeek;
    }

    public float getLeaguesPerWeekTotal() { return leaguesPerWeekTotal; }

    public ArrayList getPopularLeaguesPerMonth() {
        return popularLeaguesPerMonth;
    }

    public float getLeaguesPerMonthTotal() { return leaguesPerMonthTotal; }

    public ArrayList getPopularBetTypesPerDay() {
        return popularBetTypesPerDay;
    }

    public float getBetTypesPerDayTotal() { return betTypesPerDayTotal; }

    public ArrayList getPopularBetTypesPerWeek() {
        return popularBetTypesPerWeek;
    }

    public float getBetTypesPerWeekTotal() { return betTypesPerWeekTotal; }

    public ArrayList getPopularBetTypesPerMonth() {
        return popularBetTypesPerMonth;
    }

    public float getBetTypesPerMonthTotal() { return betTypesPerMonthTotal; }

    public UserStats getUserStats() {
        return userStats;
    }

    public void setUserStats(UserStats userStats) {
        this.userStats = userStats;
    }
}
