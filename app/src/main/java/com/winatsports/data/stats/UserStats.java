package com.winatsports.data.stats;

import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.Util;
import com.winatsports.data.MyBet;
import com.winatsports.data.MyParlay;
import com.winatsports.data.elements.Game;
import com.winatsports.data.elements.Sport;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by dkorunek on 30/08/16.
 */
public class UserStats extends DefaultHandler {
    private ArrayList<SportBet> countryStats;
    private float countryStatTotal;
    private ArrayList<MyParlay> bestWins;
    private ArrayList<MyParlay> lastBets;
    private ArrayList<WeeklyMoneyInfo> weeklyMoneyInfos;

    public static class WeeklyMoneyInfo {
        public String date;
        public double betAmount;
        public double betWin;
        public double betLost;
        public double betReturn;
        public int countBet;
        public int countWon;
        public int countLost;
        public int countReturn;
        public double walletStart;
        public double walletEnd;
    }

    public static class SportBet {
        public String sportName;
        public Float betNumber;
    }


    private final String TAG_WEEKLY_MONEY_INFO = "WeeklyMoneyInfo";
    private final String TAG_WEEK = "weekinfo";
    private final String TAG_LAST_BETS = "LastBets";
    private final String TAG_BEST_WINS = "BestWins";
    private final String TAG_PARLAY = "parlay";
    private final String TAG_MY_BET = "myBet";
    //private final String TAG_COUNTRY = "Country";
    private final String TAG_COUNTRY_STATS = "CountryStats";
    private final String TAG_COUNTRY_SPORTS = "CountrySports";
    private final String TAG_SPORT = "Sport";

    private boolean inWeeklyMoneyInfo;
    private boolean inLastBets;
    private boolean inParlay;
    private boolean inBestWins;
    private boolean inCountryStats;
    private boolean inCountrySports;

    private MyParlay parlay;

    public UserStats(StringBuffer sb) {
        weeklyMoneyInfos = new ArrayList<WeeklyMoneyInfo>();
        lastBets = new ArrayList<MyParlay>();
        bestWins = new ArrayList<MyParlay>();
        countryStats = new ArrayList<SportBet>();
        countryStatTotal = 0f;

        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser mSaxParser = factory.newSAXParser();
            XMLReader mXmlReader = mSaxParser.getXMLReader();
            mXmlReader.setContentHandler(this);
            InputSource is = new InputSource(new StringReader(sb.toString()));
            mXmlReader.parse(is);
        } catch (Exception e) {
            Log.e(StatsCollection.class.getSimpleName(), "Error instating", e);
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (localName.equalsIgnoreCase(TAG_WEEKLY_MONEY_INFO))
            inWeeklyMoneyInfo = true;
        if (localName.equalsIgnoreCase(TAG_LAST_BETS))
            inLastBets = true;
        if (localName.equalsIgnoreCase(TAG_BEST_WINS))
            inBestWins = true;
        if (localName.equalsIgnoreCase(TAG_COUNTRY_STATS))
            inCountryStats = true;
        if (localName.equalsIgnoreCase(TAG_COUNTRY_SPORTS))
            inCountrySports = true;

        if (localName.equalsIgnoreCase(TAG_PARLAY)) {
            inParlay = true;

            parlay = new MyParlay();
            parlay.setAmount(Double.parseDouble(attributes.getValue("amount")));
            parlay.setBetWin(Double.parseDouble(attributes.getValue("bet_win")));
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            Date date = null;
            try {
                date = format.parse(attributes.getValue("time"));
                parlay.setTime(Util.convertDateWithTimezoneToDate(attributes.getValue("time")));
            } catch (ParseException e) {
                Log.e(UserStats.class.getSimpleName(), "", e);
            }

            parlay.setTeaserPoints(Double.parseDouble(attributes.getValue("teaser_points") == null ? "-1" : attributes.getValue("teaser_points")));
            parlay.setTeaserOdd(Double.parseDouble(attributes.getValue("teaser_odd") == null ? "-1" : attributes.getValue("teaser_odd")));

        }

        if (localName.equalsIgnoreCase(TAG_SPORT)) {
            if (inCountryStats && inCountrySports) {
                String sportId = attributes.getValue("SportID");
                String betId = attributes.getValue("BetNumber");

                String sportName = "";
                if (Util.isPopulated(sportId)) {
                    Sport sport = Util.getSport(Long.parseLong(sportId));
                    if (sport != null)
                        sportName = sport.getSportName();
                }

                SportBet sb = new SportBet();
                sb.sportName = sportName;
                sb.betNumber = Float.parseFloat(betId);
                countryStatTotal += sb.betNumber;
                getCountryStats().add(sb);
            }
        }

        if (localName.equalsIgnoreCase(TAG_WEEK)) {
            if (inWeeklyMoneyInfo) {
                WeeklyMoneyInfo week = new WeeklyMoneyInfo();
                week.date = attributes.getValue("week");
                week.betAmount = Double.parseDouble(attributes.getValue("BetAmount"));
                week.betWin = Double.parseDouble(attributes.getValue("BetWin"));
                week.betLost = Double.parseDouble(attributes.getValue("BetLost"));
                week.betReturn = Double.parseDouble(attributes.getValue("BetReturn"));
                week.countBet = Integer.parseInt(attributes.getValue("CountBet"));
                week.countWon = Integer.parseInt(attributes.getValue("CountWin"));
                week.countLost = Integer.parseInt(attributes.getValue("CountLost"));
                week.countReturn = Integer.parseInt(attributes.getValue("CountReturn"));
                week.walletStart = attributes.getValue("WalletStart") == null ? -1 : Double.parseDouble(attributes.getValue("WalletStart"));
                week.walletEnd = attributes.getValue("WalletEnd") == null ? -1 : Double.parseDouble(attributes.getValue("WalletEnd"));

                getWeeklyMoneyInfos().add(week);
            }
        } else if (localName.equalsIgnoreCase(TAG_MY_BET)) {
            if (inParlay) {
                MyBet myBet = new MyBet();
                myBet.setEventID(Long.parseLong(attributes.getValue("EventID")));
                myBet.setStartDate(attributes.getValue("StartDate"));
                myBet.setSportID(Long.parseLong(attributes.getValue("SportID")));
                myBet.setLeagueID(Long.parseLong(attributes.getValue("LeagueID")));
                myBet.setLeagueType(1);
                myBet.setBetTypeId(Long.parseLong(attributes.getValue("bet_type_id")));
                myBet.setBetValue(Long.parseLong(attributes.getValue("bet_value")));
                myBet.setBetOdd(Double.parseDouble(attributes.getValue("bet_odd")));
                if( attributes.getValue("bet_line").equals("") != true )
                    myBet.setBetLine(Double.parseDouble(attributes.getValue("bet_line")));
                myBet.setTypeLangValue(attributes.getValue("type_lang_value"));
                myBet.setTypeLangLine(attributes.getValue("type_lang_line"));
                myBet.setHomeID(Integer.parseInt(attributes.getValue("home_id")));
                myBet.setAwayID(Integer.parseInt(attributes.getValue("away_id")));
                if (myBet.getHomeID() == 0 && myBet.getAwayID() == 0)
                    myBet.setEventDesc(attributes.getValue("event_desc"));
                else {
                    Game game = Util.getGame(myBet.getEventID());
                    if (game != null)
                        myBet.setEventDesc(Util.formatHomeVsAway(game, MyApplication.getInstance()));
                    else
                        myBet.setEventDesc("");
                }
                myBet.setTeamName(attributes.getValue("team_name"));
                myBet.setTeamId(Integer.parseInt(attributes.getValue("team_id")));
                parlay.getMyBets().add(myBet);
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (localName.equalsIgnoreCase(TAG_WEEKLY_MONEY_INFO))
            inWeeklyMoneyInfo = false;
        if (localName.equalsIgnoreCase(TAG_LAST_BETS))
            inLastBets = false;
        if (localName.equalsIgnoreCase(TAG_BEST_WINS))
            inBestWins = false;
        if (localName.equalsIgnoreCase(TAG_COUNTRY_STATS))
            inCountryStats = false;
        if (localName.equalsIgnoreCase(TAG_COUNTRY_SPORTS))
            inCountrySports = false;
        if (localName.equalsIgnoreCase(TAG_PARLAY)) {
            inParlay = false;
            if (inLastBets)
                getLastBets().add(parlay);
            if (inBestWins)
                getBestWins().add(parlay);
        }
    }

    public ArrayList<SportBet> getCountryStats() {
        return countryStats;
    }

    public ArrayList<MyParlay> getBestWins() {
        return bestWins;
    }

    public ArrayList<MyParlay> getLastBets() {
        return lastBets;
    }

    public ArrayList<WeeklyMoneyInfo> getWeeklyMoneyInfos() {
        return weeklyMoneyInfos;
    }

    public float getCountryStatTotal() {
        return countryStatTotal;
    }
}
