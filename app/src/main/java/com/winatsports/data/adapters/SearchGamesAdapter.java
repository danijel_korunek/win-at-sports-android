package com.winatsports.data.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.data.elements.Game;
import com.winatsports.data.elements.Sport;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by broda on 14/01/2016.
 */
public class SearchGamesAdapter extends BaseAdapter {

    private LayoutInflater mInflater;

    //The variable that will hold our text data to be tied to list.
    private ArrayList<Game> allGames;
    private ArrayList<Bitmap> gamesIcon;

    public SearchGamesAdapter(Context context, ArrayList<Game> gameArrayList) {

        mInflater = LayoutInflater.from(context);
        this.allGames = gameArrayList;
        gamesIcon = new ArrayList<Bitmap>();

        // I need to display just time of game(event), without date
        // In the end of this function I will also add GAME_ICON ==> for every game(event)
        // So that gamesIcon, allGames and nameOfTeams  ARRAYLIST-s will be the same size
        // And this will work
        // And in constructor I will prepare all DATA,, which I will later shown in ==> getView() METHOD
        Calendar calendar = Calendar.getInstance();
        for( int index1=0; index1<allGames.size(); index1++ ) {

            Game game = allGames.get(index1);

            // here I'm starting to add GAME_ICON for every game(events)
            for( int index2=0; index2<MyApplication.getInstance().getAllSports().size(); index2++ ) {

                Sport sport = MyApplication.getInstance().getAllSports().get(index2);
                if( game.getSportID() == sport.getSportID() ) {
                    gamesIcon.add(sport.getScaledIcon());
                    break;
                }
            }

        }

    }

    @Override
    public int getCount() {
        return allGames.size();
    }

    @Override
    public Object getItem(int position) {
        return allGames.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder
    {
        ImageView ivSearchGameIcon;
        TextView tvSearchTeamsName;
        TextView tvSearchTimeOfGame;
    }

    //A view to hold each row in the list
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // A ViewHolder keeps references to children views to avoid unneccessary calls
        // to findViewById() on each row.
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listrow_search_events, null);

            holder = new ViewHolder();
            holder.ivSearchGameIcon = (ImageView) convertView.findViewById(R.id.ivSearchGameIcon);
            holder.tvSearchTeamsName = (TextView) convertView.findViewById(R.id.tvSearchTeamsName);
            holder.tvSearchTimeOfGame = (TextView) convertView.findViewById(R.id.tvSearchTimeOfGame);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Game game = allGames.get(position);

        Bitmap correctGameIcon = gamesIcon.get(position);

        holder.ivSearchGameIcon.setImageBitmap( correctGameIcon );
        holder.tvSearchTeamsName.setText(Util.formatHomeVsAway(game, mInflater.getContext()));
        holder.tvSearchTimeOfGame.setText( game.getStartDate() );

        if (position % 2 == 0)
            convertView.setBackgroundColor(mInflater.getContext().getResources().getColor(R.color.list_black_row));
        else
            convertView.setBackgroundColor(mInflater.getContext().getResources().getColor(R.color.list_gray_row));

        if( allGames.get(position).getOddArrayList().size() > 0 ) {
            convertView.setAlpha(0.7f);
            return convertView;
        }
        else {
            convertView.setAlpha(0.4f);
            return convertView;
        }
    }
}