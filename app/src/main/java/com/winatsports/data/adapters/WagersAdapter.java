package com.winatsports.data.adapters;



import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.data.Constants;
import com.winatsports.data.MyBet;
import com.winatsports.data.MyParlay;
import com.winatsports.data.elements.BetType;
import com.winatsports.data.elements.Game;
import com.winatsports.data.elements.League;
import com.winatsports.data.elements.Sport;
import com.winatsports.data.elements.Team;
import com.winatsports.settings.Settings;

import java.util.ArrayList;

/**
 * @author dkorunek
 */

public class WagersAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;

    private ArrayList<MyParlay> parlays;

    private Bitmap parlayCardIcon;

    private ArrayList<Bitmap> icons;
    private ArrayList<String> gameNames;
    private ArrayList<String> teamVsTeamValues;
    private ArrayList<String> teamAndPointValues;
    private ArrayList<String> dateAndTimeValues;
    private ArrayList<String> wagerAmounts;
    private ArrayList<String> betTypeValues;
    private ArrayList<String> detailEntries;

    private Context context;

    public WagersAdapter(Context context) {

        layoutInflater = LayoutInflater.from(context);
        this.context = context;

        icons = new ArrayList<Bitmap>();
        gameNames = new ArrayList<String>();
        teamVsTeamValues = new ArrayList<String>();
        teamAndPointValues = new ArrayList<String>();
        dateAndTimeValues = new ArrayList<String>();
        wagerAmounts = new ArrayList<String>();
        betTypeValues = new ArrayList<String>();
        detailEntries = new ArrayList<String>();
        parlays = new ArrayList<MyParlay>();
        parlayCardIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.parlay);

        prepareData();
    }

    @Override
    public int getCount() {
        return parlays.size();
    }

    @Override
    public Object getItem(int position) {
        return parlays.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        ImageView ivIcon;
        TextView tvGameNamePlusWagerAmount;
        TextView tvTeamVsTeam;
        TextView tvTeamAndPoints;
        TextView tvDateAndTime;
        TextView tvWagerAmount;
        TextView tvBetType;
        TextView tvDetails;
    }

    //A view to hold each row in the list
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.listrow_wagers_parlay_with_records, null);

            holder = new ViewHolder();
            holder.ivIcon = (ImageView) convertView.findViewById(R.id.ivIcon);
            holder.tvGameNamePlusWagerAmount = (TextView) convertView.findViewById(R.id.tvGameNamePlusWagerAmount);
            holder.tvTeamVsTeam = (TextView) convertView.findViewById(R.id.tvTeamVsTeam);
            holder.tvTeamAndPoints = (TextView) convertView.findViewById(R.id.tvTeamAndPoints);
            holder.tvDateAndTime = (TextView) convertView.findViewById(R.id.tvDateAndTime);
            holder.tvWagerAmount = (TextView) convertView.findViewById(R.id.tvWagerAmount);
            holder.tvBetType = (TextView) convertView.findViewById(R.id.tvBetType);
            holder.tvDetails = (TextView) convertView.findViewById(R.id.tvDetails);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        // image filter is set only for parlay icon
        Bitmap iconToSet = parlays.get(position).getIcon();
        if (iconToSet != null && iconToSet.equals(parlayCardIcon)) {
            // alpha from 0 to 255 in integer value, float value from 0.0f to 1.0f
            // only if I set alpha = 50 or 210, it is the same
            /*int alpha = 210;
            Paint p = new Paint();

            ColorFilter filter = new LightingColorFilter(Color.WHITE, 0);
            p.setAlpha(alpha);
            p.setColorFilter(filter);

            holder.ivIcon.setColorFilter(filter);*/
        }

        holder.ivIcon.setImageBitmap(iconToSet);

        holder.tvGameNamePlusWagerAmount.setText(gameNames.get(position));
        holder.tvTeamVsTeam.setText(teamVsTeamValues.get(position));
        holder.tvTeamAndPoints.setText(teamAndPointValues.get(position));
        holder.tvDateAndTime.setText(dateAndTimeValues.get(position));
        holder.tvWagerAmount.setText(wagerAmounts.get(position));
        holder.tvBetType.setText(betTypeValues.get(position));
        holder.tvDetails.setText(detailEntries.get(position));

        if (holder.tvGameNamePlusWagerAmount.getText().length() == 0)
            holder.tvGameNamePlusWagerAmount.setVisibility(View.GONE);
        else
            holder.tvGameNamePlusWagerAmount.setVisibility(View.VISIBLE);

        if (holder.tvTeamVsTeam.getText().length() == 0)
            holder.tvTeamVsTeam.setVisibility(View.GONE);
        else
            holder.tvTeamVsTeam.setVisibility(View.VISIBLE);

        if (holder.tvDateAndTime.getText().length() == 0)
            holder.tvDateAndTime.setVisibility(View.GONE);
        else
            holder.tvDateAndTime.setVisibility(View.VISIBLE);

        if (holder.tvWagerAmount.getText().length() == 0)
            holder.tvWagerAmount.setVisibility(View.GONE);
        else
            holder.tvWagerAmount.setVisibility(View.VISIBLE);

        if (holder.tvBetType.getText().length() == 0)
            holder.tvBetType.setVisibility(View.GONE);
        else
            holder.tvBetType.setVisibility(View.VISIBLE);

        if (holder.tvDetails.getText().length() == 0)
            holder.tvDetails.setVisibility(View.GONE);
        else
            holder.tvDetails.setVisibility(View.VISIBLE);

        if (position % 2 == 0)
            convertView.setBackgroundColor(context.getResources().getColor(R.color.list_black_row));
        else
            convertView.setBackgroundColor(context.getResources().getColor(R.color.list_gray_row));

        return convertView;

    }

    public void prepareData() {
        gameNames.clear();
        teamVsTeamValues.clear();
        teamAndPointValues.clear();
        dateAndTimeValues.clear();
        wagerAmounts.clear();
        betTypeValues.clear();
        detailEntries.clear();
        parlays = MyApplication.getInstance().getUserData().getUserBets();

        for (MyParlay parlay : parlays) {
            if (parlay.getMyBets().size() == 1) {
                MyBet myBet = parlay.getMyBets().get(0);

                League league = Util.getLeague(myBet.getLeagueID(), myBet.getLeagueType());
                if (league == null)
                    league = Util.getLeague(myBet.getLeagueID());

                String leagueName = league == null ? "" : league.getLeagueName();
                Sport sport = Util.getSport(myBet.getSportID());
                Game game = Util.getGame(myBet.getEventID());

                parlay.setIcon(sport.getScaledIcon());
                dateAndTimeValues.add(Util.formatDate(myBet.getStartDate()));

                double toWin = parlay.getAmount() * Util.convertUSOddsToDecimalOdds(myBet.getBetOdd());
                if (Settings.isWinAmount(context)) toWin -= parlay.getAmount();

                String wagerAmount = String.format("%s (%s) %s %s", Util.formatMoney(parlay.getAmount(), false),
                        Util.formatOdds(context, myBet.getBetOdd()),
                        Util.getString(Constants.MSG_KEY_APP_TOWIN),
                        Util.formatMoney(toWin, false));
                wagerAmounts.add(wagerAmount);

                if (Util.isPopulated(myBet.getTypeName()))
                    betTypeValues.add(leagueName + ", " + myBet.getTypeName());
                else {
                    String betTypeName = null;
                    for (BetType bt : sport.getBetTypes()) {
                        if (bt.getTypeID() == myBet.getBetTypeId()) {
                            betTypeName = bt.getTypeName();
                            break;
                        }
                    }

                    if (Util.isPopulated(betTypeName))
                        betTypeValues.add(leagueName + ", " + betTypeName);
                    else
                        betTypeValues.add(leagueName);
                }

                String descriptionOdds = "";
                /*if( Util.isPopulated(myBet.getOutValue())  ) {
                    String outValue = odd.getOutValue();
                    descriptionOdds = MyApplication.getInstance().getAllMessages().get(outValue);

                    if( odd.getOutValue() != null && odd.getOutBetLine() != null )
                        descriptionOdds += " " + odd.getOutBetLine();
                }
                else {
                    descriptionOdds = odd.getTeamName();

                    if( Util.isPopulated( odd.getOutBetLine() ) ) {
                        descriptionOdds += " " + odd.getOutBetLine();
                    }
                }*/

                String gameTitle = "";
                String teamName = "";
                if (!Util.isPopulated(myBet.getTeamName())) {
                    Team team = Util.getTeam((int) myBet.getSportID(), (int) myBet.getTeamId());
                    if (team != null) teamName = team.getName();
                } else
                    teamName = myBet.getTeamName();

                if (Util.isPopulated(myBet.getTypeName2ndHalf())) {
                    if (!Util.isPopulated(myBet.getTypeLangValue()))
                        gameTitle = String.format("%s %s (%s)", teamName, myBet.getTypeName2ndHalf(), Util.formatOdds(context, myBet.getBetOdd()));
                    else
                        gameTitle = String.format("%s %s (%s)", Util.getString(myBet.getTypeLangValue()), myBet.getTypeName2ndHalf(), Util.formatOdds(context, myBet.getBetOdd()));

                } else {
                    gameTitle = String.format("%s (%s)", teamName, Util.formatOdds(context, myBet.getBetOdd()));
                }

                String teamVsTeam = game == null ? "" : Util.formatHomeVsAway(game, context);

                gameNames.add(gameTitle);
                teamVsTeamValues.add(teamVsTeam);
                teamAndPointValues.add("");
                detailEntries.add("");
            } else {
                double totalOdds = 1.0;
                String betOdds = "";

                if (parlay.getTeaserPoints() > 0 && Math.abs(parlay.getTeaserOdd()) > 0.0) {
                    totalOdds = Util.convertUSOddsToDecimalOdds(parlay.getTeaserOdd());
                    betOdds = Util.formatOdds(context, parlay.getTeaserOdd());

                    String points = null;
                    if (parlay.getTeaserPoints() == ((int) parlay.getTeaserPoints()))
                        points = "" + (long) parlay.getTeaserPoints();
                    else
                        points = String.format("%.01f", parlay.getTeaserPoints());

                    MyBet myBet = parlay.getMyBets().get(0);
                    Sport sport = Util.getSport(myBet.getSportID());

                    teamVsTeamValues.add(String.format("%s, %d %s", sport.getSportName(), parlay.getMyBets().size(), Util.getString(Constants.MSG_KEY_APP_ENTRIES)));
                    parlay.setIcon(sport.getScaledIcon());
                    teamAndPointValues.add(String.format("%d %s, %s %s %s", parlay.getMyBets().size(), Util.getString(Constants.MSG_KEY_TEASER_TEAM), points,
                            Util.getString(Constants.MSG_KEY_TEASER_PTS), betOdds));
                    gameNames.add(Util.getString(Constants.MSG_KEY_INPLAYTEASER));

                    dateAndTimeValues.add("");
                    betTypeValues.add("");
                    detailEntries.add("");
                } else {
                    for (Object bet : parlay.getMyBets())
                        totalOdds *= Util.convertUSOddsToDecimalOdds(((MyBet) bet).getBetOdd());

                    betOdds = Util.formatDecimalOdds(context, totalOdds);
                    gameNames.add(Util.getString(Constants.MSG_KEY_INPLAYPARLAY));
                    teamVsTeamValues.add(String.format("%d %s", parlay.getMyBets().size(), Util.getString(Constants.MSG_KEY_APP_ENTRIES)));
                    parlay.setIcon(parlayCardIcon);
                    teamAndPointValues.add("");
                    dateAndTimeValues.add("");
                    betTypeValues.add("");
                    detailEntries.add("");
                }

                double toWin = parlay.getAmount() * totalOdds;
                if (Settings.isWinAmount(context))
                    toWin -= parlay.getAmount();
                wagerAmounts.add(String.format("%s (%s) %s %s", Util.formatMoney(parlay.getAmount(), false), betOdds, Util.getString(Constants.MSG_KEY_APP_TOWIN), Util.formatMoney(toWin, false)));

            }

        }
    }
}
