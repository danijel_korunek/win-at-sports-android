package com.winatsports.data.adapters;



import com.winatsports.R;

/**
 * Created by broda on 07/12/2015.
 */

public enum CustomTutorialEnum {

    FIRST_TUTORIAL_SCREEN(R.string.firstTutorialScreen, R.layout.tutorial_first_screen),
    SECOND_TUTORIAL_SCREEN(R.string.secondTutorialScreen, R.layout.tutorial_second_screen),
    THIRT_TUTORIAL_SCREEN(R.string.thirdTutorialScreen, R.layout.tutorial_third_screen),
    FOURTH_TUTORIAL_SCREEN(R.string.fourthTutorialScreen, R.layout.tutorial_fourth_screen),
    FIFTH_TUTORIAL_SCREEN(R.string.fifthTutorialScreen, R.layout.tutorial_fifth_screen);

    private int mTitleResId;
    private int mLayoutResId;

    CustomTutorialEnum(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;


    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

}