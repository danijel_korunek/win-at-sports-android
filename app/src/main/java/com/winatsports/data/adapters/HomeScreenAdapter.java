package com.winatsports.data.adapters;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.activities.GamesActivity;
import com.winatsports.data.firstpage.FirstPageGameSection;
import com.winatsports.data.elements.Game;
import com.winatsports.data.elements.League;
import com.winatsports.data.elements.Sport;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * Created by broda on 10/12/2015.
 */



public class HomeScreenAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private Context context;

    //The variable that will hold our text data to be tied to list.
    private String headerTitle;
    private ArrayList<Game> games;
    private ArrayList<String> teams;
    private ArrayList<String> dateAndTimeArrayList;

    private Map<Integer, Sport> sportLinkedMap;
    private Map<Integer, League> leagueLinkedMap;

    public HomeScreenAdapter(Context context) {
        this.context = context;
        mInflater = LayoutInflater.from(context);

        games = new ArrayList<Game>();
        teams = new ArrayList<String>();
        dateAndTimeArrayList = new ArrayList<String>();
        sportLinkedMap = new LinkedHashMap<Integer, Sport>();
        leagueLinkedMap = new LinkedHashMap<Integer, League>();
    }

    public String getHeaderTitle() {
        return headerTitle;
    }

    @Override
    public int getCount() {
        return games.size();
    }

    @Override
    public Object getItem(int position) {
        return games.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder
    {
        ImageView ivSportIcon;
        TextView tvLeagueName;
        TextView tvDateAndTime;
        TextView tvSportName;
        TextView tvTeamName;
    }

    //A view to hold each row in the list
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        // A ViewHolder keeps references to children views to avoid unneccessary calls
        // to findViewById() on each row.
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listrow_first_page_games, null);

            holder = new ViewHolder();
            holder.ivSportIcon = (ImageView) convertView.findViewById(R.id.ivSportIcon);
            holder.tvLeagueName = (TextView) convertView.findViewById(R.id.tvLeagueName);
            holder.tvDateAndTime = (TextView) convertView.findViewById(R.id.tvDateAndTime);
            holder.tvSportName = (TextView) convertView.findViewById(R.id.tvSportName);
            holder.tvTeamName = (TextView) convertView.findViewById(R.id.tvTeamName);

            holder.tvDateAndTime.setAlpha(0.7f);
            holder.tvSportName.setAlpha(0.7f);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position % 2 == 0)
            convertView.setBackgroundColor(context.getResources().getColor(R.color.list_black_row));
        else
            convertView.setBackgroundColor(context.getResources().getColor(R.color.list_gray_row));
        Game game =  games.get( position );
        League league = leagueLinkedMap.get( game.getLeagueID() );
        Sport sport = sportLinkedMap.get(game.getSportID());

        holder.ivSportIcon.setImageBitmap(sport.getScaledIcon());
        holder.tvLeagueName.setText(league.getLeagueName());
        holder.tvDateAndTime.setText(dateAndTimeArrayList.get(position));
        holder.tvSportName.setText(sport.getSportName());
        holder.tvTeamName.setText(teams.get(position));

        return convertView;
    }

    private void setUpDateAndTimeOnFirstPage( ArrayList<Game> games )  {
        // I need to display correctly time..
        // some specific format like Wednesday, 9 December 2015 at 20:45
        dateAndTimeArrayList.clear();
        for( int index = 0; index < games.size(); index++ ) {
            Game game = games.get(index);
            String dateAndTime = game.getStartDate();
            dateAndTimeArrayList.add(Util.convertDateWithTimezone(dateAndTime));
        }
    }

    private void sortGamesInArrayListByDate( ArrayList<Game>  games ) {

        if ( games.size()  > 0) {

            for (int index1 = 0; index1 <games.size(); index1++) {

                Collections.sort(games, new Comparator<Game>() {
                    @Override
                    public int compare(Game lhs, Game rhs) {

                        /* Before I had written like this, and sometimes I get this error ==>
                        Date format error:  java.text.ParseException: Unparseable date: "2016-01-28T17:10" (at offset 16)
                        String dateAndTime = game.getStartDate().replace(" ", "T");
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                        Then I replace this lines of code, with codes bellow */

                        String lDateString = lhs.getStartDate();
                        String rDateString = rhs.getStartDate();

                        SimpleDateFormat format = new SimpleDateFormat("EEEE, MMMM d, yyyy 'at' HH:mm");
                        try {
                            Date lDate = format.parse(lDateString);
                            Date rDate = format.parse(rDateString);

                            return lDate.compareTo(rDate);

                        } catch (ParseException e) {
                            Log.e(GamesActivity.class.getSimpleName(), "Date format error: ", e);
                            return -1;
                        }
                    }
                });
            }
        }
    }

    public void prepareData(FirstPageGameSection gameSection) {
        // TODO: First i need to sort here this games which are in  gameSection.getEventIds() arrayList
        // so that later I can add them to arrayList ==> games

        for( int index1 = 0; index1 < gameSection.getEventIds().size(); index1++ ) {

            int eventID = gameSection.getEventIds().get(index1);
            for( int index2 = 0; index2 < MyApplication.getInstance().getAllGames().size(); index2++ ) {
                Game game = MyApplication.getInstance().getAllGames().get(index2);
                if( game.getEventID() == eventID ) {

                    games.add(game);

                    for (int index3 = 0; index3 < MyApplication.getInstance().getAllLeagues().size(); index3++) {
                        League league = MyApplication.getInstance().getAllLeagues().get(index3);
                        if (game.getLeagueID() == league.getLeagueID()) {
                            leagueLinkedMap.put(game.getLeagueID(), league);
                            break;
                        }
                    }

                    for (int index4 = 0; index4 < MyApplication.getInstance().getAllSports().size(); index4++) {
                        Sport sport = MyApplication.getInstance().getAllSports().get(index4);
                        if (sport.getSportID() == game.getSportID()) {
                            sportLinkedMap.put(game.getSportID(), sport);
                            break;
                        }
                    }

                    break;
                }

            }
        }

        // after I have added all games to arrayList, then I want to sort them
        sortGamesInArrayListByDate( games );

        // after that I'm looking for team name, based on teamID
        // First I'm trought all games that I need to display on "Home Screen"
        for( int index1=0; index1< games.size(); index1++ ) {

            Game game = games.get(index1);
            String teamName = Util.formatHomeVsAway(game, context);
            if( !teamName.isEmpty() )
                teams.add(teamName);
            else
                teams.add("");
        }

        setUpDateAndTimeOnFirstPage( games );

        headerTitle = MyApplication.getInstance().getAllMessages().get( gameSection.getTitleId() );
    }

}