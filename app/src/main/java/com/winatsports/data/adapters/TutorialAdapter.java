package com.winatsports.data.adapters;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.activities.GuestActivity;
import com.winatsports.activities.MainActivity;
import com.winatsports.data.Constants;
import com.winatsports.settings.Settings;

import java.util.Iterator;


/**
 * Created by broda on 07/12/2015.
 */


public class TutorialAdapter extends PagerAdapter {

    private Context mContext;

    // all string variables, textViews and buttons on tutorial FIRST SCREEN
    TextView tvTutorialTitle, tvVegasSportsBook, tvBetOnAllMajorSports;
    Button btSkipTutorialScreen1;
    String getTutorialTitle = "", getVegasSportsBook = "", getBetOnAllMajorSports = "";
    // all string variables, textViews and buttons on tutorial SECOND SCREEN
    TextView tvSwipeLeftOrRight, tvTutorialScreen2Text2;
    Button btSkipTutorialScreen2;
    String getSwipeLeftOrRight = "", getTutorialScreen2Text2 = "";
    // all string variables, textViews and buttons on tutorial THIRT SCREEN
    TextView tvTutorialScreen3Text1, tvTutorialScreen3Text2;
    Button btSkipTutorialScreen3;
    String getTutorialScreen3Text1 = "", getTutorialScreen3Text2 = "";
    // all string variables, textViews and buttons on tutorial FOURTH SCREEN
    TextView tvTutorialScreen4Text1, tvTutorialScreen4Text2;
    Button btSkipTutorialScreen4;
    String getTutorialScreen4Text1 = "", getTutorialScreen4Text2 = "";

    private boolean fromProfile;

    public TutorialAdapter(Context context, boolean startedFromProfile) {
        mContext = context;

        fromProfile = startedFromProfile;
        // I will use this counter only to break loop (For, do-while, while), so that application will work faster
        int counterOnlyToBreakLoop = 0;

        Iterator myVeryOwnIterator = MyApplication.getInstance().getAllMessages().keySet().iterator();
        while ( myVeryOwnIterator.hasNext() ) {

            String key = (String) myVeryOwnIterator.next();
            String value = MyApplication.getInstance().getAllMessages().get(key);

            if ( key.equals(Constants.MSG_KEY_APPTUTORIALP1_1)) {
                getTutorialTitle = value;
                counterOnlyToBreakLoop++;
            }
            else if( key.equals(Constants.MSG_KEY_APPTUTORIALP1_2)) {
                getVegasSportsBook = value;
                counterOnlyToBreakLoop++;
            }
            else if( key.equals(Constants.MSG_KEY_APPTUTORIALP1_3) ) {
                getBetOnAllMajorSports = value;
                counterOnlyToBreakLoop++;
            }
            else if ( key.equals(Constants.MSG_KEY_APPTUTORIALP2_1 )) {
                getSwipeLeftOrRight = value;
                counterOnlyToBreakLoop++;
            }
            else if ( key.equals(Constants.MSG_KEY_APPTUTORIALP2_2)) {
                getTutorialScreen2Text2 = value;
                counterOnlyToBreakLoop++;
            }
            else if( key.equals(Constants.MSG_KEY_APPTUTORIALP3_1)) {
                getTutorialScreen3Text1 = value;
                counterOnlyToBreakLoop++;
            }
            else if( key.equals(Constants.MSG_KEY_APPTUTORIALP3_2)) {
                getTutorialScreen3Text2 = value;
                counterOnlyToBreakLoop++;
            }
            else if( key.equals(Constants.MSG_KEY_APPTUTORIALP4_1)) {
                getTutorialScreen4Text1 = value;
                counterOnlyToBreakLoop++;
            }
            else if( key.equals(Constants.MSG_KEY_APPTUTORIALP4_2)) {
                getTutorialScreen4Text2 = value;
                counterOnlyToBreakLoop++;
            }

            else if( counterOnlyToBreakLoop == 9 )
                break;

        }

    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {

        CustomTutorialEnum customTutorialEnum = CustomTutorialEnum.values()[position];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(customTutorialEnum.getLayoutResId(), collection, false);
        collection.addView(layout);

        if (position == 0) {

            tvTutorialTitle = (TextView) layout.findViewById(R.id.tvTutorialTitle);
            tvVegasSportsBook = (TextView) layout.findViewById(R.id.tvVegasSportsBook);
            tvBetOnAllMajorSports = (TextView) layout.findViewById(R.id.tvBetOnAllMajorSports);

            tvTutorialTitle.setText(getTutorialTitle);
            tvVegasSportsBook.setText(getVegasSportsBook);
            tvBetOnAllMajorSports.setText(getBetOnAllMajorSports);

            btSkipTutorialScreen1 = (Button) layout.findViewById(R.id.btSkipTutorialScreen1);
            btSkipTutorialScreen1.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SKIP));
            btSkipTutorialScreen1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    skip();
                }
            });
        } else if (position == 1) {

            tvSwipeLeftOrRight = (TextView) layout.findViewById(R.id.tvSwipeLeftOrRight);
            tvTutorialScreen2Text2 = (TextView) layout.findViewById(R.id.tvTutorialScreen2Text2);

            tvSwipeLeftOrRight.setText(getSwipeLeftOrRight);
            tvTutorialScreen2Text2.setText(getTutorialScreen2Text2);

            btSkipTutorialScreen2 = (Button) layout.findViewById(R.id.btSkipTutorialScreen2);
            btSkipTutorialScreen2.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SKIP));
            btSkipTutorialScreen2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    skip();
                }
            });
        } else if (position == 2) {

            tvTutorialScreen3Text1 = (TextView) layout.findViewById(R.id.tvTutorialScreen3Text1);
            tvTutorialScreen3Text2 = (TextView) layout.findViewById(R.id.tvTutorialScreen3Text2);

            tvTutorialScreen3Text1.setText(getTutorialScreen3Text1);
            tvTutorialScreen3Text2.setText(getTutorialScreen3Text2);

            btSkipTutorialScreen3 = (Button) layout.findViewById(R.id.btSkipTutorialScreen3);
            btSkipTutorialScreen3.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SKIP));
            btSkipTutorialScreen3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    skip();
                }
            });
        } else if (position == 3) {

            tvTutorialScreen4Text1 = (TextView) layout.findViewById(R.id.tvTutorialScreen4Text1);
            tvTutorialScreen4Text2 = (TextView) layout.findViewById(R.id.tvTutorialScreen4Text2);

            tvTutorialScreen4Text1.setText(getTutorialScreen4Text1);
            tvTutorialScreen4Text2.setText(getTutorialScreen4Text2);

            btSkipTutorialScreen4 = (Button) layout.findViewById(R.id.btSkipTutorialScreen4);
            btSkipTutorialScreen4.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SKIP));
            btSkipTutorialScreen4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    skip();
                }
            });
        }

        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return CustomTutorialEnum.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        CustomTutorialEnum customTutorialEnum = CustomTutorialEnum.values()[position];
        return mContext.getString(customTutorialEnum.getTitleResId());
    }

    private void skip() {
        Intent nextActivityIntent = null;
        if (fromProfile)
            nextActivityIntent = new Intent(mContext, MainActivity.class);
        else if (Settings.getUserR(mContext).equals(Settings.DEFAULT_USER_R))
            nextActivityIntent = new Intent(mContext, GuestActivity.class);
        else
            nextActivityIntent = new Intent(mContext, MainActivity.class);
        mContext.startActivity(nextActivityIntent);
        ((Activity) mContext).finish();
        ((Activity) mContext).overridePendingTransition(R.anim.end_activity_slide_in_left, R.anim.end_activity_slide_out_right);
    }
}
