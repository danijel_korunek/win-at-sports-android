package com.winatsports.data.adapters;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.data.SupportLang;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dkorunek on 15/02/16.
 */
public class LanguageAdapter extends BaseAdapter {
    private ArrayList<Language> languages;
    private LayoutInflater inflater;

    private class ViewHolder {
        TextView tvTitle;
        ImageView ivCheckMark;
    }

    public class Language {
        public SupportLang lang;
        public boolean selected;
    }

    public LanguageAdapter(ArrayList<SupportLang> languages) {
        this.languages = new ArrayList<Language>();
        for (SupportLang lang : languages) {
            Language newEntry = new Language();
            newEntry.lang = lang;
            newEntry.selected = false;
            this.languages.add(newEntry);
        }

        inflater = LayoutInflater.from(MyApplication.getInstance());
    }

    @Override
    public int getCount() {
        return languages.size();
    }

    @Override
    public Object getItem(int position) {
        return languages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listrow_language_view, null);

            holder = new ViewHolder();
            holder.ivCheckMark = (ImageView) convertView.findViewById(R.id.ivCheckMark);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Language l = (Language) getItem(position);

        holder.tvTitle.setText(l.lang.getLangName());
        Drawable checkMark = MyApplication.getInstance().getApplicationContext()
                .getResources().getDrawable(R.drawable.glyph_check_mark);
        holder.ivCheckMark.setImageDrawable(l.selected ? checkMark : null);

        return convertView;
    }

    public void setSelected(int position) {
        for (int i = 0; i < languages.size(); i++) {
            Language lang = languages.get(i);
            if (i == position)
                lang.selected = true;
            else
                lang.selected = false;
        }
    }

    public List<Language> getItems() {
        return languages;
    }
}
