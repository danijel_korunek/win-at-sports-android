package com.winatsports.data.adapters;


/**
 * Created by broda on 17/11/2015.
 */


import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.winatsports.R;
import com.winatsports.data.elements.League;

import java.util.ArrayList;

public class LeagueAdapter extends BaseAdapter {

    private LayoutInflater mInflater;

    //The variable that will hold our text data to be tied to list.
    private ArrayList<League> data;
    private Bitmap picture;

    public LeagueAdapter(Context context, ArrayList<League> items, Bitmap picture) {
        mInflater = LayoutInflater.from( context );


        this.data = items;
        this.picture = picture;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder
    {
        TextView text;
        ImageView img;
    }

    //A view to hold each row in the list
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // A ViewHolder keeps references to children views to avoid unneccessary calls
        // to findViewById() on each row.
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listrow_leagues, null);

            holder = new ViewHolder();
            holder.img = (ImageView) convertView.findViewById(R.id.ivSportIcon);
            holder.text = (TextView) convertView.findViewById(R.id.tvLeagueName);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.img.setImageBitmap(picture);
        holder.text.setText( data.get(position).getLeagueName() );

        if( data.get(position).getGameArrayList().size() <= 0 )
            convertView.setAlpha(0.4f);
        else
            convertView.setAlpha(1.0f);

        if (position % 2 == 0)
            convertView.setBackgroundColor(mInflater.getContext().getResources().getColor(R.color.list_black_row));
        else
            convertView.setBackgroundColor(mInflater.getContext().getResources().getColor(R.color.list_gray_row));

        return convertView;
    }

}