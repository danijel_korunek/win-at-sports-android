package com.winatsports.data.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.winatsports.R;

/**
 * Created by dkorunek on 18/08/16.
 */
public class MainPagerAdapter extends PagerAdapter {
    private Context mContext;
    private final int ITEMS_NUMBER = 6;

    public MainPagerAdapter(Context context) {
        mContext = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = null;

        switch (position) {
            case 0:
                layout = (ViewGroup) inflater.inflate(R.layout.vp_main, collection, false);
                break;
            case 1:
                layout = (ViewGroup) inflater.inflate(R.layout.vp_sports, collection, false);
                break;
            case 2:
                layout = (ViewGroup) inflater.inflate(R.layout.vp_wagers_in_play, collection, false);
                break;
            case 3:
                layout = (ViewGroup) inflater.inflate(R.layout.vp_livescores, collection, false);
                break;
            case 4:
                layout = (ViewGroup) inflater.inflate(R.layout.vp_search, collection, false);
                break;
            default:
                layout = (ViewGroup) inflater.inflate(R.layout.vp_profile, collection, false);
                break;
        }

        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return ITEMS_NUMBER;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

}
