package com.winatsports.data.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.data.MyBet;
import com.winatsports.data.MyParlay;
import com.winatsports.data.elements.Game;
import com.winatsports.data.elements.League;
import com.winatsports.data.elements.Sport;
import com.winatsports.data.livescores.LiveEventScore;
import com.winatsports.data.livescores.Period;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class LiveScoresActiveGamesAdapter extends BaseAdapter {

    private LayoutInflater mInflater;

    private ArrayList<DataHolder> dataToShow;

    public LiveScoresActiveGamesAdapter(final Context context, ArrayList<LiveEventScore> liveScores) {

        mInflater = LayoutInflater.from(context);

        dataToShow = new ArrayList<DataHolder>();
        for (LiveEventScore score : liveScores) {
            DataHolder dataHolder = new DataHolder();
            dataHolder.score = score;
            dataHolder.game = Util.getGame(score.getEventID());
            dataHolder.league = Util.getLeague(score.getLeagueID());
            dataHolder.sport = Util.getSport(score.getSportID());
            dataToShow.add(dataHolder);
        }

    }

    @Override
    public int getCount() {
        return dataToShow.size();
    }

    @Override
    public Object getItem(int position) {
        return dataToShow.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder
    {
        ImageView ivSportIcon;
        TextView tvTeamsPlay;
        TextView tvLeagueName;
        TextView tvDateTime;
        TextView tvEndScore;
        TextView tvScoreBySection;
    }

    private class DataHolder {
        Sport sport;
        Game game;
        League league;
        LiveEventScore score;
    }

    //A view to hold each row in the list
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // A ViewHolder keeps references to children views to avoid unneccessary calls
        // to findViewById() on each row.
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listrow_livescores, null);

            holder = new ViewHolder();
            holder.ivSportIcon = (ImageView) convertView.findViewById(R.id.ivSportIcon);
            holder.tvTeamsPlay = (TextView) convertView.findViewById(R.id.tvTeamsPlay);
            holder.tvLeagueName = (TextView) convertView.findViewById(R.id.tvLeagueName);
            holder.tvDateTime = (TextView) convertView.findViewById(R.id.tvDateAndTime);
            holder.tvEndScore = (TextView) convertView.findViewById(R.id.tvEndScore);
            holder.tvScoreBySection = (TextView) convertView.findViewById(R.id.tvScoreBySection);

            holder.tvLeagueName.setAlpha(0.7f);
            holder.tvDateTime.setAlpha(0.7f);
            holder.tvScoreBySection.setAlpha(0.7f);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        DataHolder dataHolder = dataToShow.get(position);
        holder.ivSportIcon.setImageBitmap(dataHolder.sport.getScaledIcon());
        holder.tvTeamsPlay.setText(Util.formatHomeVsAway(dataHolder.game, MyApplication.getInstance()));
        holder.tvDateTime.setText(Util.getOnlyDayAndTime(dataHolder.game.getStartDate()));
        holder.tvLeagueName.setText(dataHolder.league.getLeagueName());

        String a, b, periodDesc = null;
        a = dataHolder.score.getCurrentLeft();
        b = dataHolder.score.getCurrentRight();
        if (Util.isPopulated(a) && Util.isPopulated(b)) periodDesc = "" + a + " - " + b;
        if (Util.isPopulated(a) && !Util.isPopulated(b)) periodDesc = a;
        if (!Util.isPopulated(a) && Util.isPopulated(b)) periodDesc = b;

        if (Util.isPopulated(periodDesc)) {
            if (Util.canSwapHomeAndAway(dataHolder.score.getSportID()))
                holder.tvEndScore.setText(periodDesc + " " + dataHolder.score.getAwayScore() + " : " + dataHolder.score.getHomeScore());
            else
                holder.tvEndScore.setText(periodDesc + " " + dataHolder.score.getHomeScore() + " : " + dataHolder.score.getAwayScore());
        } else {
            if (Util.canSwapHomeAndAway(dataHolder.score.getSportID()))
                holder.tvEndScore.setText("" + dataHolder.score.getAwayScore() + " : " + dataHolder.score.getHomeScore());
            else
                holder.tvEndScore.setText("" + dataHolder.score.getHomeScore() + " : " + dataHolder.score.getAwayScore());
        }

        StringBuffer sbSectionText = new StringBuffer();
        if (Util.isPopulated(dataHolder.score.getExtraEventInfo1()))
            sbSectionText.append(" * " + dataHolder.score.getExtraEventInfo1());
        if (Util.isPopulated(dataHolder.score.getExtraEventInfo2()))
            sbSectionText.append(" * " + dataHolder.score.getExtraEventInfo2());
        if (dataHolder.score.getPeriods() != null && dataHolder.score.getPeriods().size() > 0) {
            for (Period period : dataHolder.score.getPeriods()) {
                if (Util.canSwapHomeAndAway(dataHolder.score.getSportID())) {
                    sbSectionText.append(period.getPeriodName() + " " + period.getAwayScore() + " : " + period.getHomeScore() + " ");
                } else {
                    sbSectionText.append(period.getPeriodName() + " " + period.getHomeScore() + " : " + period.getAwayScore() + " ");
                }
            }
        }
        holder.tvScoreBySection.setText(sbSectionText.toString() );

        if (position % 2 == 0)
            convertView.setBackgroundColor(mInflater.getContext().getResources().getColor(R.color.list_black_row));
        else
            convertView.setBackgroundColor(mInflater.getContext().getResources().getColor(R.color.list_gray_row));

        return convertView;

    }

    public static ArrayList<LiveEventScore> filterLiveScores(boolean inPlay) {
        ArrayList<LiveEventScore> scores = new ArrayList<>();

        for (LiveEventScore score : MyApplication.getInstance().getAllActiveLiveScores()) {
            if (inPlay) {
                boolean present = false;
                for (MyParlay parlay : MyApplication.getInstance().getUserData().getUserBets()) {
                    for (MyBet bet : parlay.getMyBets()) {
                        if (bet.getEventID() == score.getEventID()) {
                            present = true;
                            break;
                        }
                    }

                    if (present) {
                        scores.add(score);
                    }
                }
            } else {
                scores.add(score);
            }
        }

        return scores;
    }

}