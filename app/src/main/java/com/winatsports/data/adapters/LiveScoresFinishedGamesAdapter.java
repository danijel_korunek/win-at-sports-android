package com.winatsports.data.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.data.Constants;
import com.winatsports.data.elements.League;
import com.winatsports.data.elements.Sport;
import com.winatsports.data.elements.Team;
import com.winatsports.data.livescores.LiveEventScore;
import com.winatsports.data.livescores.Period;
import com.winatsports.settings.Settings;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

public class LiveScoresFinishedGamesAdapter extends BaseAdapter {

    private LayoutInflater mInflater;

    private ArrayList<DataHolder> dataToShow;

    public LiveScoresFinishedGamesAdapter(Context context, ArrayList<LiveEventScore> finishedLiveScores) {

        mInflater = LayoutInflater.from(context);

        dataToShow = new ArrayList<DataHolder>();

        for (LiveEventScore score : finishedLiveScores) {
            DataHolder dataHolder = new DataHolder();
            dataHolder.league = Util.getLeague(score.getLeagueID());
            dataHolder.sport = Util.getSport(score.getSportID());
            dataHolder.homeTeam = score.getHomeTeam();
            if (dataHolder.homeTeam == null)
                dataHolder.homeTeam = Util.getTeam(score.getSportID(), score.getHomeTeamID());

            dataHolder.awayTeam = score.getAwayTeam();
            if (dataHolder.awayTeam == null)
                dataHolder.awayTeam = Util.getTeam(score.getSportID(), score.getAwayTeamID());
            dataHolder.score = score;

            dataToShow.add(dataHolder);
        }

        Collections.sort(dataToShow, new Comparator<DataHolder>() {
            @Override
            public int compare(DataHolder o1, DataHolder o2) {
                try {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date d1 = df.parse(o1.score.getStartDate());
                    Date d2 = df.parse(o2.score.getStartDate());

                    return d1.before(d2) ? 1 : -1;

                } catch (Exception e) {
                    return 0;
                }

            }
        });


    }

    @Override
    public int getCount() {
        return dataToShow.size();
    }

    @Override
    public Object getItem(int position) {
        return dataToShow.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder
    {
        ImageView ivSportIcon;
        TextView tvTeamsPlay;
        TextView tvLeagueName;
        TextView tvDateTime;
        TextView tvEndScore;
        TextView tvScoreBySection;
    }

    private class DataHolder {
        Sport sport;
        Team homeTeam, awayTeam;
        League league;
        LiveEventScore score;
    }
    //A view to hold each row in the list
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // A ViewHolder keeps references to children views to avoid unneccessary calls
        // to findViewById() on each row.
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listrow_livescores, null);

            holder = new ViewHolder();
            holder.ivSportIcon = (ImageView) convertView.findViewById(R.id.ivSportIcon);
            holder.tvTeamsPlay = (TextView) convertView.findViewById(R.id.tvTeamsPlay);
            holder.tvLeagueName = (TextView) convertView.findViewById(R.id.tvLeagueName);
            holder.tvDateTime = (TextView) convertView.findViewById(R.id.tvDateAndTime);
            holder.tvEndScore = (TextView) convertView.findViewById(R.id.tvEndScore);
            holder.tvScoreBySection = (TextView) convertView.findViewById(R.id.tvScoreBySection);

            holder.tvLeagueName.setAlpha(0.7f);
            holder.tvDateTime.setAlpha(0.7f);
            holder.tvScoreBySection.setAlpha(0.7f);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        DataHolder dataHolder = dataToShow.get(position);
        holder.ivSportIcon.setImageBitmap(dataHolder.sport.getScaledIcon());

        String teamVsTeam = "";

        try {
            if (Util.canSwapHomeAndAway(dataHolder.sport.getSportID())) {
                int setting = Settings.getTeamOrder(MyApplication.getInstance());

                if (setting == Constants.C_SETTINGS_AWAY_VS_HOME) {
                    teamVsTeam = dataHolder.awayTeam.getName() + " " + Util.getString(Constants.MSG_KEY_APP_VS) + " " + dataHolder.homeTeam.getName();
                } else {
                    teamVsTeam = dataHolder.homeTeam.getName() + " " + Util.getString(Constants.MSG_KEY_APP_VS) + " " + dataHolder.awayTeam.getName();
                }
            } else {
                teamVsTeam = dataHolder.homeTeam.getName() + " " + Util.getString(Constants.MSG_KEY_APP_VS) + " " + dataHolder.awayTeam.getName();
            }
        } catch (Exception e) {
            /*Log.e(LiveScoresFinishedGamesAdapter.class.getSimpleName(), "No team names for team id: " + dataHolder.score.getHomeTeamID()
                    + " away team id: " + dataHolder.score.getAwayTeamID()
                    + " game event id: " + dataHolder.score.getEventID(), e);*/
            teamVsTeam = Util.getString(Constants.MSG_KEY_TEASER_TEAM) + " " + Util.getString(Constants.MSG_KEY_APP_VS) + " " + Util.getString(Constants.MSG_KEY_TEASER_TEAM);
        }

        holder.tvTeamsPlay.setText(teamVsTeam);
        holder.tvLeagueName.setText(dataHolder.league.getLeagueName());
        holder.tvDateTime.setText(Util.getOnlyDayAndTime(Util.formatDate(dataHolder.score.getStartDate())));

        String a, b, periodDesc = null;
        a = dataHolder.score.getCurrentLeft();
        b = dataHolder.score.getCurrentRight();
        if (Util.isPopulated(a) && Util.isPopulated(b)) periodDesc = "" + a + " - " + b;
        if (Util.isPopulated(a) && !Util.isPopulated(b)) periodDesc = a;
        if (!Util.isPopulated(a) && Util.isPopulated(b)) periodDesc = b;

        if (Util.isPopulated(periodDesc)) {
            if (Util.canSwapHomeAndAway(dataHolder.score.getSportID()))
                holder.tvEndScore.setText(periodDesc + " " + dataHolder.score.getAwayScore() + " : " + dataHolder.score.getHomeScore());
            else
                holder.tvEndScore.setText(periodDesc + " " + dataHolder.score.getHomeScore() + " : " + dataHolder.score.getAwayScore());
        } else {
            if (Util.canSwapHomeAndAway(dataHolder.score.getSportID()))
                holder.tvEndScore.setText("" + dataHolder.score.getAwayScore() + " : " + dataHolder.score.getHomeScore());
            else
                holder.tvEndScore.setText("" + dataHolder.score.getHomeScore() + " : " + dataHolder.score.getAwayScore());
        }

        StringBuffer sbSectionText = new StringBuffer();
        if (Util.isPopulated(dataHolder.score.getExtraEventInfo1()))
            sbSectionText.append(" * " + dataHolder.score.getExtraEventInfo1());
        if (Util.isPopulated(dataHolder.score.getExtraEventInfo2()))
            sbSectionText.append(" * " + dataHolder.score.getExtraEventInfo2());
        if (dataHolder.score.getPeriods() != null && dataHolder.score.getPeriods().size() > 0) {
            for (Period period : dataHolder.score.getPeriods()) {
                if (Util.canSwapHomeAndAway(dataHolder.score.getSportID())) {
                    sbSectionText.append(period.getPeriodName() + " " + period.getAwayScore() + " : " + period.getHomeScore() + " ");
                } else {
                    sbSectionText.append(period.getPeriodName() + " " + period.getHomeScore() + " : " + period.getAwayScore() + " ");
                }
            }
        }
        holder.tvScoreBySection.setText(sbSectionText.toString() );

        if (position % 2 == 0)
            convertView.setBackgroundColor(mInflater.getContext().getResources().getColor(R.color.list_black_row));
        else
            convertView.setBackgroundColor(mInflater.getContext().getResources().getColor(R.color.list_gray_row));

        return convertView;

    }

}