package com.winatsports.data.adapters;


import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.data.elements.League;
import com.winatsports.data.elements.Sport;

import java.util.ArrayList;

/**
 * Created by broda on 14/01/2016.
 */


public class SearchLeaguesAdapter extends BaseAdapter {

    private LayoutInflater mInflater;

    //The variable that will hold our text data to be tied to list.
    private ArrayList<League> leagueArrayList;

    public SearchLeaguesAdapter(Context context, ArrayList<League> leagueArrayList) {

        mInflater = LayoutInflater.from(context);
        this.leagueArrayList = leagueArrayList;
    }

    @Override
    public int getCount() {
        return leagueArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return leagueArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder
    {
        ImageView ivSearchLeagueIcon;
        TextView tvSearchLeagueName;
    }

    //A view to hold each row in the list
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // A ViewHolder keeps references to children views to avoid unneccessary calls
        // to findViewById() on each row.
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listrow_search_leagues, null);

            holder = new ViewHolder();
            holder.ivSearchLeagueIcon = (ImageView) convertView.findViewById(R.id.ivSearchLeagueIcon);
            holder.tvSearchLeagueName = (TextView) convertView.findViewById(R.id.tvSearchLeagueName);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        League league = leagueArrayList.get(position);

        Bitmap getCorrectIcon = null;
        for( int index1=0; index1<MyApplication.getInstance().getAllSports().size(); index1++ ) {

            Sport sport = MyApplication.getInstance().getAllSports().get(index1);
            if( league.getSportID() == sport.getSportID() ) {
                getCorrectIcon = sport.getScaledIcon();
                break;
            }
        }

        holder.ivSearchLeagueIcon.setImageBitmap(getCorrectIcon);
        holder.tvSearchLeagueName.setText( league.getLeagueName() );

        if( leagueArrayList.get(position).getGameArrayList().size() > 0 ) {
            convertView.setAlpha(1.0f);
            return convertView;
        }
        else {
            convertView.setAlpha(0.4f);
            return  convertView;
        }

    }




}