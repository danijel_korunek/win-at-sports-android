package com.winatsports.data.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.data.Constants;
import com.winatsports.data.MyBet;
import com.winatsports.data.MyParlay;
import com.winatsports.data.elements.League;
import com.winatsports.data.elements.Sport;
import com.winatsports.data.stats.StatsCollection;
import com.winatsports.settings.Settings;

import java.util.ArrayList;

/**
 * Created by dkorunek on 29/08/16.
 */
public class StatBestWinsAdapter extends BaseAdapter {

    private StatsCollection statsCollection;
    private LayoutInflater layoutInflater;

    private ArrayList<MyParlay> parlays;

    private Bitmap parlayCardIcon;

    private ArrayList<String> gameNames;
    private ArrayList<String> teamVsTeamValues;
    private ArrayList<String> teamAndPointValues;
    private ArrayList<String> dateAndTimeValues;
    private ArrayList<String> wagerAmounts;
    private ArrayList<String> betTypeValues;
    private ArrayList<String> detailEntries;

    private Context context;

    public StatBestWinsAdapter(Context context, ArrayList<MyParlay> parlays) {
        layoutInflater = LayoutInflater.from(context);
        this.parlays = parlays;
        this.context = context;

        gameNames = new ArrayList<String>();
        teamVsTeamValues = new ArrayList<String>();
        teamAndPointValues = new ArrayList<String>();
        dateAndTimeValues = new ArrayList<String>();
        wagerAmounts = new ArrayList<String>();
        betTypeValues = new ArrayList<String>();
        detailEntries = new ArrayList<String>();

        parlayCardIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.parlay);

        prepareData();
    }

    @Override
    public int getCount() {
        return parlays.size();
    }

    @Override
    public Object getItem(int position) {
        return parlays.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        ImageView ivIcon;
        TextView tvGameNamePlusWagerAmount;
        TextView tvTeamVsTeam;
        TextView tvTeamAndPoints;
        TextView tvDateAndTime;
        TextView tvWagerAmount;
        TextView tvBetType;
        TextView tvDetails;
    }


    //A view to hold each row in the list
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.listrow_wagers_parlay_with_records, null);

            holder = new ViewHolder();
            holder.ivIcon = (ImageView) convertView.findViewById(R.id.ivIcon);
            holder.tvGameNamePlusWagerAmount = (TextView) convertView.findViewById(R.id.tvGameNamePlusWagerAmount);
            holder.tvTeamVsTeam = (TextView) convertView.findViewById(R.id.tvTeamVsTeam);
            holder.tvTeamAndPoints = (TextView) convertView.findViewById(R.id.tvTeamAndPoints);
            holder.tvDateAndTime = (TextView) convertView.findViewById(R.id.tvDateAndTime);
            holder.tvWagerAmount = (TextView) convertView.findViewById(R.id.tvWagerAmount);
            holder.tvBetType = (TextView) convertView.findViewById(R.id.tvBetType);
            holder.tvDetails = (TextView) convertView.findViewById(R.id.tvDetails);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        // image filter is set only for parlay icon
        Bitmap iconToSet = parlays.get(position).getIcon();
        if (iconToSet != null && iconToSet.equals(parlayCardIcon)) {
            // alpha from 0 to 255 in integer value, float value from 0.0f to 1.0f
            // only if I set alpha = 50 or 210, it is the same
            /*int alpha = 210;
            Paint p = new Paint();

            ColorFilter filter = new LightingColorFilter(Color.WHITE, 0);
            p.setAlpha(alpha);
            p.setColorFilter(filter);

            holder.ivIcon.setColorFilter(filter);*/
            Log.d(WagerHistoryAdapter.class.getSimpleName(), "Setting color filter on bitmap");
        }

        holder.ivIcon.setImageBitmap(iconToSet);

        holder.tvGameNamePlusWagerAmount.setText(gameNames.get(position));
        holder.tvTeamVsTeam.setText(teamVsTeamValues.get(position));
        holder.tvTeamAndPoints.setText(teamAndPointValues.get(position));
        holder.tvDateAndTime.setText(dateAndTimeValues.get(position));
        holder.tvWagerAmount.setText(wagerAmounts.get(position));
        holder.tvBetType.setText(betTypeValues.get(position));
        holder.tvDetails.setText(detailEntries.get(position));

        if (holder.tvGameNamePlusWagerAmount.getText().length() == 0)
            holder.tvGameNamePlusWagerAmount.setVisibility(View.GONE);
        else
            holder.tvGameNamePlusWagerAmount.setVisibility(View.VISIBLE);

        if (holder.tvTeamVsTeam.getText().length() == 0)
            holder.tvTeamVsTeam.setVisibility(View.GONE);
        else
            holder.tvTeamVsTeam.setVisibility(View.VISIBLE);

        if (holder.tvDateAndTime.getText().length() == 0)
            holder.tvDateAndTime.setVisibility(View.GONE);
        else
            holder.tvDateAndTime.setVisibility(View.VISIBLE);

        if (holder.tvWagerAmount.getText().length() == 0)
            holder.tvWagerAmount.setVisibility(View.GONE);
        else
            holder.tvWagerAmount.setVisibility(View.VISIBLE);

        if (holder.tvBetType.getText().length() == 0)
            holder.tvBetType.setVisibility(View.GONE);
        else
            holder.tvBetType.setVisibility(View.VISIBLE);

        if (holder.tvDetails.getText().length() == 0)
            holder.tvDetails.setVisibility(View.GONE);
        else
            holder.tvDetails.setVisibility(View.VISIBLE);

        // text coloring
        MyParlay parlay = parlays.get(position);
        if (parlay.getBetResult() == 1)
            holder.tvWagerAmount.setTextColor(context.getResources().getColor(R.color.green));
        else if (parlay.getBetResult() == 2)
            holder.tvWagerAmount.setTextColor(context.getResources().getColor(R.color.red));
        else if (parlay.getBetResult() != 1 && parlay.getBetResult() != 2)
            holder.tvWagerAmount.setTextColor(context.getResources().getColor(R.color.yellow));
        else
            holder.tvWagerAmount.setTextColor(context.getResources().getColor(R.color.white));

        if (position % 2 == 0)
            convertView.setBackgroundColor(context.getResources().getColor(R.color.list_black_row));
        else
            convertView.setBackgroundColor(context.getResources().getColor(R.color.list_gray_row));

        return convertView;

    }

    public void prepareData() {
        for (MyParlay parlay : parlays) {
            double betWin = parlay.getBetWin();
            if (betWin == 0.0)
                betWin = parlay.getAmount();

            if (parlay.getMyBets().size() == 1) {
                MyBet myBet = parlay.getMyBets().get(0);

                League league = Util.getLeague(myBet.getLeagueID(), myBet.getLeagueType());
                if (league == null)
                    league = Util.getLeague(myBet.getLeagueID());

                String leagueName = league == null ? "" : league.getLeagueName();
                Sport sport = Util.getSport(myBet.getSportID());
                String betResult = Util.getBetResult(parlay);

                parlay.setIcon(sport.getScaledIcon());
                teamVsTeamValues.add(myBet.getEventDesc());
                dateAndTimeValues.add(Util.formatDate(myBet.getStartDate()));

                double toWin = parlay.getAmount() * Util.convertUSOddsToDecimalOdds(myBet.getBetOdd());
                if (parlay.getBetResult() == 1)
                    if (Settings.isWinAmount(context)) toWin -= parlay.getAmount();

                String wagerAmount = String.format("%s (%s), %s %s", Util.formatMoney(parlay.getAmount(), false),
                        Util.formatOdds(context, myBet.getBetOdd()),
                        betResult,
                        Util.formatMoney(toWin, false));
                wagerAmounts.add(wagerAmount);
                if (Util.isPopulated(myBet.getTypeName()))
                    betTypeValues.add(leagueName + ", " + myBet.getTypeName());
                else
                    betTypeValues.add(leagueName);

                String gameName = null;
                if (Util.isPopulated(myBet.getTypeName2ndHalf()))
                    gameName = String.format("%s %s (%s)", myBet.getTeamName(), myBet.getTypeName2ndHalf(), Util.formatOdds(context, myBet.getBetOdd()));
                else
                    gameName = String.format("%s (%s)", myBet.getTeamName(), Util.formatOdds(context, myBet.getBetOdd()));
                gameNames.add(gameName);

                teamAndPointValues.add("");
                if (Util.isPopulated(myBet.getTextCache()))
                    detailEntries.add(myBet.getTextCache());
                else
                    detailEntries.add(""); // TODO update this so that it immediately shows everything

            } else if (parlay.getMyBets().size() > 1) {
                String betOdds = "";

                if (parlay.getTeaserPoints() > 0 && Math.abs(parlay.getTeaserOdd()) > 0.0) {
                    betOdds = Util.formatOdds(context, parlay.getTeaserOdd());

                    String points = null;
                    if (parlay.getTeaserPoints() == ((int) parlay.getTeaserPoints()))
                        points = "" + (long) parlay.getTeaserPoints();
                    else
                        points = String.format("%.01f", parlay.getTeaserPoints());

                    MyBet myBet = parlay.getMyBets().get(0);
                    Sport sport = Util.getSport(myBet.getSportID());

                    teamVsTeamValues.add(String.format("%s, %d %s", sport.getSportName(), parlay.getMyBets().size(), Util.getString(Constants.MSG_KEY_APP_ENTRIES)));
                    parlay.setIcon(sport.getScaledIcon());
                    teamAndPointValues.add(String.format("%d %s, %s %s %s", parlay.getMyBets().size(), Util.getString(Constants.MSG_KEY_TEASER_TEAM), points,
                            Util.getString(Constants.MSG_KEY_TEASER_PTS), betOdds));
                    gameNames.add(Util.getString(Constants.MSG_KEY_INPLAYTEASER));

                    dateAndTimeValues.add("");
                    betTypeValues.add("");
                    detailEntries.add("");
                } else {
                    double totalOdds = 1.0;
                    for (Object bet : parlay.getMyBets())
                        totalOdds *= Util.convertUSOddsToDecimalOdds(((MyBet) bet).getBetOdd());

                    betOdds = Util.formatDecimalOdds(context, totalOdds);
                    gameNames.add(Util.getString(Constants.MSG_KEY_INPLAYPARLAY));
                    teamVsTeamValues.add(String.format("%d %s", parlay.getMyBets().size(), Util.getString(Constants.MSG_KEY_APP_ENTRIES)));
                    parlay.setIcon(parlayCardIcon);
                    teamAndPointValues.add("");
                    dateAndTimeValues.add("");
                    betTypeValues.add("");
                    detailEntries.add("");
                }

                String betResult = Util.getBetResult(parlay);
                double toWin = Math.abs(betWin);
                if (parlay.getBetResult() == 1) {
                    if (Settings.isWinAmount(context))
                        toWin -= parlay.getAmount();
                }
                wagerAmounts.add(String.format("%s (%s), %s %s", Util.formatMoney(parlay.getAmount(), false), betOdds, betResult, Util.formatMoney(toWin, false)));

            }

        }
    }
}
