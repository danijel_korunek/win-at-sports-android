package com.winatsports.data.adapters;


import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.data.Constants;
import com.winatsports.data.elements.Game;

import java.util.ArrayList;


/**
 * Created by broda on 18/11/2015.
 */



public class GamesAdapter extends BaseAdapter {

    private LayoutInflater mInflater;

    //The variable that will hold our text data to be tied to list.
    private Bitmap picture;
    private ArrayList<Game> games;
    ArrayList<String> timeOfGameTempArraylist;
    ArrayList<String>  teamTempArrayList;

    public GamesAdapter(Context context, Bitmap picture, ArrayList<Game> games) {
        // TODO Auto-generated constructor stub
        mInflater = LayoutInflater.from(context);
        this.picture = picture;
        this.games = games;

        timeOfGameTempArraylist = new ArrayList<String>();
        teamTempArrayList = new ArrayList<String>();

        String getVsInEnglishSpanishPortugieses = MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_VS);

        for( int index1=0; index1<games.size(); index1++ ) {

            Game game = games.get(index1);
            String[] split = game.getStartDate().split("at ");
            String toAdd = split[1];

            if (game.getGameDesc() != null)
                toAdd += ", " + game.getGameDesc();
            timeOfGameTempArraylist.add(toAdd);
        }

        // I need to go trought all games, and based on "TeamID", I need to find "TeamName"
        for( int index1=0; index1<games.size(); index1++ ) {

            int teamID = -1;
            String teamsName = "";
            Game game = games.get(index1);

            for (int index2 = 0; index2 < games.get(index1).getTeamArrayList().size(); index2++) {

                teamID = games.get(index1).getTeamArrayList().get(index2).getTeamID();

                for( int index3=0; index3< MyApplication.getInstance().getAllSports().get(game.getSportID()-1).getTeams().size(); index3++ ) {
                    if( teamID == MyApplication.getInstance().getAllSports().get(game.getSportID()-1).getTeams().get(index3).getTeamID() ) {
                        teamsName +=  MyApplication.getInstance().getAllSports().get(game.getSportID()-1).getTeams().get(index3).getName();
                        break;
                    }
                }
                if( index2 == 0 )
                    teamsName += " " + getVsInEnglishSpanishPortugieses + " "; // I need this separator to display good team names in listView
            }
            teamTempArrayList.add(teamsName);
        }


    }

    @Override
    public int getCount() {
        return games.size();
    }

    @Override
    public Object getItem(int position) {
        return games.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder
    {
        TextView teamsPlay;
        TextView timeOfGame;
        ImageView sportIcon;
    }

    //A view to hold each row in the list
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // A ViewHolder keeps references to children views to avoid unneccessary calls
        // to findViewById() on each row.
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listrow_games_of_league, null);

            holder = new ViewHolder();
            holder.sportIcon = (ImageView) convertView.findViewById(R.id.games_icon_sport);
            holder.teamsPlay = (TextView) convertView.findViewById(R.id.games_teams_play);
            holder.timeOfGame = (TextView) convertView.findViewById(R.id.games_time);

            holder.timeOfGame.setAlpha(0.7f);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.sportIcon.setImageBitmap(picture);

        holder.teamsPlay.setText( ""+ teamTempArrayList.get(position) );
        holder.timeOfGame.setText( ""+ timeOfGameTempArraylist.get(position) );

        if( games.get(position).getOddArrayList().size() <= 0 )
            convertView.setAlpha(0.4f);
        else
            convertView.setAlpha(1.0f);

        if (position % 2 == 0)
            convertView.setBackgroundColor(mInflater.getContext().getResources().getColor(R.color.list_black_row));
        else
            convertView.setBackgroundColor(mInflater.getContext().getResources().getColor(R.color.list_gray_row));

        return convertView;
    }
}