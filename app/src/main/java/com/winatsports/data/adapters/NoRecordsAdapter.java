package com.winatsports.data.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.winatsports.R;

/**
 * Created by dkorunek on 25/04/16.
 */
public class NoRecordsAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private String mTitleText;

    public NoRecordsAdapter(Context context, String titleText) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mTitleText = titleText;
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class NoRecordsHolder {
        public TextView tvTitle;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NoRecordsHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listrow_wagers_parlay_no_records, null);

            holder = new NoRecordsHolder();
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tvWagersParlayNoRecords);

            convertView.setTag(holder);
        } else {
            holder = (NoRecordsHolder) convertView.getTag();
        }

        holder.tvTitle.setText(mTitleText);
        return convertView;
    }
}
