package com.winatsports.data.adapters;



import android.app.Notification;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.data.firstpage.HomeNotification;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;



public class SeparatedFirstPageAdapter extends BaseAdapter {

    private Context context;
    public final Map<String, Adapter> sections;
    public final ArrayAdapter<String> headers;
    public final static int TYPE_SECTION_HEADER = 0;

    public SeparatedFirstPageAdapter(Context context) {
        headers = new ArrayAdapter<String>(context, R.layout.listrow_first_page_header);
        sections = new LinkedHashMap<String, Adapter>();
        this.context = context;
        prepareData();
    }

    public void addSection(String section, Adapter adapter)
    {
        this.headers.add(section);
        this.sections.put(section, adapter);
    }

    public void removeSection(String section) {
        this.headers.remove(section);
        this.sections.remove(section);
    }

    @Override
    public Object getItem(int position)
    {
        for (Object section : this.sections.keySet())
        {
            Adapter adapter = sections.get(section);
            int size = adapter.getCount() + 1;

            // check if position inside this section
            if (position == 0) return section;
            if (position < size) return adapter.getItem(position - 1);

            // otherwise jump into next section
            position -= size;
        }
        return null;
    }

    public Adapter getSection(String section) {
        return sections.get(section);
    }


    public int getCount()
    {
        // total together all sections, plus one for each section header
        int total = 0;
        for (Adapter adapter : this.sections.values())
            total += adapter.getCount() + 1;
        return total;
    }

    @Override
    public int getViewTypeCount()
    {
        // assume that headers count as one, then total all sections
        int total = 1;
        for (Adapter adapter : this.sections.values())
            total += adapter.getViewTypeCount();
        return total;
    }

    @Override
    public int getItemViewType(int position)
    {
        int type = 1;
        for (Object section : this.sections.keySet())
        {
            Adapter adapter = sections.get(section);
            int size = adapter.getCount() + 1;

            // check if position inside this section
            if (position == 0) return TYPE_SECTION_HEADER;
            if (position < size) return type + adapter.getItemViewType(position - 1);

            // otherwise jump into next section
            position -= size;
            type += adapter.getViewTypeCount();
        }
        return -1;
    }

    @Override
    public boolean isEnabled(int position)
    {
        return (getItemViewType(position) != TYPE_SECTION_HEADER);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        try {
            int sectionnum = 0;
            for (Object section : this.sections.keySet()) {
                Adapter adapter = sections.get(section);
                int size = adapter.getCount() + 1;

                // check if position inside this section
                if (position == 0) return headers.getView(sectionnum, convertView, parent);
                if (position < size) return adapter.getView(position - 1, convertView, parent);

                // otherwise jump into next section
                position -= size;
                sectionnum++;
            }
        } catch (Exception e) {
            Log.e(SeparatedFirstPageAdapter.class.getSimpleName(), "", e);
        }
        return null;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    public void prepareData() {
        headers.clear();
        sections.clear();

        ArrayList<HomeNotification> generated = MyApplication.getInstance().getUserData().generateDailyWinNotifications();
        generated.addAll(MyApplication.getInstance().getUserData().getHomeNotifications());

        if (generated.size() > 0) {
            HomeScreenNotificationAdapter notifications = new HomeScreenNotificationAdapter(MyApplication.getInstance(), generated);
            addSection(notifications.getHeaderTitle(), notifications);
        }

        HomeScreenAdapter featuredGamesAdapter = new HomeScreenAdapter(MyApplication.getInstance());
        featuredGamesAdapter.prepareData(MyApplication.getInstance().getFirstPage().getFeaturedGames());

        if (featuredGamesAdapter.getCount() > 0) {
            addSection(featuredGamesAdapter.getHeaderTitle(), featuredGamesAdapter);
        }

        HomeScreenFeaturedLeaguesAdapter featuredLeaguesAdapter = new HomeScreenFeaturedLeaguesAdapter(MyApplication.getInstance());
        featuredLeaguesAdapter.prepareData();
        if (featuredLeaguesAdapter.getCount() > 0)
            addSection(featuredLeaguesAdapter.getHeaderTitle(), featuredLeaguesAdapter);

        HomeScreenAdapter upNextAdapter = new HomeScreenAdapter(MyApplication.getInstance());
        upNextAdapter.prepareData(MyApplication.getInstance().getFirstPage().getUpNext());
        if (upNextAdapter.getCount() > 0)
            addSection(upNextAdapter.getHeaderTitle(), upNextAdapter);
    }

    public void notifyDataUpdated() {
        for (Object section : this.sections.keySet()) {
            Adapter adapter = sections.get(section);
            if (adapter instanceof HomeScreenAdapter) {
                ((HomeScreenAdapter) adapter).notifyDataSetChanged();
            } else if (adapter instanceof HomeScreenFeaturedLeaguesAdapter) {
                ((HomeScreenFeaturedLeaguesAdapter) adapter).notifyDataSetChanged();
            }
        }
        notifyDataSetChanged();
    }

}
