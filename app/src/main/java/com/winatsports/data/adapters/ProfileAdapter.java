package com.winatsports.data.adapters;



import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.winatsports.R;

import java.util.ArrayList;


/**
 * Created by broda on 30/11/2015.
 */


public class ProfileAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private Context context;

    //The variable that will hold our text data to be tied to list.
    private ArrayList<String> profileTextOnTop;
    private ArrayList<String> profileTextOnBottom;
    private ArrayList<Integer> profilePictures;

    public ProfileAdapter(Context context, ArrayList<Integer> profilePictures, ArrayList<String> profileTextOnTop, ArrayList<String> profileTextOnBottom) {
        mInflater = LayoutInflater.from( context );
        this.profileTextOnTop = profileTextOnTop;
        this.profileTextOnBottom = profileTextOnBottom;
        this.profilePictures = profilePictures;
        this.context = context;
    }

    @Override
    public int getCount() {
        return profilePictures.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder
    {
        TextView textOnTop;
        TextView textOnBottom;
        ImageView image;
    }

    //A view to hold each row in the list
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // A ViewHolder keeps references to children views to avoid unneccessary calls
        // to findViewById() on each row.
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listrow_profile, null);

            holder = new ViewHolder();
            holder.image = (ImageView) convertView.findViewById(R.id.ivProfileIcon);
            holder.textOnTop = (TextView) convertView.findViewById(R.id.tvProfileTextOnTop);
            holder.textOnBottom = (TextView) convertView.findViewById(R.id.tvProfileTextOnBottom);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        // NOTE: all icons have to have a color filter because they're read from apk file

        // alpha from 0 to 255 in integer value, float value from 0.0f to 1.0f
        // only if I set alpha = 50 or 210, it is the same
        /*int alpha = 210;
        Paint p = new Paint();

        ColorFilter filter = new LightingColorFilter(Color.WHITE, 0);
        p.setAlpha(alpha);
        p.setColorFilter(filter);
        holder.image.setColorFilter(filter);*/

        holder.image.setImageResource(profilePictures.get(position));

        holder.textOnTop.setText( profileTextOnTop.get(position) );
        holder.textOnBottom.setText( profileTextOnBottom.get(position) );

        convertView.setEnabled(true);

        if (position % 2 == 0)
            convertView.setBackgroundColor(mInflater.getContext().getResources().getColor(R.color.list_black_row));
        else
            convertView.setBackgroundColor(mInflater.getContext().getResources().getColor(R.color.list_gray_row));

        return convertView;
    }
}
