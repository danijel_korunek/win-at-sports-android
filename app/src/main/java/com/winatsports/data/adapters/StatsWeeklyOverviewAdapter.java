package com.winatsports.data.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.data.Constants;
import com.winatsports.data.stats.UserStats;

/**
 * Created by dkorunek on 02/09/16.
 */
public class StatsWeeklyOverviewAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;

    private class ViewHolder {
        public TextView tvHeader;
        public TextView tvWeek;
        public TextView tvTotalBets;
        public TextView tvBetsWon;
        public TextView tvBetsLost;
        public TextView tvPush;
    }

    public StatsWeeklyOverviewAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return MyApplication.getInstance().getStatsCollection().getUserStats().getWeeklyMoneyInfos().size();
    }

    @Override
    public Object getItem(int position) {
        return MyApplication.getInstance().getStatsCollection().getUserStats().getWeeklyMoneyInfos().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.listrow_weekly_summary, parent, false);

            holder = new ViewHolder();

            holder.tvHeader = (TextView) convertView.findViewById(R.id.tvHeader);
            holder.tvWeek = (TextView) convertView.findViewById(R.id.tvWeek);
            holder.tvTotalBets = (TextView) convertView.findViewById(R.id.tvTotalBets);
            holder.tvBetsWon = (TextView) convertView.findViewById(R.id.tvBetsWon);
            holder.tvBetsLost = (TextView) convertView.findViewById(R.id.tvBetsLost);
            holder.tvPush = (TextView) convertView.findViewById(R.id.tvPush);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        UserStats.WeeklyMoneyInfo wmi = MyApplication.getInstance().getStatsCollection().getUserStats().getWeeklyMoneyInfos().get(position);
        if (wmi.walletStart != -1 && wmi.walletEnd != -1) {
            holder.tvWeek.setText(Util.getString(Constants.MSG_KEY_STATS_STARTED_WITH) + " " + Util.formatMoney(wmi.walletStart, false) + " " +
            Util.getString(Constants.MSG_KEY_STATS_ENDED_WITH) + " " + Util.formatMoney(wmi.walletEnd, false));
            holder.tvWeek.setVisibility(View.VISIBLE);
        } else {
            holder.tvWeek.setText("");
            holder.tvWeek.setVisibility(View.GONE);
        }

        String aWeekLater = Util.addWeekToDate(wmi.date);
        holder.tvHeader.setText(Util.formatWMIDate(wmi.date) + " - " + aWeekLater);

        holder.tvTotalBets.setText(String.format(Util.getString(Constants.MSG_KEY_STATS_TOTAL_BETS) + " %d (%s)", wmi.countBet, Util.formatMoney(wmi.betAmount, false)));
        double wonPercent = Math.abs(((double) wmi.countWon / (double) wmi.countBet) * 100.00f);
        double lostPercent = Math.abs(((double) wmi.countLost / (double) wmi.countBet) * 100.00f);
        double pushPercent = Math.abs(((double) wmi.countReturn / (double) wmi.countBet) * 100.00f);

        String strWon = String.format("%s: %s %.2f%% (%s)", Util.getString(Constants.MSG_KEY_STATS_WON), wmi.countWon, wonPercent, Util.formatMoney(Math.abs(wmi.betWin), false));
        String strLost = String.format("%s %s %.2f%% (%s)", Util.getString(Constants.MSG_KEY_STATS_LOST), wmi.countLost, lostPercent, Util.formatMoney(Math.abs(wmi.betLost), false));
        String strPush = String.format("%s %s %.2f%% (%s)", Util.getString(Constants.MSG_KEY_STATS_PUSH), wmi.countReturn, pushPercent, Util.formatMoney(Math.abs(wmi.betReturn), false));

        holder.tvBetsWon.setText(strWon);
        holder.tvBetsLost.setText(strLost);
        holder.tvPush.setText(strPush);

        if (position % 2 == 0)
            convertView.setBackgroundColor(layoutInflater.getContext().getResources().getColor(R.color.list_black_row));
        else
            convertView.setBackgroundColor(layoutInflater.getContext().getResources().getColor(R.color.list_gray_row));

        return convertView;
    }
}
