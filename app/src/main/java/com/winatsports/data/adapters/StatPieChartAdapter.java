package com.winatsports.data.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.custom.MyPieChart;
import com.winatsports.data.Constants;
import com.winatsports.data.stats.StatsCollection;
import com.winatsports.data.stats.UserStats;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by dkorunek on 05/09/16.
 */
public class StatPieChartAdapter extends BaseAdapter {
    private LayoutInflater layoutInflater;
    private Context context;

    private List<Integer> defaultColors;

    private final int COUNTRY_STATS = 0;
    private final int EVENTS_PER_DAY = 1;
    private final int EVENTS_PER_WEEK = 2;
    private final int EVENTS_PER_MONTH = 3;
    private final int SPORTS_PER_DAY = 4;
    private final int SPORTS_PER_WEEK = 5;
    private final int SPORTS_PER_MONTH = 6;
    private final int LEAGUES_PER_DAY = 7;
    private final int LEAGUES_PER_WEEK = 8;
    private final int LEAGUES_PER_MONTH = 9;
    private final int BET_TYPES_PER_DAY = 10;
    private final int BET_TYPES_PER_WEEK = 11;
    private final int BET_TYPES_PER_MONTH = 12;

    private class ViewHolder {
        public TextView tvTitle;
        public FrameLayout flChartArea;
        public TextView tvNoRecords;
    }

    public StatPieChartAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        Integer [] colors =
                { 0xff03a9f4, 0xff9c27b0, 0xff4caf50, 0xffffc107, 0xfff44336};
                //{ Color.BLUE, Color.MAGENTA, Color.GREEN, Color.CYAN, Color.RED };
        defaultColors = Arrays.asList(colors);
    }

    @Override
    public int getCount() {
        return 13;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return prepareDataForDisplay(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.listrow_piechart, parent, false);
            View inflated = layoutInflater.inflate(R.layout.listrow_wagers_parlay_no_records, null);

            holder = new ViewHolder();
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tvHeader);
            holder.flChartArea = (FrameLayout) convertView.findViewById(R.id.flChartArea);
            holder.tvNoRecords = (TextView) inflated.findViewById(R.id.tvWagersParlayNoRecords);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.flChartArea.removeAllViews();

        LinkedHashMap<String, Float> sportStats = prepareDataForDisplay(position);

        if (!sportStats.isEmpty()) {
            MyPieChart chart = new MyPieChart(context);
            chart.setData(new ArrayList<String>(sportStats.keySet()), defaultColors, new ArrayList<Float>(sportStats.values()));
            chart.setAlpha(1.0f);
            holder.flChartArea.addView(chart);
        } else {
            holder.tvNoRecords.setText(Util.getString(Constants.MSG_KEY_NO_RECORDS));
            holder.flChartArea.addView(holder.tvNoRecords);
        }

        holder.tvTitle.setText(getTitle(position));

        convertView.setBackgroundColor(context.getResources().getColor(R.color.list_divider_color));

        return convertView;
    }

    private String getTitle(int position) {
        switch (position) {
            case EVENTS_PER_DAY:
                return Util.getString(Constants.MSG_KEY_STATS_EVENT_DAY);

            case EVENTS_PER_WEEK:
                return Util.getString(Constants.MSG_KEY_STATS_EVENT_WEEK);

            case EVENTS_PER_MONTH:
                return Util.getString(Constants.MSG_KEY_STATS_EVENT_MONTH);

            case SPORTS_PER_DAY:
                return Util.getString(Constants.MSG_KEY_STATS_SPORT_DAY);

            case SPORTS_PER_WEEK:
                return Util.getString(Constants.MSG_KEY_STATS_SPORT_WEEK);

            case SPORTS_PER_MONTH:
                return Util.getString(Constants.MSG_KEY_STATS_SPORT_MONTH);

            case LEAGUES_PER_DAY:
                return Util.getString(Constants.MSG_KEY_STATS_LEAGUE_DAY);

            case LEAGUES_PER_WEEK:
                return Util.getString(Constants.MSG_KEY_STATS_LEAGUE_WEEK);

            case LEAGUES_PER_MONTH:
                return Util.getString(Constants.MSG_KEY_STATS_LEAGUE_MONTH);

            case BET_TYPES_PER_DAY:
                return Util.getString(Constants.MSG_KEY_STATS_BETTYPE_DAY);

            case BET_TYPES_PER_WEEK:
                return Util.getString(Constants.MSG_KEY_STATS_BETTYPE_WEEK);

            case BET_TYPES_PER_MONTH:
                return Util.getString(Constants.MSG_KEY_STATS_BETTYPE_MONTH);
            default:
                return Util.getString(Constants.MSG_KEY_STATS_YOURCOUNTRYSPORTS);
        }
    }

    private LinkedHashMap<String, Float> prepareDataForDisplay(int position) {
        LinkedHashMap<String, Float> sportStats = new LinkedHashMap<>();
        float total = 0f;
        float theRest = 0f;
        int len = 0;

        if (position == COUNTRY_STATS) {

            len = MyApplication.getInstance().getStatsCollection().getUserStats().getCountryStats().size();
            // this array is already sorted in an descending order
            UserStats.SportBet [] list = MyApplication.getInstance().getStatsCollection().getUserStats().getCountryStats().toArray(new UserStats.SportBet [len]);

            total = MyApplication.getInstance().getStatsCollection().getUserStats().getCountryStatTotal();

            for (int i = 0; i < list.length; i++) {
                // pie chart will display only 5 pieces
                if (i > 3)
                    theRest += list[ i ].betNumber;
                else {
                    float percentage = (list[ i ].betNumber / total) * 100.0f;
                    sportStats.put(list[ i ].sportName, percentage);
                }
            }

            float percentage = (theRest / total) * 100.0f;
            sportStats.put(Util.getString(Constants.MSG_KEY_STATS_PIECHART_OTHER), percentage);
        } else {
            StatsCollection.SportEvent [] list = null;

            switch (position) {
                case EVENTS_PER_DAY:
                    total = MyApplication.getInstance().getStatsCollection().getEventsPerDayTotal();
                    len = MyApplication.getInstance().getStatsCollection().getEventsPerDay().size();
                    list = (StatsCollection.SportEvent []) MyApplication.getInstance().getStatsCollection().getEventsPerDay().toArray(new StatsCollection.SportEvent [len]);
                    break;

                case EVENTS_PER_WEEK:
                    total = MyApplication.getInstance().getStatsCollection().getEventsPerWeekTotal();
                    len = MyApplication.getInstance().getStatsCollection().getEventsPerWeek().size();
                    list = (StatsCollection.SportEvent [])  MyApplication.getInstance().getStatsCollection().getEventsPerWeek().toArray(new StatsCollection.SportEvent [len]);
                    break;

                case EVENTS_PER_MONTH:
                    total = MyApplication.getInstance().getStatsCollection().getEventsPerMonthTotal();
                    len = MyApplication.getInstance().getStatsCollection().getEventsPerMonth().size();
                    list = (StatsCollection.SportEvent []) MyApplication.getInstance().getStatsCollection().getEventsPerMonth().toArray(new StatsCollection.SportEvent [len]);
                    break;

                case SPORTS_PER_DAY:
                    total = MyApplication.getInstance().getStatsCollection().getSportsPerDayTotal();
                    len = MyApplication.getInstance().getStatsCollection().getPopularSportsPerDay().size();
                    list = (StatsCollection.SportEvent []) MyApplication.getInstance().getStatsCollection().getPopularSportsPerDay().toArray(new StatsCollection.SportEvent [len]);
                    break;

                case SPORTS_PER_WEEK:
                    total = MyApplication.getInstance().getStatsCollection().getSportsPerWeekTotal();
                    len = MyApplication.getInstance().getStatsCollection().getPopularSportsPerWeek().size();
                    list = (StatsCollection.SportEvent []) MyApplication.getInstance().getStatsCollection().getPopularSportsPerWeek().toArray(new StatsCollection.SportEvent [len]);
                    break;

                case SPORTS_PER_MONTH:
                    total = MyApplication.getInstance().getStatsCollection().getSportsPerMonthTotal();
                    len = MyApplication.getInstance().getStatsCollection().getPopularSportsPerMonth().size();
                    list = (StatsCollection.SportEvent []) MyApplication.getInstance().getStatsCollection().getPopularSportsPerMonth().toArray(new StatsCollection.SportEvent [len]);
                    break;

                case LEAGUES_PER_DAY:
                    total = MyApplication.getInstance().getStatsCollection().getLeaguesPerDayTotal();
                    len = MyApplication.getInstance().getStatsCollection().getPopularLeaguesPerDay().size();
                    list = (StatsCollection.SportEvent []) MyApplication.getInstance().getStatsCollection().getPopularLeaguesPerDay().toArray(new StatsCollection.SportEvent [len]);
                    break;

                case LEAGUES_PER_WEEK:
                    total = MyApplication.getInstance().getStatsCollection().getLeaguesPerWeekTotal();
                    len = MyApplication.getInstance().getStatsCollection().getPopularLeaguesPerWeek().size();
                    list = (StatsCollection.SportEvent []) MyApplication.getInstance().getStatsCollection().getPopularLeaguesPerWeek().toArray(new StatsCollection.SportEvent [len]);
                    break;

                case LEAGUES_PER_MONTH:
                    total = MyApplication.getInstance().getStatsCollection().getLeaguesPerMonthTotal();
                    len = MyApplication.getInstance().getStatsCollection().getPopularLeaguesPerMonth().size();
                    list = (StatsCollection.SportEvent []) MyApplication.getInstance().getStatsCollection().getPopularLeaguesPerMonth().toArray(new StatsCollection.SportEvent [len]);
                    break;

                case BET_TYPES_PER_DAY:
                    total = MyApplication.getInstance().getStatsCollection().getBetTypesPerDayTotal();
                    len = MyApplication.getInstance().getStatsCollection().getPopularBetTypesPerDay().size();
                    list = (StatsCollection.SportEvent []) MyApplication.getInstance().getStatsCollection().getPopularBetTypesPerDay().toArray(new StatsCollection.SportEvent [len]);
                    break;

                case BET_TYPES_PER_WEEK:
                    total = MyApplication.getInstance().getStatsCollection().getBetTypesPerWeekTotal();
                    len = MyApplication.getInstance().getStatsCollection().getPopularBetTypesPerWeek().size();
                    list = (StatsCollection.SportEvent []) MyApplication.getInstance().getStatsCollection().getPopularBetTypesPerWeek().toArray(new StatsCollection.SportEvent [len]);
                    break;

                case BET_TYPES_PER_MONTH:
                    total = MyApplication.getInstance().getStatsCollection().getBetTypesPerMonthTotal();
                    len = MyApplication.getInstance().getStatsCollection().getPopularBetTypesPerMonth().size();
                    list = (StatsCollection.SportEvent []) MyApplication.getInstance().getStatsCollection().getPopularBetTypesPerMonth().toArray(new StatsCollection.SportEvent [len]);
                    break;

            }


            for (int i = 0; i < list.length; i++) {
                // pie chart will display only 5 pieces
                if (i > 3)
                    theRest += list[ i ].value;
                else {
                    float percentage = (list[ i ].value / total) * 100.0f;
                    sportStats.put(list[ i ].name, percentage);
                }
            }

            float percentage = (theRest / total) * 100.0f;
            sportStats.put(Util.getString(Constants.MSG_KEY_STATS_PIECHART_OTHER), percentage);
        }

        return sportStats;
    }
}
