package com.winatsports.data.adapters;




import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.data.Constants;
import com.winatsports.data.MyBet;
import com.winatsports.data.MyParlay;
import com.winatsports.data.elements.BetType;
import com.winatsports.data.elements.League;
import com.winatsports.data.elements.Sport;
import com.winatsports.data.elements.Team;
import com.winatsports.settings.Settings;

import java.util.ArrayList;

/**
 * Created by broda on 15/02/2016.
 */

public class WagersDetailsAdapter extends BaseAdapter {

    private LayoutInflater mInflater;

    private MyParlay myParlay;

    private ArrayList<Bitmap> sportIcon;
    private ArrayList<String> titleArrayList;
    private ArrayList<String> teamsName;
    private ArrayList<String> dateAndTimeOfBeggingGame;
    private ArrayList<String> leagueNameAndOddType;

    int oddType = -1;

    public WagersDetailsAdapter(Context context, MyParlay myParlay1) {
        mInflater = LayoutInflater.from(context);

        myParlay = myParlay1;

        sportIcon = new ArrayList<Bitmap>();
        titleArrayList = new ArrayList<String>();
        teamsName = new ArrayList<String>();
        dateAndTimeOfBeggingGame = new ArrayList<String>();
        leagueNameAndOddType = new ArrayList<String>();

        oddType = Settings.getOddType(mInflater.getContext());

        for( int index1=0; index1<myParlay.getMyBets().size(); index1++ ) {

            MyBet myBet = (MyBet) myParlay.getMyBets().get(index1);

            double correctTotalOdds = 1.0;
            String correctOddBet = "";
            // if this condition is true, then this is parlay, otherwise is teaser
            if (myParlay.getTeaserOdd() == 0.0) {

                correctTotalOdds = correctTotalOdds * Util.convertUSOddsToDecimalOdds(myBet.getBetOdd());
                correctTotalOdds = Util.fixDecimal(correctTotalOdds);
                // This  "correctTotalOdds" is in decimal format.. depending on setting from user I need to display correctly odd values
                // Which settings user has set I become from ===>  Settings.getOddType(context);
                if (oddType == Constants.C_US_ODDS) {
                    correctOddBet = Util.formatDecimalOdds(mInflater.getContext(), correctTotalOdds);
                }
                // if user has setup ===> decimal odds
                else if (oddType == Constants.C_DECIMAL_ODDS) {
                    correctOddBet = String.valueOf(correctTotalOdds);
                }
                // if user has setup ==> fractional odds
                else if (oddType == Constants.C_FRACTIONAL_ODDS) {
                    String decimalToFractionalOdds = Util.formatDecimalOdds(mInflater.getContext(), correctTotalOdds);
                    correctOddBet = decimalToFractionalOdds;
                }
                if( correctOddBet.endsWith(".0") )
                    correctOddBet = correctOddBet.replace(".0", "");
                titleArrayList.add( myBet.getTeamName() + " (" + correctOddBet + ")" );
            }
            // this is teaser
            else {
                if( myBet.getTypeLangValue() == null ) {
                    if( myBet.getTypeLangLine() != null )
                        titleArrayList.add( myBet.getTeamName() + " " + myBet.getTypeLangLine() );
                    else
                        if( myBet.getTypeName2ndHalf().equals("0") )
                            titleArrayList.add( myBet.getTeamName() );
                        else
                            titleArrayList.add( myBet.getTeamName() + " " + myBet.getTypeName2ndHalf() );
                } else {
                    if( myBet.getTypeLangValue().equals(Constants.MSG_KEY_BETTYPE_OVER) )
                        titleArrayList.add( MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_BETTYPE_OVER) + " " + myBet.getTypeLangLine() );
                    else
                        titleArrayList.add( MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_BETTYPE_UNDER) + " " + myBet.getTypeLangLine() );
                }
            }

            String homeName = "";
            String awayName = "";
            String leagueNameAndOddBetType = "";
            int counterToBreakForLoop = 0;
            for( int index2=0; index2< MyApplication.getInstance().getAllSports().size(); index2++ ) {

                Sport sport = MyApplication.getInstance().getAllSports().get(index2);
                if( sport.getSportID() == myBet.getSportID() )  {
                    // here I add sport icon
                    sportIcon.add(sport.getScaledIcon());
                    //break;
                    // after that I'm adding teams name WagerDetailsActivity.java
                    if( myBet.getEventDesc().equals("") ) {
                        for (int index3 = 0; index3 < sport.getTeams().size(); index3++) {

                            Team team = sport.getTeams().get(index3);
                            if (team.getTeamID() == myBet.getHomeID()) {
                                homeName = team.getName();
                                counterToBreakForLoop++;
                            } else if (team.getTeamID() == myBet.getAwayID()) {
                                awayName = team.getName();
                                counterToBreakForLoop++;
                            } else if (counterToBreakForLoop == 2) {
                                break;
                            }
                        }
                        // here I add teams name
                        teamsName.add(homeName + " " + MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_VS) +
                                " " + awayName);
                    }
                    else  {
                        teamsName.add(myBet.getEventDesc());
                    }

                    // add league name and odd type
                    for( int index3=0; index3<sport.getLeagues().size(); index3++ )  {

                        League league = sport.getLeagues().get(index3);
                        if( league.getLeagueID() == myBet.getLeagueID() )  {
                            leagueNameAndOddBetType += league.getLeagueName() + ", ";
                            break;
                        }
                    }
                    // add type name of odd
                    for( int index3=0; index3<sport.getBetTypes().size(); index3++ )  {

                        BetType betType = sport.getBetTypes().get(index3);
                        if( betType.getTypeID() == myBet.getBetTypeId() )  {
                            leagueNameAndOddBetType += betType.getTypeName();
                        }
                    }

                    // here I add league name and odd type
                    leagueNameAndOddType.add(leagueNameAndOddBetType);
                }
            }

            dateAndTimeOfBeggingGame.add(Util.formatDate(myBet.getStartDate()));
        }


    }

    @Override
    public int getCount() {
        return myParlay.getMyBets().size();
    }

    @Override
    public Object getItem(int position) {
        return myParlay.getMyBets().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        ImageView ivSportIconWagersDetails;
        TextView tvOddTitleWagersDetails;
        TextView tvTeamsNameWagersDetails;
        TextView tvStartOfGameWagersDetails;
        TextView tvLeagueNameOddTypeWagersDetails;
        TextView tvTapForDetailsWagersDetails;
    }

    //A view to hold each row in the list
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // A ViewHolder keeps references to children views to avoid unneccessary calls
        // to findViewById() on each row.
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listrow_wagers_details, null);

            holder = new ViewHolder();
            holder.ivSportIconWagersDetails = (ImageView) convertView.findViewById(R.id.ivSportIconWagersDetails);
            holder.tvOddTitleWagersDetails = (TextView) convertView.findViewById(R.id.tvOddTitleWagersDetails);
            holder.tvTeamsNameWagersDetails = (TextView) convertView.findViewById(R.id.tvTeamsNameWagersDetails);
            holder.tvStartOfGameWagersDetails = (TextView) convertView.findViewById(R.id.tvStartOfGameWagersDetails);
            holder.tvLeagueNameOddTypeWagersDetails = (TextView) convertView.findViewById(R.id.tvLeagueNameOddTypeWagersDetails);
            holder.tvTapForDetailsWagersDetails = (TextView) convertView.findViewById(R.id.tvTapForDetailsWagersDetails);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Bitmap parlayOrTeaserIcon = sportIcon.get(position);
        String title = titleArrayList.get(position);
        String teamName = teamsName.get(position);
        String dateAndTimeOfGame = dateAndTimeOfBeggingGame.get(position);
        String leagueNameOddType = leagueNameAndOddType.get(position);

        // alpha from 0 to 255 in integer value, float value from 0.0f to 1.0f
        // only if I set alpha = 50 or 210, it is the same
        /*int alpha = 210;
        Paint p = new Paint();

        ColorFilter filter = new LightingColorFilter(Color.WHITE, 0);
        p.setAlpha(alpha);
        p.setColorFilter(filter);
        holder.ivSportIconWagersDetails.setColorFilter(filter);*/

        holder.ivSportIconWagersDetails.setImageBitmap(parlayOrTeaserIcon);
        holder.tvOddTitleWagersDetails.setText(title);
        holder.tvTeamsNameWagersDetails.setText(teamName);
        holder.tvStartOfGameWagersDetails.setText(dateAndTimeOfGame);
        holder.tvLeagueNameOddTypeWagersDetails.setText(leagueNameOddType);

        if (position % 2 == 0)
            convertView.setBackgroundColor(mInflater.getContext().getResources().getColor(R.color.list_black_row));
        else
            convertView.setBackgroundColor(mInflater.getContext().getResources().getColor(R.color.list_gray_row));

        return convertView;

    }

}