package com.winatsports.data.adapters;


import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.data.firstpage.Feature;
import com.winatsports.data.firstpage.FirstPageLeagueSection;
import com.winatsports.data.elements.League;
import com.winatsports.data.elements.Sport;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * Created by broda on 10/12/2015.
 */



public class HomeScreenFeaturedLeaguesAdapter extends BaseAdapter {

    private LayoutInflater mInflater;

    private String headerTitle;
    //The variable that will hold our text data to be tied to list.
    private ArrayList<League> leagues;

    // This map reflects a relationship between a league and a sport.
    private Map<Integer, Sport> leagueSportMap;

    public HomeScreenFeaturedLeaguesAdapter(Context context) {
        mInflater = LayoutInflater.from(context);

        leagues = new ArrayList<League>();
        leagueSportMap = new LinkedHashMap<Integer, Sport>();
    }

    public String getHeaderTitle() {
        return headerTitle;
    }

    @Override
    public int getCount() {
        return leagues.size();
    }

    @Override
    public Object getItem(int position) {
        return leagues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        ImageView ivSportIcon;
        TextView tvLeagueName;
        TextView tvSportName;
    }

    //A view to hold each row in the list
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // A ViewHolder keeps references to children views to avoid unneccessary calls
        // to findViewById() on each row.
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listrow_first_page_featured_leagues, null);

            holder = new ViewHolder();
            holder.ivSportIcon = (ImageView) convertView.findViewById(R.id.ivSportIcon);
            holder.tvLeagueName = (TextView) convertView.findViewById(R.id.tvLeagueName);
            holder.tvSportName = (TextView) convertView.findViewById(R.id.tvSportName);

            holder.tvSportName.setAlpha(0.7f);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        League league = leagues.get(position);
        Sport sport = leagueSportMap.get(league.getLeagueID());

        String leagueName = league.getLeagueName();
        Bitmap sportIcon = sport.getScaledIcon();
        String sportName = sport.getSportName();

        holder.ivSportIcon.setImageBitmap(sportIcon);
        holder.tvLeagueName.setText(leagueName);
        holder.tvSportName.setText(sportName);

        if (position % 2 == 0)
            convertView.setBackgroundColor(mInflater.getContext().getResources().getColor(R.color.list_black_row));
        else
            convertView.setBackgroundColor(mInflater.getContext().getResources().getColor(R.color.list_gray_row));

        return convertView;

    }

    public void prepareData() {
        leagues.clear();
        leagueSportMap.clear();

        FirstPageLeagueSection leagueSection = MyApplication.getInstance().getFirstPage().getFeaturedLeagues();

        for (int i = 0; i < leagueSection.getFeatures().size(); i++) {
            Feature feature = leagueSection.getFeatures().get(i);
            League league = Util.getLeague(feature.getLeagueID());
            if (league != null) {
                leagues.add(league);
                // find a sport for this league
                Sport tempSport = Util.getSport(league.getSportID());
                if (tempSport != null)
                    leagueSportMap.put(league.getLeagueID(), tempSport);
            }

        }

        headerTitle = MyApplication.getInstance().getAllMessages().get( leagueSection.getTitleId() );
    }


}