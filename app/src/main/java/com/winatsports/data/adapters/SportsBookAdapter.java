package com.winatsports.data.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.data.elements.Sport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SportsBookAdapter extends BaseAdapter {

    Context context;
    private LayoutInflater inflater = null;
    // this collection is used to show data within the list
    private ArrayList<Sport> allSports;


    public SportsBookAdapter(Context context) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.allSports = new ArrayList<Sport>();

        prepareData();
    }

    public class ViewHolder
    {
        public ImageView iv_ImageXML_Image;
        public TextView tv_ImageXML_Name;
    }

    @Override
    public int getCount() {
        return allSports.size();
    }

    @Override
    public Object getItem(int position) {
        return allSports.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // A ViewHolder keeps references to children views to avoid unneccessary calls
        // to findViewById() on each row.
        final ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listrow_sport_book, null);

            holder = new ViewHolder();
            holder.iv_ImageXML_Image = (ImageView) convertView.findViewById(R.id.ivSportIcon);
            holder.tv_ImageXML_Name = (TextView) convertView.findViewById(R.id.tvSportName);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        // Bind the data efficiently with the holder.
        Sport sport = allSports.get(position);
        holder.tv_ImageXML_Name.setText(sport.getSportName());

        holder.iv_ImageXML_Image.setImageBitmap(sport.getScaledIcon());

        if (position % 2 == 0)
            convertView.setBackgroundColor(context.getResources().getColor(R.color.list_black_row));
        else
            convertView.setBackgroundColor(context.getResources().getColor(R.color.list_gray_row));

        return convertView;
    }

    public void prepareData() {
        allSports.clear();
        allSports.addAll(MyApplication.getInstance().getAllSports());
        Collections.sort(allSports, new Comparator<Sport>() {
            @Override
            public int compare(Sport o1, Sport o2) {
                return o1.getSportName().compareTo(o2.getSportName());
            }
        });
    }

    public void clear() {
        allSports.clear();
    }
}

