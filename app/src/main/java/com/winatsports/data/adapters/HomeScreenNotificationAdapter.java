package com.winatsports.data.adapters;



import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.data.Constants;
import com.winatsports.data.firstpage.HomeNotification;

import java.util.ArrayList;


/**
 * Created by broda on 23/12/2015.
 */


public class HomeScreenNotificationAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private Context context;

    private String headerTitle = "";
    //The variable that will hold our text data to be tied to list.
    private ArrayList<HomeNotification> homeNotifications;

    public HomeScreenNotificationAdapter(Context context, ArrayList<HomeNotification> homeNotifications) {

        this.context = context;
        mInflater = LayoutInflater.from(context);

        this.homeNotifications = homeNotifications;
        this.headerTitle = Util.getString(Constants.MSG_KEY_FPAGE_NOTIFICATIONS);

    }

    public String getHeaderTitle() {
        return headerTitle;
    }

    @Override
    public int getCount() {
        return homeNotifications.size();
    }

    @Override
    public Object getItem(int position) {
        return homeNotifications.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        ImageView ivNotificationIcon;
        TextView tvTitleNotification;
        TextView tvSubtitleNotification;
        ImageView ivDismiss;
    }

    //A view to hold each row in the list
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // A ViewHolder keeps references to children views to avoid unneccessary calls
        // to findViewById() on each row.
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listrow_first_page_notifications, null);

            holder = new ViewHolder();
            holder.ivNotificationIcon = (ImageView) convertView.findViewById(R.id.ivNotificationIcon);
            holder.tvTitleNotification = (TextView) convertView.findViewById(R.id.tvTitleNotification);
            holder.tvSubtitleNotification = (TextView) convertView.findViewById(R.id.tvSubtitleNotification);
            holder.ivDismiss = (ImageView) convertView.findViewById(R.id.ivDismiss);

            holder.tvSubtitleNotification.setAlpha(0.7f);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        HomeNotification homeNotification = homeNotifications.get(position);

        Bitmap notificationIcon = null;

        if (homeNotification.isWinLoseReport())
            notificationIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.glyph_notif_report);
        else if (homeNotification.getSportID() > 0) {
            Bitmap bm = Util.getIconForSportId(homeNotification.getSportID());
            if (bm == null)
                notificationIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.glyph_notification);
            else
                notificationIcon = bm;
        } else {
            notificationIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.glyph_notification);
        }

        String titleNotification = homeNotification.getTitle();
        String subTitleNotification = homeNotification.getSubTitle();

        holder.ivNotificationIcon.setImageBitmap(notificationIcon);
        holder.tvTitleNotification.setText(titleNotification);
        holder.tvSubtitleNotification.setText(subTitleNotification);

        holder.ivDismiss.setVisibility(homeNotification.isDismissible() ? ImageView.VISIBLE : ImageView.GONE);

        if (position % 2 == 0)
            convertView.setBackgroundColor(mInflater.getContext().getResources().getColor(R.color.list_black_row));
        else
            convertView.setBackgroundColor(mInflater.getContext().getResources().getColor(R.color.list_gray_row));

        return convertView;

    }

}
