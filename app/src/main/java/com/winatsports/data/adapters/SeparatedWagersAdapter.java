package com.winatsports.data.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.data.Constants;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by broda on 15/02/2016.
 */
public class SeparatedWagersAdapter extends BaseAdapter {


    public final Map<String, Adapter> sections = new LinkedHashMap<String, Adapter>();
    public final ArrayAdapter<String> headers;
    public final static int TYPE_SECTION_HEADER = 0;

    public SeparatedWagersAdapter(Context context) {
        headers = new ArrayAdapter<String>(context, R.layout.listrow_wagers_details_header);
        prepareData();
    }

    public void addSection(String section, Adapter adapter)
    {
        this.headers.add(section);
        this.sections.put(section, adapter);
    }

    @Override
    public Object getItem(int position)
    {
        for (Object section : this.sections.keySet())
        {
            Adapter adapter = sections.get(section);
            int size = adapter.getCount() + 1;

            // check if position inside this section
            if (position == 0) return section;
            if (position < size) return adapter.getItem(position - 1);

            // otherwise jump into next section
            position -= size;
        }
        return null;
    }

    public int getCount()
    {
        // total together all sections, plus one for each section header
        int total = 0;
        for (Adapter adapter : this.sections.values())
            total += adapter.getCount() + 1;
        return total;
    }

    @Override
    public int getViewTypeCount()
    {
        // assume that headers count as one, then total all sections
        int total = 1;
        for (Adapter adapter : this.sections.values())
            total += adapter.getViewTypeCount();
        return total;
    }

    @Override
    public int getItemViewType(int position)
    {
        int type = 1;
        for (Object section : this.sections.keySet())
        {
            Adapter adapter = sections.get(section);
            int size = adapter.getCount() + 1;

            // check if position is inside this section
            if (position == 0) return TYPE_SECTION_HEADER;
            if (position < size) return type + adapter.getItemViewType(position - 1);

            // otherwise jump into next section
            position -= size;
            type += adapter.getViewTypeCount();
        }
        return -1;
    }


    @Override
    public boolean isEnabled(int position)
    {
        return (getItemViewType(position) != TYPE_SECTION_HEADER);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        int sectionnum = 0;
        for (Object section : this.sections.keySet())
        {
            Adapter adapter = sections.get(section);
            int size = adapter.getCount() + 1;

            // check if position inside this section
            if (position == 0) return headers.getView( sectionnum, convertView, parent);
            if (position < size) return adapter.getView(position - 1, convertView, parent);

            // otherwise jump into next section
            position -= size;
            sectionnum++;
        }
        return null;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    public void prepareData() {
        headers.clear();
        sections.clear();

        if (MyApplication.getInstance().getUserData().getUserBets().size() > 0) {
            final WagersAdapter adapterWithRecords = new WagersAdapter(MyApplication.getInstance());
            adapterWithRecords.prepareData();
            addSection(Util.getString(Constants.MSG_KEY_APP_IN_PLAY), adapterWithRecords);
        } else {
            NoRecordsAdapter noRecordsAdapter = new NoRecordsAdapter(MyApplication.getInstance(), Util.getString(Constants.MSG_KEY_NO_IN_PLAY_GAMES));
            addSection(Util.getString(Constants.MSG_KEY_APP_IN_PLAY), noRecordsAdapter);
        }

        if (MyApplication.getInstance().getHistoryWagers().size() > 0) {
            final WagerHistoryAdapter adapterWithHistoryRecords = new WagerHistoryAdapter(MyApplication.getInstance(), MyApplication.getInstance().getHistoryWagers());
            adapterWithHistoryRecords.prepareData();
            addSection(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PROFILEHISTORYTEXT1), adapterWithHistoryRecords);
        } else {
            NoRecordsAdapter noRecordsAdapter = new NoRecordsAdapter(MyApplication.getInstance(), Util.getString(Constants.MSG_KEY_NO_HISTORY_GAMES));
            addSection(Util.getString(Constants.MSG_KEY_PROFILEHISTORYTEXT1), noRecordsAdapter);
        }
    }

    public void notifyDataUpdated() {
        for (Object section : this.sections.keySet()) {
            Adapter adapter = sections.get(section);
            if (adapter instanceof WagersAdapter) {
                ((WagersAdapter) adapter).notifyDataSetChanged();
            } else if (adapter instanceof WagerHistoryAdapter) {
                ((WagerHistoryAdapter) adapter).notifyDataSetChanged();
            }
        }
        notifyDataSetChanged();
    }
}
