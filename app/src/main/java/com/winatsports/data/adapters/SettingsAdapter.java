package com.winatsports.data.adapters;




import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.winatsports.R;
import com.winatsports.activities.SettingsActivity;
import com.winatsports.settings.AsyncChangeSettings;
import com.winatsports.settings.Settings;

import java.util.ArrayList;


/**
 * Created by broda on 02/12/2015.
 */



public class SettingsAdapter extends BaseAdapter {

    public static final int OPTION_START_GAME_NOTIFICATION = 0;
    public static final int OPTION_END_GAME_NOTIFICATION = 1;
    public static final int OPTION_BET_CONFIRMATION = 2;
    public static final int OPTION_PREDEFINED_WAGER_AMOUNT = 3;
    public static final int OPTION_SHOW_EMPTY_SCHEDULE = 4;
    public static final int OPTION_TO_WIN_AMOUNT = 5;

    Context context;
    private LayoutInflater mInflater;
    SettingsActivity activity;

    //The variable that will hold our text data to be tied to list.
    private ArrayList<String> settingsUpperText;
    private ArrayList<String> settingsLowerText;

    public SettingsAdapter(SettingsActivity activity1, Context context,
                           ArrayList<String> settingsUpperText, ArrayList<String> settingsLowerText) {
        this.activity = activity1;
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.settingsUpperText = settingsUpperText;
        this.settingsLowerText = settingsLowerText;
    }

    @Override
    public int getCount() {
        return settingsUpperText.size();
    }

    @Override
    public Object getItem(int position) {
        return settingsUpperText.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private static class ViewHolder
    {
        TextView upperText;
        TextView lowerText;
        public ToggleButton switchSettings;
    }

    //A view to hold each row in the list
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        // A ViewHolder keeps references to children views to avoid unneccessary calls
        // to findViewById() on each row.
        final ViewHolder holder;
        final View v = convertView;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listrow_settings, null);

            holder = new ViewHolder();
            holder.upperText = (TextView) convertView.findViewById(R.id.tvSettinsUpperText);
            holder.lowerText = (TextView) convertView.findViewById(R.id.tvSettinsLowerText);
            holder.switchSettings = (ToggleButton) convertView.findViewById(R.id.switchSettings);

            holder.lowerText.setAlpha(0.7f);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position == OPTION_START_GAME_NOTIFICATION) {
            holder.switchSettings.setChecked(Settings.isStartGameNotification(context));
        } else if (position == OPTION_END_GAME_NOTIFICATION) {
            holder.switchSettings.setChecked(Settings.isEndGameNotification(context));
        } else if (position == OPTION_BET_CONFIRMATION) {
            holder.switchSettings.setChecked(Settings.isBetConfirmation(context));
        } else if (position == OPTION_PREDEFINED_WAGER_AMOUNT) {
            holder.switchSettings.setChecked(Settings.isPredefinedWagerAmount(context));
        } else if (position == OPTION_SHOW_EMPTY_SCHEDULE) {
            holder.switchSettings.setChecked(Settings.isShowEmptySchedule(context));
        } else if (position == OPTION_TO_WIN_AMOUNT) {
            holder.switchSettings.setChecked(Settings.isWinAmount(context));
        }

       holder.switchSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean nextValue = holder.switchSettings.isChecked();

                AsyncChangeSettings asyncChangeSettings = new AsyncChangeSettings(activity, position, nextValue, holder.switchSettings );
                asyncChangeSettings.execute();
            }
        });

        holder.upperText.setText((String) settingsUpperText.get(position));
        holder.lowerText.setText((String) settingsLowerText.get(position));

        return convertView;
    }

}