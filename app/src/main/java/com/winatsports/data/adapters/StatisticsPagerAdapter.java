package com.winatsports.data.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.winatsports.R;
import com.winatsports.activities.StatisticsActivity;

/**
 * Created by dkorunek on 29/08/16.
 */
public class StatisticsPagerAdapter extends PagerAdapter {

    private Context uiContext;

    public StatisticsPagerAdapter(Context uiContext) {
        this.uiContext = uiContext;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(uiContext);
        ViewGroup layout = null;

        if (position == 0) {
            layout = (ViewGroup) inflater.inflate(R.layout.stat_winnings, container, false);
        } else if (position == 1) {
            layout = (ViewGroup) inflater.inflate(R.layout.stat_weekly_summary, container, false);
        } else if (position == 2) {
            layout = (ViewGroup) inflater.inflate(R.layout.stat_pie_charts, container, false);
        }

        container.addView(layout);
        return layout;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
