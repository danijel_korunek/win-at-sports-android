package com.winatsports.data.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.data.MyBet;
import com.winatsports.data.MyParlay;
import com.winatsports.data.elements.Game;
import com.winatsports.data.elements.Odd;
import com.winatsports.data.elements.OddType;
import com.winatsports.data.elements.Team;

import java.util.ArrayList;

/**
 * Created by dkorunek on 07/12/2016.
 */

public class GameOddsAdapter extends BaseAdapter {

    private Game game;
    private OddType oddType;
    private LayoutInflater layoutInflater;
    private ArrayList<Team> teams;

    public GameOddsAdapter(Context context, Game game, ArrayList<Team> teamNames, OddType oddType) {
        this.oddType = oddType;
        this.teams = teamNames;
        this.game = game;

        for (Odd o : this.oddType.getOdds()) {
            for (Team t : teams) {
                if (t.getTeamID() == o.getTeamId())
                    o.setTeamName(t.getName());
            }
        }

        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return oddType.getOdds().size();
    }

    @Override
    public Object getItem(int position) {
        return oddType.getOdds().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder
    {
        TextView tvDescriptionOdds;
        TextView tvValueOdds;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // A ViewHolder keeps references to children views to avoid unneccessary calls
        // to findViewById() on each row.
        ViewHolder holder;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.listrow_odds, null);

            holder = new ViewHolder();
            holder.tvDescriptionOdds = (TextView) convertView.findViewById(R.id.tvDescriptionOdds);
            holder.tvValueOdds = (TextView) convertView.findViewById(R.id.tvValueOdds);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Odd odd = oddType.getOdds().get(position);

        String descriptionOdds = "";
        if( Util.isPopulated(odd.getOutValue())  ) {
            String outValue = odd.getOutValue();
            descriptionOdds = MyApplication.getInstance().getAllMessages().get(outValue);

            if( odd.getOutValue() != null && odd.getOutBetLine() != null )
                descriptionOdds += " " + odd.getOutBetLine();
        }
        else {
            descriptionOdds = odd.getTeamName();

            if( Util.isPopulated( odd.getOutBetLine() ) ) {
                descriptionOdds += " " + odd.getOutBetLine();
            }
        }

        boolean hasBet = false;
        for (MyParlay parlay : MyApplication.getInstance().getUserData().getUserBets()) {
            for (MyBet bet : parlay.getMyBets()) {
                if( bet.getEventID() == game.getEventID() ) {
                    if (bet.getBetTypeId() == odd.getBetTypeId() && bet.getBetValue() == odd.getBetValue()
                            && bet.getTeamId() == odd.getTeamId()) {
                        hasBet = true;
                        break;
                    }
                }
            }

            if (hasBet)
                break;
        }


        holder.tvDescriptionOdds.setText(descriptionOdds);
        holder.tvValueOdds.setText(Util.formatOdds(MyApplication.getInstance(), odd.getBetOdds()));

        if (position % 2 == 0)
            convertView.setBackgroundColor(layoutInflater.getContext().getResources().getColor(R.color.list_black_row));
        else
            convertView.setBackgroundColor(layoutInflater.getContext().getResources().getColor(R.color.list_gray_row));

        // Sometimes happens for example under "SPORT ==> GOLF" that it shows me games that were played yesterday,
        // but then I need this games to make more transparent.. And this is what I'm doing here
        // If status is equal to 0, then user can bet on this game, otherwise not
        if( game.getStatus() == 0 ) {
            if( hasBet == true ) {
                convertView.setAlpha(1.0f);
                holder.tvDescriptionOdds.setTextColor(layoutInflater.getContext().getResources().getColor(R.color.tint_color));
                holder.tvValueOdds.setTextColor(layoutInflater.getContext().getResources().getColor(R.color.tint_color));
            } else {
                holder.tvDescriptionOdds.setTextColor(layoutInflater.getContext().getResources().getColor(R.color.white));
                holder.tvValueOdds.setTextColor(layoutInflater.getContext().getResources().getColor(R.color.white));
                convertView.setAlpha(1.0f);
            }
            return convertView;
        }
        else {
            convertView.setAlpha(0.4f);
            holder.tvDescriptionOdds.setTextColor(layoutInflater.getContext().getResources().getColor(R.color.white));
            holder.tvValueOdds.setTextColor(layoutInflater.getContext().getResources().getColor(R.color.white));
            return convertView;
        }

    }

}
