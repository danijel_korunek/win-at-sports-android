package com.winatsports.data.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.data.Constants;
import com.winatsports.data.upload.AsyncRealBetting;

import java.lang.ref.WeakReference;

/**
 * Created by dkorunek on 23/03/16.
 */
public class RealBetTutorialAdapter extends PagerAdapter {

    private Activity uiContext;
    private LayoutInflater inflater;
    private WeakReference<Dialog> dialog;
    private int eventId;

    public RealBetTutorialAdapter(Activity context, Dialog dialog, int eventId) {
        this.uiContext = context;

        inflater = LayoutInflater.from(uiContext);
        this.dialog = new WeakReference<Dialog>(dialog);
        this.eventId = eventId;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ViewGroup layout = null;

        switch (position) {
            case 0:
                layout = (ViewGroup) inflater.inflate(R.layout.real_tutorial_1, container, false);

                TextView tvHeading1 = (TextView) layout.findViewById(R.id.tvHeading1);
                tvHeading1.setText(Util.getString(Constants.MSG_KEY_REALMONEYTUTORIALP1_1));

                TextView tvHeading2 = (TextView) layout.findViewById(R.id.tvHeading2);
                tvHeading2.setText(Util.getString(Constants.MSG_KEY_REALMONEYTUTORIALP1_2));

                TextView tvLogoText = (TextView) layout.findViewById(R.id.tvLogoText);
                tvLogoText.setText(Util.getString(Constants.MSG_KEY_REALMONEYTUTORIALP1_3));

                TextView tvDescription = (TextView) layout.findViewById(R.id.tvDescription);
                tvDescription.setText(Util.getString(Constants.MSG_KEY_REALMONEYTUTORIALP1_4));

                TextView tvInBusinessFrom = (TextView) layout.findViewById(R.id.tvInBusinessFrom);
                tvInBusinessFrom.setText(Util.getString(Constants.MSG_KEY_REALMONEYTUTORIALP1_5));

                Button btnSkip = (Button) layout.findViewById(R.id.btnSkip);
                btnSkip.setText(Util.getString(Constants.MSG_KEY_SKIP));
                btnSkip.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        skip();
                    }
                });

                container.addView(layout);
                break;
            case 1:
                layout = (ViewGroup) inflater.inflate(R.layout.real_tutorial_2, container, false);
                TextView tv1 = (TextView) layout.findViewById(R.id.tvHeading1);
                tv1.setText(Util.getString(Constants.MSG_KEY_REALMONEYTUTORIALP2_1));

                TextView tv2 = (TextView) layout.findViewById(R.id.tvHeading2);
                tv2.setText(Util.getString(Constants.MSG_KEY_REALMONEYTUTORIALP2_2));

                TextView tv3 = (TextView) layout.findViewById(R.id.tvHeading3);
                tv3.setText(Util.getString(Constants.MSG_KEY_REALMONEYTUTORIALP2_3));

                Button btnSkipAll = (Button) layout.findViewById(R.id.btnSkip);
                btnSkipAll.setText(Util.getString(Constants.MSG_KEY_SKIP));
                btnSkipAll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        skip();
                    }
                });
                container.addView(layout);
                break;
            case 2:
                layout = (ViewGroup) inflater.inflate(R.layout.real_tutorial_3, container, false);
                TextView tvH1 = (TextView) layout.findViewById(R.id.tvHeading1);
                tvH1.setText(Util.getString(Constants.MSG_KEY_REALMONEYTUTORIALP3_1));

                TextView tvH2 = (TextView) layout.findViewById(R.id.tvHeading2);
                tvH2.setText(Util.getString(Constants.MSG_KEY_REALMONEYTUTORIALP3_2));

                TextView tvH3 = (TextView) layout.findViewById(R.id.tvHeading3);
                tvH3.setText(Util.getString(Constants.MSG_KEY_REALMONEYTUTORIALP3_3));

                Button btnSkipIt = (Button) layout.findViewById(R.id.btnSkip);
                btnSkipIt.setText(Util.getString(Constants.MSG_KEY_SKIP));
                btnSkipIt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        skip();
                    }
                });
                container.addView(layout);
                break;
            default:
                break;
        }

        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    private void skip() {
        if (dialog != null) dialog.get().dismiss();

        Log.d(RealBetTutorialAdapter.class.getSimpleName(), "UiContext class: " + uiContext.getClass().getSimpleName());
        MyApplication.getInstance().startRealBetting(false, uiContext, eventId);
    }
}
