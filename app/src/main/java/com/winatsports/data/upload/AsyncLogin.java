package com.winatsports.data.upload;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.activities.GuestActivity;
import com.winatsports.activities.SignInActivity;
import com.winatsports.data.Constants;
import com.winatsports.settings.Settings;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by dkorunek on 12/02/16.
 */
public class AsyncLogin extends AsyncTask {
    private String username, password;
    private Activity uiContext;
    private String displayMessage;

    public AsyncLogin(Activity uiContext, String u, String p) {
        this.username = u;
        this.password = p;

        this.uiContext = uiContext;
    }

    @Override
    protected void onCancelled() {
        if (uiContext instanceof GuestActivity) {
            ((GuestActivity) uiContext).dismissProgressDialog();
        }

        if (uiContext instanceof SignInActivity) {
            ((SignInActivity) uiContext).finishTask();
            ((SignInActivity) uiContext).dismissProgressDialog();
        }

        Util.showMessageDialog(uiContext, displayMessage, null);
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            HttpClient client = HttpClientBuilder.create()
                    .setMaxConnTotal(1)
                    .setUserAgent(System.getProperty("http.agent", "Unknown"))
                    .build();

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                    .setRedirectsEnabled(false)
                    .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                    .setCircularRedirectsAllowed(false).build();

            HttpPost post = new HttpPost((Uri.parse(Constants.URL_LOGIN_USER).toString()));
            post.setConfig(config);

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            /*@"
                             @"action"    : @"1",
                             @"lang"      : defines.selectedLanguage,
                             @"user_d"    : [SWF:@"%li", (long)defines.user_d],
                             @"device_id" : [SWF:@"%li", (long)defines.device_id],
                             @"email"     : fixNil(textField1.text),
                             @"pass"      : fixNil(textField2.text)
                             */

            pairs.add(new BasicNameValuePair("key", Constants.KEY_LOGIN_USER));
            pairs.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded charset=utf-8"));
            pairs.add(new BasicNameValuePair("lang", Settings.getLanguage(uiContext)));
            pairs.add(new BasicNameValuePair("user_d", Settings.getUserD(uiContext)));
            pairs.add(new BasicNameValuePair("device_id", Settings.getDeviceId(uiContext)));
            pairs.add(new BasicNameValuePair("email", username));
            pairs.add(new BasicNameValuePair("pass", password));
            pairs.add(new BasicNameValuePair("action", "1"));

            post.setEntity(new UrlEncodedFormEntity(pairs));

            HttpResponse response = client.execute(post);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sbReturnData = new StringBuffer();
            int character;

            while ( (character = br.read()) != -1 ) {
                sbReturnData.append((char) character);
            }

            br.close();

            String mess [] = Util.explode(sbReturnData.toString());
            String message = null;

            if (mess == null)
                message = sbReturnData.toString();
            else
                message = mess[0];

            if (message.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK)) {
                if (Util.isPopulated(mess[1])) {
                    Settings.setUserR(uiContext, mess[1]);
                    Settings.setLoginTime(uiContext, System.currentTimeMillis());
                    // user is logged in, proceed to home activity
                } else {
                    // unknown result
                    displayMessage = uiContext.getString(R.string.error_unknown_result);
                    cancel(true);
                }
            } else {
                // show return message
                displayMessage = mess[0];
                cancel(true);
            }
        } catch (Exception e) {
            Log.e(AsyncRegisterAccount.class.getSimpleName(), "", e);
            displayMessage = MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_NOINET);
            cancel(true);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        if (uiContext instanceof GuestActivity) {
            ((GuestActivity) uiContext).dismissProgressDialog();
        }

        if (uiContext instanceof SignInActivity) {
            ((SignInActivity) uiContext).finishTask();
            ((SignInActivity) uiContext).dismissProgressDialog();
        }

        new AsyncAddUser(uiContext, false).execute();
    }
}
