package com.winatsports.data.upload;




import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.Util;
import com.winatsports.activities.GameOddsActivity;
import com.winatsports.data.Constants;
import com.winatsports.data.MyBet;
import com.winatsports.data.MyParlay;
import com.winatsports.data.elements.BetType;
import com.winatsports.data.parlaytotal.ParlayOdd;
import com.tune.Tune;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by dkorunek on 03/02/16.
 */

public class AsyncPlaceBet extends AsyncTask {
    private Object returnData;
    private ParlayOdd odd;
    private GameOddsActivity uiContext;
    private double betAmount;

    public AsyncPlaceBet(GameOddsActivity uiContext, ParlayOdd odd, double amount) {
        this.odd = odd;
        this.uiContext = uiContext;
        this.betAmount = amount;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        uiContext.showProgressDialog();
    }

    @Override
    protected void onCancelled(Object o) {
        // do something about the exception
        uiContext.dismissProgressDialog();

        Log.e(AsyncPlaceBet.class.getSimpleName(), "Task cancelled");
        String addendum = "";
        if (returnData instanceof Exception) {
            addendum = Util.getString(Constants.MSG_KEY_APP_NOINET);
            Log.e(AsyncPlaceBet.class.getSimpleName(), "Cancelled", (Exception) returnData);
        }

        Util.showMessageDialog(uiContext, addendum, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            HttpClient client = HttpClientBuilder.create()
                    .setMaxConnTotal(1)
                    .setUserAgent(System.getProperty("http.agent", "Unknown"))
                    .build();

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                    .setRedirectsEnabled(false)
                    .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                    .setCircularRedirectsAllowed(false).build();

            HttpPost post = new HttpPost((Uri.parse(Constants.URL_PLACE_BET_PHP)).toString());
            post.setConfig(config);

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();

            pairs.add(new BasicNameValuePair("key", Constants.PLACE_BET_REQUEST_HEADER_KEY));
            pairs.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded charset=utf-8"));

            StringBuffer sbPostXml = new StringBuffer();
            sbPostXml.append("<meritum>");
            sbPostXml.append("<parlayInfo>");
            sbPostXml.append("<amount>" + String.format("%.2f", betAmount) + "</amount>");
            sbPostXml.append("<user_id>" + Util.getUserId(uiContext) + "</user_id>");
            sbPostXml.append("</parlayInfo>");
            sbPostXml.append("<parlayBet>");
            sbPostXml.append("<bet EventID='" + odd.getGame().getEventID()
                    + "' EventType='" + odd.getGame().getEventType()
                    + "' bet_type_id='" + odd.getOdd().getBetTypeId()
                    + "' team_id='" + odd.getOdd().getTeamId()
                    + "' bet_odd='" + odd.getOdd().getBetOdds()
                    + "' bet_line='" + odd.getOdd().getOutBetLine()
                    + "' bet_value='" + odd.getOdd().getBetValue() + "'/>");

            sbPostXml.append("</parlayBet>");
            sbPostXml.append("</meritum>");

            pairs.add(new BasicNameValuePair("xml", sbPostXml.toString()));

            post.setEntity(new UrlEncodedFormEntity(pairs));

            HttpResponse response = client.execute(post);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sbReturnData = new StringBuffer();
            int character;

            while ( (character = br.read()) != -1 ) {
                sbReturnData.append((char) character);
            }

            br.close();

            String mess [] = Util.explode(sbReturnData.toString());
            String message = null;

            if (mess == null) message = sbReturnData.toString();

            if (message.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK)) {
                MyApplication.getInstance().getUserData()
                        .setWalletAmount(MyApplication.getInstance().getUserData().getWalletAmount() - betAmount);

                MyParlay mp = new MyParlay();
                mp.setAmount(betAmount);
                mp.setTime(Calendar.getInstance().getTime());
                mp.setTeaserPoints(0.0);
                mp.setTeaserOdd(0.0);

                MyBet mb = new MyBet();
                mb.setBetTypeId(odd.getOdd().getBetTypeId());
                mb.setTeamId(odd.getOdd().getTeamId());
                mb.setBetOdd(odd.getOdd().getBetOdds());
                mb.setBetValue(odd.getOdd().getBetValue());

                mb.setTypeName(odd.getOdd().getBetTypeName());
                mb.setTypeName2ndHalf(odd.getOdd().getOutBetLine());
                mb.setEventID(odd.getGame().getEventID());
                mb.setSportID(odd.getSport().getSportID());
                mb.setLeagueID(odd.getGame().getLeagueID());
                mb.setLeagueType(odd.getGame().getLeagueType());
                mb.setStartDate(odd.getGame().getStartDate());
                mb.setEventDesc(Util.formatHomeVsAway(odd.getGame(), uiContext.getApplicationContext()));
                mb.setTeamName(odd.getOdd().getTeamName());
                mb.setTypeLangLine(odd.getOdd().getOutBetLine());
                mb.setTypeLangValue(odd.getOdd().getOutValue());

                mp.getMyBets().add(mb);

                Tune.getInstance().measureEvent("Bet");
                MyApplication.getInstance().getUserData().addParlay(mp);

            } else if (message.equalsIgnoreCase(Constants.SERVER_RESPONSE_WARNING)) {
                String eventList = mess[1];
                if (Util.isPopulated(eventList)) {

                    uiContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            uiContext.dismissProgressDialog();
                            Util.showMessageDialog(uiContext, MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PLACEBETPOPUPALREADYINPROGRESS),
                                    new DialogInterface.OnCancelListener() {
                                        @Override
                                        public void onCancel(DialogInterface dialog) {
                                            dialog.dismiss();
                                        }
                                    });
                        }
                    });

                }
            } else {
                // error
                uiContext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        uiContext.dismissProgressDialog();
                        Util.showMessageDialog(uiContext, MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_TECH_DIFF),
                                new DialogInterface.OnCancelListener() {
                                    @Override
                                    public void onCancel(DialogInterface dialog) {
                                        dialog.dismiss();
                                    }
                                });
                    }
                });
            }

            Log.d(AsyncPlaceBet.class.getSimpleName(), "Returned: " + sbReturnData.toString());
        } catch (Exception e) {
            Log.e(AsyncPlaceBet.class.getSimpleName(), "Exception: ", e);
            returnData = e;
            cancel(true);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        uiContext.dismissProgressDialog();
        uiContext.initializeUI();
    }


}
