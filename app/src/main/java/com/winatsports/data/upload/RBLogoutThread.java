package com.winatsports.data.upload;

/**
 * Created by dkorunek on 18/01/2017.
 */

import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.data.Constants;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;


/**
 * Created by dkorunek on 18/01/2017.
 */

public class RBLogoutThread extends Thread {
    @Override
    public void run() {
        try {
            HttpClient client = HttpClientBuilder.create()
                    .setMaxConnTotal(1)
                    .setUserAgent(System.getProperty("http.agent", "Unknown"))
                    .build();

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                    .setRedirectsEnabled(false)
                    .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                    .setCircularRedirectsAllowed(false).build();

            HttpGet getRequest = new HttpGet(URI.create(MyApplication.getInstance().getUserData().getBookerLogoutUrl()));
            getRequest.setConfig(config);

            HttpResponse response = client.execute(getRequest);
            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sbReturnData = new StringBuffer();
            int character;

            while ( (character = br.read()) != -1 ) {
                sbReturnData.append((char) character);
            }

            br.close();

            //Log.d(RBLogoutThread.class.getSimpleName(), "Return data: " + sbReturnData.toString());
            Log.d(RBLogoutThread.class.getSimpleName(), "Done with RB logout.");

        } catch (Exception e) {
            Log.e(RBLogoutThread.class.getSimpleName(), "", e);
        }
    }
}
