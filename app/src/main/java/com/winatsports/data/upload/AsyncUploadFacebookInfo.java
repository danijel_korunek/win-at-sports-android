package com.winatsports.data.upload;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.activities.GuestActivity;
import com.winatsports.data.Constants;
import com.winatsports.settings.Settings;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by dkorunek on 04/03/16.
 */
public class AsyncUploadFacebookInfo extends AsyncTask {

    public static class FBData {
        public String id;
        public String firstName;
        public String lastName;
        public String birthDate;
        public String gender;
        public String email;
        public String localLanguage;
        public String city;
    }

    private FBData data;
    private Context uiContext;
    private String displayMessage;

    public AsyncUploadFacebookInfo(Context uiContext, FBData fbData) {
        this.data = fbData;
        this.uiContext = uiContext;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            HttpClient client = HttpClientBuilder.create()
                    .setMaxConnTotal(1)
                    .setUserAgent(System.getProperty("http.agent", "Unknown"))
                    .build();

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                    .setRedirectsEnabled(false)
                    .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                    .setCircularRedirectsAllowed(false).build();

            HttpPost post = new HttpPost((Uri.parse(Constants.URL_LOGIN_USER).toString()));
            post.setConfig(config);

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
           /* @"action"       : @"2",
                               @"avatar_url"   : @"",
                               @"mobile_phone" : @"",

                               @"user_d"    : [SWF:@"%li", (long)defines.user_d],
                               @"device_id" : [SWF:@"%li", (long)defines.device_id],
                               @"lang"      : defines.selectedLanguage,

                               @"loginu"         : fixNil([list valueForKeyPath:@"id"]),
                               @"firstname"      : fixNil([list valueForKeyPath:@"first_name"]),
                               @"lastname"       : fixNil([list valueForKeyPath:@"last_name"]),
                               @"birth_date"     : fixNil([list valueForKeyPath:@"birthday"]),
                               @"sex"            : fixNil([list valueForKeyPath:@"gender"]),
                               @"email"          : fixNil([list valueForKeyPath:@"email"]),
                               @"local_language" : fixNil([list valueForKeyPath:@"locale"]),

                               @"country" : @"",
                               @"state"   : @"",
                               @"city"    : fixNil([list valueForKeyPath:@"location.name"]),
                               @"zip"     : @""};
*/
            pairs.add(new BasicNameValuePair("key", Constants.KEY_LOGIN_USER));
            pairs.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded charset=utf-8"));

            pairs.add(new BasicNameValuePair("lang", Settings.getLanguage(uiContext)));
            pairs.add(new BasicNameValuePair("user_d", Settings.getUserD(uiContext)));
            pairs.add(new BasicNameValuePair("device_id", Settings.getDeviceId(uiContext)));

            pairs.add(new BasicNameValuePair("action", "2"));

            pairs.add(new BasicNameValuePair("loginu", data.id));
            pairs.add(new BasicNameValuePair("firstname", data.firstName));
            pairs.add(new BasicNameValuePair("lastname", data.lastName));
            pairs.add(new BasicNameValuePair("birth_date", data.birthDate));
            pairs.add(new BasicNameValuePair("sex", data.gender));
            pairs.add(new BasicNameValuePair("email", data.email));
            pairs.add(new BasicNameValuePair("local_language", data.localLanguage));

            pairs.add(new BasicNameValuePair("country", ""));
            pairs.add(new BasicNameValuePair("state", ""));
            pairs.add(new BasicNameValuePair("city", data.city));
            pairs.add(new BasicNameValuePair("zip", ""));


            post.setEntity(new UrlEncodedFormEntity(pairs));

            HttpResponse response = client.execute(post);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sbReturnData = new StringBuffer();
            int character;

            while ( (character = br.read()) != -1 ) {
                sbReturnData.append((char) character);
            }

            br.close();

            String mess [] = Util.explode(sbReturnData.toString());
            String message = null;

            if (mess == null)
                message = sbReturnData.toString();
            else
                message = mess[0];

            if (message.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK)) {
                if (Util.isPopulated(mess[1])) {
                    // user is logged in, proceed to home activity
                    Log.d(AsyncUploadFacebookInfo.class.getSimpleName(), "return data: " + mess[1]);
                    Settings.setUserR(uiContext, mess[1]);
                    Settings.setLoggedInLocally(uiContext, false);
                    Settings.setLoggedInWithFb(uiContext, true);
                    // TODO: save parlay array
                    /*if (isPopulated(array[1])) {
                        defines.user_r = [helper intFromString:array[1]];
                        [Prefs setBool:NO forKey:@"v3_isLoggedInLocally"];
                        [Prefs setBool:YES forKey:@"v3_isLoggedInFacebook"];
                        [Prefs setInteger:defines.user_r forKey:@"user_r"];
                        [Prefs synchronize];

                        defines.inParlayMode = NO;
                        [defines.parleyArray removeAllObjects];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadParlayView" object:nil];
                        [defines saveParlayArray];
                        dc.ignoreDownloadTimer = YES;
                        [dc startRedownload];
                    }
*/

                } else {
                    // unknown result
                    displayMessage = uiContext.getString(R.string.error_unknown_result);
                    cancel(true);
                }
            } else {
                // show return message
                displayMessage = Util.getString(Constants.MSG_KEY_LOGINTRYLATER) + "\n" + message;
                cancel(true);
            }
        } catch (Exception e) {
            Log.e(AsyncRegisterAccount.class.getSimpleName(), "", e);
            displayMessage = MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_NOINET);
            cancel(true);
        }
        return null;
    }

    @Override
    protected void onCancelled() {
        if (uiContext instanceof GuestActivity) {
            ((GuestActivity) uiContext).dismissProgressDialog();
        }

        if (Util.isPopulated(displayMessage))
            Util.showMessageDialog(uiContext, displayMessage, null);
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        if (uiContext instanceof GuestActivity) {
            ((GuestActivity) uiContext).dismissProgressDialog();
        }

        MyApplication.getInstance().downloadData(uiContext, true, true, false);


    }
}
