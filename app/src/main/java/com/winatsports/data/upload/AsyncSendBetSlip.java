package com.winatsports.data.upload;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.Util;
import com.winatsports.activities.BetSlipActivity;
import com.winatsports.activities.MainActivity;
import com.winatsports.data.Constants;
import com.winatsports.data.MyBet;
import com.winatsports.data.MyParlay;
import com.winatsports.data.elements.BetType;
import com.winatsports.data.elements.Teaser;
import com.winatsports.data.parlaytotal.ParlayOdd;
import com.winatsports.settings.Settings;
import com.tune.Tune;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by dkorunek on 09/05/16.
 */
public class AsyncSendBetSlip extends AsyncTask {

    private BetSlipActivity activity;
    private Teaser teaser;
    private StringBuffer sbReturnData;
    private double betAmount;

    public AsyncSendBetSlip(BetSlipActivity c, Teaser teaser, double betAmount) {
        this.activity = c;
        this.teaser = teaser;
        this.sbReturnData = new StringBuffer();
        this.betAmount = betAmount;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        activity.showProgressDialog();
    }

    @Override
    protected Object doInBackground(Object[] params) {
        if (teaser == null)
            sendParlayToServer();
        else
            sendTeaserToServer();
        return null;
    }

    @Override
    protected void onCancelled() {
        activity.dismissProgressDialog();
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        activity.dismissProgressDialog();

        double walletAmount = MyApplication.getInstance().getUserData().getWalletAmount();
        MyApplication.getInstance().getUserData().setWalletAmount(walletAmount - betAmount);

        // 2) send this parlay to wagers under section ===> in play
        MyParlay mp = new MyParlay();
        mp.setAmount(betAmount);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        String currentDateandTime = sdf.format(c.getTime());

        Date convertedDate = new Date();
        try {
            convertedDate = sdf.parse(currentDateandTime);
        } catch (Exception e) {
            Log.e(AsyncSendBetSlip.class.getSimpleName(), "", e);
        }

        mp.setTime( convertedDate );
        mp.setTeaserPoints(teaser == null ? 0.0 : teaser.getPoints());
        mp.setTeaserOdd(teaser == null ? 0.0 : teaser.getOdd());

        for( int i = 0; i < MyApplication.getInstance().getParlayArray().size(); i++ ) {

            ParlayOdd parlayOdd = MyApplication.getInstance().getParlayArray().get(i);

            MyBet myBet = new MyBet();
            myBet.setBetTypeId(parlayOdd.getOdd().getBetTypeId());
            myBet.setTeamId(parlayOdd.getOdd().getTeamId());
            myBet.setBetOdd((long) parlayOdd.getOdd().getBetOdds());
            myBet.setBetValue((long) MyApplication.getInstance().getParlayArray().get(i).getOdd().getBetValue());

            String typeName = null;
            for (BetType bt : parlayOdd.getSport().getBetTypes()) {
                if (bt.getTypeID() == parlayOdd.getOdd().getBetTypeId()) {
                    typeName = bt.getTypeName();
                }
            }
            myBet.setTypeName(typeName);
            myBet.setTypeName2ndHalf(parlayOdd.getOdd().getOutBetLine());

            myBet.setEventID((long) MyApplication.getInstance().getParlayArray().get(i).getGame().getEventID());
            myBet.setSportID((long) MyApplication.getInstance().getParlayArray().get(i).getGame().getSportID());
            myBet.setLeagueID((long) MyApplication.getInstance().getParlayArray().get(i).getGame().getLeagueID());
            myBet.setLeagueType((long) 1);
            myBet.setStartDate(MyApplication.getInstance().getParlayArray().get(i).getGame().getStartDate());

            myBet.setEventDesc(Util.formatHomeVsAway(parlayOdd.getGame(), MyApplication.getInstance()));

            if( Util.isPopulated( parlayOdd.getOdd().getOutValue() ) ) {
                myBet.setTeamName( MyApplication.getInstance().getAllMessages().get(parlayOdd.getOdd().getOutValue()) );
            } else {
                myBet.setTeamName( parlayOdd.getOdd().getTeamName() );
            }

            mp.getMyBets().add(myBet);
        }

        Tune.getInstance().measureEvent("Bet");
        MyApplication.getInstance().getUserData().addParlay(mp);

        MyApplication.getInstance().getParlayArray().clear();
        Settings.saveParlay(MyApplication.getInstance(), null);

        activity.goBack();
    }

    private void sendParlayToServer() {

        HttpClient client = null;
        StringBuffer xmlBuffer = new StringBuffer();
        xmlBuffer.append("<meritum>");
        xmlBuffer.append("<parlayInfo><amount>"+ betAmount +"</amount>" +
                "<user_id>"+ Util.getUserId(activity)+"</user_id>" +
                "</parlayInfo><parlayBet>");

        for( int index1=0; index1<MyApplication.getInstance().getParlayArray().size(); index1++ ) {

            ParlayOdd parlayOdd = MyApplication.getInstance().getParlayArray().get(index1);
            xmlBuffer.append("<bet EventID='" + parlayOdd.getGame().getEventID() + "'" +
                    " EventType='" + parlayOdd.getGame().getEventType() +"'" +
                    " bet_type_id='" + parlayOdd.getOdd().getBetTypeId() + "'" +
                    " team_id='" + parlayOdd.getOdd().getTeamId() + "'" +
                    " bet_odd='" + parlayOdd.getOdd().getBetOdds() + "'" +
                    " bet_line='" + parlayOdd.getOdd().getBetLine() + "'" +
                    " bet_value='" + parlayOdd.getOdd().getBetValue() + "'/>");
        }

        xmlBuffer.append("</parlayBet>");
        xmlBuffer.append("</meritum>");

        // replace all apostrophes in xmlBuffer with escaped double quotes \" if needed
        try {
            final String URL_USER_COMMON = Constants.URL_PLACE_BET_PHP;
            final String REQUEST_HEADER_KEY = Constants.PLACE_BET_REQUEST_HEADER_KEY;

            URL url = new URL(URL_USER_COMMON);

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                    .setRedirectsEnabled(false)
                    .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                    .setCircularRedirectsAllowed(false).build();

            HttpPost post = new HttpPost(Uri.parse(URL_USER_COMMON).toString());
            post.setConfig(config);

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("key", REQUEST_HEADER_KEY));
            pairs.add(new BasicNameValuePair("lang", Settings.getLanguage(activity))); // TODO:  must reflect settings
            pairs.add(new BasicNameValuePair("xml", xmlBuffer.toString()));
            // TODO: replace with user ID from settings
            pairs.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded charset=utf-8"));

            post.setEntity(new UrlEncodedFormEntity(pairs));

            client = HttpClientBuilder.create()
                    .setMaxConnTotal(1)
                    .setUserAgent(System.getProperty("http.agent", "Unknown"))
                    .build();

            HttpResponse response = client.execute(post);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            //returnUserDataBuffer.append("Response code: " + response.getStatusLine().getStatusCode() + "\n");
            //returnUserDataBuffer.append("Response status: " + response.getStatusLine().getReasonPhrase() + "\n");

            int character;

            while ( (character = br.read()) != -1 ) {
                sbReturnData.append((char) character);
            }
            // moved handling of response to onPostExecute()
            // Log.d(BetSlipPlaceBetDialog.class.getSimpleName(),  BetSlipPlaceBetDialog.class.getSimpleName() + " return data: " + sbPlaceBet);

            br.close();

        } catch ( Exception e ) {
            Log.e(MainActivity.class.getSimpleName(), e.getLocalizedMessage(), e);
        } finally {
            if( client != null )
                client = null;
        }
    }

    private void sendTeaserToServer() {

        HttpClient client = null;
        StringBuffer xmlBuffer = new StringBuffer();
        xmlBuffer.append("<meritum>");
        xmlBuffer.append("<parlayInfo><amount>"+ betAmount +"</amount>" +
                "<user_id>"+ Util.getUserId(activity) + "</user_id>"
                + "<teaser_odd>" + teaser.getOdd() + "</teaser_odd>" + "<teaser_points>"  + teaser.getPoints() + "</teaser_points>"
                + "</parlayInfo><parlayBet>");

        for( int index1=0; index1<MyApplication.getInstance().getParlayArray().size(); index1++ ) {

            ParlayOdd parlayOdd = MyApplication.getInstance().getParlayArray().get(index1);
            xmlBuffer.append("<bet EventID='" + parlayOdd.getGame().getEventID() + "'" +
                    " bet_type_id='" + parlayOdd.getOdd().getBetTypeId() + "'" +
                    " team_id='" + parlayOdd.getOdd().getTeamId() + "'" +
                    " bet_line='" + parlayOdd.getOdd().getBetLine() + "'" +
                    " bet_value='" + parlayOdd.getOdd().getBetValue() + "'/>");
        }
        xmlBuffer.append("</parlayBet>");
        xmlBuffer.append("</meritum>");

        // replace all apostrophes in xmlBuffer with escaped double quotes \" if needed
        try {
            final String URL_USER_COMMON = Constants.URL_PLACE_BET_PHP;
            final String REQUEST_HEADER_KEY = Constants.PLACE_BET_REQUEST_HEADER_KEY;

            URL url = new URL(URL_USER_COMMON);

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                    .setRedirectsEnabled(false)
                    .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                    .setCircularRedirectsAllowed(false).build();

            HttpPost post = new HttpPost(Uri.parse(URL_USER_COMMON).toString());
            post.setConfig(config);

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("key", REQUEST_HEADER_KEY));
            pairs.add(new BasicNameValuePair("lang", Settings.getLanguage(activity))); // TODO:  must reflect settings
            pairs.add(new BasicNameValuePair("xml", xmlBuffer.toString()));
            // TODO: replace with user ID from settings
            pairs.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded charset=utf-8"));

            post.setEntity(new UrlEncodedFormEntity(pairs));

            client = HttpClientBuilder.create()
                    .setMaxConnTotal(1)
                    .setUserAgent(System.getProperty("http.agent", "Unknown"))
                    .build();

            HttpResponse response = client.execute(post);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            //returnUserDataBuffer.append("Response code: " + response.getStatusLine().getStatusCode() + "\n");
            //returnUserDataBuffer.append("Response status: " + response.getStatusLine().getReasonPhrase() + "\n");

            int character;

            while ( (character = br.read()) != -1 ) {
                sbReturnData.append((char) character);
            }
            // moved handling of response to onPostExecute()
            // Log.d(BetSlipPlaceBetDialog.class.getSimpleName(),  BetSlipPlaceBetDialog.class.getSimpleName() + " return data: " + sbPlaceBet);

            br.close();

        } catch ( Exception e ) {
            Log.e(MainActivity.class.getSimpleName(), e.getLocalizedMessage(), e);
        } finally {
            if( client != null )
                client = null;
        }
    }
}
