package com.winatsports.data.upload;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.Util;
import com.winatsports.activities.MainActivity;
import com.winatsports.data.Constants;
import com.winatsports.settings.Settings;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by dkorunek on 15/02/16.
 */
public class AsyncChangeLanguage extends AsyncTask {
    private String language;
    private Context uiContext;

    public AsyncChangeLanguage(Context c, String newLanguage) {
        this.language = newLanguage;
        this.uiContext = c;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        HttpClient client = null;
        StringBuffer xmlBuffer = new StringBuffer();

        xmlBuffer.append("<meritum>");
        xmlBuffer.append("<userSettings>");
        xmlBuffer.append("<lang>" + language + "</lang>");
        xmlBuffer.append("</userSettings>");
        xmlBuffer.append("</meritum>");

        try {

            final String URL_USER_COMMON = Constants.URL_USER_COMMON;
            final String REQUEST_HEADER_KEY = Constants.USER_COMMON_REQUEST_HEADER_KEY;

            URL url = new URL(URL_USER_COMMON);

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                    .setRedirectsEnabled(false)
                    .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                    .setCircularRedirectsAllowed(false).build();

            HttpPost post = new HttpPost(Uri.parse(URL_USER_COMMON).toString());
            post.setConfig(config);

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("key", REQUEST_HEADER_KEY));
            pairs.add(new BasicNameValuePair("lang", Settings.getLanguage(uiContext)));
            pairs.add(new BasicNameValuePair("p", "settings"));
            pairs.add(new BasicNameValuePair("xml", xmlBuffer.toString()));
            pairs.add(new BasicNameValuePair("user_id", Util.getUserId(uiContext)));
            pairs.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded charset=utf-8"));

            post.setEntity(new UrlEncodedFormEntity(pairs));

            client = HttpClientBuilder.create()
                    .setMaxConnTotal(1)
                    .build();

            HttpResponse response = client.execute(post);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            //returnUserDataBuffer.append("Response code: " + response.getStatusLine().getStatusCode() + "\n");
            //returnUserDataBuffer.append("Response status: " + response.getStatusLine().getReasonPhrase() + "\n");
            StringBuffer returnData = new StringBuffer();
            int character;

            while ( (character = br.read()) != -1 ) {
                returnData.append((char) character);
            }

            br.close();

        } catch ( Exception e ) {
            Log.e(MainActivity.class.getSimpleName(), e.getLocalizedMessage(), e);
        } finally {
            if( client != null )
                client = null;
        }

        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        MyApplication.getInstance().downloadData(uiContext, true, true, false);

    }
}
