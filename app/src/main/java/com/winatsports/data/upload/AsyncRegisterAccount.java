package com.winatsports.data.upload;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.activities.GuestActivity;
import com.winatsports.activities.RegistrationActivity;
import com.winatsports.activities.SignInActivity;
import com.winatsports.data.Constants;
import com.winatsports.settings.Settings;
import com.tune.Tune;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by dkorunek on 11/02/16.
 */
public class AsyncRegisterAccount extends AsyncTask {
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String mobile;

    private RegistrationActivity uiContext;

    private String returnData;

    public AsyncRegisterAccount(RegistrationActivity uiContext, String email, String password, String firstName, String lastName, String mobile) {
        this.uiContext = uiContext;

        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mobile = mobile;

        Tune.getInstance().measureEvent("Register");
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onCancelled() {
        // show text in curtain
        if (uiContext instanceof Activity && uiContext.hasWindowFocus())
            Util.showMessageDialog(uiContext, returnData, null);

        uiContext.setRegistrationTask(null);
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            HttpClient client = HttpClientBuilder.create()
                    .setMaxConnTotal(1)
                    .setUserAgent(System.getProperty("http.agent", "Unknown"))
                    .build();

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                    .setRedirectsEnabled(false)
                    .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                    .setCircularRedirectsAllowed(false).build();

            HttpPost post = new HttpPost((Uri.parse(Constants.URL_REGISTER_USER)).toString());
            post.setConfig(config);

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();

            pairs.add(new BasicNameValuePair("key", Constants.REG_USER_REQUEST_HEADER_KEY));
            pairs.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded charset=utf-8"));
            pairs.add(new BasicNameValuePair("email", Util.fixNull(email)));
            pairs.add(new BasicNameValuePair("user_d", Settings.getUserD(uiContext)));
            pairs.add(new BasicNameValuePair("device_id", Settings.getDeviceId(uiContext)));
            pairs.add(new BasicNameValuePair("lang", Settings.getLanguage(uiContext)));
            pairs.add(new BasicNameValuePair("pass", Util.fixNull(password)));
            pairs.add(new BasicNameValuePair("firstname", Util.fixNull(firstName)));
            pairs.add(new BasicNameValuePair("lastname", Util.fixNull(lastName)));
            pairs.add(new BasicNameValuePair("mobile", Util.fixNull(mobile)));
            pairs.add(new BasicNameValuePair("city", ""));

            pairs.add(new BasicNameValuePair("action", "1")); // leave this as such

            post.setEntity(new UrlEncodedFormEntity(pairs));

            HttpResponse response = client.execute(post);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sbReturnData = new StringBuffer();
            int character;

            while ( (character = br.read()) != -1 ) {
                sbReturnData.append((char) character);
            }

            br.close();

            String mess [] = Util.explode(sbReturnData.toString());
            String message = null;

            Log.d(AsyncRegisterAccount.class.getSimpleName(), "Message is: " + message);

            if (mess == null)
                message = sbReturnData.toString();
            else
                message = mess[0];

            if (message.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK)) {
                returnData = mess[1]; // this will be shown in a dialog

            } else {
                // show return message
                returnData = message;
                cancel(true);
            }
        } catch (Exception e) {
            Log.e(AsyncRegisterAccount.class.getSimpleName(), "", e);
            returnData = Util.getString(Constants.MSG_KEY_APP_NOINET);
            cancel(true);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        // TODO register "Register" event in  FBSDKEvents
        uiContext.setRegistrationTask(null);
        // show return data and run login activity
        Util.showMessageDialog(uiContext, returnData, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                uiContext.finish();
                Intent signInIntent = new Intent(uiContext, SignInActivity.class);
                signInIntent.putExtra(SignInActivity.KEY_EMAIL, email);
                signInIntent.putExtra(SignInActivity.KEY_PASSWORD, password);
                uiContext.startActivity(signInIntent);
                uiContext.overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
            }
        });
    }
}
