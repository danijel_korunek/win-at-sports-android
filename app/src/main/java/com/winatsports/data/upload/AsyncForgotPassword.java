package com.winatsports.data.upload;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.activities.PasswordRecoveryActivity;
import com.winatsports.activities.SignInActivity;
import com.winatsports.data.Constants;
import com.winatsports.settings.Settings;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by dkorunek on 17/02/16.
 */
public class AsyncForgotPassword extends AsyncTask {
    private Activity uiContext;
    private String returnData;
    private String email;

    public AsyncForgotPassword(Activity a, String email) {
        this.uiContext = a;
        this.email = email;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            HttpClient client = HttpClientBuilder.create()
                    .setMaxConnTotal(1)
                    .setUserAgent(System.getProperty("http.agent", "Unknown"))
                    .build();

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                    .setRedirectsEnabled(false)
                    .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                    .setCircularRedirectsAllowed(false).build();

            HttpPost post = new HttpPost((Uri.parse(Constants.URL_REGISTER_USER).toString()));
            post.setConfig(config);

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
   /*
            NSDictionary *params = @{@"key"       : @"j3nZ8996%u7B*z43aN56bhG",
                             @"action"    : @"2",
                             @"email"     : fixNil(self.textField1.text),
                             @"lang"      : defines.selectedLanguage,
                             @"user_d"    : [SWF:@"%li", (long)defines.user_d],
                             @"device_id" : [SWF:@"%li", (long)defines.device_id]};
             */
            pairs.add(new BasicNameValuePair("key", Constants.KEY_FORGOT_PASSWORD));
            pairs.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded charset=utf-8"));
            pairs.add(new BasicNameValuePair("lang", Settings.getLanguage(uiContext)));
            pairs.add(new BasicNameValuePair("user_d", Settings.getUserD(uiContext)));
            pairs.add(new BasicNameValuePair("device_id", Settings.getDeviceId(uiContext)));
            pairs.add(new BasicNameValuePair("email", Util.fixNull(email)));
            pairs.add(new BasicNameValuePair("action", "2"));

            post.setEntity(new UrlEncodedFormEntity(pairs));

            HttpResponse response = client.execute(post);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sbReturnData = new StringBuffer();
            int character;

            while ( (character = br.read()) != -1 ) {
                sbReturnData.append((char) character);
            }

            br.close();

            String mess [] = Util.explode(sbReturnData.toString());
            String message = null;

            if (mess == null)
                message = sbReturnData.toString();
            else
                message = mess[0];

            if (message.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK)) {
                returnData = mess[1];
            } else {
                // show return message
                returnData = message;
                cancel(true);
            }

        } catch (Exception e) {
            Log.e(AsyncForgotPassword.class.getSimpleName(), "Error sending forgot password request", e);
            returnData = Util.getString(Constants.MSG_KEY_APP_NOINET);
            cancel(true);
        }
        return null;
    }

    @Override
    protected void onCancelled() {
        if (uiContext instanceof PasswordRecoveryActivity) {
            ((PasswordRecoveryActivity) uiContext).resetTask();
            ((PasswordRecoveryActivity) uiContext).dismissProgressDialog();
        }

        Util.showMessageDialog(uiContext, returnData, null);
    }

    @Override
    protected void onPostExecute(Object o) {
        if (uiContext instanceof PasswordRecoveryActivity) {
            ((PasswordRecoveryActivity) uiContext).resetTask();
            ((PasswordRecoveryActivity) uiContext).dismissProgressDialog();
        }

        Util.showMessageDialog(uiContext, returnData, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                uiContext.finish();
                Intent nextActivityIntent = new Intent(uiContext, SignInActivity.class);
                nextActivityIntent.putExtra(SignInActivity.KEY_EMAIL, email);
                uiContext.startActivity(nextActivityIntent);
                uiContext.overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
            }
        });
    }
}
