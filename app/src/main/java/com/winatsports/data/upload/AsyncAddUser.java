package com.winatsports.data.upload;



import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.Util;
import com.winatsports.activities.MainActivity;
import com.winatsports.activities.SettingsActivity;
import com.winatsports.activities.SignInActivity;
import com.winatsports.activities.SplashScreen;
import com.winatsports.data.Constants;
import com.winatsports.data.download.AsyncDownloadData;
import com.winatsports.settings.Settings;
import com.facebook.login.LoginManager;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by dkorunek on 15/02/16.
 */

public class AsyncAddUser extends AsyncTask {

    private Context uiContext;
    private Object returnData;
    private boolean logout;

    public AsyncAddUser(Context uiContext, boolean logout) {
        this.uiContext = uiContext;
        this.logout = logout;
    }

    @Override
    protected void onCancelled() {
        String addendum = "";
        if (returnData instanceof Exception) {
            addendum = Util.getString(Constants.MSG_KEY_APP_NOINET);
            Log.e(AsyncDownloadData.class.getSimpleName(), "Cancelled", (Exception) returnData);
        }

        if (uiContext instanceof Activity) {
            Util.showMessageDialog(uiContext, addendum, new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    if (uiContext instanceof SplashScreen)
                        ((SplashScreen) uiContext).finish();
                    else {
                        MyApplication.getInstance().setQuitting(true);
                        MyApplication.getInstance().quit();
                        MyApplication.getInstance().shutDownVM();
                    }
                }
            });
        }
    }

    @Override
    protected Object doInBackground(Object[] params) {

        // Delete all user bets from arrayList, so that latter I can add correct user bets
        // And that I will not have duplicate data

        try {
            Util.executeAddUser(uiContext, logout, false);
        } catch (Exception e) {
            Log.e(AsyncDownloadData.class.getSimpleName(), "add_user exception: ", e);
            returnData = e;
            cancel(true);
            return returnData;
        }
        return null;
    }



    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        if (logout) {
            if (Settings.isLoggedInWithFb(uiContext)) {
                Log.d(AsyncAddUser.class.getSimpleName(), "Logged out of Facebook");
                LoginManager.getInstance().logOut();
            }

            Settings.setUserR(uiContext, Settings.DEFAULT_USER_R);
            Settings.setLoggedInWithFb(uiContext, false);
            Settings.setLoggedInLocally(uiContext, false);
            Settings.saveParlay(uiContext, null);
            MyApplication.getInstance().getParlayArray().clear();
            Log.d(AsyncAddUser.class.getSimpleName(), "After logout: user_r = " + Settings.getUserR(uiContext) + " user_d = " + Settings.getUserD(uiContext));

            //new AsyncAddUser(uiContext, false).execute();
            if (uiContext instanceof MainActivity)
                ((MainActivity) uiContext).setupProfileView();
            MyApplication.getInstance().downloadData(uiContext, true, true, false);
            return;
        }

        if (uiContext instanceof SplashScreen) {
            if (!Settings.isFirstTimeLanguageSetup(uiContext)) {
                ;//((SplashScreen) uiContext).showLanguageDialog();
            } else {
                MyApplication.getInstance().downloadData(uiContext, true, true, false);
            }
        } else if (uiContext instanceof MainActivity) {
            // refresh the account view
            ((MainActivity) uiContext).scrollToTab(MyApplication.getInstance().getLastActiveTab());
            MyApplication.getInstance().downloadData(uiContext, true, true, false);
        } else if (uiContext instanceof SettingsActivity) {
            MyApplication.getInstance().downloadData(uiContext, true, true, false);
        } else if (uiContext instanceof SignInActivity) {
            MyApplication.getInstance().downloadData(uiContext, true, true, false);
        } else {
            // uiContext is not an instance of any activity and therefore was called
            // from a background thread
            Log.d(AsyncAddUser.class.getSimpleName(), "Called from a background thread. Nothing to do after end.");
            return;
        }

        Log.d(AsyncAddUser.class.getSimpleName(), "After: user_r = " + Settings.getUserR(uiContext) + " user_d = " + Settings.getUserD(uiContext));

    }


}
