package com.winatsports.data.upload;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Process;
import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.Util;
import com.winatsports.activities.BrowserActivity;
import com.winatsports.activities.GameOddsActivity;
import com.winatsports.activities.GuestActivity;
import com.winatsports.activities.MainActivity;
import com.winatsports.activities.TutorialActivity;
import com.winatsports.data.Constants;
import com.winatsports.settings.Settings;
import com.tune.Tune;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by dkorunek on 18/03/16.
 */
public class AsyncRealBetting extends AsyncTask {
    private Activity uiContext;
    private String returnMessage;
    private int eventId;

    private String urlToShow;

    public static final String URL_REAL = "REAL";

    public AsyncRealBetting (Activity activity, int eventId) {
        this.uiContext = activity;
        this.eventId = eventId;
    }

    @Override
    protected void onCancelled() {
        MyApplication.getInstance().stopRealBettingTask();

        try {
            if (uiContext instanceof MainActivity) {
                ((MainActivity) uiContext).dismissProgressDialog();
            } else if (uiContext instanceof GameOddsActivity) {
                ((GameOddsActivity) uiContext).dismissProgressDialog();
            }
            Util.showMessageDialog(uiContext, returnMessage, null);
        } catch (Exception e) {
            Log.e(AsyncRealBetting.class.getSimpleName(), "", e);
        }

    }

    @Override
    protected void onPreExecute() {
        try {
            if (uiContext instanceof MainActivity) {
                ((MainActivity) uiContext).showProgressDialog();
            } else if (uiContext instanceof GameOddsActivity) {
                ((GameOddsActivity) uiContext).showProgressDialog();
            }
        } catch (Exception e) {
            Log.e(AsyncRealBetting.class.getSimpleName(), "", e);
        }

    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            Log.d(AsyncRealBetting.class.getSimpleName(), "Real betting bg task started");

            Tune.getInstance().measureEvent("Interaction");
            HttpClient client = HttpClientBuilder.create()
                    .setMaxConnTotal(1)
                    .setUserAgent(System.getProperty("http.agent", "Unknown"))
                    .build();

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                    .setRedirectsEnabled(false)
                    .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                    .setCircularRedirectsAllowed(false).build();

            HttpPost post = new HttpPost((Uri.parse(Constants.URL_REAL_BETTING).toString()));
            post.setConfig(config);

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            /*@"
                           NSDictionary *params = @{@"key"     : @"k67J3n89IO34ny346hjU",
                             @"lang"    : selectedLanguage,
                             @"user_id" : [SWF:@"%li", (long)user_r]};
                             */

            pairs.add(new BasicNameValuePair("key", Constants.REAL_BETTING_URL_KEY));
            pairs.add(new BasicNameValuePair("lang", Settings.getLanguage(MyApplication.getInstance())));
            pairs.add(new BasicNameValuePair("user_id", Settings.getUserR(MyApplication.getInstance())));

            UrlEncodedFormEntity entityToSend = new UrlEncodedFormEntity(pairs);
            post.setEntity(entityToSend);
/*
            BufferedReader brtmp = new BufferedReader(new InputStreamReader(entityToSend.getContent()));
            StringBuffer sendBody = new StringBuffer();
            int ch = -1;
            while ((ch = brtmp.read()) != -1) sendBody.append((char) ch);
            brtmp.close();


            Log.d(AsyncRealBetting.class.getSimpleName(), "Sending to server: " + sendBody.toString());*/

            HttpResponse response = client.execute(post);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sbReturnData = new StringBuffer();
            int character;

            while ( (character = br.read()) != -1 ) {
                sbReturnData.append((char) character);
            }

            br.close();

            String mess [] = Util.explode(sbReturnData.toString());
            String message = null;
            //Log.d(AsyncRealBetting.class.getSimpleName(), "Real betting returned: " + sbReturnData.toString());

            if (mess == null)
                message = sbReturnData.toString();
            else
                message = mess[0];

            if (message.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK)) {
                if (Util.isPopulated(mess[1])) {
                    urlToShow = mess[1];
                } else {
                    // unknown result
                    returnMessage = sbReturnData.toString();
                    cancel(true);
                    return returnMessage;
                }
            } else {
                // error
                Log.e(AsyncRealBetting.class.getSimpleName(), "Error: " + message);
                if (Settings.getUserR(MyApplication.getInstance()).equalsIgnoreCase(Settings.DEFAULT_USER_R)) {
                    // not logged in
                    try {
                        if (uiContext instanceof MainActivity) {
                            ((MainActivity) uiContext).dismissProgressDialog();
                        } else if (uiContext instanceof GameOddsActivity) {
                            ((GameOddsActivity) uiContext).dismissProgressDialog();
                        }
                    } catch (Exception e) {
                        Log.e(AsyncRealBetting.class.getSimpleName(), "", e);
                    }

                    uiContext.finish();

                    Intent intent = new Intent(MyApplication.getInstance(), GuestActivity.class);
                    uiContext.startActivity(intent);

                } else {
                    returnMessage = getRealMoneyDownMessage();
                    cancel(true);
                    return returnMessage;
                }
            }
        } catch (Exception e) {
            Log.e(AsyncRealBetting.class.getSimpleName(), "Error communicating with the server", e);
            returnMessage = Util.getString(Constants.MSG_KEY_APP_NOINET);
            cancel(true);
            return e;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {

        try {
            if (uiContext instanceof MainActivity) {
                ((MainActivity) uiContext).dismissProgressDialog();
            } else if (uiContext instanceof GameOddsActivity) {
                ((GameOddsActivity) uiContext).dismissProgressDialog();
            }
        } catch (Exception e) {
            Log.e(AsyncRealBetting.class.getSimpleName(), "", e);
        }

        if (!MyApplication.getInstance().getUserData().isConfirmedUser()) {
            Intent browserActivity = new Intent(uiContext, BrowserActivity.class);
            browserActivity.putExtra(BrowserActivity.KEY_URL, urlToShow);
            browserActivity.putExtra(GameOddsActivity.KEY_EVENT_ID, eventId);
            uiContext.startActivity(browserActivity);
            uiContext.finish();
        } else {

            if (!Settings.isRealBettingInSafari(uiContext)) {
                if (Util.isNetworkConnected(uiContext)) {
                    Intent browserActivity = new Intent(uiContext, BrowserActivity.class);
                    browserActivity.putExtra(BrowserActivity.KEY_URL, urlToShow);
                    browserActivity.putExtra(GameOddsActivity.KEY_EVENT_ID, eventId);
                    uiContext.startActivity(browserActivity);
                    uiContext.finish();
                } else {
                    // skip - we don't want to show
                }
            } else {
                Util.showUrlInBrowser(urlToShow);
            }
        }

        MyApplication.getInstance().stopRealBettingTask();
    }

    private String getRealMoneyDownMessage() {
        StringBuffer sb = new StringBuffer();
        sb.append(Util.getString(Constants.MSG_KEY_REALMONEYDOWN) + "\n");
        sb.append(Util.getString(Constants.MSG_KEY_REALMONEYDOWN2) + "\n");
        sb.append(Util.getString(Constants.MSG_KEY_REALMONEYDOWN3) + "\n");
        sb.append(Util.getString(Constants.MSG_KEY_REALMONEYDOWN4) + "\n");
        sb.append(Util.getString(Constants.MSG_KEY_REALMONEYDOWN5) + "\n");
        sb.append(Util.getString(Constants.MSG_KEY_REALMONEYDOWN6) + "\n");
        return sb.toString();
    }
}
