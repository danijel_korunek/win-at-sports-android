package com.winatsports.data.upload;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.Util;
import com.winatsports.data.Constants;
import com.winatsports.data.firstpage.HomeNotification;
import com.winatsports.settings.Settings;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by dkorunek on 15/03/16.
 */
public class AsyncRemoveNotification extends AsyncTask {

    private HomeNotification notification;

    public AsyncRemoveNotification(HomeNotification notification) {
        this.notification = notification;
    }

    @Override
    protected Object doInBackground(Object[] params) {

        try {
            HttpClient client = HttpClientBuilder.create()
                    .setMaxConnTotal(1)
                    .setUserAgent(System.getProperty("http.agent", "Unknown"))
                    .build();

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                    .setRedirectsEnabled(false)
                    .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                    .setCircularRedirectsAllowed(false).build();

            HttpPost post = new HttpPost((Uri.parse(Constants.URL_USER_COMMON)).toString());
            post.setConfig(config);

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();

            pairs.add(new BasicNameValuePair("key", Constants.USER_COMMON_REQUEST_HEADER_KEY));
            pairs.add(new BasicNameValuePair("p", "note"));
            pairs.add(new BasicNameValuePair("lang", Settings.getLanguage(MyApplication.getInstance())));
            pairs.add(new BasicNameValuePair("user_id", Util.getUserId(MyApplication.getInstance())));

            pairs.add(new BasicNameValuePair("note_id", String.valueOf(notification.getNoteID())));

            post.setEntity(new UrlEncodedFormEntity(pairs));
            for (NameValuePair pair : pairs) {
                Log.d(AsyncRemoveNotification.class.getSimpleName(), "Pair: " + pair.getName() + "->" + pair.getValue());
            }

            HttpResponse response = client.execute(post);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sbReturnData = new StringBuffer();
            int character;

            while ( (character = br.read()) != -1 ) {
                sbReturnData.append((char) character);
            }

            br.close();

            Log.d(AsyncRemoveNotification.class.getSimpleName(), "Notification removed or not? Returned: " + sbReturnData.toString());
        } catch (Exception e) {
            Log.e(AsyncRemoveNotification.class.getSimpleName(), "Exception: ", e);
         }
        return null;
    }
}
