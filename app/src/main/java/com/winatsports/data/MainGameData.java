package com.winatsports.data;

import com.winatsports.data.header.HeaderMainClass;
import com.winatsports.data.elements.Game;
import com.winatsports.data.elements.Sport;

import java.util.ArrayList;

/**
 * Created by broda on 30/10/2015.
 */

public class MainGameData {

    // later I will create one arrayList => AllSports in which I will save data from class => sportsInfo
    // later I will create one arrayList => AllActiveGames in which I will save data from class => game
    private HeaderMainClass header;
    public ArrayList<Sport> allSportsArrayList;
    public ArrayList<Game> allActiveGamesArrayList;

    public MainGameData() {
        header = new HeaderMainClass();
        allSportsArrayList = new ArrayList<Sport>();
        allActiveGamesArrayList = new ArrayList<Game>();
    }

    public HeaderMainClass getHeader() {
        return header;
    }

    public ArrayList<Sport> getSportsArrayList() {
        return allSportsArrayList;
    }

    public ArrayList<Game> getGamesArrayList() {
        return allActiveGamesArrayList;
    }



}


