package com.winatsports.data.download;

import android.content.Intent;
import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.Util;
import com.winatsports.data.MyParlay;

import java.util.ArrayList;

/**
 * Created by dkorunek on 24/08/16.
 */
public class DownloadHistoryThread extends Thread {

    public volatile boolean running;

    public DownloadHistoryThread() {
        running = false;
    }

    @Override
    public void run() {
        running = true;

        try {
            Log.i(DownloadHistoryThread.class.getSimpleName(), "Started downloading history records");

            int size = MyApplication.getInstance().getHistoryWagers().size();
            if (size > 0) {
                ArrayList<MyParlay> historyRecords = Util.downloadHistoryForWagersView(size);
                Log.i(DownloadHistoryThread.class.getSimpleName(), "Downloaded: " + historyRecords.size());

                if (historyRecords.size() == 0) {
                    running = false;
                    return;
                }

                MyApplication.getInstance().getHistoryWagers().addAll(historyRecords);

                Intent refreshWagers = new Intent(MyApplication.ACTION_ADD_WAGERS_HISTORY);
                MyApplication.getInstance().sendBroadcast(refreshWagers);
            } else {
                // nothing... doesn't matter
            }

            Log.i(DownloadHistoryThread.class.getSimpleName(), "Ended downloading history records");
        } catch (Exception e) {
            Log.e(DownloadHistoryThread.class.getSimpleName(), "Error", e);
        }

        running = false;
    }
}
