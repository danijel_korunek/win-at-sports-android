package com.winatsports.data.download;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.activities.GuestActivity;
import com.winatsports.activities.MainActivity;
import com.winatsports.activities.SettingsActivity;
import com.winatsports.activities.SignInActivity;
import com.winatsports.activities.SplashScreen;
import com.winatsports.activities.TutorialActivity;
import com.winatsports.data.Constants;
import com.winatsports.data.MyParlay;
import com.winatsports.data.elements.BetType;
import com.winatsports.data.elements.Game;
import com.winatsports.data.elements.League;
import com.winatsports.data.elements.Odd;
import com.winatsports.data.elements.Sport;
import com.winatsports.data.elements.SportIcon;
import com.winatsports.data.elements.Team;
import com.winatsports.data.parsers.XmlDefaultLanguagesHelper;
import com.winatsports.data.parsers.XmlFirstPageFeaturedGamesHelper;
import com.winatsports.data.parsers.XmlFirstPageFeaturedLeaguesHelper;
import com.winatsports.data.parsers.XmlFirstPageUpNextHelper;
import com.winatsports.data.parsers.XmlGameDataHelper;
import com.winatsports.data.parsers.XmlLanguageCodesHelper;
import com.winatsports.data.parsers.XmlMessagesHelper;
import com.winatsports.dialogs.CustomProgressDialog;
import com.winatsports.settings.Settings;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;

public class AsyncDownloadData extends AsyncTask {

    private ProgressDialog progressDialog;
    private Context context;

    private StringBuffer languageDefaults;
    private StringBuffer messages;
    private StringBuffer mainDataPart1;
    private StringBuffer mainDataPart2;
    private StringBuffer firstPageFeaturedGames;
    private StringBuffer firstPageFeaturedLeagues;
    private StringBuffer firstPageUpNext;
    private StringBuffer historyForWagers;
    private Object returnData;
    private HttpClient client;
    private boolean showProgress;
    private boolean reDownload;
    private boolean newVersionAvailable;
    private boolean isMaintenanceActive;
    private boolean silentStop;

    private boolean quitProgram;

    public AsyncDownloadData(Context context, boolean showProgress, boolean reDownload){
        this.context = context;
        this.showProgress = showProgress;
        this.reDownload = reDownload;
        this.quitProgram = false;
        this.newVersionAvailable = false;
        this.isMaintenanceActive = false;
        silentStop = false;

        if (showProgress)
            progressDialog = CustomProgressDialog.ctor(this.context);

        languageDefaults = new StringBuffer();
        messages = new StringBuffer();
        mainDataPart1 = new StringBuffer();
        mainDataPart2 = new StringBuffer();
        firstPageFeaturedGames = new StringBuffer();
        firstPageFeaturedLeagues = new StringBuffer();
        firstPageUpNext = new StringBuffer();
        historyForWagers = new StringBuffer();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        //Log.d(AsyncDownloadData.class.getSimpleName(), "Context = " + context.getClass().getSimpleName());

        client = HttpClientBuilder.create()
                .setMaxConnTotal(1)
                .setUserAgent(System.getProperty("http.agent", "Unknown"))
                .build();

        if (!reDownload)
            MyApplication.getInstance().setAllowReDownload(false);

        showProgressDialog();

        MyApplication.getInstance().setIsDownloading(true);
        context.sendBroadcast(new Intent(MyApplication.ACTION_DOWNLOAD_STATE));
    }

    @Override
    protected Object doInBackground(Object[] params) {

        try {
            downloadMessages();
            XmlMessagesHelper xmlMessagesHelper = new XmlMessagesHelper();
            xmlMessagesHelper.parseXMLFile(messages.toString(), reDownload);
            downloadMainData();
            XmlGameDataHelper xmlGameDataHelper = new XmlGameDataHelper();
            xmlGameDataHelper.parseXMLFile(mainDataPart1.toString(), reDownload);

            int maintenance;
            if (reDownload) {
                maintenance = MyApplication.getInstance().getHeaderMainClassCopy().getAppMaintenance();
            } else {
                maintenance = MyApplication.getInstance().getHeaderMainClass().getAppMaintenance();
            }

            if (maintenance == Constants.APP_MAINTENANCE_ON) {
                isMaintenanceActive = true;
                cancel(true);
                return null;
            }

        } catch (Exception e) {
            Log.e(AsyncDownloadData.class.getSimpleName(), "Download exception: ", e);
            returnData = e;
            cancel(true);
            return returnData;
        }

        try {
            //Log.d(AsyncDownloadData.class.getSimpleName(), "Language: " + Settings.getLanguage(context));
            Util.executeAddUser(context, false, reDownload);

            int ver = MyApplication.getInstance().getHeaderMainClass().getAppVersionProtect();
            if (ver > Constants.APP_VERSION) {
                newVersionAvailable = true;
                cancel(true);
                return null;
            }

            downloadLanguageDefaults();
            downloadHomeScreenFeaturedGames();
            downloadHomeScreenFeaturedLeagues();
            downloadHomeScreenUpNext();

            ArrayList<MyParlay> wagerHistory = Util.downloadHistoryForWagersView(0);
            if (reDownload)
                MyApplication.getInstance().getHistoryWagersCopy().addAll(wagerHistory);
            else
                MyApplication.getInstance().getHistoryWagers().addAll(wagerHistory);
        } catch (Exception e) {
            Log.e(AsyncDownloadData.class.getSimpleName(), "Download exception: ", e);
            returnData = e;
            cancel(true);
            return returnData;
        }

        try {
            XmlDefaultLanguagesHelper xmlDefaultLanguagesHelper = new XmlDefaultLanguagesHelper();
            xmlDefaultLanguagesHelper.parseXMLFile(languageDefaults.toString(), reDownload);

            XmlLanguageCodesHelper xmlLanguageCodesHelper = new XmlLanguageCodesHelper();
            xmlLanguageCodesHelper.parseXMLFile(mainDataPart2.toString(), reDownload);

            XmlFirstPageFeaturedGamesHelper featuredGamesHelper = new XmlFirstPageFeaturedGamesHelper();
            featuredGamesHelper.parseXMLFile(firstPageFeaturedGames.toString(), reDownload);
            XmlFirstPageFeaturedLeaguesHelper featuredLeaguesHelper = new XmlFirstPageFeaturedLeaguesHelper();
            featuredLeaguesHelper.parseXMLFile(firstPageFeaturedLeagues.toString(), reDownload);
            XmlFirstPageUpNextHelper upNextHelper = new XmlFirstPageUpNextHelper();
            upNextHelper.parseXMLFile(firstPageUpNext.toString(), reDownload);

        } catch (Exception e) {
            // we should abort if any parsing fails.
            returnData = e;
            cancel(true);
            return returnData;
        }

        downloadIconsForSports(reDownload);
        linkLeaguesWithSports(reDownload);
        linkGamesWithLeagues(reDownload);
        linkTeamsWithGames(reDownload);
        linkBetTypesWithOdds(reDownload);

        if (reDownload) {
            MyApplication.getInstance().switchArrays();

            Intent arraysSwitchedIntent = new Intent(MyApplication.ACTION_ARRAYS_SWITCHED);
            if (showProgress && reDownload && context instanceof  MainActivity) {
                arraysSwitchedIntent.putExtra(MyApplication.KEY_REENTERED, MyApplication.getInstance().wasAppInBgMoreThanXSeconds);
            }
            MyApplication.getInstance().sendBroadcast(arraysSwitchedIntent);
        }

        if (MyApplication.getInstance().getHeaderMainClass().getAppMaintenance() == Constants.APP_MAINTENANCE_ON) {
            quitProgram = true;
            cancel(true);
        }

        MyApplication.getInstance().removeEmptyScheduleFromData();
        MyApplication.getInstance().restoreParlayArray();

        return returnData;
    }

    @Override
    protected void onPostExecute(Object o) {

        super.onPostExecute(o);

        if (!reDownload)
            MyApplication.getInstance().setAllowReDownload(true);

        if (showProgress) {
            try {
                // this can be executed when all the windows have been removed
                progressDialog.dismiss();
            } catch (Exception ignored) {}

            MyApplication.getInstance().getExecutorService().submit(new DownloadLiveScoresThread(context));
        }

        Log.d(AsyncDownloadData.class.getSimpleName(), "Redownload = " + reDownload);
        Log.d(AsyncDownloadData.class.getSimpleName(), "Num sports " + MyApplication.getInstance().getAllSports().size());
        Log.d(AsyncDownloadData.class.getSimpleName(), "Num messages " + MyApplication.getInstance().getAllMessages().size());
        Log.d(AsyncDownloadData.class.getSimpleName(), "Num bets " + MyApplication.getInstance().getUserData().getUserBets().size());
        Log.d(AsyncDownloadData.class.getSimpleName(), "Num games " + MyApplication.getInstance().getAllGames().size());
        Log.d(AsyncDownloadData.class.getSimpleName(), "Num leagues " + MyApplication.getInstance().getAllLeagues().size());
        Log.d(AsyncDownloadData.class.getSimpleName(), "Num live terms " + MyApplication.getInstance().getAllLiveTerms().size());
        Log.d(AsyncDownloadData.class.getSimpleName(), "Wallet amount: " + MyApplication.getInstance().getUserData().getWalletAmount());

        Intent nextActivityIntent = null;
        if (MyApplication.getInstance().getHeaderMainClass().isShowingAppTutorial()
                && Settings.getTimesRun(context) == (Settings.DEFAULT_TIMES_RUN+1) )
            nextActivityIntent = new Intent(context, TutorialActivity.class);
        else if (Settings.getUserR(context).equals(Settings.DEFAULT_USER_R)) {
            nextActivityIntent = new Intent(context, GuestActivity.class);
        } else {
            nextActivityIntent = new Intent(context, MainActivity.class);
        }

        if (context instanceof SplashScreen) {
            SplashScreen activity = (SplashScreen) context;
            // finish this SplashScreen activity
            activity.finish();
            context.startActivity(nextActivityIntent);
        } else if (context instanceof SettingsActivity) {
            // nothing
        } else if (context instanceof SignInActivity) {
            ((SignInActivity) context).finish();
            nextActivityIntent.setClass(context, MainActivity.class);
            context.startActivity(nextActivityIntent);
        } else if (context instanceof MainActivity) {
            //((MainActivity) context).recreateViews();
            //mainViewsRedrawn = true;
        } else if (context instanceof GuestActivity) {
            ((GuestActivity) context).finish();
            nextActivityIntent.setClass(context, MainActivity.class);
            context.startActivity(nextActivityIntent);
        }

        this.progressDialog = null;
        this.languageDefaults = null;
        this.messages = null;
        this.mainDataPart1 = null;
        this.mainDataPart2 = null;
        this.firstPageFeaturedGames = null;
        this.firstPageFeaturedLeagues = null;
        this.firstPageUpNext = null;
        this.client = null;

        MyApplication.getInstance().setLastTimeDownloadRan();
        MyApplication.getInstance().setIsDownloading(false);

        Intent updateIntent = new Intent(MyApplication.ACTION_DOWNLOAD_STATE);
        context.sendBroadcast(updateIntent);

        Log.d(AsyncDownloadData.class.getSimpleName(), "Download completed. shown progress = " + showProgress);
    }

    @Override
    protected void onCancelled(Object o) {
        // handle cancellation

        Log.d(AsyncDownloadData.class.getSimpleName(), "Cancelled task");

        if (silentStop) {
            Log.d(AsyncDownloadData.class.getSimpleName(), "Silently stopped.");
            return;
        }

        String addendum = "";
        if (returnData instanceof Exception || returnData == null) {
            addendum = context.getString(R.string.error_no_internet);
            Log.e(AsyncDownloadData.class.getSimpleName(), "Cancelled with exception", (Exception) returnData);
            quitProgram = true;
        }

        if (!Util.isNetworkConnected(context)) {
            quitProgram = true;
        }

        if (isMaintenanceActive) {
            addendum = MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_MAINTENANCE);
        }

        if (context != null && context instanceof Activity) {
            if (newVersionAvailable) {
                Util.showUpdateDialog(context);
            } else {
                hideProgressDialog();

                Util.showMessageDialog(context, addendum, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        if (context instanceof SplashScreen) {
                            ((SplashScreen) context).finish();
                            MyApplication.getInstance().quit();
                            MyApplication.getInstance().shutDownVM();
                        }

                        if (quitProgram) {
                            MyApplication.getInstance().quit();
                            MyApplication.getInstance().shutDownVM();
                        }
                    }

                });
            }
        }
    }

    public void hideProgressDialog() {
        if (context instanceof Activity && progressDialog != null && progressDialog.isShowing()) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            });

        }
    }

    public void showProgressDialog() {
        if (context instanceof Activity && showProgress && progressDialog != null) {
            progressDialog.show();
        }
    }

    private void downloadLanguageDefaults() throws Exception {

        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                .setRedirectsEnabled(false)
                .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                .setCircularRedirectsAllowed(false).build();

        HttpGet httpGet = new HttpGet(Uri.parse(Constants.URL_DEFAULT_LANGUAGES ).toString());
        httpGet.setConfig(config);

        HttpResponse response = client.execute(httpGet);

        BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(response.getEntity().getContent())));
        StringBuilder result = new StringBuilder();
        String line;
        while( ( line = reader.readLine() ) != null ) {
            result.append(line);
        }

        languageDefaults.append(result.toString());
        reader.close();
    }

    private void downloadMessages() throws Exception {
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                .setRedirectsEnabled(false)
                .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                .setCircularRedirectsAllowed(false).build();

        HttpGet httpGet = new HttpGet(Uri.parse(Constants.URL_MESSAGES.replace("XX", Settings.getLanguage(context))).toString());
        httpGet.setConfig(config);

        HttpResponse response = client.execute(httpGet);

        BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(response.getEntity().getContent())));
        StringBuilder result = new StringBuilder();
        String line;
        while( ( line = reader.readLine() ) != null ) {
            result.append(line);
        }

        messages.append(result.toString());
        reader.close();
    }

    private void downloadMainData() throws Exception {

        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                .setRedirectsEnabled(false)
                .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                .setCircularRedirectsAllowed(false).build();

        HttpGet httpGet = new HttpGet(Uri.parse(Constants.URL_GAME_DATA
                .replace(Constants.URL_TOKEN_TO_BE_REPLACED, Settings.getLanguage(context))).toString());
        httpGet.setConfig(config);
        HttpResponse response = client.execute(httpGet);

        BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(response.getEntity().getContent())));
        StringBuilder result = new StringBuilder();
        String line;
        while( ( line = reader.readLine() ) != null ) {
            result.append(line);
        }

        String[] getMainData = result.toString().split(Constants.MAIN_XML_SEPARATOR);
        mainDataPart1.append( getMainData[0].toString() );
        mainDataPart2.append( getMainData[1].toString() );
        reader.close();

    }

    private void downloadHomeScreenFeaturedGames() throws Exception {
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                .setRedirectsEnabled(false)
                .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                .setCircularRedirectsAllowed(false).build();

        HttpGet httpGet = new HttpGet(Uri.parse(Constants.URL_HOME_SCREEN_FEATURED_GAMES_SECTION_1).toString());
        httpGet.setConfig(config);
        HttpResponse response = client.execute(httpGet);

        BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(response.getEntity().getContent())));
        StringBuilder result = new StringBuilder();
        String line;
        while( ( line = reader.readLine() ) != null ) {
            result.append(line);
        }
        firstPageFeaturedGames.append(result.toString() + "\n");
        reader.close();

    }

    private void downloadHomeScreenFeaturedLeagues() throws Exception {
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                .setRedirectsEnabled(false)
                .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                .setCircularRedirectsAllowed(false).build();

        HttpGet httpGet = new HttpGet(Uri.parse(Constants.URL_HOME_SCREEN_FEATURED_LEAGUES_SECTION_2).toString());
        httpGet.setConfig(config);
        HttpResponse response = client.execute(httpGet);

        BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(response.getEntity().getContent())));
        StringBuilder result = new StringBuilder();
        String line;

        while ((line = reader.readLine()) != null) {
            result.append(line);
        }

        firstPageFeaturedLeagues.append(result.toString() + "\n");
        reader.close();

    }

    private void downloadHomeScreenUpNext() throws Exception {

        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                .setRedirectsEnabled(false)
                .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                .setCircularRedirectsAllowed(false).build();
        HttpGet httpGet = new HttpGet(Uri.parse(Constants.URL_HOME_SCREEN_UP_NEXT_SECTION_3).toString());
        httpGet.setConfig(config);
        HttpResponse response = client.execute(httpGet);

        BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(response.getEntity().getContent())));
        StringBuilder result = new StringBuilder();
        String line;

        while ((line = reader.readLine()) != null) {
            result.append(line);
        }

        firstPageUpNext.append(result.toString() + "\n");
        reader.close();
    }

    private void downloadIconsForSports(boolean reDownload) {
        for( int i = 0; !reDownload ? i < MyApplication.getInstance().getAllSports().size() : i < MyApplication.getInstance().getAllSportsCopy().size(); i++ ){
            Sport tempSport = !reDownload ? MyApplication.getInstance().getAllSports().get(i) : MyApplication.getInstance().getAllSportsCopy().get(i);

            MyApplication.getInstance().getExecutorService().submit(new ImageDownloadThread(tempSport.getSportID()));
        }

    }

    private void linkLeaguesWithSports(boolean reDownload) {
        if (!reDownload) {
            if (MyApplication.getInstance().getAllLeagues().size() > 0 &&
                    MyApplication.getInstance().getAllSports().size() > 0) {

                for (int i = 0; i < MyApplication.getInstance().getAllLeagues().size(); i++) {
                    League league = MyApplication.getInstance().getAllLeagues().get(i);

                    for (int j = 0; j < MyApplication.getInstance().getAllSports().size(); j++) {
                        Sport sport = MyApplication.getInstance().getAllSports().get(j);
                        if (sport.getSportID() == league.getSportID() && league.getStatus() == Sport.SPORT_ACTIVE) {
                            sport.getLeagues().add(league);
                            break;
                        }
                    }
                }
            }
        } else {
            if (MyApplication.getInstance().getAllLeaguesCopy().size() > 0 &&
                    MyApplication.getInstance().getAllSportsCopy().size() > 0) {

                for (int i = 0; i < MyApplication.getInstance().getAllLeaguesCopy().size(); i++) {
                    League league = MyApplication.getInstance().getAllLeaguesCopy().get(i);

                    for (int j = 0; j < MyApplication.getInstance().getAllSportsCopy().size(); j++) {
                        Sport sport = MyApplication.getInstance().getAllSportsCopy().get(j);
                        if (sport.getSportID() == league.getSportID() && league.getStatus() == Sport.SPORT_ACTIVE) {
                            sport.getLeagues().add(league);
                            break;
                        }
                    }
                }
            } else {
                Log.d(AsyncDownloadData.class.getSimpleName(), "Copy array sizes are 0");
            }
        }
    }

    private void linkGamesWithLeagues(boolean reDownload) {
        if( !reDownload ) {
            if (MyApplication.getInstance().getAllGames().size() > 0 &&
                    MyApplication.getInstance().getAllLeagues().size() > 0) {

                for (int i = 0; i < MyApplication.getInstance().getAllGames().size(); i++) {
                    Game game = MyApplication.getInstance().getAllGames().get(i);

                    for (int j = 0; j < MyApplication.getInstance().getAllLeagues().size(); j++) {
                        League league = MyApplication.getInstance().getAllLeagues().get(j);

                        if (game.getLeagueID() == league.getLeagueID()) {
                            league.getGameArrayList().add(game);
                            break;
                        }
                    }
                }
            }
        } else {
            if (MyApplication.getInstance().getAllGamesCopy().size() > 0 &&
                    MyApplication.getInstance().getAllLeaguesCopy().size() > 0) {

                for (int i = 0; i < MyApplication.getInstance().getAllGamesCopy().size(); i++) {
                    Game game = MyApplication.getInstance().getAllGamesCopy().get(i);

                    for (int j = 0; j < MyApplication.getInstance().getAllLeaguesCopy().size(); j++) {
                        League league = MyApplication.getInstance().getAllLeaguesCopy().get(j);

                        if (game.getLeagueID() == league.getLeagueID()) {
                            league.getGameArrayList().add(game);
                            break;
                        }
                    }
                }
            } else {
                Log.d(AsyncDownloadData.class.getSimpleName(), "Copy array sizes are 0");
            }
        }
    }

    private void linkBetTypesWithOdds(boolean reDownload) {
        for (Sport sport : !reDownload ? MyApplication.getInstance().getAllSports() :  MyApplication.getInstance().getAllSportsCopy()) {
            for (BetType betType : sport.getBetTypes()) {
                for (League league : sport.getLeagues()) {
                    for (Game game : league.getGameArrayList()) {
                        for (Odd odd : game.getOddArrayList()) {
                            if (!Util.isPopulated(odd.getBetTypeName())
                                    && betType.getSportID() == sport.getSportID()
                                    && odd.getBetTypeId() == betType.getTypeID())
                                odd.setBetTypeName(betType.getTypeName());
                        }
                    }
                }
            }
        }
    }

    // 1) TEAM UNDER SPORT OBJECT ARE MISSING STATUS, AND THIS IS WHAT I'M HERE DOING, ADDING TEAM STATUS ==> TEAMS UNDER SPORT OBJECT
    // 2) TEAM UNDER GAMES OBJECT ARE MISSING NAME ADN SportID, AND THIS IS WHAT I'M HERE DOING, ADDING NAME ADN SportID ==> TEAMS UNDER GAME OBJECT
    private void linkTeamsWithGames(boolean reDownload)  {

        if (!reDownload) {
            if (MyApplication.getInstance().getAllGames().size() > 0 &&
                    MyApplication.getInstance().getAllSports().size() > 0) {

                int counterOnlyToBreakLoop = 0; // I'm using this counter only to break loop, so that application will work faster

                for (int i = 0; i < MyApplication.getInstance().getAllGames().size(); i++) {

                    Game game = MyApplication.getInstance().getAllGames().get(i);
                    for (int x = 0; x < game.getTeamArrayList().size(); x++) {

                        Team team = game.getTeamArrayList().get(x);
                        for (int j = 0; j < MyApplication.getInstance().getAllSports().size(); j++) {

                            Sport sport = MyApplication.getInstance().getAllSports().get(j);
                            if (game.getSportID() == sport.getSportID()) {

                                for (int y = 0; y < sport.getTeams().size(); y++) {

                                    Team teamSport = sport.getTeams().get(y);
                                    if (team.getTeamID() == sport.getTeams().get(y).getTeamID()) {
                                        counterOnlyToBreakLoop++;
                                        teamSport.setTeamStatus(team.getTeamStatus());
                                        game.getTeamArrayList().get(x).setName(sport.getTeams().get(y).getName());
                                        game.getTeamArrayList().get(x).setSportID(sport.getTeams().get(y).getSportID());
                                    } else if (counterOnlyToBreakLoop == game.getTeamArrayList().size()) {
                                        counterOnlyToBreakLoop = 0;
                                        break;
                                    }
                                }
                                if (counterOnlyToBreakLoop == 0) {
                                    counterOnlyToBreakLoop = 0;
                                    break;
                                }
                            }
                        }
                        if (counterOnlyToBreakLoop == 0)
                            break;
                    }
                }
            }
        } else {
            if (MyApplication.getInstance().getAllGamesCopy().size() > 0 &&
                    MyApplication.getInstance().getAllSportsCopy().size() > 0) {

                int counterOnlyToBreakLoop = 0; // I'm using this counter only to break loop, so that application will work faster

                for (int i = 0; i < MyApplication.getInstance().getAllGamesCopy().size(); i++) {

                    Game game = MyApplication.getInstance().getAllGamesCopy().get(i);
                    for (int x = 0; x < game.getTeamArrayList().size(); x++) {

                        Team team = game.getTeamArrayList().get(x);
                        for (int j = 0; j < MyApplication.getInstance().getAllSportsCopy().size(); j++) {

                            Sport sport = MyApplication.getInstance().getAllSportsCopy().get(j);
                            if (game.getSportID() == sport.getSportID()) {

                                for (int y = 0; y < sport.getTeams().size(); y++) {

                                    Team teamSport = sport.getTeams().get(y);
                                    if (team.getTeamID() == sport.getTeams().get(y).getTeamID()) {
                                        counterOnlyToBreakLoop++;
                                        teamSport.setTeamStatus(team.getTeamStatus());
                                        game.getTeamArrayList().get(x).setName(sport.getTeams().get(y).getName());
                                        game.getTeamArrayList().get(x).setSportID(sport.getTeams().get(y).getSportID());
                                    } else if (counterOnlyToBreakLoop == game.getTeamArrayList().size()) {
                                        counterOnlyToBreakLoop = 0;
                                        break;
                                    }
                                }
                                if (counterOnlyToBreakLoop == 0) {
                                    counterOnlyToBreakLoop = 0;
                                    break;
                                }
                            }
                        }
                        if (counterOnlyToBreakLoop == 0)
                            break;
                    }
                }
            } else {
                Log.d(AsyncDownloadData.class.getSimpleName(), "Copy array sizes are 0");
            }
        }

    }

    public void silentlyStop() {
        silentStop = true;
        hideProgressDialog();
        cancel(true);
    }
}