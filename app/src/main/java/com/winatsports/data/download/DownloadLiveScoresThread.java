package com.winatsports.data.download;


import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.data.Constants;
import com.winatsports.data.elements.Game;
import com.winatsports.data.elements.Team;
import com.winatsports.data.livescores.LiveEventScore;
import com.winatsports.data.parsers.XmlLiveScoresHelper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.GZIPInputStream;


/**
 * Created by broda on 02/12/2015.
 */

public class DownloadLiveScoresThread extends Thread {

    private Context context;

    public static final String ACTION_REFRESH_LIVE_SCORES = "com.sportsbetting.data.download.REFRESH_LIVE_SCORES";

    private StringBuffer liveScores;


    public DownloadLiveScoresThread(Context context) {
        this.context = context;
        liveScores = new StringBuffer();
    }

    @Override
    public void run() {
        downloadHistoryEvents();

        fixFinishedGamesTeamNames();

        XmlLiveScoresHelper xmlLiveScoresHelper = new XmlLiveScoresHelper();
        //Log.d(DownloadLiveScoresThread.class.getName(), "Downloaded " + liveScoresAndHistoryEvents.toString());
        xmlLiveScoresHelper.parseXMLFile(liveScores.toString());

        Intent refreshLiveScores = new Intent(ACTION_REFRESH_LIVE_SCORES);
        context.sendBroadcast(refreshLiveScores);

    }

    private void downloadHistoryEvents() {

        try {
            URL url = new URL( Constants.URL_LIVE_SCORES);
            URLConnection connection = url.openConnection();
            connection.setReadTimeout(Constants.READ_TIMEOUT); //after 10 seconds, fail to read data from server
            connection.setConnectTimeout(Constants.CONNECT_TIMEOUT); //after 8 seconds, user did not connect(fail)

            BufferedReader reader = new BufferedReader( new InputStreamReader(new GZIPInputStream(connection.getInputStream())) );
            StringBuilder result = new StringBuilder();
            int character;

            while ( (character = reader.read()) != -1 ) {
                result.append((char) character);
            }

            liveScores.append(result.toString());
            reader.close();

        } catch (Exception e) {
            Log.e( AsyncDownloadData.class.getSimpleName(), "Exception: ", e );
        }

    }

    private void fixFinishedGamesTeamNames() {
        synchronized (MyApplication.getInstance().getAllGames()) {
            for (LiveEventScore score : MyApplication.getInstance().getAllFinishedLiveScores()) {
                for (Game game : MyApplication.getInstance().getAllGames()) {
                    for (Team team : game.getTeamArrayList()) {
                        if (team.getTeamID() == score.getAwayTeamID())
                            score.setAwayTeam(team);
                        else if (team.getTeamID() == score.getHomeTeamID())
                            score.setHomeTeam(team);
                    }
                }
            }
        }
    }

}
