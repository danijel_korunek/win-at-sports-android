package com.winatsports.data.download;

import com.winatsports.MyApplication;
import com.winatsports.Util;
import com.winatsports.data.elements.Sport;
import com.winatsports.data.elements.SportIcon;

/**
 * Created by dkorunek on 15/04/16.
 */
public class ImageDownloadThread extends Thread {
    private long sportId;

    public ImageDownloadThread(long sportId) {
        this.sportId = sportId;
    }

    @Override
    public void run() {
        Sport sport = Util.getSport(sportId);
        // a reference to sport might be lost because of redownload
        String url = sport.getSportIconUrl();
        String timestamp = sport.getSportIconTime();

        if (MyApplication.getInstance().getDiskCache().existsFile(url, timestamp)) {
            sport.setScaledIcon(MyApplication.getInstance().getDiskCache().get(url, timestamp));
        } else {
            // this gets executed in this thread synchronously
            MyApplication.getInstance().getDiskCache().add(url, timestamp);
            // maybe this reference to sport is lost in the meantime
            if (sport == null)
                sport = Util.getSport(sportId);
            sport.setScaledIcon(MyApplication.getInstance().getDiskCache().get(sport.getSportIconUrl(), sport.getSportIconTime()));
        }

        synchronized (MyApplication.lock) {
            SportIcon newSportIcon = new SportIcon();
            newSportIcon.setBitmap(sport.getScaledIcon());
            newSportIcon.setSportId(sport.getSportID());

            SportIcon siToRemove = null;
            for (SportIcon oldSI : MyApplication.getInstance().getSportIcons()) {
                if (oldSI.getSportId() == newSportIcon.getSportId()) {
                    siToRemove = oldSI;
                    break;
                }
            }

            if (siToRemove != null)
                MyApplication.getInstance().getSportIcons().remove(siToRemove);
            MyApplication.getInstance().getSportIcons().add(newSportIcon);
        }
    }

}
