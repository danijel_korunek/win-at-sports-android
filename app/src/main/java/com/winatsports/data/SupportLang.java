package com.winatsports.data;

/**
 * Created by broda on 21/12/2015.
 */
public class SupportLang {

    private String langID;
    private String langName;

    public SupportLang() {

        langID = null;
        langName = null;
    }

    public String getLangID() {
        return langID;
    }

    public void setLangID(String langID) {
        this.langID = langID;
    }

    public String getLangName() {
        return langName;
    }

    public void setLangName(String langName) {
        this.langName = langName;
    }



}
