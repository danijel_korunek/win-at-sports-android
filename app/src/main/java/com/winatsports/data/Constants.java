package com.winatsports.data;

/**
 * Created by broda on 28/10/2015.
 */

// constant is always written upper case
public class Constants {

   public static final String MAIN_XML_SEPARATOR = "\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*";

   /** This constant is used for DroidAppVersionProtect */
   public static final int APP_VERSION = 1;

   public static final String SERVER_RESPONSE_OK = "OK";
   public static final String SERVER_RESPONSE_ERROR = "Error";
   public static final String SERVER_RESPONSE_WARNING = "WARNING";

   public static final float SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG = 0.75f;
   public static final float SCREEN_HEIGHT_PERCENTAGE_FOR_DIALOG = 0.70f;

   // notification time and other stuff for all notifications
   public static final int NOTIFICATION_TIME = 3000;
   public static final int MESSAGE_EXPAND = 1;
   public static final int MESSAGE_CLOSE = 2;

   // all constant I need for betSlipActivity.java
   // I need this so that I can call right function in BetSlipPlaceBetDialog.java ====>
   // 1) calculateWinProfitWithManualInsertedIntegerValueParlay      or     2) calculateWinProfitForTeaser
   public static final String PARLAY = "Parlay";
   public static final String TEASER = "Teaser";
   public static final int MAXIMUM_PARLAY_TOTAL_BETS = 9; // user can insert, bet on 9 games in "ONE PARLAY"
   public static final int C_BET_TYPE_ID_POINT_SPREAD = 3;
   public static final int C_BET_TYPE_ID_TOTAL_POINTS = 7;
   public static final int C_ODD_BET_VALUE = 1;

   public static final int APP_MAINTENANCE_ON = 1;
   public static final int APP_MAINTENANCE_OFF = 0;

   // constants to check if change order from sports is 1
   public static final int C_SPORT_CHANGE_ORDER = 1;

   // all constants I need for add_user.php script
   public static String C_DTYPE = "2"; // this is device type which I need in add_user.php script

   // this is removed from some data url and replaced by the value of Settings.getLanguage(context);
   public static final String URL_TOKEN_TO_BE_REPLACED = "XX";

   // all constants I need to parse XML FILES on WEB
   public static final String URL_DEFAULT_LANGUAGES = "http://www.ebithost.com/win_at_sports/app_data/language/default_languages.gz";
   public static final String URL_MESSAGES = "http://www.ebithost.com/win_at_sports/app_data/language/messages_XX.gz";
   public static final String URL_GAME_DATA = "http://www.ebithost.com/win_at_sports/app_data/main_data_XX.gz";

   public static String URL_LIVE_SCORES = "http://www.ebithost.com/win_at_sports/app_data/live_scores_30.gz";

   public static final String URL_HOME_SCREEN_FEATURED_GAMES_SECTION_1 = "http://www.ebithost.com/win_at_sports/app_data/first_page/events_us.gz";
   public static final String URL_HOME_SCREEN_FEATURED_LEAGUES_SECTION_2 = "http://www.ebithost.com/win_at_sports/app_data/first_page/leagues_us.gz";
   public static final String URL_HOME_SCREEN_UP_NEXT_SECTION_3 = "http://www.ebithost.com/win_at_sports/app_data/first_page/about_to_start.gz";

   // I use this constant for downloading data on loading, splash screen and maybe somewhere else
   public static final int READ_TIMEOUT = 10000;
   public static final int CONNECT_TIMEOUT = 10000;

   //  constants which I use in add_user.php
   public static final String URL_ADD_USER = "http://www.ebithost.com/win_at_sports/service/add_user.php";
   public static final String ADD_USER_REQUEST_HEADER_KEY = "m4HwxkLo385m9m2T7nzeb89/uo";

   // constants which I use in user_common
   public static final String URL_USER_COMMON = "http://www.ebithost.com/win_at_sports/service/user_common.php";
   public static final String USER_COMMON_REQUEST_HEADER_KEY = "cxhsa6342H74nGH9/)hJ";

   public static final String USER_COMMON_PARAMS_HISTORY = "history";
   public static final String USER_PARAM_STATS = "mystats";

   //  constants which I use in place_bet.php
   public static final String URL_PLACE_BET_PHP = "http://www.ebithost.com/win_at_sports/service/place_bet.php";
   public static final String PLACE_BET_REQUEST_HEADER_KEY = "hd674h99baljenx678q";

   public static final String URL_REGISTER_USER = "http://www.ebithost.com/win_at_sports/service/user_account.php";
   public static final String REG_USER_REQUEST_HEADER_KEY = "j3nZ8996%u7B*z43aN56bhG";

   public static final String URL_LOGIN_USER = "http://www.ebithost.com/win_at_sports/service/user_login.php";
   public static final String KEY_LOGIN_USER = "6bF3MK2BBS94c!!Q-::i45N";

   public static final String KEY_FORGOT_PASSWORD = "j3nZ8996%u7B*z43aN56bhG";

   public static final String URL_REAL_BETTING = "http://www.ebithost.com/win_at_sports/service/sb3_integration.php";
   public static final String REAL_BETTING_URL_KEY = "k67J3n89IO34ny346hjU";

   public static final String URL_STATS_OVERALL = "http://www.ebithost.com/win_at_sports/app_data/stats/stats_overall.gz";

   // constant for switching between layout in MainActivity.java
   public static final int C_VIEW_HOME_SCREEN = 0;
   public static final int C_VIEW_SPORTSBOOK_SCREEN = 1;
   public static final int C_VIEW_BET_SCREEN = 2;
   public static final int C_VIEW_LIVESCORE_SCREEN = 3;
   public static final int C_VIEW_SEARCH_SCREEN = 4;
   public static final int C_VIEW_PROFILE_SCREEN = 5;

   // constant for switching between layout in PlaceSingleBetDialog.java
   public static final int C_VIEW_PLACE_BET_STRAIGHT = 0;
   public static final int C_VIEW_PLACE_BET_SLIP = 1;

   // constant for switching between layout in BetSLipActivity.java
   public static final int C_VIEW_BET_SLIP_PARLAY = 0;
   public static final int C_VIEW_BET_SLIP_TEASER = 1;

   // Later I will delete this constant because I will not need them,
   // For now I only need this constant in GamesInLeagueActivity.java file, but later I will delete also this file
   public static int C_ID_SPORT_ICE_HOCKEY = 1;
   public static int C_ID_SPORT_BASKETBALL = 2;
   public static int C_ID_SPORT_TENNIS = 3;
   public static int C_ID_SPORT_FOOTBALL = 4;
   public static int C_ID_SPORT_BASEBALL = 5;
   public static int C_ID_SPORT_SOCCER = 6;
   public static int C_ID_SPORT_BOXING = 7;
   public static int C_ID_SPORT_MMA = 8;
   public static int C_ID_SPORT_GOLF = 9;
   public static int C_ID_SPORT_AUTO_RACING = 10;

   // constant for custom adapters where I need to show only one ROW
   public static int C_CUSTOM_ADAPTER_ONLY_ONE_ROW_TO_SHOW = 1;

   // constants for settings
   public static int C_US_ODDS = 0;
   public static int C_DECIMAL_ODDS = 1;
   public static int C_FRACTIONAL_ODDS = 2;


   // constants for Settings(HOME vs AWAY) and  (AWAY vs HOME)
   public static int C_SETTINGS_HOME_VS_AWAY = 0;
   public static int C_SETTINGS_AWAY_VS_HOME = 1;

   // Message id equals constant names
   // I'm getting this values from languageCodes.xml file
   public static final String LANGUAGE_CODE_1ST_H = "1st h" ; // <Term term_from="1st h" term_to="1st Half"/>
   public static final String LANGUAGE_CODE_1ST_P  = "1st p" ; //<Term term_from="1st p" term_to="1st Period"/>
   public static final String LANGUAGE_CODE_1ST_Q  = "1st q" ; // <Term term_from="1st q" term_to="1st Quarter"/>
   public static final String LANGUAGE_CODE_2ST_H  = "2nd h" ; // <Term term_from="2nd h" term_to="2nd Half"/>
   public static final String LANGUAGE_CODE_2ST_P  = "2nd p" ; // <Term term_from="2nd p" term_to="2nd Period"/>
   public static final String LANGUAGE_CODE_2ST_Q  = "2nd q" ; // <Term term_from="2nd q" term_to="2nd Quarter"/>
   public static final String LANGUAGE_CODE_3ST_P  = "3rd p" ; // <Term term_from="3rd p" term_to="3rd Period"/>
   public static final String LANGUAGE_CODE_3ST_Q  = "3rd q" ; // <Term term_from="3rd q" term_to="3rd Quarter"/>
   public static final String LANGUAGE_CODE_4ST_Q  = "4th q" ; // <Term term_from="4th q" term_to="4th Quarter"/>
   public static final String LANGUAGE_CODE_END  = "end-" ; //<Term term_from="end-" term_to="Finished"/>
   public static final String LANGUAGE_CODE_FINAL  = "final" ; //<Term term_from="final" term_to="Final"/>
   public static final String LANGUAGE_CODE_FT  = "ft" ; //<Term term_from="ft" term_to="Full Time"/>
   public static final String LANGUAGE_CODE_HALF  = "half" ; // <Term term_from="half" term_to="Half Time"/>
   public static final String LANGUAGE_CODE_HT  = "ht" ; // <Term term_from="ht" term_to="Half Time"/>
   public static final String LANGUAGE_CODE_OT  = "ot" ; //  <Term term_from="ot" term_to="Over Time"/>
   public static final String LANGUAGE_CODE_OT1  = "ot1" ; // <Term term_from="ot1" term_to="Over Time 1"/>
   public static final String LANGUAGE_CODE_OT2  = "ot2" ; // <Term term_from="ot1" term_to="Over Time 2"/>
   public static final String LANGUAGE_CODE_OT3  = "ot3" ; //<Term term_from="ot3" term_to="Over Time 3"/>
   public static final String LANGUAGE_CODE_OT4  = "ot4" ; // <Term term_from="ot4" term_to="Over Time 4"/>
   public static final String LANGUAGE_CODE_OT5  = "ot5" ; //<Term term_from="ot5" term_to="Over Time 5"/>
   public static final String LANGUAGE_CODE_SO  = "so" ; // <Term term_from="so" term_to="Shutout"/>
   public static final String LANGUAGE_CODE_TIME  = "time" ; //<Term term_from="time" term_to=""/>

   // Message id equals constant names
   // I'm getting this values from messages.xml file
   public static final String MSG_KEY_BETTYPE_EVEN = "bettype_even" ; // id="bettype_even" text="Even"
   public static final String MSG_KEY_BETTYPE_ODD = "bettype_odd" ; //  id="bettype_odd"  text="Odd"
   public static final String MSG_KEY_BETTYPE_OVER = "bettype_over" ; // id="bettype_over"  text="Over"
   public static final String MSG_KEY_BETTYPE_UNDER = "bettype_under"; //  id="bettype_under" text="Under"
   public static final String MSG_KEY_BETTYPE_DRAW = "bettype_draw"; // id="bettype_draw" text="Draw"
   public static final String MSG_KEY_FPAGE_HIGHLIGHT = "fpage_highlight"; // id="fpage_highlight" text="Featured Games"
   public static final String MSG_KEY_FPAGE_FEATURED = "fpage_featured"; // id="fpage_featured" text="Featured Leagues"
   public static final String MSG_KEY_FPAGE_NEXT = "fpage_next"; // id="fpage_next" text="Up Next"
   public static final String MSG_KEY_ERR_PARLAY_BET = "err_parlay_bet"; // id="err_parlay_bet" text="Error In Parlay Bet Data!"
   public static final String MSG_KEY_ERR_TRY_AGAIN = "err_try_again"; // id="err_try_again" text="Error! Try Again Later!"
   public static final String MSG_KEY_ERR_WROUNG_AMOUNT = "err_wrong_amount"; // id="err_wrong_amount" text="Error! Wrong Amount Format."
   public static final String MSG_KEY_ERR_WROUNG_UFORMAT = "err_wrong_uformat"; // id="err_wrong_uformat" text="Error! Wrong User Format."
   public static final String MSG_KEY_ERR_WROUNG_EMAIL = "err_wrong_email"; // id="err_wrong_email" text="Wrong Email Format!"
   public static final String MSG_KEY_ERR_PASS_FORMAT = "err_pass_format"; // id="err_pass_format" text="Password must be between 6 and 30 characters!"
   public static final String MSG_KEY_ERR_PASS_ALPHA = "err_pass_alpha"; // id="err_pass_alpha" text="Only alphanumeric characters alowed in password field!"
   public static final String MSG_KEY_ERR_MANDATORY = "err_mandatory"; // id="err_mandatory" text="Please Fill All Mandatory Fields."
   public static final String MSG_KEY_ERR_ACC_EXIST = "err_acc_exist"; // id="err_acc_exist" text="Account with that email address already exist! Please confirm your account!"
   public static final String MSG_KEY_ERR_ACC_YOUHAVE = "err_acc_youhave"; //  id="err_acc_youhave" text="You already have account with this email address! Please, use password recovery function."
   public static final String MSG_KEY_ERR_CREATE_USER = "err_create_user"; //   id="err_create_user" text="Error Creating User!"
   public static final String MSG_KEY_ERR_USER_NOTF = "err_user_notf" ; //   id="err_user_notf" text="Data Error! User Not Found!"
   public static final String MSG_KEY_CONF_EMAIL = "conf_email";  //   id="conf_email" text="Confirmation email have been sent to ##email##. Please check your spam folder."
   public static final String MSG_KEY_ERR_CONF_EMAIL = "err_conf_email";  //  id="err_conf_email" text="Email address not found, try other email address or sign up for new account!"
   public static final String MSG_KEY_PASS_RECOVER_SENT = "pass_recover_sent"; // id="pass_recover_sent" text="Password Has Been Sent to"
   public static final String MSG_KEY_ERR_WRONG_EMAILPASS = "err_wrong_emailpass"; // id="err_wrong_emailpass" text="Wrong Email or Password!"
   public static final String MSG_KEY_ERR_NOT_CONFIRMED = "err_not_confirmed";  // id="err_not_confirmed" text="Cannot login! Please confirm your account through email address! Please check your spam folder."
   public static final String MSG_KEY_ERR_ENTER_USERPASSS = "err_enter_userpass"; // id="err_enter_userpass" text="Please Enter Email Address and Password!"
   public static final String MSG_KEY_TEASER_PTS = "teaser_pts" ;  // id="teaser_pts" text="Pts"
   public static final String MSG_KEY_TEASER_TEAM = "teaser_team";  // id="teaser_team" text="Team"
   public static final String MSG_KEY_PUSH_CANCEL_GAME = "push_cancel_game"; // id="push_cancel_game" text="Game Between **participants** is **problem**!"
   public static final String MSG_KEY_PUSH_END_GAME = "push_end_game"; // id="push_end_game" text="**participants** Is Over. Final Score: **score**"
   public static final String MSG_KEY_PUSH_START_GAME = "push_start_game"; // id="push_start_game" text="**participants** Is About to Start! Enjoy the Action and Good Luck."
   public static final String MSG_KEY_APP_DOWNLOADING = "app_downloading"; // id="app_downloading" text="Downloading..."
   public static final String MSG_KEY_APP_ENTER_AMOUNT = "app_enter_amount";  // id="app_enter_amount" text="Enter amount"
   public static final String MSG_KEY_APP_ENTRIES = "app_entries";  // id="app_entries" text="Selections"
   public static final String MSG_KEY_APP_ERROR = "app_error";  // id="app_error" text="Error"
   public static final String MSG_KEY_APP_FIRST_NAME = "app_first_name"; // id="app_first_name" text="First Name"
   public static final String MSG_KEY_APP_IN_PLAY = "app_in_play"; // id="app_in_play" text="In Play"
   public static final String MSG_KEY_APP_LAST_NAME = "app_last_name"; // id="app_last_name" text="Last Name"
   public static final String MSG_KEY_APP_LOADING = "app_loading"; // id="app_loading" text="Loading..."
   public static final String MSG_KEY_APP_LOGIN = "app_login"; // id="app_login" text="Login"
   public static final String MSG_KEY_APP_MAINTENANCE = "app_maintenance" ; // id="app_maintenance" text="Application is Under Maintenance. Please Try Later."
   public static final String MSG_KEY_APP_MOBILE= "app_mobile"; // id="app_mobile" text="*Mobile"
   public static final String MSG_KEY_APP_NOINET = "app_noinet";  //   id="app_noinet" text="This App Requires Internet Connection."
   public static final String MSG_KEY_APP_ON_PARLAY_CARD = "app_on_parlay_card";  //   id="app_on_parlay_card" text="On Bet Slip"
   public static final String MSG_KEY_APP_PARLAY = "app_parlay"; //   id="app_parlay" text="Bet Slip"
   public static final String MSG_KEY_APP_PARLAY_CARD = "app_parlay_card"; //    id="app_parlay_card" text="Bet Slip"
   public static final String MSG_KEY_APP_PASSWORD = "app_password"; //  id="app_password" text="Password"
   public static final String MSG_KEY_APP_PASS_RECOVER = "app_pass_recover"; //   id="app_pass_recover" text="Password Recovery"
   public static final String MSG_KEY_APP_REGISTER = "app_register"; //   id="app_register" text="Registration"
   public static final String MSG_KEY_APP_TECH_DIFF = "app_tech_diff"; //   id="app_tech_diff" text="We are experiencing some technical difficulties. Please try again later."
   public static final String MSG_KEY_APP_TOTAL = "app_total"; //    id="app_total" text="Total:"
   public static final String MSG_KEY_APP_TOTAL_WIN = "app_total_win";  //   id="app_total_win" text="To Win"
   public static final String MSG_KEY_APP_TOWIN = "app_towin";  //    id="app_towin" text="To Win"
   public static final String MSG_KEY_APP_VS = "app_vs"; //  id="app_vs" text="Vs."
   public static final String MSG_KEY_APP_WALLET = "app_wallet";  //   id="app_wallet" text="Wallet"
   public static final String MSG_KEY_AVAILABLETEASERS = "availableTeasers"; //   id="availableTeasers" text="Available Teasers"
   public static final String MSG_KEY_BETCONFIRMCANCELBUTTON = "betConfirmCancelButton"; //   id="betConfirmCancelButton" text="Cancel"
   public static final String MSG_KEY_BETCONFIRMCONFIRMBUTTON = "betConfirmConfirmButton"; //   id="betConfirmConfirmButton" text="Confirm"
   public static final String MSG_KEY_BETCONFIRMTITLE = "betConfirmTitle"; //   id="betConfirmTitle" text="Confirm Your Bet."
   public static final String MSG_KEY_BETSLIPISEMPTY = "betslipIsEmpty"; //   id="betslipIsEmpty" text="Add Wagers to Bet Slip to Submit a Parlay or Teaser."
   public static final String MSG_KEY_BETSLIPPARLAY = "betslipParlay"; //  id="betslipParlay" text="PARLAY"
   public static final String MSG_KEY_BETSLIPTEASER = "betslipTeaser"; //   id="betslipTeaser" text="TEASER"
   public static final String MSG_KEY_BETTINGRESULTS = "bettingResults"; //   id="bettingResults" text="Betting Results"
   public static final String MSG_KEY_BETTYPE1 = "betType1"; //   id="betType1" text="Wager Amount:"
   public static final String MSG_KEY_BETTYPE2 = "betType2"; //   id="betType2" text="To Win Amount:"
   public static final String MSG_KEY_FPAGE_NOTIFICATIONS = "fpage_notifications" ;  //   id="fpage_notifications" text="Notifications"
   public static final String MSG_KEY_HISTORYTAPFORDETAILS = "historyTapForDetails";  //   id="historyTapForDetails" text="Tap for Details."
   public static final String MSG_KEY_INPLAYPARLAY = "inPlayParlay"; // id="inPlayParlay" text="Parlay"
   public static final String MSG_KEY_INPLAYTEASER = "inPlayTeaser"; // id="inPlayTeaser" text="Teaser"
   public static final String MSG_KEY_LIVEGAMESINPROGRESS = "liveGamesInProgress";  // id="liveGamesInProgress" text="In Progress"
   public static final String MSG_KEY_LIVEGAMESWITHWAGERS = "liveGamesWithWagers";  // id="liveGamesWithWagers" text="Plays In Progress"
   public static final String MSG_KEY_LIVENOGAMES = "liveNoGames";  // id="liveNoGames" text="No Games In Progress!"
   public static final String MSG_KEY_LIVERECENTLYFINISHED = "liveRecentlyFinished";  // id="liveRecentlyFinished" text="Results"
   public static final String MSG_KEY_LOGINALREADYHAVE = "loginAlreadyHave";  // id="loginAlreadyHave" text="Already Have An Account?**SIGN IN NOW"
   public static final String MSG_KEY_LOGINBUTTON = "loginButton";  // <message id="loginButton" text="Login"/>
   public static final String MSG_KEY_LOGINEMAIL = "loginEmail"; // <message id="loginEmail" text="Sign Up With Email"/>
   public static final String MSG_KEY_LOGINFACEBOOK = "loginFacebook";  // <message id="loginFacebook" text="Connect Using Facebook"/>
   public static final String MSG_KEY_LOGINFORGOTYOURPASSWORD = "loginForgotYourPassword";  // <message id="loginForgotYourPassword" text="Forgot Your Password?"/>
   public static final String MSG_KEY_LOGINGUEST = "loginGuest";  // <message id="loginGuest" text="Continue As Guest"/>
   public static final String MSG_KEY_LOGINSIGNUPNOW = "loginSignUpNow";  // <message id="loginSignUpNow" text="SIGN UP NOW"/>
   public static final String MSG_KEY_LOGINTRYLATER = "loginTryLater";  // <message id="loginTryLater" text="Please Try Again Later."/>
   public static final String MSG_KEY_MAINLOGINFACEBOOKERROR = "mainLoginFacebookError"; // <message id="mainLoginFacebookError" text="There are no Facebook accounts configured. You can add or create a Facebook account in Settings."/>
   public static final String MSG_KEY_MANUALENTRYERROR1 = "manualEntryError1"; // <message id="manualEntryError1" text="Please Enter Value Larger Than Zero."/>
   public static final String MSG_KEY_MANUALENTRYERROR2 = "manualEntryError2";  // <message id="manualEntryError2" text="Value entered is bigger than amount in your Wallet (##wallet_amount##)"/>
   public static final String MSG_KEY_NO_GAMES_LEAGUE = "no_games_league";  // <message id="no_games_league" text="Schedule and Lines Are Not Yet Available."/>
   public static final String MSG_KEY_NO_ODDS = "no_odds";  // <message id="no_odds" text="Lines For The Game Are Not Yet Available."/>
   public static final String MSG_KEY_ODDSGAMEINPROGRESS = "oddsGameInProgress";  // <message id="oddsGameInProgress" text="Game Is Currently In Progress"/>
   public static final String MSG_KEY_OTHER_BET = "other_bet";  // <message id="other_bet" text="Bet"/>
   public static final String MSG_KEY_OTHER_BETS = "other_bets";  // <message id="other_bets" text="Bets"/>
   public static final String MSG_KEY_OTHER_EMAIL = "other_email";  // <message id="other_email" text="Email"/>
   public static final String MSG_KEY_OTHER_LANG = "other_lang";  // <message id="other_lang" text="Language"/>
   public static final String MSG_KEY_OTHER_LANG_CHANGE = "other_lang_change"; // <message id="other_lang_change" text="Change App Language"/>
   public static final String MSG_KEY_OTHER_LOST = "other_lost";  // <message id="other_lost" text="Lost"/>
   public static final String MSG_KEY_OTHER_PUSH = "other_push"; // <message id="other_push" text="Push"/>
   public static final String MSG_KEY_OTHER_WON = "other_won"; // <message id="other_won" text="Won"/>
   public static final String MSG_KEY_PARLAYBETPOPUPCANCELBUTTON = "parlayBetPopupCancelButton";  // <message id="parlayBetPopupCancelButton" text="Cancel"/>
   public static final String MSG_KEY_PARLAYBETPOPUPRED1 = "parlayBetPopupRed1"; // <message id="parlayBetPopupRed1" text="Wager:"/>
   public static final String MSG_KEY_PARLAYBETPOPUPRED2 = "parlayBetPopupRed2";  // <message id="parlayBetPopupRed2" text="To Win:"/>
   public static final String MSG_KEY_PARLAYBETPOPUPSUBMITBUTTON = "parlayBetPopupSubmitButton"; // <message id="parlayBetPopupSubmitButton" text="Submit"/>
   public static final String MSG_KEY_PARLAYBETPOPUPWARNING = "parlayBetPopupWarning";  // <message id="parlayBetPopupWarning" text="Please Remove the Highlighted Bets, Since They have Already Begun."/>
   public static final String MSG_KEY_PARLAYCLEARALLBUTTON = "parlayClearAllButton"; // <message id="parlayClearAllButton" text="CLEAR ALL"/>
   public static final String MSG_KEY_PARLAYPOPUPCANCELBUTTON = "parlayPopupCancelButton"; // <message id="parlayPopupCancelButton" text="Cancel"/>
   public static final String MSG_KEY_PARLAYPOPUPCONFIRMBUTTON = "parlayPopupConfirmButton"; // <message id="parlayPopupConfirmButton" text="Confirm"/>
   public static final String MSG_KEY_PARLAYPOPUPMESSAGE = "parlayPopupMessage";  // <message id="parlayPopupMessage" text="Clear All Bet Slip Selections?"/>
   public static final String MSG_KEY_PARLAYPOPUTITLE = "parlayPopupTitle"; // <message id="parlayPopupTitle" text="Warning."/>
   public static final String MSG_KEY_PARLAYSUBMITBUTTON = "parlaySubmitButton"; //<message id="parlaySubmitButton" text="SUBMIT"/>
   public static final String MSG_KEY_PARLAYWARNING = "parlayWarning"; // <message id="parlayWarning" text="Bet Slip Must Have at Least Two Selections"/>
   public static final String MSG_KEY_PARLAY_DELETE = "parlay_delete"; // <message id="parlay_delete" text="Delete"/>
   public static final String MSG_KEY_PARLAY_DONE = "parlay_done"; // <message id="parlay_done" text="Done"/>
   public static final String MSG_KEY_PARLAY_EDIT = "parlay_edit";  // <message id="parlay_edit" text="Edit"/>
   public static final String MSG_KEY_PLACEBETPOPUPADDTOPARLAYBUTTON = "placeBetPopupAddToParlayButton";  // <message id="placeBetPopupAddToParlayButton" text="Add to Bet Slip"/>
   public static final String MSG_KEY_PLACEBETPOPUPALREADYINPROGRESS = "placeBetPopupAlreadyInProgress";  // <message id="placeBetPopupAlreadyInProgress" text="Event In Progress."/>
   public static final String MSG_KEY_PLACEBETPOPUPCANNOTADDFEATURES = "placeBetPopupCannotAddFutures"; // <message id="placeBetPopupCannotAddFutures" text="Future Events Are Currently Not Allowed On Bet Slip."/>
   public static final String MSG_KEY_PLACEBETPOPUPADDMORE1 = "placeBetPopupCannotAddMore1";  // <message id="placeBetPopupCannotAddMore1" text="Up to ##parlay_count## Selections Allowed On Bet Slip."/>
   public static final String MSG_KEY_PLACEBETPOPUPADDMORE2 = "placeBetPopupCannotAddMore2";  // <message id="placeBetPopupCannotAddMore2" text="Only One Wager Per Event Allowed Per Bet Slip."/>
   public static final String MSG_KEY_PLACEBETPOPUPDISMISSBUTTON= "placeBetPopupDismissButton";  // <message id="placeBetPopupDismissButton" text="Cancel"/>
   public static final String MSG_KEY_PLACEBETPOPUPPLACEBETBUTTON = "placeBetPopupPlaceBetButton";  // <message id="placeBetPopupPlaceBetButton" text="Place Your Bet"/>
   public static final String MSG_KEY_PLACEBETPOPUPTAB1 = "placeBetPopupTab1";  // <message id="placeBetPopupTab1" text="STRAIGHT"/>
   public static final String MSG_KEY_PLACEBETPOPUPTAB2 = "placeBetPopupTab2";  // <message id="placeBetPopupTab2" text="BET SLIP"/>
   public static final String MSG_KEY_PLACEBETPOPUPTOTALWAGERS = "placeBetPopupTotalWagers"; // <message id="placeBetPopupTotalWagers" text="Bet Slip Picks:"/>
   public static final String MSG_KEY_PLACEBETPOPUPTOTALWINNINGS = "placeBetPopupTotalWinnings"; // <message id="placeBetPopupTotalWinnings" text="To Win:"/>
   public static final String MSG_KEY_PROFILECONTACTUSTEXT1 = "profileContactUsText1";  // <message id="profileContactUsText1" text="Contact Us"/>
   public static final String MSG_KEY_PROFILECONTACTUSTEXT2 = "profileContactUsText2";  // <message id="profileContactUsText2" text="Send Us an Email."/>
   public static final String MSG_KEY_PROFILEEMAILADDRES = "profileEmailAddres";  // <message id="profileEmailAddres" text="support@entertainmentworx.com"/>
   public static final String MSG_KEY_PROFILEEMAILBODY = "profileEmailBody";  // <message id="profileEmailBody" text="Write text here"/>
   public static final String MSG_KEY_PROFILEEMAILERROR = "profileEmailError";  // <message id="profileEmailError" text="Please Setup Your Email in Device Settings."/>
   public static final String MSG_KEY_PROFILEEMAILTITLE = "profileEmailTitle";  // <message id="profileEmailTitle" text="Sports Betting ##device_name##"/>
   public static final String MSG_KEY_PROFILEHISTORYTEXT1 = "profileHistoryText1";  // <message id="profileHistoryText1" text="History"/>
   public static final String MSG_KEY_PROFILEINPLAYTEXT1 = "profileInPlayText1";  // <message id="profileInPlayText1" text="In Play"/>
   public static final String MSG_KEY_PROFILELOGOUTCANCELBUTTON = "profileLogoutCancelButton";  // <message id="profileLogoutCancelButton" text="Cancel"/>
   public static final String MSG_KEY_PROFILELOGOUTCONFIRMBUTTON = "profileLogoutConfirmButton";  // <message id="profileLogoutConfirmButton" text="Log Out"/>
   public static final String MSG_KEY_PROFILELOGOUTMESSAGE = "profileLogoutMessage"; // <message id="profileLogoutMessage" text="Do you wish to log out and get back to default user account?"/>
   public static final String MSG_KEY_PROFILELOGOUTSUCESS = "profileLogoutSucess";  // <message id="profileLogoutSucess" text="Logged Out Successfully."/>
   public static final String MSG_KEY_PROFILELOGOUTTEXT1 = "profileLogOutText1";  //  <message id="profileLogOutText1" text="Log Out"/>
   public static final String MSG_KEY_PROFILELOGOUTTEXT2 = "profileLogOutText2";  // <message id="profileLogOutText2" text="Log Out From Your Account."/>
   public static final String MSG_KEY_PROFILELOGOUTTITLE = "profileLogoutTitle";  // <message id="profileLogoutTitle" text="Log Out?"/>
   public static final String MSG_KEY_PROFILESETTINGSTEXT1 = "profileSettingsText1";   // <message id="profileSettingsText1" text="Settings"/>
   public static final String MSG_KEY_PROFILESETTINGSTEXT2 = "profileSettingsText2";  // <message id="profileSettingsText2" text="Account and App Settings."/>
   public static final String MSG_KEY_PROFILESIGNUPTEXT1 = "profileSignUpText1"; // <message id="profileSignUpText1" text="Sign Up"/>
   public static final String MSG_KEY_PROFILESIGNUPTEXT2 = "profileSignUpText2";  // <message id="profileSignUpText2" text="Claim Your Welcome Bonus!"/>
   public static final String MSG_KEY_PROFILETUTORIALTEXT1 = "profileTutorialText1";  // <message id="profileTutorialText1" text="Tutorial"/>
   public static final String MSG_KEY_PROFILETUTORIALTEXT2 = "profileTutorialText2";  // <message id="profileTutorialText2" text="Learn to Play and Win!"/>
   public static final String MSG_KEY_REALBETTINGLOGINPOPUPCANCELBUTTON = "realBettingLoginPopupCancelButton";  // <message id="realBettingLoginPopupCancelButton" text="Close"/>
   public static final String MSG_KEY_REALBETTINGLOGINPOPUPCONFIRMBUTTON = "realBettingLoginPopupConfirmButton"; // <message id="realBettingLoginPopupConfirmButton" text="REGISTER"/>
   public static final String MSG_KEY_REALBETTINGLOGINPOPUPMESSAGE = "realBettingLoginPopupMessage"; // <message id="realBettingLoginPopupMessage" text="Real Money betting option is available to Registered users only."/>
   public static final String MSG_KEY_REALBETTINGLOGINPOPUPTITLE = "realBettingLoginPopupTitle"; // <message id="realBettingLoginPopupTitle" text="Real Money"/>
   public static final String MSG_KEY_REALBETTINGTAB1 = "realBettingTab1";  // <message id="realBettingTab1" text="GAME MODE"/>
   public static final String MSG_KEY_REALBETTINGTAB2 = "realBettingTab2"; // <message id="realBettingTab2" text="REAL MONEY"/>
   public static final String MSG_KEY_RECOVERPASSWORDBUTTON = "recoverPasswordButton"; // <message id="recoverPasswordButton" text="Recover Password"/>
   public static final String MSG_KEY_RECOVERPASSWORDTEXT = "recoverPasswordText";  // <message id="recoverPasswordText" text="To recover your password please enter your Email Address in the field below."/>
   public static final String MSG_KEY_SEARCHLANGUAGES = "searchLeagues";  // <message id="searchLeagues" text="Leagues"/>
   public static final String MSG_KEY_SEARCHNORESULTS = "searchNoResults"; // <message id="searchNoResults" text="No Results Found"/>
   public static final String MSG_KEY_SEARCHPLACEHOLDER = "searchPlaceholder";  // <message id="searchPlaceholder" text="Search Available Events."/>
   public static final String MSG_KEY_SEARCHTEAMS = "searchTeams";  // <message id="searchTeams" text="Events"/>
   public static final String MSG_KEY_SETTINGSBETAMOUNTMODE1 = "settingsBetAmountMode1"; //  <message id="settingsBetAmountMode1" text="To Win Amount"/>
   public static final String MSG_KEY_SETTINGSBETAMOUNTMODE2 = "settingsBetAmountMode2"; // <message id="settingsBetAmountMode2" text="Show Win Amount as Only Net Winnings."/>
   public static final String MSG_KEY_SETTINGSBETAMOUNTTEXT1 = "settingsBetAmountText1"; //  <message id="settingsBetAmountText1" text="Predefined Wager Amount"/>
   public static final String MSG_KEY_SETTINGSBETAMOUNTTEXT2 = "settingsBetAmountText2";  // <message id="settingsBetAmountText2" text="Automatically Generate Wager Amounts."/>
   public static final String MSG_KEY_SETTINGSBETCONFIRMATIONTEXT1 = "settingsBetConfirmationText1"; // <message id="settingsBetConfirmationText1" text="Bet Confirmation"/>
   public static final String MSG_KEY_SETTINGSBETCONFIRMATIONTEXT2 = "settingsBetConfirmationText2";  // <message id="settingsBetConfirmationText2" text="Two Step Process to Place a Wager."/>
   public static final String MSG_KEY_SETTINGSENDGAMETEXT1 = "settingsEndGameText1"; // <message id="settingsEndGameText1" text="End Game Notification"/>
   public static final String MSG_KEY_SETTINGSENDGAMETEXT2 = "settingsEndGameText2";  //  <message id="settingsEndGameText2" text="Get Notified After a Game Ends."/>
   public static final String MSG_KEY_SETTINGSODDDECIMAL = "settingsOddDecimal";  // <message id="settingsOddDecimal" text="Decimal Odds"/>
   public static final String MSG_KEY_SETTINGSODDFRACTIONAL = "settingsOddFractional";  // <message id="settingsOddFractional" text="Fractional Odds"/>
   public static final String MSG_KEY_SETTINGSODDTITLE = "settingsOddTitle"; // <message id="settingsOddTitle" text="Odd Type"/>
   public static final String MSG_KEY_SETTINGSODDUS = "settingsOddUS";  // <message id="settingsOddUS" text="US Odds"/>
   public static final String MSG_KEY_SETTINGSSHOWEMPTY1 = "settingsShowEmpty1";  // <message id="settingsShowEmpty1" text="Show Empty Schedule"/>
   public static final String MSG_KEY_SETTINGSSHOWEMPTY2 = "settingsShowEmpty2";  // <message id="settingsShowEmpty2" text="Show schedule and Games Without Lines."/>
   public static final String MSG_KEY_SETTINGSSTARTGAMETEXT1 = "settingsStartGameText1"; // <message id="settingsStartGameText1" text="Start Game Notification"/>
   public static final String MSG_KEY_SETTINGSSTARTGAMETEXT2 = "settingsStartGameText2";  // <message id="settingsStartGameText2" text="Get Notified Before a Game Starts."/>
   public static final String MSG_KEY_SETTINGSTEAMORDERAWAYVSHOME = "settingsTeamOrderAwayVsHome"; // <message id="settingsTeamOrderAwayVsHome" text="Away vs Home"/>
   public static final String MSG_KEY_SETTINGSTEAMORDERHOMEVSAWAY = "settingsTeamOrderHomeVsAway";  // <message id="settingsTeamOrderHomeVsAway" text="Home vs Away"/>
   public static final String MSG_KEY_SETTINGSTEAMORDERTITLE = "settingsTeamOrderTitle"; // <message id="settingsTeamOrderTitle" text="Team Order"/>
   public static final String MSG_KEY_SIGNUPLOGINBUTTON= "signUpLoginButton"; // <message id="signUpLoginButton" text="Already Have an Account?**SIGN IN NOW"/>
   public static final String MSG_KEY_SIGNUPOPTIONAL = "signupOptional"; // <message id="signupOptional" text="*Optional But Highly Recommended."/>
   public static final String MSG_KEY_SIGNUP = "signup_text"; // <message id="signup_text" text="OPEN FREE ACCOUNT AND BE A WINNER!"/>
   public static final String MSG_KEY_SIGNUPCREATEACCOUNT = "singUpCreateAccount"; // <message id="singUpCreateAccount" text="Create Account"/>
   public static final String MSG_KEY_STATS_TITLE = "stats_title"; // <message id="stats_title" text="Statistics"/>
   public static final String MSG_KEY_STATS_TITLE_DESC = "stats_title_desc"; // <message id="stats_title_desc" text="Statistics and Betting Trends."/>
   public static final String MSG_KEY_TEASERWARNING1 = "teaserWarning1"; // <message id="teaserWarning1" text="Available For Football and Basketball Only."/>
   public static final String MSG_KEY_TEASERWARNING4 = "teaserWarning4"; // <message id="teaserWarning4" text="Supports Only Point Spreads and Total Points."/>
   public static final String MSG_KEY_TEASERWARNING5 = "teaserWarning5"; // <message id="teaserWarning5" text="Teaser Supports Only 2-7 Wagers."/>
   public static final String MSG_KEY_TERM_ON = "term_on"; // <message id="term_on" text="On"/>
   public static final String MSG_KEY_TOOL_HOME = "tool_home";  //<message id="tool_home" text="HOME"/>
   public static final String MSG_KEY_TOOL_LIVE = "tool_live"; // <message id="tool_live" text="LIVE SCORES"/>
   public static final String MSG_KEY_TOOL_PROFILE = "tool_profile";  //<message id="tool_profile" text="ACCOUNT"/>
   public static final String MSG_KEY_TOOL_SEARCH = "tool_search";  // <message id="tool_search" text="SEARCH"/>
   public static final String MSG_KEY_TOOL_SPORTS = "tool_sports"; // <message id="tool_sports" text="SPORTSBOOK"/>
   public static final String MSG_KEY_TOOL_WAGERS = "tool_wagers";  // <message id="tool_wagers" text="WAGERS"/>
   public static final String MSG_KEY_SKIP = "tutorialSkipButton"; // text="SKIP"/>
   public static final String MSG_KEY_TUTORIAL1 = "tutorialText1"; // text="Tutorial text 1"/>
   public static final String MSG_KEY_TUTORIAL2 = "tutorialText2"; // text="Tutorial text 2"/>
   public static final String MSG_KEY_TUTORIAL3 = "tutorialText3"; // text="Tutorial text 3"/>
   public static final String MSG_KEY_TUTORIAL4 = "tutorialText4"; // text="Tutorial text 4"/>
   /*<message id="bonus_note_title" text="You Received Free Coins"/>
   <message id="bonus_note_text" text="You Received **bonus_coins** Free Coins!"/> */
   public static final String MSG_KEY_TEASERWARNING2 = "teaserWarning2";  // <message id="teaserWarning2" text="Teaser Doesn't Support Mixed Sports."/>
   public static final String MSG_KEY_TEASERWARNING3 = "teaserWarning3"; // <message id="teaserWarning3" text="Teaser Doesn't Support Futures Events."/>
   public static final String MSG_KEY_STATS_LEAGUE_WEEK = "stats_league_week";// <message id="stats_league_week" text="Popular Leagues Per Week"/>
   public static final String MSG_KEY_STATS_BETTYPE_DAY = "stats_bettype_day"; //<message id="stats_bettype_day" text="Popular Bet Types Per Day"/>
   public static final String MSG_KEY_STATS_BETTYPE_MONTH = "stats_bettype_month"; //<message id="stats_bettype_month" text="Popular Bet Types Per Month"/>
   public static final String MSG_KEY_STATS_BETTYPE_WEEK = "stats_bettype_week"; //<message id="stats_bettype_week" text="Popular Bet Types Per Week"/>
   public static final String MSG_KEY_STATS_PIECHART_OTHER = "stats_piechart_other"; //<message id="stats_piechart_other" text="Other"/>
   public static final String MSG_KEY_STATS_BEST_WINS = "stats_best_wins"; //<message id="stats_best_wins" text="Your Best Wins"/>
   public static final String MSG_KEY_STATS_LAST_WIN = "stats_last_win"; //<message id="stats_last_win" text="Your Last Wins"/> */
   public static final String MSG_KEY_NO_IN_PLAY_GAMES = "noInplayGames"; // <message id="noInplayGames" text="No Records Found"/>
   public static final String MSG_KEY_NO_HISTORY_GAMES = "noHistoryGames"; // <message id="noHistoryGames" text="No Records Found"/>
   public static final String MSG_KEY_NO_RECORDS = "noBestLastWins"; // <message id="noBestLastWins" text="No Records Found"/>
   public static final String MSG_KEY_STATS_TOTAL_BETS = "stats_totalBets"; //<message id="stats_totalBets" text="Total Bets:"/>
   public static final String MSG_KEY_STATS_WON = "stats_winnings"; //<message id="stats_winnings" text="Won"/>
   public static final String MSG_KEY_STATS_LOST = "stats_lost"; //<message id="stats_lost" text="Lost:"/>
   public static final String MSG_KEY_STATS_PUSH = "stats_push"; // <message id="stats_push" text="Push:"/>
   public static final String MSG_KEY_STATS_WEEKLY_SUMMARY_CC = "stats_WeeklySummaryCC"; //<message id="stats_WeeklySummaryCC" text="Weekly Summary"/>
   public static final String MSG_KEY_OTHER_PUSH3 = "other_push3"; //<message id="other_push3" text="Push (Draw)"/>
   public static final String MSG_KEY_OTHER_PUSH4 = "other_push4"; //<message id="other_push4" text="Push (Canceled)"/>
   public static final String MSG_KEY_OTHER_PUSH5 = "other_push5"; // <message id="other_push5" text="Push (Postponed)"/>
   public static final String MSG_KEY_OTHER_PUSH6 = "other_push6"; // <message id="other_push6" text="Push (Interrupted)"/>
   public static final String MSG_KEY_IPOP_TITLE = "ipop_title"; // text="Betting Options"/>
   public static final String MSG_KEY_IPOP_TEXT = "ipop_text"; //text="Select Between GAME MODE or REAL MONEY Betting Option."/>
   public static final String MSG_KEY_IPOP_LEFT = "ipop_left"; // text="GAME MODE"/>
   public static final String MSG_KEY_IPOP_RIGHT = "ipop_right"; //text="REAL MONEY"/>
   public static final String MSG_KEY_REALMONEYDOWN = "realMoneyDown1"; // text="Real Money option is temporary not available."/>
   public static final String MSG_KEY_REALMONEYDOWN2 = "realMoneyDown2"; // text="www.BetDSI.eu"/>
   public static final String MSG_KEY_REALMONEYDOWN3 = "realMoneyDown3"; // text="WEB &#xFFFD; MOBILE"/>
   public static final String MSG_KEY_REALMONEYDOWN4 = "realMoneyDown4"; // text="Access your BetDSI account directly at www.BetDSI.eu or via telephone."/>
   public static final String MSG_KEY_REALMONEYDOWN5 = "realMoneyDown5"; // text="1-877-223-8374"/>
   public static final String MSG_KEY_REALMONEYDOWN6 = "realMoneyDown6"; // text="Call BetDSI Toll FREE"/>
   public static final String MSG_KEY_REALMONEYTUTORIALP1_1 = "realMoneyTutorialP1_1"; // text="BET ON SPORTS"/>
   public static final String MSG_KEY_REALMONEYTUTORIALP1_2 = "realMoneyTutorialP1_2"; // text="With the #1 Trusted Sportsbook"/>
   public static final String MSG_KEY_REALMONEYTUTORIALP1_3 = "realMoneyTutorialP1_3"; // text="BetDSI"/>
   public static final String MSG_KEY_REALMONEYTUTORIALP1_4 = "realMoneyTutorialP1_4"; // text="Make Money Betting Sports with BetDSI. Play with a licensed A+ rated International Sportsbook."/>
   public static final String MSG_KEY_REALMONEYTUTORIALP1_5 = "realMoneyTutorialP1_5"; // text="Operating Since 1998"/>
   public static final String MSG_KEY_REALMONEYTUTORIALP2_1 = "realMoneyTutorialP2_1"; // text="CASHIER OPTIONS"/>
   public static final String MSG_KEY_REALMONEYTUTORIALP2_2 = "realMoneyTutorialP2_2"; // text="Instant Payment &amp; Free Payouts!"/>
   public static final String MSG_KEY_REALMONEYTUTORIALP2_3 = "realMoneyTutorialP2_3"; // text="Get instant payments and free payouts! Choose from multiple deposit and payment options with 24/7 live support using state-of-art security protocols."/>
   public static final String MSG_KEY_REALMONEYTUTORIALP3_1 = "realMoneyTutorialP3_1"; // text="100% WELCOME BONUS"/>
   public static final String MSG_KEY_REALMONEYTUTORIALP3_2 = "realMoneyTutorialP3_2"; // text="Get up to $300 Free Sportsbook Bonus"/>
   public static final String MSG_KEY_REALMONEYTUTORIALP3_3 = "realMoneyTutorialP3_3"; // text="Start Winning. Fund your new account and instantly receive your 100% Welcome Bonus up to $300."/> */
   public static final String MSG_KEY_APPTUTORIALP1_1 = "appTutorialP1_1";  //  <message id="appTutorialP1_1" text="Where The Action Begins"/>
   public static final String MSG_KEY_APPTUTORIALP1_2 = "appTutorialP1_2";  // <message id="appTutorialP1_2" text="The Virtual Experience of a Vegas Sportsbook"/>
   public static final String MSG_KEY_APPTUTORIALP1_3 = "appTutorialP1_3";  // <message id="appTutorialP1_3" text="Bet on All Major Sports Including International Leagues Using Real Las Vegas Odds and Rules."/>
   public static final String MSG_KEY_APPTUTORIALP2_1 = "appTutorialP2_1";  // <message id="appTutorialP2_1" text="SWIPE Left or Right to Navigate."/>
   public static final String MSG_KEY_APPTUTORIALP2_2 = "appTutorialP2_2";  // <message id="appTutorialP2_2" text="View pending wagers, current betting lines, live scores; account settings with a simple swipe or tap on tabs."/>
   public static final String MSG_KEY_APPTUTORIALP3_1 = "appTutorialP3_1"; // <message id="appTutorialP3_1" text="Tap on betting lines to place wagers or add selections to your Bet Slip."/>
   public static final String MSG_KEY_APPTUTORIALP3_2 = "appTutorialP3_2"; // <message id="appTutorialP3_2" text="Bet with virtual coins using real odds. Place straight bets or add multiple picks to Bet Slip for Parlays and Teasers."/>
   public static final String MSG_KEY_APPTUTORIALP4_1 = "appTutorialP4_1"; // <message id="appTutorialP4_1" text="Tap on Bet Slip icon located in upper right corner to submit your wagers."/>
   public static final String MSG_KEY_APPTUTORIALP4_2 = "appTutorialP4_2"; // <message id="appTutorialP4_2" text="Track your pending plays in our Live Scores section."/>

   public static final String MSG_KEY_STATS_STARTED_WITH = "stats_startedWith"; //<message id="stats_startedWith" text="Started With:"/>
   public static final String MSG_KEY_STATS_ENDED_WITH = "stats_endedWith"; //<message id="stats_endedWith" text=", Ended With:"/>
   public static final String MSG_KEY_STATS_EVENT_DAY = "stats_event_day"; //<message id="stats_event_day" text="Events Per Day"/>
   public static final String MSG_KEY_STATS_EVENT_MONTH = "stats_event_month"; //<message id="stats_event_month" text="Events Per Month"/>
   public static final String MSG_KEY_STATS_EVENT_WEEK = "stats_event_week"; //<message id="stats_event_week" text="Events Per Week"/>
   public static final String MSG_KEY_STATS_LASTBETWINS = "stats_LastBestWins"; //<message id="stats_LastBestWins" text="WINNINGS"/>
   public static final String MSG_KEY_STATS_LEAGUE_DAY = "stats_league_day"; //<message id="stats_league_day" text="Popular Leagues Per Day"/>
   public static final String MSG_KEY_STATS_LEAGUE_MONTH = "stats_league_month"; //<message id="stats_league_month" text="Popular Leagues Per Month"/>
   public static final String MSG_KEY_STATS_LEAGUE_PIECHARTS = "stats_PieCharts"; //<message id="stats_PieCharts" text="PIE CHARTS"/>
   public static final String MSG_KEY_STATS_SPORT_DAY = "stats_sport_day"; //<message id="stats_sport_day" text="Popular Sports Per Day"/>
   public static final String MSG_KEY_STATS_SPORT_MONTH = "stats_sport_month"; //<message id="stats_sport_month" text="Popular Sports Per Month"/>
   public static final String MSG_KEY_STATS_SPORT_WEEK = "stats_sport_week"; //<message id="stats_sport_week" text="Popular Sports Per Week"/>
   public static final String MSG_KEY_STATS_WEEKLY_SUMMARY = "stats_WeeklySummary"; //<message id="stats_WeeklySummary" text="WEEKLY SUMMARY"/>
   public static final String MSG_KEY_STATS_YOURCOUNTRYSPORTS = "stats_yourcountry_sports"; //<message id="stats_yourcountry_sports" text="Popular Sports In Your Country"/>*/
   public static final String MSG_KEY_UPDATE_POPUP_DESC = "updatePopupDescriptionDroid"; //<message id="updatePopupDescription" text="New Version of Sports Betting##tm## app is available on Google Play store."/>
   public static final String MSG_KEY_UPDATE_OK_BUTTON = "updatePopupOkButton"; //<message id="updatePopupOkButton" text="Update"/>
   public static final String MSG_KEY_UPDATE_TITLE = "updatePopupTitle"; //<message id="updatePopupTitle" text="Update Available"/>
   /*<message id="push_interrupt" text="Interrupted"/>
   <message id="push_cancel" text="Canceled"/>
   <message id="push_suspend" text="Suspended"/> */





}