package com.winatsports.data.firstpage;

import java.util.ArrayList;

public class FirstPageGameSection {

    private String titleId;
    private int position;

    private ArrayList<Integer> eventIds;

    public FirstPageGameSection() {
        titleId = null;
        position = -1;
        eventIds = new ArrayList<Integer>();
    }

    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }

    public ArrayList<Integer> getEventIds() {
        return eventIds;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }


}
