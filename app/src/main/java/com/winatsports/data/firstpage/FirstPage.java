package com.winatsports.data.firstpage;


/**
 * Created by broda on 09/12/2015.
 */


public class FirstPage {
    private FirstPageLeagueSection featuredLeagues;
    private FirstPageGameSection upNext;
    private FirstPageGameSection featuredGames;

    public FirstPage() {
        setUpNext(new FirstPageGameSection());
        setFeaturedGames(new FirstPageGameSection());
        featuredLeagues = new FirstPageLeagueSection();
    }

    public FirstPageLeagueSection getFeaturedLeagues() {
        return featuredLeagues;
    }

    public void setFeaturedLeagues(FirstPageLeagueSection featuredLeagues) {
        this.featuredLeagues = featuredLeagues;
    }


    public FirstPageGameSection getUpNext() {
        return upNext;
    }

    public void setUpNext(FirstPageGameSection upNext) {
        this.upNext = upNext;
    }

    public FirstPageGameSection getFeaturedGames() {
        return featuredGames;
    }

    public void setFeaturedGames(FirstPageGameSection featuredGames) {
        this.featuredGames = featuredGames;
    }
}
