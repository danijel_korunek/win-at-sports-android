package com.winatsports.data.firstpage;

import java.util.ArrayList;

/**
 * Created by broda on 10/12/2015.
 */
public class FirstPageLeagueSection {

    private String titleId;

    private int position;

    private ArrayList<Feature> features;

    public FirstPageLeagueSection() {

        titleId = null;
        position = -1;

        features = new ArrayList<Feature>();
    }

    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public ArrayList<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(ArrayList<Feature> features) {
        this.features = features;
    }





}
