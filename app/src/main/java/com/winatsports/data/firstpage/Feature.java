package com.winatsports.data.firstpage;

/**
 * Created by broda on 14/12/2015.
 */
public class Feature {

    private int leagueType;
    private int leagueID;
    private int originalLeague;


    public Feature() {

        leagueType = -1;
        leagueID = -1;
        originalLeague = -1;

    }


    public int getLeagueType() {
        return leagueType;
    }

    public void setLeagueType(int leagueType) {
        this.leagueType = leagueType;
    }

    public int getLeagueID() {
        return leagueID;
    }

    public void setLeagueID(int leagueID) {
        this.leagueID = leagueID;
    }

    public int getOriginalLeague() {
        return originalLeague;
    }

    public void setOriginalLeague(int originalLeague) {
        this.originalLeague = originalLeague;
    }


}
