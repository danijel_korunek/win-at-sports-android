package com.winatsports.data.firstpage;

/**
 * Created by broda on 23/12/2015.
 */
public class HomeNotification {

    private String title;
    private String subTitle;
    private boolean dismissible;  // for boolean variable I will not set default values
    private String url;
    private boolean opensRealBetting;  // for boolean variable I will not set default values
    private boolean isMarchMadness;  // for boolean variable I will not set default values
    private int sportID;
    private int noteID;
    private boolean winLoseReport;

    public HomeNotification() {

        title = null;
        subTitle = null;

        url = null;

        sportID = -1;
        noteID = -1;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public boolean isDismissible() {
        return dismissible;
    }

    public void setDismissible(boolean dismissible) {
        this.dismissible = dismissible;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isOpensRealBetting() {
        return opensRealBetting;
    }

    public void setOpensRealBetting(boolean opensRealBetting) {
        this.opensRealBetting = opensRealBetting;
    }

    public boolean isMarchMadness() {
        return isMarchMadness;
    }

    public void setIsMarchMadness(boolean isMarchMadness) {
        this.isMarchMadness = isMarchMadness;
    }

    public int getSportID() {
        return sportID;
    }

    public void setSportID(int sportID) {
        this.sportID = sportID;
    }

    public int getNoteID() {
        return noteID;
    }

    public void setNoteID(int noteID) {
        this.noteID = noteID;
    }


    public boolean isWinLoseReport() {
        return winLoseReport;
    }

    public void setWinLoseReport(boolean winLoseReport) {
        this.winLoseReport = winLoseReport;
    }
}
