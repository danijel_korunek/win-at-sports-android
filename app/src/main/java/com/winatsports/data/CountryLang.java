package com.winatsports.data;

/**
 * Created by broda on 02/11/2015.
 */
public class CountryLang {

    private String country;
    private String language;
    private String locale;

    public CountryLang() {
        country = null;
        language = null;
        locale = null;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String dumpAllValuesToString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Country: " + ( country==null ? "null":country )  );
        sb.append(" Language: " + ( language==null ? "null":language ));
        sb.append(" Locale: " + ( locale==null ? "null":locale ));
        return sb.toString();
    }

}
