package com.winatsports.data.elements;



import java.util.ArrayList;


/**
 * Created by broda on 18/11/2015.
 */

public class League implements Cloneable {

    private int leagueID;
    private int leagueType;
    private int sportID;
    private String leagueName;
    private int status;

    private ArrayList<Game> gameArrayList;

    public League() {
        leagueID = -1;
        leagueType = -1;
        sportID = -1;
        leagueName = null;
        status = -1;

        gameArrayList = new ArrayList<Game>();
    }

    public int getLeagueType() {
        return leagueType;
    }

    public void setLeagueType(int leagueType) {
        this.leagueType = leagueType;
    }

    public int getLeagueID() {
        return leagueID;
    }

    public void setLeagueID(int leagueID) {
        this.leagueID = leagueID;
    }

    public int getSportID() {
        return sportID;
    }

    public void setSportID(int sportID) {
        this.sportID = sportID;
    }

    public String getLeagueName() {
        return leagueName;
    }

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<Game> getGameArrayList() {
        return gameArrayList;
    }

    public String dumpAllValuesToString() {

        StringBuffer sb = new StringBuffer();
        sb.append(" \n LeagueID " + ( getLeagueID()==-1 ? -1:getLeagueID() )  );
        sb.append(" LeagueType: " + ( getLeagueType()== -1 ? -1: getLeagueType() ) );
        sb.append(" SportID " + ( getSportID()==-1 ? -1:getSportID()  ));
        sb.append(" LeagueName: " + ( getLeagueName()==null ? null:getLeagueName()  ));
        sb.append(" Status: " + ( getStatus()==-1 ? -1:getStatus()  ));
        if( getGameArrayList().size() > 0 ) {
            sb.append("\n\n  This league has: "+getGameArrayList().size()+" activity_games!!...!!! \n");
            sb.append(" \n\n Now all activity_games from this league: \n\n"  );
            for (int i = 0; i < getGameArrayList().size(); i++) {
                sb.append(" \n\n  new game:  \n\n ");
                sb.append(" EventID " + (getGameArrayList().get(i).getEventID() == -1 ? -1 : getGameArrayList().get(i).getEventID()));
                sb.append(" SportID: " + (sportID == -1 ? -1 : sportID));
                sb.append(" OriginalLeague: " + (getGameArrayList().get(i).getOriginalLeague() == -1 ? -1 : getGameArrayList().get(i).getOriginalLeague()));
                sb.append(" LeagueID: " + (leagueID == -1 ? -1 : leagueID));
                sb.append(" LeagueType: " + (leagueType == -1 ? -1 : leagueType));
                sb.append(" StartDate: " + (getGameArrayList().get(i).getStartDate() == null ? null : getGameArrayList().get(i).getStartDate()));
                sb.append(" EventType: " + (getGameArrayList().get(i).getEventType() == -1 ? -1 : getGameArrayList().get(i).getEventType()));
                sb.append(" Status: " + (getGameArrayList().get(i).getStatus() == -1 ? -1 : getGameArrayList().get(i).getStatus()));

                /*for (int x = 0; x < getGameArrayList().get(i).getTeamArrayList().size(); x++) {
                    sb.append(" TeamID: " + (getGameArrayList().get(i).getTeamArrayList().get(x).getTeamID() == -1 ? -1 : getGameArrayList().get(i).getTeamArrayList().get(x).getTeamID()));
                    sb.append(" TeamStatus: " + (getGameArrayList().get(i).getTeamArrayList().get(x).getTeamStatus() == -1 ? -1 : getGameArrayList().get(i).getTeamArrayList().get(x).getTeamStatus()));
                }
                sb.append("\n");
                for (int j = 0; j < getGameArrayList().get(i).getOddArrayList().size(); j++) {
                    sb.append(" bet_type_id: " + (getGameArrayList().get(i).getOddArrayList().get(j).getBetTypeId() == -1 ? -1 : getGameArrayList().get(i).getOddArrayList().get(j).getBetTypeId()));
                    sb.append(" team_id: " + (getGameArrayList().get(i).getOddArrayList().get(j).getTeamId() == -1 ? -1 : getGameArrayList().get(i).getOddArrayList().get(j).getTeamId()));
                    sb.append(" incorrectBetOdds: " + (getGameArrayList().get(i).getOddArrayList().get(j).getBetOdds() == -1 ? -1 : getGameArrayList().get(i).getOddArrayList().get(j).getBetOdds()));
                    sb.append(" bet_line: " + (getGameArrayList().get(i).getOddArrayList().get(j).getBetLine() == null ? null : getGameArrayList().get(i).getOddArrayList().get(j).getBetLine()));
                    sb.append(" out_value: " + (getGameArrayList().get(i).getOddArrayList().get(j).getOutValue() == null ? "ne postoji vrijednost" : getGameArrayList().get(i).getOddArrayList().get(j).getOutValue()));
                    sb.append(" out_bet_line: " + (getGameArrayList().get(i).getOddArrayList().get(j).getOutBetLine() == null ? "isto ne postoji vrijednost" : getGameArrayList().get(i).getOddArrayList().get(j).getOutBetLine()));
                    sb.append(" bet_value: " + (getGameArrayList().get(i).getOddArrayList().get(j).getBetValue() == -1 ? -1 : getGameArrayList().get(i).getOddArrayList().get(j).getBetValue() + "\n"));
                }*/
            }

        }
        else
            sb.append(" This activity_leagues does not have any game. ");

        return sb.toString();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
