package com.winatsports.data.elements;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by broda on 06/11/2015.
 */
public class Sport {

    public static final int SPORT_INACTIVE = 0;
    public static final int SPORT_ACTIVE = 1;

    private int sportID;
    private String sportIconUrl;
    private String sportIconTime;

    private int changeOrder;
    private int status;
    private int haveTeaser;

    private Bitmap scaledIcon;

    private String sportName; // I thought to use this variable, to save data from language_codes.xml, under Section Sports ==> tag name

    private ArrayList<League> leagues;
    private ArrayList<BetType> betTypes;
    private ArrayList<Team> teams;
    private ArrayList<Teaser> teasers;

    public Sport() {
        sportID = -1;
        sportIconUrl = null;
        sportIconTime = null;
        changeOrder = -1;
        status = -1;
        haveTeaser = 0;
        sportName = null;

        leagues = new ArrayList<League>();
        scaledIcon = null;

        betTypes = new ArrayList<BetType>();
        teams = new ArrayList<Team>();
        teasers = new ArrayList<Teaser>();
    }

    public Bitmap getScaledIcon() {
        return scaledIcon;
    }

    public void setScaledIcon(Bitmap scaledIcon) {
        this.scaledIcon = scaledIcon;
    }


    public String getSportIconUrl() {
        return sportIconUrl;
    }

    public void setSportIconUrl(String iconUrl) {
        this.sportIconUrl = iconUrl;
    }

    public int getSportID() {
        return sportID;
    }

    public void setSportID(int sportID) {
        this.sportID = sportID;
    }

    public String getSportIconTime() {
        return sportIconTime;
    }

    public void setSportIconTime(String iconTime) {
        this.sportIconTime = iconTime;
    }

    public int getChangeOrder() {
        return changeOrder;
    }

    public void setChangeOrder(int changeOrder) {
        this.changeOrder = changeOrder;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getHaveTeaser() {
        return haveTeaser;
    }

    public void setHaveTeaser(int teaser) {
        this.haveTeaser = teaser;
    }

    public String getSportName() {
        return sportName;
    }

    public void setSportName(String sportName) {
        this.sportName = sportName;
    }

    public ArrayList<League> getLeagues() {
        return leagues;
    }


    public ArrayList<BetType> getBetTypes() {
        return betTypes;
    }

    public ArrayList<Team> getTeams() {
        return teams;
    }

    public ArrayList<Teaser> getTeasers() {
        return teasers;
    }

}
