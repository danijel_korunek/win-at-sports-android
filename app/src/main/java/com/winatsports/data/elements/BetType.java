package com.winatsports.data.elements;

/**
 * Created by broda on 19/01/2016.
 */
public class BetType {

    private int sportID;
    private int typeID;
    private String typeName;

    public BetType() {
        sportID = -1;
        typeID = -1;
        typeName = null;
    }

    public int getTypeID() {
        return typeID;
    }

    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }


    public int getSportID() {
        return sportID;
    }

    public void setSportID(int sportID) {
        this.sportID = sportID;
    }


}
