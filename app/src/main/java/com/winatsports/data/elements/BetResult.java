package com.winatsports.data.elements;

/**
 * Created by dkorunek on 12/05/16.
 */
public class BetResult {
    private String parlayId;
    private int result;
    private double betAmount;
    private double betWin;

    public BetResult() {
        setParlayId(null);
    }

    public String getParlayId() {
        return parlayId;
    }

    public void setParlayId(String parlayId) {
        this.parlayId = parlayId;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public double getBetAmount() {
        return betAmount;
    }

    public void setBetAmount(double betAmount) {
        this.betAmount = betAmount;
    }

    public double getBetWin() {
        return betWin;
    }

    public void setBetWin(double betWin) {
        this.betWin = betWin;
    }
}
