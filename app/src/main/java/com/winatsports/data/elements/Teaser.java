package com.winatsports.data.elements;

import com.winatsports.MyApplication;
import com.winatsports.Util;
import com.winatsports.data.Constants;

/**
 * Created by broda on 30/10/2015.
 */
public class Teaser {

    private int sportID;
    private int teams;
    private double points;
    private double odd;

    public Teaser(){

        sportID = -1;
        teams = -1;
        points = 0.0;
        odd = 0.0;
    }

    public int getTeams() {
        return teams;
    }

    public void setTeams(int teams) {
        this.teams = teams;
    }

    public double getPoints() {
        return points;
    }

    public void setPoints(double points) {
        this.points = points;
    }

    public int getOdd() {
        return (int) odd;
    }

    public void setOdd(double odd) {
        this.odd = odd;
    }

    public int getSportID() {
        return sportID;
    }

    public void setSportID(int sportID) {
        this.sportID = sportID;
    }

    public String getTeaserTitle() {
        StringBuffer sb = new StringBuffer();
        sb.append("" + getTeams() + " ");
        sb.append(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_TEASER_TEAM) + ", ");
        sb.append(getPoints() + " ");
        sb.append(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_TEASER_PTS) + " ");
        sb.append(Util.formatOdds(MyApplication.getInstance(), getOdd()));
        return sb.toString();
    }
}
