package com.winatsports.data.elements;



import java.util.ArrayList;



public class Game implements Cloneable /* implements Comparable<Game> */ {

    public static String DELIMITER = "####";

    private int eventID;
    private int sportID;
    private int originalLeague;
    private int leagueID;
    private int leagueType;
    private String startDate;
    private int eventType;
    private int status;
    private String gameDesc;
    private String futuresDesc;

    private ArrayList<Team> teamArrayList;
    private ArrayList<Odd> oddArrayList;

    public Game() {

        eventID = -1;
        sportID = -1;
        originalLeague = -1;
        leagueID = -1;
        leagueType = -1;
        startDate = null;
        eventType = -1;
        status = -1;
        gameDesc = null;
        futuresDesc = null;

        teamArrayList = new ArrayList<Team>();
        oddArrayList = new ArrayList<Odd>();
    }

    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public int getSportID() {
        return sportID;
    }

    public void setSportID(int sportID) {
        this.sportID = sportID;
    }

    public int getOriginalLeague() {
        return originalLeague;
    }

    public void setOriginalLeague(int originalLeague) {
        this.originalLeague = originalLeague;
    }

    public int getLeagueID() {
        return leagueID;
    }

    public void setLeagueID(int leagueID) {
        this.leagueID = leagueID;
    }

    public int getLeagueType() {
        return leagueType;
    }

    public void setLeagueType(int leagueType) {
        this.leagueType = leagueType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<Team> getTeamArrayList() {
        return teamArrayList;
    }

    public void setTeamArrayList(ArrayList<Team> teamArrayList) {
        this.teamArrayList = teamArrayList;
    }

    public ArrayList<Odd> getOddArrayList() {
        return oddArrayList;
    }

    public void setOddArrayList(ArrayList<Odd> oddArrayList) {
        this.oddArrayList = oddArrayList;
    }

    public String getGameDesc() {
        return gameDesc;
    }

    public void setGameDesc(String gameDesc) {
        this.gameDesc = gameDesc;
    }

    public String getFuturesDesc() {
        return futuresDesc;
    }

    public void setFuturesDesc(String futuresDesc) {
        this.futuresDesc = futuresDesc;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("event_id=" + eventID + DELIMITER);
        sb.append("sport_id=" + sportID + DELIMITER);
        sb.append("league_id=" + leagueID + DELIMITER);
        sb.append("\n");
        return sb.toString();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
