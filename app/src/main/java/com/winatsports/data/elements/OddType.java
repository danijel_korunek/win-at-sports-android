package com.winatsports.data.elements;

import java.util.ArrayList;

/**
 * Created by dkorunek on 07/12/2016.
 */

public class OddType {
    private String typeName;
    private int betTypeId;
    private ArrayList<Odd> odds;

    public OddType() {
        typeName = null;
        betTypeId = -1;
        odds = new ArrayList<>();
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public int getBetTypeId() {
        return betTypeId;
    }

    public void setBetTypeId(int betTypeId) {
        this.betTypeId = betTypeId;
    }

    public ArrayList<Odd> getOdds() {
        return odds;
    }

    public void setOdds(ArrayList<Odd> odds) {
        this.odds = odds;
    }
}
