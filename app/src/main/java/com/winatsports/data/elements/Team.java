package com.winatsports.data.elements;

/**
 * Created by broda on 04/11/2015.
 */
public class Team {

    private int teamID;
    private int sportID;
    private int teamStatus;
    private String name;

    public Team() {
        teamID = -1;
        sportID = -1;
        teamStatus = -1;
        name = null;
    }

    public int getTeamStatus() {
        return teamStatus;
    }

    public void setTeamStatus(int teamStatus) {
        this.teamStatus = teamStatus;
    }

    public int getTeamID() {
        return teamID;
    }

    public void setTeamID(int teamID) {
        this.teamID = teamID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSportID() {
        return sportID;
    }

    public void setSportID(int sportID) {
        this.sportID = sportID;
    }


    public String dumpAllValuesToString() {

        StringBuffer sb = new StringBuffer();
        sb.append("TeamID: " + getTeamID() );
        sb.append(" SportID " + getSportID() );
        sb.append(" TeamStatus: " + getTeamStatus() );
        sb.append(" Name: " + getName() );

        return sb.toString();
    }



}
