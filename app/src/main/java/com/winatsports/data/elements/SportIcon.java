package com.winatsports.data.elements;

import android.graphics.Bitmap;

/**
 * Created by dkorunek on 09/12/2016.
 */

public class SportIcon {
    private int sportId;
    private Bitmap bitmap;

    public SportIcon() {
        setSportId(-1);
        setBitmap(null);
    }

    public int getSportId() {
        return sportId;
    }

    public void setSportId(int sportId) {
        this.sportId = sportId;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
