package com.winatsports.data;




import android.graphics.Bitmap;

import com.winatsports.MyApplication;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;



/**
 * Created by dkorunek on 04/02/16.
 */


public class MyParlay  {

    // added by nikola 16.02.2016, because when I have parse "user bets parlay" in XmlAddUserHelper.java, I'm missing this variable
    private int ID;

    private double amount;
    private Date time;
    private ArrayList<MyBet> myBets;

    // for teaser
    private double teaserPoints;
    private double teaserOdd;

    // for history
    private double betWin;
    private long betResult;

    // for displaying of icon
    private Bitmap icon;

    public MyParlay() {

        // added by nikola 16.02.2016, because when I have parse "user bets parlay" in XmlAddUserHelper.java, I'm missing this variable
        setID(-1);

        setTime(null);
        setAmount(-1);
        setMyBets(new ArrayList<MyBet>());

        setTeaserPoints(-1);
        setTeaserOdd(0.0);  // --------------------------------- 0.0 SHOULD NOT BE HERE
        setBetWin(-1);
        setBetResult(-1);
        setIcon(null);
    }

    // added by nikola 16.02.2016, because when I have parse "user bets parlay" in XmlAddUserHelper.java, I'm missing this variable
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }


    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public ArrayList<MyBet> getMyBets() {
        return myBets;
    }

    public void setMyBets(ArrayList<MyBet> myBets) {
        this.myBets = myBets;
    }

    public double getTeaserPoints() {
        return teaserPoints;
    }

    public void setTeaserPoints(double teaserPoints) {
        this.teaserPoints = teaserPoints;
    }

    public int getTeaserOdd() {
        return (int) teaserOdd;
    }

    public void setTeaserOdd(double teaserOdd) {
        this.teaserOdd = teaserOdd;
    }

    public double getBetWin() {
        return betWin;
    }

    public void setBetWin(double betWin) {
        this.betWin = betWin;
    }

    public long getBetResult() {
        return betResult;
    }

    public void setBetResult(long betResult) {
        this.betResult = betResult;
    }

    public Bitmap getIcon() {
        return icon;
    }

    public void setIcon(Bitmap icon) {
        this.icon = icon;
    }
}
