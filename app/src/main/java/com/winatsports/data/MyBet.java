package com.winatsports.data;



/**
 * Created by dkorunek on 04/02/16.
 */


public class MyBet {

    //odd
    private long betTypeId;
    private long teamId;
    private double betOdd;
    private long betValue;
    private String typeName;

    // added by nikola 16.02.2016, because when I have parse "user bets parlay" in XmlAddUserHelper.java, I'm missing this variable
    private double betLine;
    private String typeLangValue;
    private String typeLangLine;
    private int homeID;
    private int awayID;


    //game
    private long eventID;
    private long sportID;
    private long leagueID;
    private long leagueType;
    private String startDate;

    //custom
    private String eventDesc;
    private String teamName;
    private String textCache;
    private String typeName2ndHalf;

    public MyBet() {
        betTypeId = teamId = betValue = -1;
        typeName = null;
        eventID = sportID = leagueID = leagueType = -1;
        startDate = null;
        eventDesc = teamName = textCache = typeName2ndHalf;

        // added by nikola 16.02.2016, because when I have parse "user bets parlay" in XmlAddUserHelper.java, I'm missing this variable
        betOdd = betLine = 0.0;
        typeLangValue = typeLangLine = null;
        homeID = awayID = -1;
    }

    public long getBetTypeId() {
        return betTypeId;
    }

    public void setBetTypeId(long betTypeId) {
        this.betTypeId = betTypeId;
    }

    public long getTeamId() {
        return teamId;
    }

    public void setTeamId(long teamId) {
        this.teamId = teamId;
    }

    public int getBetOdd() {
        return (int) betOdd;
    }

    public void setBetOdd(double betOdd) {
        this.betOdd = betOdd;
    }

    public long getBetValue() {
        return betValue;
    }

    public void setBetValue(long betValue) {
        this.betValue = betValue;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public long getEventID() {
        return eventID;
    }

    public void setEventID(long eventID) {
        this.eventID = eventID;
    }

    public long getSportID() {
        return sportID;
    }

    public void setSportID(long sportID) {
        this.sportID = sportID;
    }

    public long getLeagueID() {
        return leagueID;
    }

    public void setLeagueID(long leagueID) {
        this.leagueID = leagueID;
    }

    public long getLeagueType() {
        return leagueType;
    }

    public void setLeagueType(long leagueType) {
        this.leagueType = leagueType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEventDesc() {
        return eventDesc;
    }

    public void setEventDesc(String eventDesc) {
        this.eventDesc = eventDesc;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getTextCache() {
        return textCache;
    }

    public void setTextCache(String textCache) {
        this.textCache = textCache;
    }

    public String getTypeName2ndHalf() {
        return typeName2ndHalf;
    }

    public void setTypeName2ndHalf(String typeName2ndHalf) {
        this.typeName2ndHalf = typeName2ndHalf;
    }


    // added by nikola 16.02.2016, because when I have parse "user bets parlay" in XmlAddUserHelper.java, I'm missing this variable
    public double getBetLine() {
        return betLine;
    }

    public void setBetLine(double betLine) {
        this.betLine = betLine;
    }

    public String getTypeLangValue() {
        return typeLangValue;
    }

    public void setTypeLangValue(String typeLangValue) {
        this.typeLangValue = typeLangValue;
    }

    public String getTypeLangLine() {
        return typeLangLine;
    }

    public void setTypeLangLine(String typeLangLine) {
        this.typeLangLine = typeLangLine;
    }

    public int getHomeID() {
        return homeID;
    }

    public void setHomeID(int homeID) {
        this.homeID = homeID;
    }

    public int getAwayID() {
        return awayID;
    }

    public void setAwayID(int awayID) {
        this.awayID = awayID;
    }
}

