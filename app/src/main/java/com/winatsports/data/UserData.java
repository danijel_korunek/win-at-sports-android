package com.winatsports.data;


import com.winatsports.Util;
import com.winatsports.data.firstpage.HomeNotification;

import java.util.ArrayList;
import com.winatsports.data.elements.BetResult;


/**
 * Created by broda on 18/12/2015.
 *
 * This class is used for saving data from add_user.php
 *
 */


public class UserData {

    // all variable required for UserInfo tag in XML FILE that I get from Server
    private String response;
    private String deviceId;
    private String userD;
    private String dailyBonusUserInfo;
    private String country;
    private int appVersionProtect;
    private double walletAmount;
    private int specialBonus;
    private String firstRun;

    // all variable required for userSettings tag in XML FILE that I get from Server
    private int startPush;
    private int endPush;
    private int betSlider;
    private int confirmBet;
    private int teamOrder;
    private int oddType;
    private int dailyBonusUserSettings;
    private int showInactive;
    private String lang;
    private int winType;
    private boolean confirmedUser;
    private String bookerLogoutUrl;

    private double inPlayAmount; // this is determined locally after a bet is placed or

    private ArrayList<SupportLang> supportLangs;
    private ArrayList<HomeNotification> homeNotifications;
    private ArrayList<MyParlay> userBets;
    private ArrayList<BetResult> dailyWins;

    public UserData() {

        confirmedUser = false;
        bookerLogoutUrl = "";

        response = null; // Or maybe I will set here OK, because for now here is every time written OK, in the future maybe here will be error
        deviceId = "0";
        userD = "0";
        dailyBonusUserInfo = null;
        country = null;
        appVersionProtect = -1;
        walletAmount = -1.0;
        specialBonus = -1;
        firstRun = null;

        startPush = -1;
        endPush = -1;
        betSlider = -1;
        confirmBet = -1;
        teamOrder = -1;
        oddType = -1;
        dailyBonusUserSettings = -1;
        showInactive = -1;
        lang = null;
        winType = -1;

        supportLangs = new ArrayList<SupportLang>();
        homeNotifications = new ArrayList<HomeNotification>();
        userBets = new ArrayList<MyParlay>();
        setDailyWins(new ArrayList<BetResult>());

        inPlayAmount = 0.0;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUserD() {
        return userD;
    }

    public void setUserD(String userD) {
        this.userD = userD;
    }

    public String getDailyBonusUserInfo() {
        return dailyBonusUserInfo;
    }

    public void setDailyBonusUserInfo(String dailyBonusUserInfo) {
        this.dailyBonusUserInfo = dailyBonusUserInfo;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getAppVersionProtect() {
        return appVersionProtect;
    }

    public void setAppVersionProtect(int appVersionProtect) {
        this.appVersionProtect = appVersionProtect;
    }

    public double getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(double walletAmount) {
        this.walletAmount = walletAmount;
    }

    public int getSpecialBonus() {
        return specialBonus;
    }

    public void setSpecialBonus(int specialBonus) {
        this.specialBonus = specialBonus;
    }

    public String getFirstRun() {
        return firstRun;
    }

    public void setFirstRun(String firstRun) {
        this.firstRun = firstRun;
    }

    public int getStartPush() {
        return startPush;
    }

    public void setStartPush(int startPush) {
        this.startPush = startPush;
    }

    public int getEndPush() {
        return endPush;
    }

    public void setEndPush(int endPush) {
        this.endPush = endPush;
    }

    public int getBetSlider() {
        return betSlider;
    }

    public void setBetSlider(int betSlider) {
        this.betSlider = betSlider;
    }

    public int getConfirmBet() {
        return confirmBet;
    }

    public void setConfirmBet(int confirmBet) {
        this.confirmBet = confirmBet;
    }

    public int getTeamOrder() {
        return teamOrder;
    }

    public void setTeamOrder(int teamOrder) {
        this.teamOrder = teamOrder;
    }

    public int getOddType() {
        return oddType;
    }

    public void setOddType(int oddType) {
        this.oddType = oddType;
    }

    public int getDailyBonusUserSettings() {
        return dailyBonusUserSettings;
    }

    public void setDailyBonusUserSettings(int dailyBonusUserSettings) {
        this.dailyBonusUserSettings = dailyBonusUserSettings;
    }

    public int getShowInactive() {
        return showInactive;
    }

    public void setShowInactive(int showInactive) {
        this.showInactive = showInactive;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public int getWinType() {
        return winType;
    }

    public void setWinType(int winType) {
        this.winType = winType;
    }

    public ArrayList<SupportLang> getSupportLangs() {
        return supportLangs;
    }

    public ArrayList<HomeNotification> getHomeNotifications() {
        return homeNotifications;
    }

    public double getInPlayAmount() {
        return inPlayAmount;
    }

    public void resetInPlayAmount() {
        // set in play amount
        double inPlay = 0.0;
        for (MyParlay tempMp : userBets) {
            inPlay += tempMp.getAmount();
        }
        this.inPlayAmount = inPlay;
    }

    public void addParlay(MyParlay p) {
        this.inPlayAmount += p.getAmount();
        userBets.add(0, p);
    }

    public ArrayList<MyParlay> getUserBets() {
        return userBets;
    }

    public ArrayList<BetResult> getDailyWins() {
        return dailyWins;
    }

    public void setDailyWins(ArrayList<BetResult> dailyWins) {
        this.dailyWins = dailyWins;
    }

    public ArrayList<HomeNotification> generateDailyWinNotifications() {
        ArrayList<HomeNotification> notifications = new ArrayList<HomeNotification>();

        double won = 0.0;
        double lost = 0.0;
        double pushed = 0.0;
        int wonCount = 0;
        int lostCount = 0;
        int pushedCount = 0;

        for (BetResult betResult : dailyWins) {
            if (betResult.getResult() == 1) {
                won += betResult.getBetAmount();
                wonCount++;
            } else if (betResult.getResult() == 2) {
                lost += betResult.getBetAmount();
                lost++;
            } else {
                pushed += betResult.getBetAmount();
                pushedCount++;
            }
        }

        if (wonCount > 0) {
            HomeNotification notif = new HomeNotification();
            notif.setTitle(Util.getString(Constants.MSG_KEY_BETTINGRESULTS));
            notif.setDismissible(false);
            notif.setSportID(-1);
            notif.setWinLoseReport(true);
            if (wonCount == 1) {
                notif.setSubTitle(String.format("%s %d %s: %s", Util.getString(Constants.MSG_KEY_OTHER_WON), wonCount,
                        Util.getString(Constants.MSG_KEY_OTHER_BET), Util.formatMoney(won, false)));
            } else {
                notif.setSubTitle(String.format("%s %d %s: %s", Util.getString(Constants.MSG_KEY_OTHER_WON), wonCount,
                        Util.getString(Constants.MSG_KEY_OTHER_BETS), Util.formatMoney(won, false)));
            }
            notifications.add(notif);
        }

        if (lostCount > 0) {
            HomeNotification notif = new HomeNotification();
            notif.setTitle(Util.getString(Constants.MSG_KEY_BETTINGRESULTS));
            notif.setDismissible(false);
            notif.setSportID(-1);
            notif.setWinLoseReport(true);
            if (lostCount == 1) {
                notif.setSubTitle(String.format("%s %d %s: %s", Util.getString(Constants.MSG_KEY_OTHER_LOST), lostCount,
                        Util.getString(Constants.MSG_KEY_OTHER_BET), Util.formatMoney(lost, false)));
            } else {
                notif.setSubTitle(String.format("%s %d %s: %s", Util.getString(Constants.MSG_KEY_OTHER_LOST), lostCount,
                        Util.getString(Constants.MSG_KEY_OTHER_BETS), Util.formatMoney(lost, false)));
            }
            notifications.add(notif);
        }

        if (pushedCount > 0) {
            HomeNotification notif = new HomeNotification();
            notif.setTitle(Util.getString(Constants.MSG_KEY_BETTINGRESULTS));
            notif.setDismissible(false);
            notif.setWinLoseReport(true);
            notif.setSportID(-1);
            if (pushedCount == 1) {
                notif.setSubTitle(String.format("%s %d %s: %s", Util.getString(Constants.MSG_KEY_OTHER_PUSH), pushedCount,
                        Util.getString(Constants.MSG_KEY_OTHER_BET), Util.formatMoney(pushed, false)));
            } else {
                notif.setSubTitle(String.format("%s %d %s: %s", Util.getString(Constants.MSG_KEY_OTHER_LOST), pushedCount,
                        Util.getString(Constants.MSG_KEY_OTHER_BETS), Util.formatMoney(pushed, false)));
            }
            notifications.add(notif);
        }

        return notifications;
    }

    public boolean isConfirmedUser() {
        return confirmedUser;
    }

    public void setConfirmedUser(boolean confirmedUser) {
        this.confirmedUser = confirmedUser;
    }

    public String getBookerLogoutUrl() {
        return bookerLogoutUrl;
    }

    public void setBookerLogoutUrl(String bookerLogoutUrl) {
        this.bookerLogoutUrl = bookerLogoutUrl;
    }
}
