package com.winatsports.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.winatsports.MyApplication;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;

/**
 * Created by dkorunek on 08/04/16.
 */
public class DiskCache {
    private String DIRECTORY = null;
    private final String FILE_PREFS_XML_FILE = "file_prefs";
    private SharedPreferences filePrefs;

    private static DiskCache instance;

    public static DiskCache getInstance() {
        if (instance == null) {
            instance = new DiskCache(MyApplication.getInstance().getApplicationContext());
        }

        return instance;
    }

    public DiskCache(Context context) {
        DIRECTORY = context.getCacheDir().getAbsolutePath() + "/sb_icons";
        //DIRECTORY = Environment.getExternalStorageDirectory().getAbsolutePath() + "/sb_icons";
        File directory = new File(DIRECTORY);
        directory.mkdirs();

        filePrefs = context.getSharedPreferences(FILE_PREFS_XML_FILE, Context.MODE_PRIVATE);

    }

    public void add(final String url, final String timestamp) {
        final String suffixPart = "" + url.subSequence(url.lastIndexOf("."), url.length());
        final String suffix = "_" + url.hashCode() + suffixPart;
        String newFileName = getFileName(url, timestamp);

        Log.d(DiskCache.class.getSimpleName(), "Attempting to add: " + newFileName);

        File newFile = new File(newFileName);
        if (newFile.exists()) {
            Log.d(DiskCache.class.getSimpleName(), "File " + newFile.getAbsolutePath() + " exists. Not adding.");
            return; // we don't add it
        }
        File [] files = new File(DIRECTORY).listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                if (filename.endsWith(suffix))
                    return true;
                return false;
            }
        });

        if (files != null && files.length == 1) {
            // only one file
            String prefix = files[0].getName().substring(0, files[0].getName().indexOf("_"));
            if (!prefix.equals(timestamp)) {
                // file exists, but it's probably older
                files[0].delete();
                Log.d(DiskCache.class.getSimpleName(), "File " + files[0].getAbsolutePath() + " erased.");
            }
        } else if (files != null && files.length > 1) {
            Log.d(DiskCache.class.getSimpleName(), "There's more than 1 file with that prefix!");
        } else {
            Log.d(DiskCache.class.getSimpleName(), "No files were found with that prefix");
        }

        try {
            newFile.createNewFile();
            Log.d(DiskCache.class.getSimpleName(), "File " + newFile.getAbsolutePath() + " created!");
        } catch (Exception e) {
            Log.e(DiskCache.class.getSimpleName(), "Creating new file failed", e);
        }
        // write to new file
        downloadFile(url, newFile);
    }

    public boolean existsFile(String url, String timestamp) {
        File file = new File(getFileName(url, timestamp));
        return file.exists();
    }

    private void downloadFile(final String url, File file) {
        try {
            Log.i(DiskCache.class.getSimpleName(), "Downloading from url: " + url);
            HttpClient client = HttpClientBuilder.create()
                    .setMaxConnTotal(1)
                    .build();

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                    .setRedirectsEnabled(false)
                    .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                    .setCircularRedirectsAllowed(false).build();

            setAllBytesDownloaded(file.getAbsolutePath(), false);

            HttpGet get = new HttpGet((Uri.parse(url).toString()));
            get.setConfig(config);

            HttpResponse response = client.execute(get);

            FileOutputStream fos = new FileOutputStream(file);

            int read = -1;
            do {
                read = response.getEntity().getContent().read();
                fos.write(read);
            } while (read >= 0);

            /*Bitmap downloaded = BitmapFactory.decodeStream(response.getEntity().getContent());

            Bitmap newBitmap = Bitmap.createBitmap(downloaded.getWidth(), downloaded.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas newCanvas = new Canvas(newBitmap);
            int alpha = 210; // from 0 to 250 or 255
            Paint p = new Paint();

            ColorFilter filter = new LightingColorFilter(Color.WHITE, 0);
            p.setAlpha(alpha);
            p.setColorFilter(filter);

            newCanvas.drawBitmap(downloaded, 0, 0, p);

            int width = newBitmap.getWidth() + 25;
            int height = newBitmap.getHeight() + 25;
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(newBitmap, width, height, true);

            scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);*/

            fos.close();

            setAllBytesDownloaded(file.getAbsolutePath(), true);
        } catch (Exception e) {
            Log.e(DiskCache.class.getSimpleName(), "Error downloading file", e);
            Log.e(DiskCache.class.getSimpleName(), "Erasing incomplete file " + file.getAbsolutePath() + ": " + file.delete());
        }
    }

    public Bitmap get(String url, String timestamp) {
        String bitmapFileName = getFileName(url, timestamp);
        File file = new File(bitmapFileName);
        if (file.exists()) {
            try {
                FileInputStream fis = new FileInputStream(file);
                return BitmapFactory.decodeStream(fis);
            } catch (Exception e) {
                Log.e(DiskCache.class.toString(), "Error reading file from disk cache", e);
            }
        }

        return null;
    }

    public boolean areAllBytesDownloaded(String filename) {
        String fileDescription = filePrefs.getString(filename, null);

        if (fileDescription != null) {
            return Boolean.parseBoolean(fileDescription);
        }

        return false;
    }

    public void setAllBytesDownloaded(String filename, boolean state) {
        SharedPreferences.Editor editor = filePrefs.edit();
        editor.putString(filename, new Boolean(state).toString());
        editor.commit();
    }

    private String getFileName(String url, String timestamp) {
        final String suffix = "" + url.subSequence(url.lastIndexOf("."), url.length());
        return DIRECTORY + "/" + timestamp + "_" + url.hashCode() + suffix;
    }
}
