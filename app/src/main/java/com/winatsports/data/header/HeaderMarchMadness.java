package com.winatsports.data.header;

/**
 * Created by broda on 02/11/2015.
 */
public class HeaderMarchMadness {

    private int status;
    private String marchTitle;
    private String marchText;
    private String marchURL;
    private int sportID;
    private String zipDate;

    public HeaderMarchMadness() {
        status = -1;
        marchTitle = null;
        marchText = null;
        marchURL = null;
        sportID = -1;
        zipDate = null;
    }

    public String getMarchTitle() {
        return marchTitle;
    }

    public void setMarchTitle(String marchTitle) {
        this.marchTitle = marchTitle;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMarchText() {
        return marchText;
    }

    public void setMarchText(String marchText) {
        this.marchText = marchText;
    }

    public String getMarchURL() {
        return marchURL;
    }

    public void setMarchURL(String marchURL) {
        this.marchURL = marchURL;
    }

    public int getSportID() {
        return sportID;
    }

    public void setSportID(int sportID) {
        this.sportID = sportID;
    }

    public String getZipDate() {
        return zipDate;
    }

    public void setZipDate(String zipDate) {
        this.zipDate = zipDate;
    }

}
