package com.winatsports.data.header;

/**
 * Created by broda on 30/10/2015.
 *
 * modified by Danijel on 22/12/2016
 */
public class HeaderRealBetSettings {

    private boolean realbetInSafari;
    private boolean realbetOn;
    private int realbetRuns;
    private boolean realbetGuestsOn;
    private int realbetGuestsRuns;
    private String realbetFromRegDate;

    public HeaderRealBetSettings(){
        this.realbetInSafari = false;
        realbetOn = false;
        realbetRuns = -1;
        realbetGuestsOn = false;
        realbetGuestsRuns = -1;
        realbetFromRegDate = null;
    }

    public boolean isRealbetInSafari() {
        return realbetInSafari;
    }

    public void setRealbetInSafari(boolean realbetInSafari) {
        this.realbetInSafari = realbetInSafari;
    }

    public boolean isRealbetOn() {
        return realbetOn;
    }

    public void setRealbetOn(boolean realbetOn) {
        this.realbetOn = realbetOn;
    }

    public int getRealbetRuns() {
        return realbetRuns;
    }

    public void setRealbetRuns(int realbetRuns) {
        this.realbetRuns = realbetRuns;
    }

    public boolean isRealbetGuestsOn() {
        return realbetGuestsOn;
    }

    public void setRealbetGuestsOn(boolean realbetGuestsOn) {
        this.realbetGuestsOn = realbetGuestsOn;
    }

    public int getRealbetGuestsRuns() {
        return realbetGuestsRuns;
    }

    public void setRealbetGuestsRuns(int realbetGuestsRuns) {
        this.realbetGuestsRuns = realbetGuestsRuns;
    }

    public String getRealbetFromRegDate() {
        return realbetFromRegDate;
    }

    public void setRealbetFromRegDate(String realbetFromRegDate) {
        this.realbetFromRegDate = realbetFromRegDate;
    }

}
