package com.winatsports.data.header;

/**
 * Created by broda on 30/10/2015.
 *
 * modified by Danijel on Dec 22 2016
 */



public class HeaderMainClass {

    public int registerBonus;
    private int appVersionProtect;
    private int appMaintenance;
    private int parlayLimit;
    private boolean showingAppTutorial;
    private boolean showingRealBettingTutorial;

    private HeaderRealBetSettings headerRealBetSettings;
    private HeaderMarchMadness marchMadness;
    private HeaderAppTutorial appTutorial;
    private HeaderRealBettingTutorial realBettingTutorial;

    public HeaderMainClass() {
        // prvo sve inicijalizirati na null, -1 ili ""
        registerBonus = -1;
        appVersionProtect = -1;
        appMaintenance = -1;
        parlayLimit = -1;

        headerRealBetSettings = null;
        marchMadness = null;
        appTutorial = null;
        realBettingTutorial = null;
        setShowingAppTutorial(false);
    }

    public HeaderRealBetSettings getRealBetSettings() {
        return headerRealBetSettings;
    }

    public void setRealBetSettings(HeaderRealBetSettings headerRealBetSettings) {
        this.headerRealBetSettings = headerRealBetSettings;
    }

    public HeaderMarchMadness getMarchMadness() {
        return marchMadness;
    }

    public void setMarchMadness(HeaderMarchMadness marchMadness) {
        this.marchMadness = marchMadness;
    }

    public HeaderAppTutorial getAppTutorial() {
        return appTutorial;
    }

    public void setAppTutorial(HeaderAppTutorial appTutorial) {
        this.appTutorial = appTutorial;
    }

    public HeaderRealBettingTutorial getRealBettingTutorial() {
        return realBettingTutorial;
    }

    public void setRealBettingTutorial(HeaderRealBettingTutorial realBettingTutorial) {
        this.realBettingTutorial = realBettingTutorial;
    }

    public int getRegisterBonus() {
        return registerBonus;
    }

    public void setRegisterBonus(int registerBonus) {
        this.registerBonus = registerBonus;
    }

    public int getAppVersionProtect() {
        return appVersionProtect;
    }

    public void setAppVersionProtect(int appVersionProtect) {
        this.appVersionProtect = appVersionProtect;
    }

    public int getAppMaintenance() {
        return appMaintenance;
    }

    public void setAppMaintenance(int appMaintenance) {
        this.appMaintenance = appMaintenance;
    }

    public int getParlayLimit() {
        return parlayLimit;
    }

    public void setParlayLimit(int parlayLimit) {
        this.parlayLimit = parlayLimit;
    }

    public boolean isShowingAppTutorial() {
        return showingAppTutorial;
    }

    public void setShowingAppTutorial(boolean showingAppTutorial) {
        this.showingAppTutorial = showingAppTutorial;
    }

    public boolean isShowingRealBettingTutorial() {
        return showingRealBettingTutorial;
    }

    public void setShowingRealBettingTutorial(boolean showingRealBettingTutorial) {
        this.showingRealBettingTutorial = showingRealBettingTutorial;
    }

/*
    public String dumpAllValuesToString() {
        StringBuffer sb = new StringBuffer();
        sb.append("RegisterBonus: " + ( registerBonus==-1 ? -1:registerBonus )  );
        sb.append(" AppVersionProtect: " + ( appVersionProtect==-1 ? -1:appVersionProtect  ));
        sb.append(" AppMaintenance: " + ( appMaintenance==-1 ? -1:appMaintenance  ));
        sb.append(" ParlayLimit: " + ( parlayLimit==-1 ? -1:parlayLimit  ) + "\n" );

        sb.append(" RealBetSettings realBetInSafari: " + (  getRealBetSettings() == null ? null:getRealBetSettings().getRealbetInSafari()  ));
        sb.append(" RealbetOn: " + (  getRealBetSettings() == null ? null:getRealBetSettings().getRealbetOn()  ));
        sb.append(" RealbetRuns: " + (  getRealBetSettings() == null ? null:getRealBetSettings().getRealbetRuns()  ));
        sb.append(" RealbetGuestsOn: " + (getRealBetSettings() == null ? null:getRealBetSettings().getRealbetGuestsOn()  ));
        sb.append(" RealbetGuestsRuns: " + ( headerRealBetSettings == null ? null:headerRealBetSettings.getRealbetGuestsRuns()) );
        sb.append(" RealbetFromRegDate: " + (  headerRealBetSettings == null ? null:headerRealBetSettings.getRealbetFromRegDate()) + "\n"  );

        sb.append(" MarchMadness Status: " + (  getMarchMadness() == null ? null:getMarchMadness().getStatus()  ));
        sb.append(" MarchTitle: " + (  getMarchMadness() == null ? null:getMarchMadness().getMarchTitle()   ));
        sb.append(" MarchText: " + (  getMarchMadness() == null ? null:getMarchMadness().getMarchText()   ));
        sb.append(" MarchURL: " + (  getMarchMadness() == null ? null:getMarchMadness().getMarchURL()   ));
        sb.append(" sportID: " + (  marchMadness == null ? null:marchMadness.getSportID() ) );
        sb.append(" zipDate: " + (  marchMadness == null ? null:marchMadness.getZipDate() )   + "\n"  );

        sb.append(" App Tutorial lastUpdate: " + (  getAppTutorial() == null ? null : getAppTutorial().getLastUpdate() ) );
        sb.append(" pageNumber: " + (  appTutorial == null ? null:appTutorial.getPageNumber() )   + "\n"  );

        sb.append(" RealBetting Tutorial lastUpdate: " + (  getRealBettingTutorial() == null ? null : getRealBettingTutorial().getLastUpdate() ) );
        sb.append(" pageNumber: " + (  realBettingTutorial == null ? null:realBettingTutorial.getPageNumber() )   + "\n"  );

        return sb.toString();
    }

*/




}
