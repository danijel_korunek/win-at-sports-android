package com.winatsports.data.header;

/**
 * Created by broda on 02/11/2015.
 */


public class HeaderRealBettingTutorial {

    public String lastUpdate;
    public int pageNumber;

    public HeaderRealBettingTutorial() {
        lastUpdate = null;
        pageNumber = -1;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

}
