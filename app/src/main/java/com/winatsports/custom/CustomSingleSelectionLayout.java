package com.winatsports.custom;


import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.activities.SettingsActivity;
import com.winatsports.settings.AsyncChangeSettings;
import com.winatsports.settings.Settings;

import java.util.ArrayList;



/**
 * Created by broda on 02/12/2015.
 */


public class CustomSingleSelectionLayout extends LinearLayout {

    private SettingsActivity settingsActivity;

    private View rootView;
    private TextView tvTitle;
    private LinearLayout llOptions;
    private ArrayList<String> options;
    private int underlinedItem; // something like index
    private AsyncChangeSettings taskOddType, taskTeamOrder;

    private String optionKey;

    public CustomSingleSelectionLayout(Context context) {
        super(context);
        initializeViews(context);
    }

    public CustomSingleSelectionLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeViews(context);
    }

    public CustomSingleSelectionLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeViews(context);
    }

    private void initializeViews( Context context ) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rootView = inflater.inflate(R.layout.single_selection_layout, this);

        options = new ArrayList<String>();

        tvTitle = (TextView) rootView.findViewById(R.id.tvSettingsActivityWallet);
        llOptions = (LinearLayout)rootView.findViewById(R.id.llOptions);
        redrawOptions();

        optionKey = null;
        taskOddType = taskTeamOrder = null;

    }

    public ArrayList<String> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<String> options) {
        this.options = options;
        redrawOptions();
    }

    private void redrawOptions() {

        llOptions.removeAllViews();

        // make sure that width is distributed evenly among all the elements
        if (options.size() > 0) setWeightSum(options.size());

        if( optionKey != null && optionKey.equals(Settings.KEY_ODD_TYPE) ) {
            // IMPORTANT TO KNOW, AND TO LOOK IN "XmlAddUserHelper.java" CLASS
            // AFTER THIS LINE OF CODE ===> MyApplication.getInstance().setUserData(userData);
            underlinedItem = Settings.getOddType(getContext() );
        }
        // IMPORTANT TO KNOW, AND TO LOOK IN "XmlAddUserHelper.java" CLASS
        // AFTER THIS LINE OF CODE ===> MyApplication.getInstance().setUserData(userData);
        if( optionKey != null && optionKey.equals(Settings.KEY_TEAM_ORDER) ) {
            underlinedItem = Settings.getTeamOrder(getContext());
        }

        OnClickListener clickListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                String tag = (String) v.getTag();
                try {
                    int newElementIndex = Integer.parseInt(tag);

                    int oldElementIndex = underlinedItem; // from variable underlinedItem ==> I get current index

                    // show animations, IF USER click ON NEW ITEM(NEW INDEX),, AND when underlined item changes on user's click
                    //if( newElementIndex != oldElementIndex )
                      //  setUnderlinedItem(newElementIndex, true);
                    // I need this if user want to change settings but there is no internet
                    // so if user click 4,5 or more times on the same "ODD_TYPE INDEX" or the same "TEAM_ORDER INDEX", then this message will display
                    if( Util.isNetworkConnected(getContext()) == false ) {
                        setUnderlinedItem(oldElementIndex, true);
                    }
                        //setUnderlinedItem(newElementIndex, true);
                    else if( newElementIndex != oldElementIndex && Util.isNetworkConnected(getContext()) == true )
                        setUnderlinedItem(newElementIndex, true);

                } catch (Exception e) {
                    Log.e(CustomSingleSelectionLayout.class.getSimpleName(), "Error in class CustomSingleSelectionLayout: ", e);
                }
            }
        };

        for( int index = 0; index < options.size(); index++ ) {
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1);
            SelectionElement temp = new SelectionElement(getContext());

            temp.getTvLabel().setText(options.get(index));

            if(index == underlinedItem) {
                temp.setUnderlined(true, false);
                temp.getTvLabel().setTextColor(getResources().getColor(R.color.white));
            }

            temp.setOnClickListener(clickListener);
            temp.setTag("" + index); // this setTag is connected with clickListener,, (String) v.getTag();
            llOptions.addView(temp, lp);
        }

    }

    public void setTitle(String text) {
        tvTitle.setText(text);
    }

    public void passActivity(SettingsActivity settingsActivity1) {
        this.settingsActivity = settingsActivity1;
    }

    public int getUnderlinedItem() {
        return underlinedItem;
    }


    // this is method which happens, on user click
    // this method is STARTED ONCE, every time the SettingsActivity STARTS
    public void setUnderlinedItem(int underlinedItem, boolean animate) {

        int oldUnderlinedItem = this.underlinedItem;
        // for example if I set oddTypes.setUnderlinedItem(1, false), when this Activity start, then is underlinedItem = 1
        // that means,, this.underlinedItem = 1.. After that, on every user click I change value of ==> this.underlinedItem with help of parameters
        this.underlinedItem = underlinedItem;

        if( optionKey != null && optionKey.equals(Settings.KEY_ODD_TYPE) ) {
            if (taskOddType == null) {
                taskOddType = new AsyncChangeSettings( settingsActivity, llOptions, AsyncChangeSettings.OPERATION.ODD_TYPE, underlinedItem, oldUnderlinedItem, animate);
                taskOddType.execute();
                taskOddType = null;
            } else {
                // nothing - still running, let it finish
            }

        }
        else if( optionKey != null && optionKey.equals(Settings.KEY_TEAM_ORDER) ) {
            if (taskTeamOrder == null) {
                taskTeamOrder = new AsyncChangeSettings( settingsActivity, llOptions, AsyncChangeSettings.OPERATION.TEAM_ORDER, underlinedItem, oldUnderlinedItem, animate);
                taskTeamOrder.execute();
                taskTeamOrder = null;
            } else {
                // nothing - still running, let it finish
            }
        }

    }


    public String getOptionKey() {
        return optionKey;
    }

    public void setOptionKey(String optionKey) {
        this.optionKey = optionKey;
    }

}
