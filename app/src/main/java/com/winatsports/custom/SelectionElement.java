package com.winatsports.custom;



import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.winatsports.R;

/**
 * Created by broda on 01/12/15.
 */


public class SelectionElement extends LinearLayout {

    private TextView tvLabel;
    private View underscoreView;
    private boolean underlined;
    private int underlinedColor;
    private int regularColor;

    public SelectionElement(Context context) {
        super(context);
        initViews(context);
    }

    public SelectionElement(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(context);
    }

    public SelectionElement(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews(context);
    }

    private void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.selection_element, this, true);
        tvLabel = (TextView) findViewById(R.id.tvLabel);
        //tvLabel.setTextSize(getResources().getDimension(R.dimen.smaller_list_row_text_size));
        underscoreView = findViewById(R.id.underscoreView);
        setOrientation(LinearLayout.VERTICAL);
        // initially we assume false and don't animate the initial state.. in other word at start of activity we don't animate the initial state
        underlined = false;
        underlinedColor = Color.WHITE;
        regularColor = Color.GRAY;
        setUnderlined(underlined, false);
    }

    public boolean isUnderlined() {
        return underlined;
    }

    // first parameter is to check if some item has underline or not.. If underlined equals to true, then we need to draw underline
    // second parameter is to show animation on some item or not,, and to change text color
    public void setUnderlined(boolean underlined, boolean animated) {
        this.underlined = underlined;
        // customize text color to show that the selected element is of a different color
        Integer colorFrom = null;
        Integer colorTo = null;
        ValueAnimator colorAnimation = null;

        if( underlined == true ) {
            colorFrom = regularColor;
            colorTo = underlinedColor;
            if (animated)
                underscoreView.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.fade_in_settings));
            underscoreView.setVisibility(VISIBLE);

        } else {
            colorFrom = underlinedColor;
            colorTo = regularColor;
            if (animated)
                underscoreView.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.fade_out_settings));
            underscoreView.setVisibility(INVISIBLE);
        }

        if (animated) {
            colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
            colorAnimation.setDuration(200); // 200 ms - exactly as long as the fade animations (fade_in_settings, fade_out_settings).. I have this animations in folder => anim
            colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                @Override
                public void onAnimationUpdate(ValueAnimator animator) {
                    tvLabel.setTextColor((Integer) animator.getAnimatedValue());
                }

            });

            colorAnimation.start();
        }
    }

    public TextView getTvLabel() {
        return tvLabel;
    }

    public View getUnderscoreView() {
        return underscoreView;
    }

}