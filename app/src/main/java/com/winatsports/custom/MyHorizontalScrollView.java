package com.winatsports.custom;





import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.HorizontalScrollView;

import com.winatsports.MyApplication;
import com.winatsports.activities.MainActivity;



public class MyHorizontalScrollView extends HorizontalScrollView {

	private DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
	private GestureDetector gestureDetector;
    OnTouchListener gestureListener;
	private HorizontalScrollView myHorizontalScrollView; 
	private final String TAG = MyHorizontalScrollView.class.getSimpleName();
	
	private int mActiveFeature = 0;
	private static final int SWIPE_MIN_DISTANCE = 5;
	private static final int SWIPE_THRESHOLD_VELOCITY = 300;
	private static final int NUMBER_OF_SUB_VIEWS = 6;
	 
	
	private MainActivity mainActivity;
	
	/**
	 * this is only for the menu bar scroll view
	 * @param setUpMainActivity
	 */
	public void setMainActivity(MainActivity setUpMainActivity){
		this.mainActivity = setUpMainActivity;
	}
	
	private OnTouchListener gListener = new OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			//If the user swipes
			// performClick(); // I thought that this method will remove the yellow line ==> onTouch(View v)
			if (gestureDetector.onTouchEvent(event)) {  
				return true;
			}
		    else if(event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL ) {
				int scrollX = getScrollX();
				int featureWidth = v.getMeasuredWidth();
				mActiveFeature = ((scrollX + ( featureWidth / 2 ) )/featureWidth);
				int scrollTo = mActiveFeature * featureWidth;

				// hide soft keyboard if it's visible
				hideSoftKeyboardFromWindow();
				smoothScrollTo(scrollTo, 0);

				if(mainActivity != null) {
					MyApplication.getInstance().setLastActiveTab(mActiveFeature);
					mainActivity.centerMenuItem(mActiveFeature);
					mainActivity.changeBackgroundColorMenu(mActiveFeature);
				}
				//Log.e("KOJI AKTIVAN", "koji je view sada aktivan? " + mActiveFeature + " scrollTo iznosi: " + scrollTo + " koliko ce iznositi " + aaa.oneItemWidth2);
				
				return true;
			}
			else {
				return false;
			}
		}
	};
    
	public MyHorizontalScrollView(Context context) {
		super(context);
		myHorizontalScrollView = this; 
		//gestureDetector = new GestureDetector(new XScrollDetector());
	    setFadingEdgeLength(0);
	    setOnTouchListener(gListener);
	    gestureDetector = new GestureDetector(new MyGestureDetector());   
	}
	
	public MyHorizontalScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		myHorizontalScrollView = this;
		setOnTouchListener(gListener);
		gestureDetector = new GestureDetector(new MyGestureDetector());
		//gestureDetector = new GestureDetector(new XScrollDetector());
	    setFadingEdgeLength(0); 
	}
	
	public MyHorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		myHorizontalScrollView = this;
		setOnTouchListener(gListener);
		gestureDetector = new GestureDetector(new MyGestureDetector());
		//gestureDetector = new GestureDetector(new XScrollDetector());
	    setFadingEdgeLength(0); 
	}
	 
	/*@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
	    //Call super first because it does some hidden motion event handling
	   boolean result = super.onInterceptTouchEvent(ev);
	   //Now see if we are scrolling horizontally with the custom gesture detector
	   if (gestureDetector.onTouchEvent(ev)) {
	        return result;
	   } 
	   //If not scrolling horizontally (more x than y), don't hijack the event.
	    else {
	        return false;
	   }
	}*/

	@Override
	public boolean performClick() {
		return super.performClick();
	}
	
	class MyGestureDetector extends SimpleOnGestureListener {
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
			try {
				//right to left
				if( e1 == null || e2 == null )
					return false;
				if( e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					int featureWidth = getMeasuredWidth();
					mActiveFeature = (mActiveFeature < (NUMBER_OF_SUB_VIEWS - 1))? mActiveFeature + 1:NUMBER_OF_SUB_VIEWS -1;
					hideSoftKeyboardFromWindow();
					smoothScrollTo( mActiveFeature*featureWidth , 0 );
					if(mainActivity != null){
						MyApplication.getInstance().setLastActiveTab(mActiveFeature);
						mainActivity.centerMenuItem(mActiveFeature);
						mainActivity.changeBackgroundColorMenu(mActiveFeature);

					} 
					//horizontal_Menu.smoothScrollTo(mActiveFeature*featureWidth, 0);
					//Log.e("koliko", "aktivan je view: " + mActiveFeature + "  bbb " + mActiveFeature*featureWidth);
					return true;
				}
				//left to right
				else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					int featureWidth = getMeasuredWidth();
					mActiveFeature = (mActiveFeature > 0)? mActiveFeature - 1:0;
					hideSoftKeyboardFromWindow();
					smoothScrollTo( mActiveFeature*featureWidth , 0 );
					if(mainActivity != null){
						MyApplication.getInstance().setLastActiveTab(mActiveFeature);
						mainActivity.centerMenuItem(mActiveFeature);
						mainActivity.changeBackgroundColorMenu(mActiveFeature);
					}
					//horizontal_Menu.smoothScrollTo(mActiveFeature*featureWidth, 0);
					//Log.e("koliko", "koji je view sada aktivan? " + mActiveFeature + "  bbb " + mActiveFeature*featureWidth );
					return true;
				}
			} catch (Exception e) {
				Log.e("Fling", "There was an error processing the Fling event:" + e.getMessage(), e);
			}
			return false;
		}
	}


	
	// Return false if we're scrolling in the y direction  
	/** For now I don't need that Class
	   class XScrollDetector extends SimpleOnGestureListener {
		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float     distanceY) {
			 
	        float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
	        float dpWidth = displayMetrics.widthPixels;//  / displayMetrics.density;
			try {
				 
				if ( Math.abs(distanceX) > Math.abs(distanceY) ) {
					
					if( distanceX > (dpWidth/2) ){
						// scroll horizontally for the width of the screen
						myHorizontalScrollView.scrollBy((int) distanceX, 0);
						Log.d(TAG, "  DistanceX = " + ( (int)distanceX) );
					}
									
					return true;
				} else {
					return false;
				}
			} catch (Exception e) {
				// nothing
			}
			return false;
		}
	}*/

	public void hideSoftKeyboardFromWindow() {
		// hide soft keyboard if it's visible
		InputMethodManager imm = (InputMethodManager) MyApplication.getInstance().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}

}
