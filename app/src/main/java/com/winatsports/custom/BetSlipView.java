package com.winatsports.custom;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.data.Constants;
import com.winatsports.data.elements.League;
import com.winatsports.data.elements.Odd;
import com.winatsports.data.elements.Sport;
import com.winatsports.data.elements.Teaser;
import com.winatsports.data.parlaytotal.ParlayOdd;
import com.winatsports.settings.Settings;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by dkorunek on 06/05/16.
 */
public class BetSlipView extends LinearLayout {
    public static class TeaserRowModel {
        private String displayString;
        private boolean checked;
        private Teaser teaser;

        public View parent;
        public TeaserViewHolder viewHolder;
        public int position;

        public TeaserRowModel() {
            setDisplayString("");
            setChecked(false);
            setTeaser(null);
        }

        public String getDisplayString() {
            return displayString;
        }

        public void setDisplayString(String displayString) {
            this.displayString = displayString;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }

        public Teaser getTeaser() {
            return teaser;
        }

        public void setTeaser(Teaser teaser) {
            this.teaser = teaser;
        }
    }

    public static class ParlayRowModel {
        private String title;
        private String gameParticipants;
        private String dateTime;
        private String leagueAndOddName;
        private Bitmap betIcon;

        public ParlayViewHolder viewHolder;
        public View parent;
        public int position;

        public ParlayRowModel() {
            title = gameParticipants = dateTime = leagueAndOddName = null;
            betIcon = null;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getGameParticipants() {
            return gameParticipants;
        }

        public void setGameParticipants(String gameParticipants) {
            this.gameParticipants = gameParticipants;
        }

        public String getDateTime() {
            return dateTime;
        }

        public void setDateTime(String dateTime) {
            this.dateTime = dateTime;
        }

        public String getLeagueAndOddName() {
            return leagueAndOddName;
        }

        public void setLeagueAndOddName(String leagueAndOddName) {
            this.leagueAndOddName = leagueAndOddName;
        }

        public Bitmap getBetIcon() {
            return betIcon;
        }

        public void setBetIcon(Bitmap betIcon) {
            this.betIcon = betIcon;
        }
    }

    private ArrayList<ParlayRowModel> parlayRows;
    private ArrayList<TeaserRowModel> teaserRows;

    private Context context;
    private TextView tvTitleWithOdds;
    private LinearLayout llParlayContainer;
    private TextView tvTeaserTitle;
    private TextView tvTeaserMessage;
    private LinearLayout llTeaserContainer;

    private LayoutInflater inflater;
    private View rootView;

    private int prevSelectedPosition;
    private Teaser selectedTeaser;
    private boolean initExecuted;

    public static class ParlayViewHolder {
        public ImageView ivSportIcon;
        public TextView tvGameName;
        public TextView tvParticipants;
        public TextView tvDateAndTime;
        public TextView tvLeagueName;
        public Button btnDelete;
    }

    public static class TeaserViewHolder {
        public CheckBox cbSelected;
        public TextView tvTeamPointsTeaser;
    }

    public BetSlipView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public BetSlipView(Context c, AttributeSet set) {
        super(c, set);
        this.context = c;
        init();
    }

    public BetSlipView(Context c, AttributeSet set, int style) {
        super(c, set, style);
        this.context = c;
        init();
    }

    public void init() {
        initExecuted = false;
        parlayRows = new ArrayList<ParlayRowModel>();
        teaserRows = new ArrayList<TeaserRowModel>();
        prevSelectedPosition = -1;

        inflater = LayoutInflater.from(context);
        rootView = inflater.inflate(R.layout.bet_slip_view, this);

        tvTitleWithOdds = (TextView) rootView.findViewById(R.id.tvTitleWithOdds);
        llParlayContainer = (LinearLayout) rootView.findViewById(R.id.llParlayContainer);
        tvTeaserTitle = (TextView) rootView.findViewById(R.id.tvTeaserTitle);
        tvTeaserMessage = (TextView) rootView.findViewById(R.id.tvTeaserMessage);
        llTeaserContainer = (LinearLayout) rootView.findViewById(R.id.llTeaserContainer);

        llParlayContainer.setDrawingCacheEnabled(false);

        if (MyApplication.getInstance().getParlayArray().size() > 0) {
            for (int i = 0; i < MyApplication.getInstance().getParlayArray().size(); i++) {
                final ParlayOdd odd = MyApplication.getInstance().getParlayArray().get(i);
                final ParlayRowModel row = new ParlayRowModel();

                row.setTitle(odd.getTitleOfOdd());

                String teamVsTeam = Util.formatHomeVsAway(odd.getGame(), MyApplication.getInstance());
                row.setGameParticipants(teamVsTeam);
                String leagueAndOddName = "";
                League league = Util.getLeague(odd.getGame().getLeagueID());
                String oddName = Util.getOddName(odd);
                if (league != null) {
                    leagueAndOddName = league.getLeagueName() + ", " + oddName;
                }
                row.setLeagueAndOddName(leagueAndOddName);
                row.setBetIcon(odd.getSport().getScaledIcon());
                row.setDateTime(odd.getGame().getStartDate());

                View view = inflater.inflate(R.layout.listrow_bet_slip_parlay, null);

                if (i % 2 == 0)
                    view.setBackgroundColor(getResources().getColor(R.color.list_black_row));
                else
                    view.setBackgroundColor(getResources().getColor(R.color.list_gray_row));

                ParlayViewHolder holder = new ParlayViewHolder();
                holder.ivSportIcon = (ImageView) view.findViewById(R.id.ivSportIcon);
                holder.ivSportIcon.setImageBitmap(row.getBetIcon());

                holder.tvGameName = (TextView) view.findViewById(R.id.tvGameName);
                holder.tvGameName.setText(row.getTitle());

                holder.tvParticipants = (TextView) view.findViewById(R.id.tvParticipants);
                holder.tvParticipants.setText(row.getGameParticipants());

                holder.tvDateAndTime = (TextView) view.findViewById(R.id.tvDateAndTime);
                holder.tvDateAndTime.setText(row.getDateTime());

                holder.tvLeagueName = (TextView) view.findViewById(R.id.tvLeagueName);
                holder.tvLeagueName.setText(row.getLeagueAndOddName());

                holder.btnDelete = (Button) view.findViewById(R.id.btnDelete);
                holder.btnDelete.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PARLAY_DELETE));

                if (!MyApplication.getInstance().isBetSlipInEditMode())
                    holder.btnDelete.setVisibility(View.GONE);
                else
                    holder.btnDelete.setVisibility(View.VISIBLE);

                holder.btnDelete.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MyApplication.getInstance().getParlayArray().remove(odd);

                        Set<String> parlaySet = new TreeSet<String>();
                        for (ParlayOdd odd : MyApplication.getInstance().getParlayArray()) {
                            parlaySet.add(odd.toString());
                        }

                        Settings.saveParlay(MyApplication.getInstance(), parlaySet);

                        BetSlipView.this.removeAllViews();
                        invalidate();
                        init();
                    }
                });

                row.parent = view;
                row.viewHolder = holder;

                parlayRows.add(row);
                llParlayContainer.addView(view);
            }

            tvTeaserTitle.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_AVAILABLETEASERS));
            tvTeaserTitle.setGravity(Gravity.CENTER_HORIZONTAL);

            llTeaserContainer.setDrawingCacheEnabled(false);

            String msg = areTeasersAvailable();
            if (msg == null) {
                Sport sport = MyApplication.getInstance().getParlayArray().get(0).getSport();
                if (sport.getTeasers().size() > 0) {

                    int i = 0;
                    for (Teaser teaser : sport.getTeasers()) {
                        if (teaser.getTeams() == MyApplication.getInstance().getParlayArray().size()) {
                            String currentTitle = teaser.getTeaserTitle();
                            TeaserRowModel row = new TeaserRowModel();
                            row.setTeaser(teaser);
                            row.setChecked(false);
                            row.setDisplayString(currentTitle);

                            View rowView = inflater.inflate(R.layout.listrow_bet_slip_teaser, null);
                            if (i % 2 == 0)
                                rowView.setBackgroundColor(getResources().getColor(R.color.list_black_row));
                            else
                                rowView.setBackgroundColor(getResources().getColor(R.color.list_gray_row));
                            rowView.setTag(row);

                            TeaserViewHolder viewHolder = new TeaserViewHolder();
                            viewHolder.cbSelected = (CheckBox) rowView.findViewById(R.id.cbSelected);

                            viewHolder.tvTeamPointsTeaser = (TextView) rowView.findViewById(R.id.tvTeamPointsTeaser);
                            viewHolder.tvTeamPointsTeaser.setText(row.getDisplayString());

                            rowView.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    TeaserRowModel row = (TeaserRowModel) v.getTag();
                                    //Log.d(BetSlipView.class.getSimpleName(), "Clicked on position: " + row.position);

                                    if (row.position == prevSelectedPosition) {
                                        row.setChecked(false);
                                        row.viewHolder.tvTeamPointsTeaser.setTypeface(null, Typeface.NORMAL);
                                        row.viewHolder.cbSelected.setChecked(false);
                                        prevSelectedPosition = -1;
                                        selectedTeaser = null;
                                    } else {
                                        row.setChecked(true);
                                        row.viewHolder.tvTeamPointsTeaser.setTypeface(null, Typeface.BOLD);
                                        row.viewHolder.cbSelected.setChecked(true);
                                        selectedTeaser = row.teaser;
                                        prevSelectedPosition = row.position;
                                    }

                                    for (int j = 0; j < teaserRows.size(); j++) {

                                        if (j == row.position)
                                            continue;

                                        TeaserRowModel temp = teaserRows.get(j);

                                        temp.setChecked(false);
                                        temp.viewHolder.tvTeamPointsTeaser.setTypeface(null, Typeface.NORMAL);
                                        temp.viewHolder.cbSelected.setChecked(false);
                                    }

                                    if (prevSelectedPosition == -1 && !initExecuted) {
                                        prevSelectedPosition = row.position;
                                        initExecuted = true;
                                        selectedTeaser = null;
                                    }

                                    updateParlayRows();
                                }
                            });

                            row.parent = rowView;
                            row.viewHolder = viewHolder;
                            row.position = i;

                            i++;

                            teaserRows.add(row);
                            llTeaserContainer.addView(rowView);
                        }
                    }
                }
                tvTeaserMessage.setText("");
                tvTeaserMessage.setVisibility(GONE);
            } else {
                tvTeaserMessage.setText(msg);
                tvTeaserMessage.setVisibility(VISIBLE);
            }

            //tvTeaserTitle.setVisibility(VISIBLE);
        } else {
            //tvTeaserTitle.setVisibility(INVISIBLE);
        }
        tvTitleWithOdds.setText(Util.getBetSlipTitle());
        tvTeaserTitle.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_AVAILABLETEASERS));
        tvTeaserTitle.setGravity(Gravity.CENTER_HORIZONTAL);
    }

    public void refresh() {
        for (int i = 0; i < parlayRows.size(); i++) {
            ParlayRowModel row = parlayRows.get(i);

            if (MyApplication.getInstance().isBetSlipInEditMode()) {
                row.viewHolder.btnDelete.setVisibility(VISIBLE);
            } else {
                row.viewHolder.btnDelete.setVisibility(GONE);
            }
        }
    }

    private void updateParlayRows() {
        for (int i = 0; i < parlayRows.size(); i++) {
            ParlayRowModel row = parlayRows.get(i);
            ParlayOdd odd = MyApplication.getInstance().getParlayArray().get(i);
            if (selectedTeaser != null) {
                String out = reformatBetLine(odd.getOdd(), selectedTeaser);
                row.viewHolder.tvGameName.setText(makeBetTitle(odd.getOdd(), out));
            } else {
                row.viewHolder.tvGameName.setText(makeBetTitle(odd.getOdd(), null));
            }
        }
    }

    private String makeBetTitle(Odd odd, String mOutBetLine) {
        String descriptionOdds = null;

        if( Util.isPopulated(odd.getOutValue())  ) {
            String outValue = odd.getOutValue();
            descriptionOdds = MyApplication.getInstance().getAllMessages().get(outValue);

            if (mOutBetLine == null) {

                if (odd.getOutValue() != null && odd.getOutBetLine() != null)
                    descriptionOdds += " " + odd.getOutBetLine();
            } else {
                descriptionOdds += " " + mOutBetLine;
            }
        }
        else {
            descriptionOdds = odd.getTeamName();

            if (mOutBetLine == null) {
                if (Util.isPopulated(odd.getOutBetLine())) {
                    descriptionOdds += " " + odd.getOutBetLine();
                }
            } else {
                descriptionOdds += " " + mOutBetLine;
            }
        }

        return descriptionOdds + " (" + Util.formatOdds(MyApplication.getInstance(), odd.getBetOdds()) + ")";
    }

    private String reformatBetLine(Odd odd, Teaser t) {
        try {
            double bline = Double.parseDouble(odd.getBetLine());
            String out = "";

            if (odd.getBetTypeId() == 3) {
                bline += t.getPoints();
            }
            if (odd.getBetTypeId() == 7)
                if (odd.getBetValue() == 1)
                    bline = bline - t.getPoints();
                else
                    bline = bline + t.getPoints();

            if (bline == Math.round(bline))
                out = "" + (long) Math.abs(bline);
            else
                out = String.format("%.01f", Math.abs(bline));

            if (odd.getBetTypeId() == 3) {
                if (bline > 0.0) out = "+" + out;
                if (bline < 0.0) out = "-" + out;
            }

            return out;
        } catch (Exception e) {
            Log.e(BetSlipView.class.getName(), "reformat bet line", e);
        }

        return "";
    }

    /**
     *
     * @return null if teasers are available, otherwise a warning message
     */
    private String areTeasersAvailable() {
        // teasers are supported only when between 2 and 7 wagers are present in bet slip
        if (MyApplication.getInstance().getParlayArray().size() < 2 || MyApplication.getInstance().getParlayArray().size() > 7)
            return Util.getString(Constants.MSG_KEY_TEASERWARNING5);

        // teasers are available only for
        ParlayOdd prevParlayOdd = null;
        for (ParlayOdd parlayOdd : MyApplication.getInstance().getParlayArray()) {
            if ( !(parlayOdd.getGame().getSportID() == Constants.C_ID_SPORT_BASKETBALL ||
                    parlayOdd.getGame().getSportID() == Constants.C_ID_SPORT_FOOTBALL))
                return MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_TEASERWARNING1);

            if (parlayOdd.getGame().getEventType() == 3)
                return MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_TEASERWARNING3);

            if (parlayOdd.getOdd().getBetTypeId() != 3 && parlayOdd.getOdd().getBetTypeId() != 7) {
                return MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_TEASERWARNING4);
            }

            if (prevParlayOdd != null && parlayOdd.getGame().getSportID() != prevParlayOdd.getGame().getSportID())
                return MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_TEASERWARNING2);

            prevParlayOdd = parlayOdd;
        }

        return null;
    }

    public Teaser getSelectedTeaser() {
        return selectedTeaser;
    }
}
