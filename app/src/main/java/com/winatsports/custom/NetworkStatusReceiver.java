package com.winatsports.custom;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

import com.winatsports.MyApplication;
import com.winatsports.Util;

/**
 * Created by dkorunek on 19/09/16.
 *
 * In this app this was ONLY for BrowserActivity!
 */
public class NetworkStatusReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(NetworkStatusReceiver.class.getSimpleName(), "Connected? " + Util.isNetworkConnected(context));
        // even if this broadcast action is deprecated for Android Nougat, I'll keep it here.
        Intent toSend = new Intent(MyApplication.ACTION_NETWORK_STATUS_UPDATE);
        toSend.putExtra(MyApplication.KEY_NETWORK_STATUS, Util.isNetworkConnected(context));
        context.sendBroadcast(toSend);

    }
}
