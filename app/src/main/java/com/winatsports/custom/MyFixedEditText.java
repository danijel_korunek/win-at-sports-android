package com.winatsports.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import com.winatsports.R;

/**
 * Created by dkorunek on 25/03/16.
 */
public class MyFixedEditText extends EditText {

    public MyFixedEditText(Context context) {
        super(context);
        init();
    }

    public MyFixedEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyFixedEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        int paddingLeftRight = (int) getResources().getDimension(R.dimen.registration_edit_text_padding);
        int topPadding = this.getPaddingTop();
        int bottomPadding = this.getPaddingBottom();
        setPadding(paddingLeftRight, topPadding, paddingLeftRight, bottomPadding);
    }
}