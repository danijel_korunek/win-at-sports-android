package com.winatsports.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import java.util.List;

/**
 * Created by dkorunek on 05/09/16.
 */
public class MyPieChart extends View {


    List<String> labels;
    List<Integer> colors;
    List<Float> values;
    Paint piePaint;
    RectF mBounds;
    int width;
    int height;
    TextPaint textPaint;
    Paint squarePaint;
    float scale;

    public MyPieChart(Context context) {
        super(context);

        init();
    }

    public MyPieChart(Context context, AttributeSet attributeSet){
        super(context, attributeSet);

        init();
    }


    public void init(){

        piePaint = new Paint();

        piePaint.setAntiAlias(true);
        piePaint.setDither(true);
        piePaint.setStyle(Paint.Style.FILL);

        textPaint = new TextPaint();
        // Convert the dips to pixels
        scale = getContext().getResources().getDisplayMetrics().density;
        textPaint.setTextSize(16.0f * scale + 0.5f);
        textPaint.setColor(Color.WHITE);

        squarePaint = new Paint();
        squarePaint.setStyle(Paint.Style.FILL);

        setMinimumWidth(300);
        setMinimumHeight(300);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Try for a width based on our minimum
        int minw = getPaddingLeft() + getPaddingRight() + getSuggestedMinimumWidth();

        width = Math.max(minw, MeasureSpec.getSize(widthMeasureSpec));

        // Whatever the width ends up being, ask for a height that would let the pie
        // get as big as it can
        int minh = width  + getPaddingBottom() + getPaddingTop() + 300;
        if (heightMeasureSpec == 0)
            heightMeasureSpec = widthMeasureSpec;
        height = Math.min(MeasureSpec.getSize(heightMeasureSpec), minh);
        setMeasuredDimension(width, height);
    }


    @Override
    protected void onDraw(Canvas canvas){
        mBounds = new RectF(0, 0, getWidth(), getHeight());
        piePaint.setColor(Color.BLACK);
        canvas.drawRect(mBounds, piePaint);

        if (values != null) {

            int top = 300;
            int diameter = getHeight() - top - 50;
            int left = (getWidth() - diameter) / 2;
            int right = getWidth() / 2 + diameter / 2;
            int bottom = top + diameter;

            mBounds = new RectF(left, top, right, bottom);

            float[] segment = pieSegments();

            float segStartPoint = 0;

            for (int i = 0; i < segment.length; i++){

                piePaint.setColor(colors.get(i));
                canvas.drawArc(mBounds, segStartPoint, segment[i], true, piePaint);
                segStartPoint += segment[i];
            }
        } else {
            piePaint.setColor(Color.BLUE);
            canvas.drawOval(mBounds, piePaint);
        }

        int top = (int) (16 * scale);
        int left = (int) (5 * scale);

        if (labels != null && colors != null) {
            for (int i = 0; i < labels.size(); i++) {
                int changedY = top + ((int) (i * 16 * scale));

                Rect square = new Rect(left, changedY - 35, left + 35, changedY);
                squarePaint.setColor(colors.get(i));
                canvas.drawRect(square, squarePaint);
                canvas.drawText(labels.get(i) + String.format(": %.2f%%", values.get(i)), left + 50, changedY, textPaint);
            }
        }
    }


    private float[] pieSegments() {

        float[] segValues = new float[values.size()];
        float total = getTotal();

        for (int i = 0; i < values.size(); i++){

            segValues[i] = (values.get(i) / total) * 360;
        }

        return segValues;
    }


    private float getTotal(){

        float total = 0;

        for(int i = 0; i < values.size(); i++)
            total += values.get(i);

        return total;
    }


    public void setData(List<String> labels, List<Integer> colors, List<Float> values){
        this.labels = labels;
        this.colors = colors;
        this.values = values;
        invalidate();
    }

}