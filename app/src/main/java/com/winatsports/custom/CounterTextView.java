package com.winatsports.custom;



import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.TextView;

import com.winatsports.R;

/**
 * Created by dkorunek on 27/01/16.
 */
public class CounterTextView extends TextView {
    private int counter;
    private int circleFillColor;

    public CounterTextView(Context context) {
        super(context);
        this.counter = 0;
        this.circleFillColor = Color.WHITE;
    }

    public CounterTextView(Context context, AttributeSet set) {
        super(context, set);
        setTextColor(getResources().getColor(R.color.gray));
        this.counter = 0;
        this.circleFillColor = Color.WHITE;
    }

    public CounterTextView(Context context, AttributeSet set, int defStyle) {
        super(context, set, defStyle);
        setTextColor(getResources().getColor(R.color.gray));
        this.counter = 0;
        this.circleFillColor = Color.WHITE;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        RectF rect = new RectF(0, 0, getWidth(), getHeight());

        Paint clearPaint = new Paint();
        clearPaint.setStyle(Paint.Style.FILL);
        clearPaint.setColor(Color.TRANSPARENT);
        canvas.drawRect(rect, clearPaint);

        Paint circlePaint = new Paint();
        circlePaint.setColor(Color.WHITE);
        circlePaint.setStyle(Paint.Style.FILL);
        canvas.drawOval(rect, circlePaint);

        TextPaint tp = new TextPaint();
        tp.bgColor = Color.TRANSPARENT;
        tp.setColor(getCurrentTextColor());
        tp.setTextSize(getResources().getDimensionPixelSize(R.dimen.counter_text_size));

        Rect textBounds = new Rect();
        tp.getTextBounds(getText().toString().toCharArray(), 0, getText().length(), textBounds);

        float width = tp.measureText(getText().toString(), 0, getText().length());

        canvas.drawText(getText().toString().toCharArray(), 0, getText().length(), (getWidth() - width) / 2, (getHeight() + textBounds.height()) / 2, tp);
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
        setText("" + this.counter);
    }

    public int getCircleFillColor() {
        return circleFillColor;
    }

    public void setCircleFillColor(int circleFillColor) {
        this.circleFillColor = circleFillColor;
    }
}
