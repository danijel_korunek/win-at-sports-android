package com.winatsports;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Process;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.tune.ma.application.TuneApplication;
import com.winatsports.activities.GuestActivity;
import com.winatsports.data.Constants;
import com.winatsports.data.CountryLang;
import com.winatsports.data.DiskCache;
import com.winatsports.data.MyParlay;
import com.winatsports.data.UserData;
import com.winatsports.data.download.AsyncDownloadData;
import com.winatsports.data.download.DownloadLiveScoresThread;
import com.winatsports.data.firstpage.FirstPage;
import com.winatsports.data.header.HeaderMainClass;
import com.winatsports.data.elements.Game;
import com.winatsports.data.elements.League;
import com.winatsports.data.elements.Sport;
import com.winatsports.data.elements.SportIcon;
import com.winatsports.data.livescores.LiveEventScore;
import com.winatsports.data.parlaytotal.ParlayOdd;
import com.winatsports.data.stats.StatsCollection;
import com.winatsports.data.upload.AsyncRealBetting;
import com.winatsports.settings.Settings;
import com.facebook.FacebookSdk;
import com.tune.Tune;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MyApplication extends TuneApplication {

	private static MyApplication instance;

	public static MyApplication getInstance() {
		return instance;
	}

	private ExecutorService executorService;

	private UserData userData;
	private UserData userDataCopy;

	private ArrayList<SportIcon> sportIcons;

	private ArrayList<CountryLang> allDefaultLanguages;
	private ArrayList<CountryLang> allDefaultLanguagesCopy;

	private LinkedHashMap<String, String> allMessages;
	private LinkedHashMap<String, String> allMessagesCopy;

	private HeaderMainClass headerMainClass;
	private HeaderMainClass headerMainClassCopy;
	private ArrayList<Sport> allSports;
	private ArrayList<Sport> allSportsCopy;

	private ArrayList<Game> allGames;
	private ArrayList<Game> allGamesCopy;

	private ArrayList<League> allLeagues;
	private ArrayList<League> allLeaguesCopy;
	private LinkedHashMap<String, String> allLiveTerms;
	private LinkedHashMap<String, String> allLiveTermsCopy;

	private ArrayList<LiveEventScore> allFinishedLiveScores;
    private ArrayList<LiveEventScore> allActiveLiveScores;

	private FirstPage firstPage;
	private FirstPage firstPageCopy;

	private ArrayList<ParlayOdd> parlayArray;

	private ArrayList<MyParlay> historyWagers;
	private ArrayList<MyParlay> historyWagersCopy;

	public Dialog rbDialog;

	private AsyncRealBetting rbTask;

	public static final Object lock = new Object();

	private int lastActiveTab;

	private boolean rbShownAtStart;

	private volatile boolean betSlipInEditMode;

	private Thread inspectionThread;
	private boolean isDownloading;
	private volatile boolean inspectionThreadRunning;
	private final long DOWNLOAD_REPEAT_INTERVAL =  2 * 60 * 1000 + 40 * 1000; // 2 minutes 40 seconds
	//private final long DOWNLOAD_REPEAT_INTERVAL =  25 * 1000; // 25 seconds
	private boolean allowReDownload;
	private AsyncDownloadData downloadTask;

	/** This is only used for download activity indicator. Each activity should show it or hide it
	 * based on receiving of intent with this action.
	 */
	public final static String ACTION_DOWNLOAD_STATE = "com.winatsports.ACTION_DOWNLOAD_STATE";
	public final static String ACTION_REFRESH_WAGERS = "com.winatsports.ACTION_REFRESH_WAGERS";
	/** This is for real betting dialog. */
	public final static String ACTION_SHOW_RB_DIALOG = "com.winatsports.ACTION_SHOW_RB_DIALOG";
	/** This indicates that most of the main data was downloaded and is available for display. */
	public final static String ACTION_ARRAYS_SWITCHED = "com.winatsports.ACTION_ARRAYS_SWITCHED";

	public final static String ACTION_ADD_WAGERS_HISTORY = "com.winatsports.ACTION_ADD_WAGERS_HISTORY";
	public final static String ACTION_NETWORK_STATUS_UPDATE = "com.winatsports.ACTION_NETWORK_STATUS_UPDATED";
	public final static String KEY_NETWORK_STATUS = "network_status";
	public final static String ACTION_QUITTING = "com.winatsports.ACTION_QUIT";
	public final static String ACTION_NETWORK_FAILURE = "com.winatsports.ACTION_NETWORK_FAILURE";

	// this is for the nag screen
	public final static String ACTION_LAUNCH_GUEST_ACTIVITY = "com.winatsports.ACTION_LAUNCH_GUEST_ACTIVITY";
	public final static String KEY_REENTERED = "reentered";

	public final static String ACTION_REDOWNLOAD = "com.winatsports.redownload";


	public static final long BG_TOLERANCE_TIME = 10000;

	// Don't ever set this to null! It's used for custom navigation
	private Stack<String> activityStack;

	private MyParlay parlayToDisplay;

	private String searchString;

	private volatile long lastTimeDownloadRan = 0;

	private com.winatsports.data.DiskCache diskCache;

	public static final long REAL_BETTING_BROWSER_RELOG_TIME = 2400;
	public static final long REAL_BETTING_DIALOG_TIME = 8; // 8 seconds

	private static final String ADVERTISER_ID = "13330";
	private static final String CONVERSION_KEY = "0b3527629b667545aef87cab8c05a30b";

	private volatile boolean alreadyInForeground;
	private volatile boolean wasAppSentToBackground;
	private long timeMyActivityPaused;
	public volatile boolean wasAppInBgMoreThanXSeconds;

	private Thread.UncaughtExceptionHandler defaultHandler;

	private StatsCollection statsCollection;
	private boolean quitting;

	private final int FAILED_CONNECTS_TOLERANCE = 5;

	@Override
	public void onCreate() {
		super.onCreate();

		instance = this;
		setTimeMyActivityPaused(0);

		rbShownAtStart = false;

		Tune.init(this, ADVERTISER_ID, CONVERSION_KEY);

		FacebookSdk.sdkInitialize(getApplicationContext());

		// TODO leave this for testing but remove it for release
//		defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
//
//		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
//			@Override
//			public void uncaughtException(Thread thread, Throwable ex) {
//
//				inspectionThreadRunning = false;
//				executorService.shutdown();
//
//				String report = Util.generateCrashReport(ex);
//
//				Intent activity = new Intent(getApplicationContext(), BugReportActivity.class);
//				activity.putExtra("bug_report", report);
//				activity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//				startActivity(activity);
//
//				android.os.Process.killProcess(Process.myPid());
//				System.exit(2);
//
//				defaultHandler.uncaughtException(thread, ex);
//			}
//		});

		betSlipInEditMode = false;

		allowReDownload = false;
		diskCache = new com.winatsports.data.DiskCache(this);

		executorService = Executors.newFixedThreadPool(15); // 15 fixed threads

		activityStack = new Stack<String>();

		sportIcons = new ArrayList<>();

		userData = new UserData();
		userDataCopy = new UserData();

		allDefaultLanguages = new ArrayList<CountryLang>();
		allDefaultLanguagesCopy = new ArrayList<CountryLang>();

		allMessages = new LinkedHashMap<String, String>();
		allMessagesCopy = new LinkedHashMap<String, String>();

		headerMainClass = new HeaderMainClass();
		headerMainClassCopy = new HeaderMainClass();

		allSports = new ArrayList<Sport>();
		allSportsCopy = new ArrayList<Sport>();

		allGames = new ArrayList<Game>();
		allGamesCopy = new ArrayList<Game>();

		allLeagues = new ArrayList<League>();
		allLeaguesCopy = new ArrayList<League>();

		allLiveTerms = new LinkedHashMap<String, String>();
		allLiveTermsCopy = new LinkedHashMap<String, String>();

		allFinishedLiveScores = new ArrayList<LiveEventScore>();
		allActiveLiveScores = new ArrayList<LiveEventScore>();

		firstPage = new FirstPage();
		firstPageCopy = new FirstPage();

		parlayArray = new ArrayList<ParlayOdd>();

		historyWagers = new ArrayList<MyParlay>();
		historyWagersCopy = new ArrayList<MyParlay>();

		wasAppInBgMoreThanXSeconds = true;

		MyApplication.getInstance().setLastActiveTab(Constants.C_VIEW_HOME_SCREEN);

		inspectionThread = new Thread(new Runnable() {

			final long LIVE_SCORE_REPEAT = 30000; // every 30 sec
			long lastLiveScoreDownloaded = 0;
			long lastTimeInBackground = 0;
			boolean alreadyAddedSeconds = false;
			boolean overwriteBgValue = true;

			@Override
			public void run() {
				inspectionThreadRunning = true;
				wasAppSentToBackground = false;
				overwriteBgValue = true;
				int failCount = 0;

				while (inspectionThreadRunning) {
					try {
						Thread.sleep(500);
					} catch (Exception e) {
						// ignored
					}

					if (!inspectionThreadRunning)
						return;

					boolean inBg = Util.isApplicationSentToBackground(MyApplication.getInstance());
					long now = System.currentTimeMillis();

					if (allowReDownload && (lastLiveScoreDownloaded == 0 || (now - lastLiveScoreDownloaded > LIVE_SCORE_REPEAT))) {
						// download live scores
						lastLiveScoreDownloaded = now;
						if (!inspectionThreadRunning)
							return;
						if (Util.isNetworkConnected(instance)) {
							failCount = 0;
							executorService.submit(new DownloadLiveScoresThread(MyApplication.getInstance()));
						} else
							failCount++;

						if (failCount > FAILED_CONNECTS_TOLERANCE) {
							Intent intent = new Intent(MyApplication.ACTION_NETWORK_FAILURE);
							sendBroadcast(intent);
						}

					} else {
						// let the time pass
					}

					if (inBg) {
						stopDownloadTask();

						//Log.d(MyApplication.class.getName(), "last time diff : " + (now - lastTimeInBackground));
						if (lastTimeInBackground == 0 || (now - lastTimeInBackground) > BG_TOLERANCE_TIME) {
							if (overwriteBgValue) {
								lastTimeInBackground = now;
								wasAppSentToBackground = true;
								wasAppInBgMoreThanXSeconds = false;
								Settings.setTimeWhenEnteredBg(instance, now);
								overwriteBgValue = false;
							}
						} else
							overwriteBgValue = false;

						alreadyInForeground = false;

						if (!alreadyAddedSeconds) {
							long stats_timer = Settings.getStatsTimer(instance);
							long loginTime = Settings.getLoginTime(instance);
							long fgTimer = Settings.getTimeWhenEnteredFg(instance);
							long timeSpentInAppUntilNow = 0;

							if (loginTime > fgTimer)
								timeSpentInAppUntilNow = (now - loginTime) / 1000;
							else
								timeSpentInAppUntilNow = (now - fgTimer) / 1000;

							stats_timer += timeSpentInAppUntilNow;
							Log.d(MyApplication.class.getSimpleName(), "Stats_timer increase for " + timeSpentInAppUntilNow);
							Settings.setStatsTimer(instance, stats_timer);
							alreadyAddedSeconds = true;
						}

					} else {
						alreadyAddedSeconds = false;

						if (!alreadyInForeground) {
							Settings.setTimeWhenEnteredFg(instance, now);
							alreadyInForeground = true;
							overwriteBgValue = true;
							long timeSpentOutsideOfTheApp = (Settings.getTimeWhenEnteredFg(instance) - Settings.getTimeWhenEnteredBg(instance)) / 1000L;

							Log.d(MyApplication.class.getName(), "Time spent outside of the app: " + timeSpentOutsideOfTheApp);

							if (timeSpentOutsideOfTheApp > REAL_BETTING_DIALOG_TIME) {
								wasAppInBgMoreThanXSeconds = true;
							}

							if ((timeSpentOutsideOfTheApp * 1000) > BG_TOLERANCE_TIME && allowReDownload) {
								Log.d(MyApplication.class.getName(), "Redownloading...");

								// count times run as a reentry
								Settings.setTimesRun(instance, Settings.getTimesRun(instance) + 1);
								String topActivity = Util.getTopActivity(getApplicationContext());

								if (!Util.isApplicationSentToBackground(getApplicationContext())) {

									if (topActivity != null) {
										if (Settings.getUserR(instance).equals(Settings.DEFAULT_USER_R)) {
											// launch guest activity in the current task
											if (!topActivity.equalsIgnoreCase(GuestActivity.class.getName())) {
												Intent broadcastIntent = new Intent(MyApplication.ACTION_LAUNCH_GUEST_ACTIVITY);
												sendBroadcast(broadcastIntent);
											}
										} else {
											// user is logged in
											Intent intent = new Intent(MyApplication.ACTION_REDOWNLOAD);
											sendBroadcast(intent);
										}
									}
								}
							}

						}

						wasAppSentToBackground = false;

					}

					// handle download stuff
					if (!isDownloading && !allowReDownload && lastTimeDownloadRan != 0) {

						if ((now - lastTimeDownloadRan) < DOWNLOAD_REPEAT_INTERVAL)
							allowReDownload = false;
						else
							allowReDownload = true;
					}

					if (!inspectionThreadRunning)
						return;

					if (!isDownloading && allowReDownload) {
						if (now - lastTimeDownloadRan > DOWNLOAD_REPEAT_INTERVAL) {
							lastTimeDownloadRan = now;
							stopDownloadTask();
							downloadData(MyApplication.getInstance(), false, false, true);
						}
					}

				}
			}
		});

		// add a shutdown hook to stop executor service
//		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
//			@Override
//			public void run() {
//				Log.d(MyApplication.class.getSimpleName(), "Performing some shutdown cleanup...");
//				if (executorService != null) {
//					executorService.shutdown();
//
//					while (true) {
//						try {
//							Log.d(MyApplication.class.getSimpleName(), "Waiting for the service to terminate...");
//							if (executorService.awaitTermination(5, TimeUnit.SECONDS)) {
//								break;
//							}
//						} catch (InterruptedException e) {
//						}
//					}
//				}
//
//				Log.d(MyApplication.class.getSimpleName(), "Done cleaning");
//			}
//		}));
	}

	public synchronized void setUserData(UserData userData) {
		synchronized (lock) {
			this.userData = userData;
		}
	}

	public AsyncDownloadData getDownloadTask() { return downloadTask; }

	public synchronized UserData getUserData() {
		return userData;
	}

	public synchronized ArrayList<CountryLang> getAllDefaultLanguages() {
		return allDefaultLanguages;
	}

	public synchronized LinkedHashMap<String, String> getAllMessages() {
		return allMessages;
	}

	public synchronized HeaderMainClass getHeaderMainClass() {
		return headerMainClass;
	}

	public synchronized void setHeaderMainClass(HeaderMainClass headerMainClass) {
		synchronized (lock) {
			this.headerMainClass = headerMainClass;
		}
	}

	public synchronized HeaderMainClass getHeaderMainClassCopy() {
		return headerMainClassCopy;
	}

	public synchronized ArrayList<Sport> getAllSports() {
		return allSports;
	}

	public synchronized ArrayList<Game> getAllGames() {
		return allGames;
	}

	public synchronized ArrayList<League> getAllLeagues() {
		return allLeagues;
	}

	public synchronized LinkedHashMap<String, String> getAllLiveTerms() {
		return allLiveTerms;
	}

	public synchronized ArrayList<LiveEventScore> getAllFinishedLiveScores() {
		return allFinishedLiveScores;
	}

	public synchronized ArrayList<LiveEventScore> getAllActiveLiveScores() {
		return allActiveLiveScores;
	}

	public synchronized void setAllFinishedLiveScores(ArrayList<LiveEventScore> fls) {
		synchronized (lock) {
			ArrayList<LiveEventScore> temp = allFinishedLiveScores;
			allFinishedLiveScores = fls;
		}
	}

	public void setAllActiveLiveScores(ArrayList<LiveEventScore> als) {
		synchronized (lock) {
			ArrayList<LiveEventScore> temp = allActiveLiveScores;
			allActiveLiveScores = als;
		}
	}

	public synchronized FirstPage getFirstPage() {
		return firstPage;
	}

	public synchronized ArrayList<ParlayOdd> getParlayArray() {
		return parlayArray;
	}

	public synchronized ArrayList<MyParlay> getHistoryWagers() {
		return historyWagers;
	}

	public int getLastActiveTab() {
		return lastActiveTab;
	}

	public void setLastActiveTab(int lastActiveTab) {
		this.lastActiveTab = lastActiveTab;
	}

	public void setAllDefaultLanguagesCopy(ArrayList<CountryLang> allDefaultLanguagesCopy) {
		this.allDefaultLanguagesCopy = allDefaultLanguagesCopy;
	}

	public LinkedHashMap<String, String> getAllMessagesCopy() {
		return allMessagesCopy;
	}


	public void setHeaderMainClassCopy(HeaderMainClass headerMainClassCopy) {
		this.headerMainClassCopy = headerMainClassCopy;
	}

	public ArrayList<Sport> getAllSportsCopy() {
		return allSportsCopy;
	}

	public void setAllSportsCopy(ArrayList<Sport> allSportsCopy) {
		this.allSportsCopy = allSportsCopy;
	}

	public ArrayList<Game> getAllGamesCopy() {
		return allGamesCopy;
	}

	public void setAllGamesCopy(ArrayList<Game> allGamesCopy) {
		this.allGamesCopy = allGamesCopy;
	}

	public ArrayList<League> getAllLeaguesCopy() {
		return allLeaguesCopy;
	}

	public LinkedHashMap<String, String> getAllLiveTermsCopy() {
		return allLiveTermsCopy;
	}

	public FirstPage getFirstPageCopy() {
		return firstPageCopy;
	}

	public void setAllDefaultLanguages(ArrayList<CountryLang> allDefaultLanguages) {
		this.allDefaultLanguages = allDefaultLanguages;
	}

	public void setAllSports(ArrayList<Sport> allSports) {
		this.allSports = allSports;
	}

	public void setAllGames(ArrayList<Game> allGames) {
		this.allGames = allGames;
	}

	public void switchArrays() {
		//long before = System.currentTimeMillis();

		synchronized (lock) {
			ArrayList<CountryLang> temp = allDefaultLanguages;
			allDefaultLanguages = allDefaultLanguagesCopy;
			allDefaultLanguagesCopy = new ArrayList<CountryLang>();
		}

		synchronized (lock) {
			LinkedHashMap<String, String> temp = allMessages;
			allMessages = allMessagesCopy;
			allMessagesCopy = new LinkedHashMap<String, String>();
		}

		synchronized (lock) {
			HeaderMainClass temp = headerMainClass;
			headerMainClass = headerMainClassCopy;
			headerMainClassCopy = new HeaderMainClass();
		}

		synchronized (lock) {
			ArrayList<Sport> temp = allSports;
			allSports = allSportsCopy;
			allSportsCopy = new ArrayList<Sport>();
		}

		synchronized (lock) {
			ArrayList<Game> temp = allGames;
			allGames = allGamesCopy;
			allGamesCopy = new ArrayList<Game>();
		}

		//Log.d(MyApplication.class.getSimpleName(), "leagues: " + allLeagues.size() + " copy: " + allLeaguesCopy.size());

		synchronized (lock) {
			ArrayList<League> temp = allLeagues;
			allLeagues = allLeaguesCopy;
			allLeaguesCopy = new ArrayList<League>();
		}

		synchronized (lock) {
			LinkedHashMap<String, String> temp = allLiveTerms;
			allLiveTerms = allLiveTermsCopy;
			allLiveTermsCopy = new LinkedHashMap<String, String>();
		}

		synchronized (lock) {
			FirstPage temp = firstPage;
			firstPage = firstPageCopy;
			firstPageCopy = new FirstPage();
		}
		//Log.d(MyApplication.class.getSimpleName(), "Total time taken to switch arrays (in ms) : " + (System.currentTimeMillis() - before));
	}

	public boolean isDownloading() {
		return isDownloading;
	}

	public void setIsDownloading(boolean isDownloading) {
		this.isDownloading = isDownloading;
	}

	public void setAllowReDownload(boolean allowReDownload) {
		this.allowReDownload = allowReDownload;
	}

	public boolean isAllowedRedownload() { return allowReDownload; }

	public void downloadData(Context context, boolean showProgress, boolean interrupt, boolean redownload) {
		//Log.d(MyApplication.class.getName(), "in DownloadData()");

		if (downloadTask == null) {
			downloadTask = new AsyncDownloadData(context, showProgress, redownload);
			downloadTask.execute();
		} else {
		//	Log.d(MyApplication.class.getName(), "previous task is already running");
			if (interrupt) {
        //        Log.d(MyApplication.class.getName(), "interrupted!");
                downloadTask.cancel(true);
				downloadTask = null;
				downloadTask = new AsyncDownloadData(context, showProgress, redownload);
				downloadTask.execute();
			} else {
				downloadTask.showProgressDialog();
			}
		}
	}

	public void stopDownloadTask() {
		if (downloadTask != null) {
			downloadTask.silentlyStop();
			downloadTask = null;
		}
	}

	public synchronized Stack<String> getActivityStack() {
		return activityStack;
	}

	public void setLastTimeDownloadRan() {
		lastTimeDownloadRan = System.currentTimeMillis();
	}

	public MyParlay getParlayToDisplay() {
		return parlayToDisplay;
	}

	public void setParlayToDisplay(MyParlay parlayToDisplay) {
		this.parlayToDisplay = parlayToDisplay;
	}

	public DiskCache getDiskCache() {
		return diskCache;
	}

	public boolean areAllPermissionsGranted() {
		if (Build.VERSION.SDK_INT >= 23) {
			boolean readPhoneState = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED;
			/*boolean readStorageGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) ==
					PackageManager.PERMISSION_GRANTED;
			boolean writeStorageGranted = ContextCompat.checkSelfPermission(this,
					Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
*/
			if (readPhoneState /*&& readStorageGranted && writeStorageGranted*/)
				return true;
		}

		return false;
	}

	public void adjustCertainAppFeatures() {

		// if app is not distributed through Google Play Store, collect data anyway
		Tune.getInstance().setAndroidId(android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID));
		//String deviceId = ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		//Tune.getInstance().setDeviceId(deviceId);
		// WifiManager objects may be null
		/*try {
			WifiManager wm = (WifiManager) getSystemService(Context.WIFI_SERVICE);
			Tune.getInstance().setMacAddress(wm.getConnectionInfo().getMacAddress());
		} catch (NullPointerException e) {
		}*/

		if (inspectionThread != null && !inspectionThread.isAlive()) {
			Log.d(MyApplication.class.getSimpleName(), "Inspection thread started");
			inspectionThread.start();
		}

	}

	public ExecutorService getExecutorService() {
		return executorService;
	}

	public void setQuitting(boolean state) {
		quitting = state;
	}

	public boolean isQuitting() { return quitting; }

	public void quit() {
		sendBroadcast(new Intent(ACTION_QUITTING));

		if (rbTask != null) {
			rbTask.cancel(true);
			rbTask = null;
		}

		if (inspectionThread != null) {
			inspectionThreadRunning = false;
			inspectionThread.interrupt();
			inspectionThread = null;
		}

		stopDownloadTask();

		if (executorService != null) {
			executorService.shutdown();
			executorService = null;
		}

	}

	public void shutDownVM() {
		System.exit(0);
		android.os.Process.killProcess(Process.myPid());
	}

	public ArrayList<MyParlay> getHistoryWagersCopy() {
		return historyWagersCopy;
	}

	public void setHistoryWagersCopy(ArrayList<MyParlay> historyWagersCopy) {
		this.historyWagersCopy = historyWagersCopy;
	}

	public UserData getUserDataCopy() {
		return userDataCopy;
	}

	public void setUserDataCopy(UserData userDataCopy) {
		this.userDataCopy = userDataCopy;
	}

	public boolean isBetSlipInEditMode() {
		return betSlipInEditMode;
	}

	public void setBetSlipInEditMode(boolean betSlipInEditMode) {
		this.betSlipInEditMode = betSlipInEditMode;
	}

	public void restoreParlayArray() {
		parlayArray.clear();
		Set<String> parlaySet = Settings.loadParlay(this);
		if (parlaySet != null) {
			for (String parlayOdd : parlaySet) {
				ParlayOdd odd = new ParlayOdd(parlayOdd);

				// don't add parlay entries for which a game hasn't been found
				if (odd.getGame() != null)
					parlayArray.add(odd);
			}
		}
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public boolean wasAppSentToBackground() {
		return wasAppSentToBackground;
	}

	public long getTimeMyActivityPaused() {
		return timeMyActivityPaused;
	}

	public void setTimeMyActivityPaused(long timeMyActivityPaused) {
		this.timeMyActivityPaused = timeMyActivityPaused;
	}

	public StatsCollection getStatsCollection() {
		return statsCollection;
	}

	public void setStatsCollection(StatsCollection statsCollection) {
		this.statsCollection = statsCollection;
	}

	public void startRealBetting(boolean interrupt, Activity uiContext, int eventId) {
		if (rbTask == null) {
			rbTask = new AsyncRealBetting(uiContext, eventId);
			rbTask.execute();
		} else {
			if (interrupt) {
				rbTask.cancel(true);
				rbTask = null;
				rbTask = new AsyncRealBetting(uiContext, eventId);
				rbTask.execute();
			}
		}
	}

	public void stopRealBettingTask() {
		if (rbTask != null && !rbTask.isCancelled())
			rbTask.cancel(true);
		rbTask = null;
	}

	public void removeEmptyScheduleFromData() {
		if (Settings.isShowEmptySchedule(this))
			return;

		synchronized (lock) {
			for (Sport sport : MyApplication.getInstance().getAllSports()) {
				for (League league : sport.getLeagues()) {
					ArrayList<Game> discardedGames = new ArrayList<>();
					for (Game game : league.getGameArrayList()) {
						if (game.getOddArrayList().isEmpty())
							discardedGames.add(game);
					}

					league.getGameArrayList().removeAll(discardedGames);
				}
			}

			for (Sport sport : MyApplication.getInstance().getAllSports()) {
				ArrayList<League> discardedLeagues = new ArrayList<>();
				for (League league : sport.getLeagues()) {
					if (league.getGameArrayList().isEmpty())
						discardedLeagues.add(league);
				}

				sport.getLeagues().removeAll(discardedLeagues);
			}

			ArrayList<Sport> discardedSports = new ArrayList<>();
			for (Sport sport :  MyApplication.getInstance().getAllSports()) {
				if (sport.getLeagues().isEmpty()) {
					discardedSports.add(sport);
				}
			}
			MyApplication.getInstance().getAllSports().removeAll(discardedSports);
		}
	}

	public ArrayList<SportIcon> getSportIcons() {
		return sportIcons;
	}

	public boolean isRbShownAtStart() { return rbShownAtStart; }

	public void setRbShownAtStart(boolean state) {
		rbShownAtStart = state;
	}

}
