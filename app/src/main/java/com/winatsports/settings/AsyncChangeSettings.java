package com.winatsports.settings;


import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Message;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.winatsports.MyApplication;
import com.winatsports.Util;
import com.winatsports.activities.MainActivity;
import com.winatsports.activities.SettingsActivity;
import com.winatsports.custom.SelectionElement;
import com.winatsports.data.Constants;
import com.winatsports.data.SupportLang;
import com.winatsports.data.adapters.SettingsAdapter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.message.BasicNameValuePair;


/**
 * Created by broda on 21/12/2015.
 *
 */
public class AsyncChangeSettings extends AsyncTask {

    private Context uiContext;

    private String returnData;
    private StringBuffer returnUserDataBuffer;

    private int sendValueFromListView; // using for changing listView Settings(Start game, End game and other) in SettingsActivity.java
    private int position;  // using for  listView in SettingsActivity.java
    private boolean nextValueOfSwitch;  // using for  listView in SettingsActivity.java
    private ToggleButton switchSettings;  // using for  listView in SettingsActivity.java

    private LinearLayout llOptions;  // using for changing Odd Type and Team Order  in SettingsActivity.java
    private int underlinedItem;  // using for changing Odd Type and Team Order  in SettingsActivity.java
    private int oldUnderlinedItem;  // using for changing Odd Type and Team Order  in SettingsActivity.java
    private boolean animate;  // using for changing Odd Type and Team Order  in SettingsActivity.java

    private String languageID;  //  using for changing language in SettingsActivity.java
    private String language;
    private TextView tvSetLanguage; //  using for changing language in SettingsActivity.java

    public enum OPERATION {LIST, ODD_TYPE, TEAM_ORDER, LANGUAGE};
    private OPERATION currentOption;

    // constructor for changing Odd Type or Team Order
    public AsyncChangeSettings (Context uiContext, LinearLayout llOptions, OPERATION operation, int underlinedItem, int oldUnderlinedItem, boolean animate) {
        this.uiContext = uiContext;
        this.underlinedItem = underlinedItem;
        this.oldUnderlinedItem = oldUnderlinedItem;
        this.animate = animate;
        this.currentOption = operation;
        this.llOptions = llOptions;
        returnUserDataBuffer = new StringBuffer();
    }

    // constructor for changing language
    public AsyncChangeSettings(Context uiContext, TextView tvSetLanguage, String languageName, String languageID) {
        this.uiContext = uiContext;
        this.language = languageName;
        this.languageID = languageID;

        this.tvSetLanguage = tvSetLanguage;

        returnUserDataBuffer = new StringBuffer();
        currentOption = OPERATION.LANGUAGE;
    }

    // Constructor for changing start game notification, end game notification and other stuff in listView
    public AsyncChangeSettings( Context uiContext, int position, boolean nextValueOfSwitch,
                               ToggleButton switchSettings) {
        this.uiContext = uiContext;

        this.position = position;
        this.nextValueOfSwitch = nextValueOfSwitch;
        this.switchSettings = switchSettings;

        returnUserDataBuffer = new StringBuffer();
        currentOption = OPERATION.LIST;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (uiContext instanceof SettingsActivity) {
            ((SettingsActivity) uiContext).showProgressDialog();
        }
    }

    @Override
    protected void onCancelled() {
        if (uiContext instanceof SettingsActivity) {
            ((SettingsActivity) uiContext).dismissProgressDialog();
        }

        Util.showMessageDialog(uiContext, Util.getString(Constants.MSG_KEY_APP_NOINET), null);
    }

    @Override
    protected Object doInBackground(Object[] params) {

        if( Util.isNetworkConnected(uiContext) == true ) {
            // here we change odd type or team order
            if (currentOption == OPERATION.TEAM_ORDER || currentOption == OPERATION.ODD_TYPE)
                submitTeamOrderOrOddTypeSettingsToServer();
                // here we change language
            else if (currentOption == OPERATION.LANGUAGE)
                submitLanguageSettingsToServer(languageID);
                // change settings from listView
            else
                submitListViewSettingsToServer();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        if (uiContext instanceof SettingsActivity) {
            ((SettingsActivity) uiContext).dismissProgressDialog();
        }

        boolean launchRedownload = false;

        // handle return data and do UI changes
        if (currentOption == OPERATION.LIST) {
            if (returnUserDataBuffer.toString().equals(Constants.SERVER_RESPONSE_OK)) {

                if (position == SettingsAdapter.OPTION_START_GAME_NOTIFICATION) {
                    Settings.setStartGameNotification(uiContext, nextValueOfSwitch);
                } else if (position == SettingsAdapter.OPTION_END_GAME_NOTIFICATION) {
                    Settings.setEndGameNotification(uiContext, nextValueOfSwitch);
                } else if (position == SettingsAdapter.OPTION_BET_CONFIRMATION) {
                    Settings.setBetConfirmation(uiContext, nextValueOfSwitch);
                } else if (position == SettingsAdapter.OPTION_PREDEFINED_WAGER_AMOUNT) {
                    Settings.setPredefinedWagerAmount(uiContext, nextValueOfSwitch);
                } else if (position == SettingsAdapter.OPTION_SHOW_EMPTY_SCHEDULE) {
                    //Log.d(AsyncChangeSettings.class.getSimpleName(), "EMPTY SCHEDULE WILL BE: " + nextValueOfSwitch);
                    Settings.setShowEmptySchedule(uiContext, nextValueOfSwitch);
                    if (!nextValueOfSwitch) {
                        MyApplication.getInstance().removeEmptyScheduleFromData();
                        launchRedownload = false;
                    } else
                        launchRedownload = true;
                } else if (position == SettingsAdapter.OPTION_TO_WIN_AMOUNT) {
                    Settings.setWinAmount(uiContext, nextValueOfSwitch);
                }

                switchSettings.setChecked(nextValueOfSwitch);
            } else {
                // TODO show error in dropdown layout
                switchSettings.setChecked(!nextValueOfSwitch); // always the opposite

                Message message = new Message();
                message.what = Constants.MESSAGE_EXPAND;
                ((SettingsActivity) uiContext).notificationHandler.sendMessage(message);
            }
        } else if ( currentOption == OPERATION.ODD_TYPE ) {
            if (returnUserDataBuffer.toString().equals(Constants.SERVER_RESPONSE_OK)) {
                Settings.setOddType(uiContext, underlinedItem);

                for (int index = 0; index < llOptions.getChildCount(); index++) {
                    if (llOptions.getChildAt(index) instanceof SelectionElement) {
                        SelectionElement temp = (SelectionElement) llOptions.getChildAt(index);
                        // user clicked on new item, I need to draw underline on new item and change text color to BLACK
                        if (index == underlinedItem)
                            temp.setUnderlined(true, animate);
                        // from old item, I need to undraw underline on old item and change text color to GRAY
                        if (index == oldUnderlinedItem)
                            temp.setUnderlined(false, animate);
                    }
                }
            } else {
                // show error if settings are not successfully changed!
                Message message = new Message();
                message.what = Constants.MESSAGE_EXPAND;;
                ((SettingsActivity) uiContext).notificationHandler.sendMessage(message);
            }
        } else if (currentOption == OPERATION.TEAM_ORDER) {
            if (returnUserDataBuffer.toString().equals(Constants.SERVER_RESPONSE_OK)) {
                Settings.setTeamOrder(uiContext, underlinedItem);

                for (int index = 0; index < llOptions.getChildCount(); index++) {
                    if (llOptions.getChildAt(index) instanceof SelectionElement) {
                        SelectionElement temp = (SelectionElement) llOptions.getChildAt(index);
                        // user clicked on new item, I need to draw underline on new item and change text color to BLACK
                        if (index == underlinedItem)
                            temp.setUnderlined(true, animate);
                        // from old item, I need to undraw underline on old item and change text color to GRAY
                        if (index == oldUnderlinedItem)
                            temp.setUnderlined(false, animate);
                    }
                }
            } else {
                // show error if settings are not successfully changed!
                Message message = new Message();
                message.what = Constants.MESSAGE_EXPAND;;
                ((SettingsActivity) uiContext).notificationHandler.sendMessage(message);
            }
        } else if (currentOption == OPERATION.LANGUAGE) {
            if (returnUserDataBuffer.toString().equals(Constants.SERVER_RESPONSE_OK)) {
                Settings.setLanguage(uiContext, languageID.toLowerCase());

                for (int index = 0; index < MyApplication.getInstance().getUserData().getSupportLangs().size(); index++) {

                    SupportLang supportLang = MyApplication.getInstance().getUserData().getSupportLangs().get(index);
                    if (supportLang.getLangID().equalsIgnoreCase(languageID)) {
                        // Here I change the langID in UserData Class,, because I'm using the same for loop,, when I'm drawing SettingsActivity.java
                        // And because I'm using the same langID also for drawing SettingsActivity.java
                        MyApplication.getInstance().getUserData().setLang(supportLang.getLangID());
                        tvSetLanguage.setText(supportLang.getLangName());
                        break;
                    }
                }

                // initiate download again
                MyApplication.getInstance().downloadData(uiContext, true, true, false);
            } else {
                // TODO show error if settings are not successfully changed!
                Message message = new Message();
                message.what = Constants.MESSAGE_EXPAND;;
                ((SettingsActivity) uiContext).notificationHandler.sendMessage(message);
            }
        }

        if (launchRedownload) {
            MyApplication.getInstance().downloadData(uiContext, true, true, MyApplication.getInstance().isAllowedRedownload());
        }
    }

    private void submitTeamOrderOrOddTypeSettingsToServer() {
        HttpClient client = null;
        StringBuffer xmlBuffer = new StringBuffer();
        xmlBuffer.append("<meritum>");
        xmlBuffer.append("<userSettings>");

        // this "settingsOddTitle", I'm getting in CustomSingleSelectionLayout.java CLASS ===> from Settings.KEY_ODD_TYPE
        // If we have change odd type, then we are going inside this if statement
        if( currentOption == OPERATION.ODD_TYPE  )
            // Why do I have here underlinedItem+1??
            // Because in legend of user_common.php is written that I need to send 1,2,3 and this underlinedItem is 0,1 or 2
            xmlBuffer.append("<odd_type>" + (underlinedItem+1) + "</odd_type>");
        // otherwise we have changed team order
        else if (currentOption == OPERATION.TEAM_ORDER)
            // The same reason as above
            xmlBuffer.append("<team_order>" + (underlinedItem+1) + "</team_order>");

        xmlBuffer.append("</userSettings>");
        xmlBuffer.append("</meritum>");

        // replace all apostrophes in xmlBuffer with escaped double quotes \" if needed
        try {
            final String URL_USER_COMMON = Constants.URL_USER_COMMON;
            final String REQUEST_HEADER_KEY = Constants.USER_COMMON_REQUEST_HEADER_KEY;

            URL url = new URL(URL_USER_COMMON);

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                    .setRedirectsEnabled(false)
                    .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                    .setCircularRedirectsAllowed(false).build();

            HttpPost post = new HttpPost(Uri.parse(URL_USER_COMMON).toString());
            post.setConfig(config);

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("key", REQUEST_HEADER_KEY));
            pairs.add(new BasicNameValuePair("lang", "en")); // TODO:  must reflect settings
            pairs.add(new BasicNameValuePair("p", "settings"));
            pairs.add(new BasicNameValuePair("xml", xmlBuffer.toString()));
            pairs.add(new BasicNameValuePair("user_id", Util.getUserId(uiContext)));
            pairs.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded charset=utf-8"));

            post.setEntity(new UrlEncodedFormEntity(pairs));

            client = HttpClientBuilder.create()
                    .setMaxConnTotal(1)
                    .setUserAgent(System.getProperty("http.agent", "Unknown"))
                    .build();

            HttpResponse response = client.execute(post);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            //returnUserDataBuffer.append("Response code: " + response.getStatusLine().getStatusCode() + "\n");
            //returnUserDataBuffer.append("Response status: " + response.getStatusLine().getReasonPhrase() + "\n");

            int character;

            while ( (character = br.read()) != -1 ) {
                returnUserDataBuffer.append((char) character);
            }
            // moved handling of response to onPostExecute()
            //Log.d(AsyncChangeSettings.class.getSimpleName(), "return data: " + returnUserDataBuffer);

            br.close();

        } catch ( Exception e ) {
            Log.e(MainActivity.class.getSimpleName(), e.getLocalizedMessage(), e);
            returnData = e.getMessage();
            cancel(true);
        } finally {
            if( client != null )
                client = null;
        }

    }


    private void submitListViewSettingsToServer()  {
        currentOption = OPERATION.LIST;
        HttpClient client = null;
        StringBuffer xmlBuffer = new StringBuffer();
        xmlBuffer.append("<meritum>");
        xmlBuffer.append("<userSettings>");

        if( nextValueOfSwitch == true )
            sendValueFromListView = 1;
        else
            sendValueFromListView = 0;

        if( position == SettingsAdapter.OPTION_START_GAME_NOTIFICATION) {
            xmlBuffer.append("<start_push>" + sendValueFromListView + "</start_push>");
        } else if( position == SettingsAdapter.OPTION_END_GAME_NOTIFICATION ) {
            xmlBuffer.append("<end_push>" + sendValueFromListView + "</end_push>");
        } else if( position == SettingsAdapter.OPTION_BET_CONFIRMATION) {
            xmlBuffer.append("<confirm_bet>" + sendValueFromListView + "</confirm_bet>");
        } else if( position == SettingsAdapter.OPTION_PREDEFINED_WAGER_AMOUNT ) {
            xmlBuffer.append("<bet_slider>" + sendValueFromListView + "</bet_slider>");
        } else if( position == SettingsAdapter.OPTION_SHOW_EMPTY_SCHEDULE ) {
            xmlBuffer.append("<show_inactive>" + sendValueFromListView + "</show_inactive>");
        } else if( position == SettingsAdapter.OPTION_TO_WIN_AMOUNT ) {
            xmlBuffer.append("<win_type>" + sendValueFromListView + "</win_type>");
        }

        xmlBuffer.append("</userSettings>");
        xmlBuffer.append("</meritum>");

        try {

            final String URL_USER_COMMON = Constants.URL_USER_COMMON;
            final String REQUEST_HEADER_KEY = Constants.USER_COMMON_REQUEST_HEADER_KEY;

            URL url = new URL(URL_USER_COMMON);

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                    .setRedirectsEnabled(false)
                    .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                    .setCircularRedirectsAllowed(false).build();

            HttpPost post = new HttpPost(Uri.parse(URL_USER_COMMON).toString());
            post.setConfig(config);

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("key", REQUEST_HEADER_KEY));
            pairs.add(new BasicNameValuePair("lang", Settings.getLanguage(uiContext)));
            pairs.add(new BasicNameValuePair("p", "settings"));
            pairs.add(new BasicNameValuePair("xml", xmlBuffer.toString()));
            pairs.add(new BasicNameValuePair("user_id", Util.getUserId(uiContext)));
            pairs.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded charset=utf-8"));

            post.setEntity(new UrlEncodedFormEntity(pairs));

            client = HttpClientBuilder.create()
                    .setMaxConnTotal(1)
                    .build();

            HttpResponse response = client.execute(post);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            //returnUserDataBuffer.append("Response code: " + response.getStatusLine().getStatusCode() + "\n");
            //returnUserDataBuffer.append("Response status: " + response.getStatusLine().getReasonPhrase() + "\n");

            int character;

            while ( (character = br.read()) != -1 ) {
                returnUserDataBuffer.append((char) character);
            }

            br.close();

        } catch ( Exception e ) {
            Log.e(MainActivity.class.getSimpleName(), e.getLocalizedMessage(), e);
            returnData = e.getMessage();
            cancel(true);
        } finally {
            if( client != null )
                client = null;
        }

    }


    private void submitLanguageSettingsToServer(String language)  {
        HttpClient client = null;
        StringBuffer xmlBuffer = new StringBuffer();
        xmlBuffer.append("<meritum>");
        xmlBuffer.append("<userSettings>");

        xmlBuffer.append("<lang>" + language.toUpperCase() + "</lang>");

        xmlBuffer.append("</userSettings>");
        xmlBuffer.append("</meritum>");

        try {

            final String URL_USER_COMMON = Constants.URL_USER_COMMON;
            final String REQUEST_HEADER_KEY = Constants.USER_COMMON_REQUEST_HEADER_KEY;

            URL url = new URL(URL_USER_COMMON);

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                    .setRedirectsEnabled(false)
                    .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                    .setCircularRedirectsAllowed(false).build();

            HttpPost post = new HttpPost(Uri.parse(URL_USER_COMMON).toString());
            post.setConfig(config);

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("key", REQUEST_HEADER_KEY));
            pairs.add(new BasicNameValuePair("lang", language));
            pairs.add(new BasicNameValuePair("p", "settings"));
            pairs.add(new BasicNameValuePair("xml", xmlBuffer.toString()));
            pairs.add(new BasicNameValuePair("user_id", Util.getUserId(uiContext)));
            pairs.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded charset=utf-8"));

            post.setEntity(new UrlEncodedFormEntity(pairs));

            client = HttpClientBuilder.create()
                    .setMaxConnTotal(1)
                    .build();

            HttpResponse response = client.execute(post);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            //returnUserDataBuffer.append("Response code: " + response.getStatusLine().getStatusCode() + "\n");
            //returnUserDataBuffer.append("Response status: " + response.getStatusLine().getReasonPhrase() + "\n");

            int character;

            while ( (character = br.read()) != -1 ) {
                returnUserDataBuffer.append((char) character);
            }

            br.close();


        } catch ( Exception e ) {
            Log.e(MainActivity.class.getSimpleName(), e.getLocalizedMessage(), e);
            returnData = e.getMessage();
            cancel(true);
        } finally {
            if( client != null )
                client = null;
        }
    }
}
