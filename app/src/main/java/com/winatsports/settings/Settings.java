package com.winatsports.settings;

import android.content.Context;
import android.content.SharedPreferences;

import com.winatsports.MyApplication;
import com.winatsports.data.Constants;

import java.util.Set;

public class Settings {

    // prefs.xml is literal
    // all settings are saved in this XML FILE by name ==> prefs.xml
    private  static  final String XML_FILE = "prefs.xml";
    private static final String IMAGE_XML_FILE = "images.xml";

    // all values and variable requeired for add_user.php
    public static final String KEY_DEVICE_ID = "device_id";
    public static final String DEFAULT_DEVICE_ID = "0";

    public static final String KEY_USER_D = "user_d";
    public static final String DEFAULT_USER_D = "0";

    public static final String KEY_USER_R = "user_r";
    public static final String DEFAULT_USER_R = "0";

    public static final String KEY_LOGIN_TIME = "login_time";
    public static final long DEFAULT_LOGIN_TIME = 0;

    // all values and variable requeired for add_user.php
    // all values and variable requeired for SettingsActivity.java

    public static final String KEY_ODD_TYPE = Constants.MSG_KEY_SETTINGSODDTITLE;
    public static final int DEFAULT_VALUE_ODD_TYPE = 1;

    public static final String KEY_TEAM_ORDER = Constants.MSG_KEY_SETTINGSTEAMORDERTITLE;
    public static final int DEFAULT_TEAM_ORDER = Constants.C_SETTINGS_HOME_VS_AWAY;

    public static final String KEY_LANGUAGE = "other_lang";
    public static final String DEFAULT_LANGUAGE = "en";

    public static final String KEY_START_GAME_NOTIFICATION = "start_game_notification";
    public static final boolean DEFAULT_VALUE_START_GAME_NOTIFICATION = true;

    public static final String KEY_END_GAME_NOTIFICATION = "end_game_notification";
    public static final boolean DEFAULT_VALUE_END_GAME_NOTIFICATION = false;

    public static final String KEY_BET_CONFIRMATION = "bet_confirmation";
    public static final boolean DEFAULT_VALUE_BET_CONFIRMATION = false;

    public static final String KEY_PREDEFINED_WAGER_AMOUNT = "predefined_wager_amount";
    public static final boolean DEFAULT_VALUE_PREDEFINED_WAGER_AMOUNT = true;

    public static final String KEY_SHOW_EMPTY_SCHEDULE = "show_empty_schedule";
    public static final boolean DEFAULT_VALUE_SHOW_EMPTY_SCHEDULE = true;

    public static final String KEY_WIN_AMOUNT = "win_amount";
    public static final boolean DEFAULT_VALUE_WIN_AMOUNT = false;

    public static final String KEY_TIMES_RUN = "times_run";
    public static final int DEFAULT_TIMES_RUN = 0;

    public static final String KEY_FIRST_TIME_LANGUAGE_SETUP = "first_time_lang_setup";
    public static final boolean DEFAULT_FIRST_TIME_LANGUAGE_SETUP = false;

    public static final String KEY_LOGGED_IN_WITH_FB = "logged_in_with_fb";
    public static final boolean DEFAULT_LOGGED_IN_WITH_FB = false;

    public static final String KEY_LOGGED_IN_LOCALLY = "logged_in_locally";
    public static final boolean DEFAULT_LOGGED_IN_LOCALLY = false;

    public static final String KEY_REAL_BETTING_TUTORIAL_SHOWN = "real_betting_tutorial_shown";
    public static final boolean DEFAULT_REAL_BETTING_TUTORIAL_SHOWN = false;

    public static final String KEY_REAL_BETTING_DATE = "rb_date";
    public static final String KEY_REAL_BETTING_IN_SAFARI = "rb_in_safari";
    public static final String KEY_REAL_BETTING_ON = "rb_on";
    public static final String KEY_REAL_BETTING_NUM_OF_RUNS = "rb_runs";
    public static final String KEY_REAL_BETTING_GUESTS_ON = "rb_guests";
    public static final String KEY_REAL_BETTING_NUM_OF_GUEST_RUNS = "rb_guest_runs";

    public static final String KEY_STATS_TIMER = "stats_timer";
    public static final long DEFAULT_STATS_TIME = 0;

    public static final String KEY_ENTERED_FG = "fg_time";
    public static final long DEFAULT_FG_TIME = 0;

    public static final String KEY_ENTERED_BG = "bg_time";
    public static final long DEFAULT_BG_TIME = 0;

    public static final String KEY_USER_REFUSED_TO_GRANT_PERMISSIONS = "refused_to_grant";

    public static final String KEY_PARLAY = "saved_parlay";

    public static final String KEY_RUNS_FROM_START = "num_of_download_runs";

    public static final String KEY_PUSH_TOKEN = "push_token";

    private static SharedPreferences getPreferences(Context c) {
        return c.getSharedPreferences(XML_FILE, Context.MODE_PRIVATE);
    }

    public static String getDeviceId(Context c) {
        return getPreferences(c).getString(KEY_DEVICE_ID, DEFAULT_DEVICE_ID);
    }

    public static void setDeviceId(Context c, String value)  {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_DEVICE_ID, value);
        editor.commit();
    }

    public static String getUserD(Context c) {
        return getPreferences(c).getString(KEY_USER_D, DEFAULT_USER_D);
    }

    public static void setUserD(Context c, String value)  {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_USER_D, value);
        editor.commit();
    }

    public static String getUserR(Context c) {
        return getPreferences(c).getString(KEY_USER_R, DEFAULT_USER_R);
    }

    public static void setUserR(Context c, String value) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_USER_R, value);
        editor.commit();
    }

    public static String getLanguage(Context c) {
        return getPreferences(c).getString(KEY_LANGUAGE, DEFAULT_LANGUAGE);
    }

    public static void setLanguage(Context c, String value)  {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_LANGUAGE, value.toLowerCase());
        editor.commit();
    }

    public static int getOddType(Context c)  {
        return getPreferences(c).getInt(KEY_ODD_TYPE, DEFAULT_VALUE_ODD_TYPE);
    }

    public static void setOddType(Context c, int value)  {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(KEY_ODD_TYPE, value);
        editor.commit();
    }

    public static int getTeamOrder(Context c) {
        return getPreferences(c).getInt(KEY_TEAM_ORDER, DEFAULT_TEAM_ORDER);
    }

    public static void setTeamOrder(Context c, int value) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(KEY_TEAM_ORDER, value);
        editor.commit();
    }


    public static boolean isStartGameNotification(Context c)  {
        return getPreferences(c).getBoolean(KEY_START_GAME_NOTIFICATION, DEFAULT_VALUE_START_GAME_NOTIFICATION);
    }

    public static void setStartGameNotification(Context c, boolean state ) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(KEY_START_GAME_NOTIFICATION, state);
        editor.commit();
    }


    public static boolean isEndGameNotification(Context c) {
        boolean getResults = getPreferences(c).getBoolean(KEY_END_GAME_NOTIFICATION, DEFAULT_VALUE_END_GAME_NOTIFICATION);
        return getResults;
    }

    public static void setEndGameNotification(Context c, boolean state ) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(KEY_END_GAME_NOTIFICATION, state);
        editor.commit();
    }


    public static boolean isBetConfirmation(Context c) {
        return getPreferences(c).getBoolean(KEY_BET_CONFIRMATION, DEFAULT_VALUE_BET_CONFIRMATION);
    }

    public static void setBetConfirmation(Context c, boolean state ) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(KEY_BET_CONFIRMATION, state);
        editor.commit();
    }


    public static boolean isPredefinedWagerAmount(Context c) {
        return getPreferences(c).getBoolean(KEY_PREDEFINED_WAGER_AMOUNT, DEFAULT_VALUE_PREDEFINED_WAGER_AMOUNT);
    }

    public static void setPredefinedWagerAmount(Context c, boolean state ) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(KEY_PREDEFINED_WAGER_AMOUNT, state);
        editor.commit();
    }


    public static boolean isShowEmptySchedule(Context c) {
        return getPreferences(c).getBoolean(KEY_SHOW_EMPTY_SCHEDULE, DEFAULT_VALUE_SHOW_EMPTY_SCHEDULE);
    }

    public static void setShowEmptySchedule(Context c, boolean state ) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(KEY_SHOW_EMPTY_SCHEDULE, state);
        editor.commit();
    }

    public static boolean isWinAmount(Context c) {
        boolean getResults = getPreferences(c).getBoolean(KEY_WIN_AMOUNT, DEFAULT_VALUE_WIN_AMOUNT);
        return getResults;
    }


    public static void setWinAmount(Context c, boolean state ) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(KEY_WIN_AMOUNT, state);
        editor.commit();
    }

    public static void setTimesRun(Context c, int times) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(KEY_TIMES_RUN, times);
        editor.commit();
    }

    public static int getTimesRun(Context c) {
        return getPreferences(c).getInt(KEY_TIMES_RUN, DEFAULT_TIMES_RUN);
    }

    public static boolean isFirstTimeLanguageSetup(Context c) {
        return getPreferences(c).getBoolean(KEY_FIRST_TIME_LANGUAGE_SETUP, DEFAULT_FIRST_TIME_LANGUAGE_SETUP);
    }

    public static void setFirstTimeLanguageSetup(Context c, boolean state ) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(KEY_FIRST_TIME_LANGUAGE_SETUP, state);
        editor.commit();
    }

    public static boolean isLoggedInWithFb(Context c) {
        return getPreferences(c).getBoolean(KEY_LOGGED_IN_WITH_FB, DEFAULT_LOGGED_IN_WITH_FB);
    }

    public static void setLoggedInWithFb(Context c, boolean state ) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(KEY_LOGGED_IN_WITH_FB, state);
        editor.commit();
    }

    public static boolean isLoggedInLocally(Context c) {
        return getPreferences(c).getBoolean(KEY_LOGGED_IN_LOCALLY, DEFAULT_LOGGED_IN_LOCALLY);
    }

    public static void setLoggedInLocally(Context c, boolean state ) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(KEY_LOGGED_IN_LOCALLY, state);
        editor.commit();
    }

    public static void setRealBettingTutorialShown(Context c, boolean state) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(KEY_REAL_BETTING_TUTORIAL_SHOWN, state);
        editor.commit();
    }

    public static boolean wasRealBettingTutorialShown(Context c) {
        return getPreferences(c).getBoolean(KEY_REAL_BETTING_TUTORIAL_SHOWN, DEFAULT_REAL_BETTING_TUTORIAL_SHOWN);
    }

    public static void setRealBettingDate(Context c, String date) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_REAL_BETTING_DATE, date);
        editor.commit();
    }

    public static String getRealBettingDate(Context c) {
        return getPreferences(c).getString(KEY_REAL_BETTING_DATE, "");
    }

    public static void setRealBettingInSafari(Context c, boolean state) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(KEY_REAL_BETTING_IN_SAFARI, state);
        editor.commit();
    }

    public static boolean isRealBettingInSafari(Context c) {
        return getPreferences(c).getBoolean(KEY_REAL_BETTING_IN_SAFARI, false);
    }

    public static void setRealBettingOn(Context c, boolean state) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(KEY_REAL_BETTING_ON, state);
        editor.commit();
    }

    public static boolean isRealBettingOn(Context c) {
        return getPreferences(c).getBoolean(KEY_REAL_BETTING_ON, false);
    }

    public static void setRealBettingNumOfRuns(Context c, int num) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(KEY_REAL_BETTING_NUM_OF_RUNS, num);
        editor.commit();
    }

    public static int getRealBettingNumOfRuns(Context c) {
        return getPreferences(c).getInt(KEY_REAL_BETTING_NUM_OF_RUNS, 0);
    }

    public static void setRealBettingGuestsOn(Context c, boolean state) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(KEY_REAL_BETTING_GUESTS_ON, state);
        editor.commit();
    }

    public static boolean isRealBettingGuestsOn(Context c) {
        return getPreferences(c).getBoolean(KEY_REAL_BETTING_GUESTS_ON, false);
    }

    public static void setRealBettingNumOfGuestRuns(Context c, int num) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(KEY_REAL_BETTING_NUM_OF_GUEST_RUNS, num);
        editor.commit();
    }

    public static int getRealBettingNumOfGuestRuns(Context c) {
        return getPreferences(c).getInt(KEY_REAL_BETTING_NUM_OF_GUEST_RUNS, 0);
    }

    public static void setStatsTimer(Context c, long seconds) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putLong(KEY_STATS_TIMER, seconds);
        editor.commit();
    }

    public static long getStatsTimer(Context c) {
        return getPreferences(c).getLong(KEY_STATS_TIMER, DEFAULT_STATS_TIME);
    }

    public static long getLoginTime(Context c) {
        return getPreferences(c).getLong(KEY_LOGIN_TIME, DEFAULT_LOGIN_TIME);
    }

    public static void setLoginTime(Context c, long value)  {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putLong(KEY_LOGIN_TIME, value);
        editor.commit();
    }

    public static long getTimeWhenEnteredFg(Context c) {
        return getPreferences(c).getLong(KEY_ENTERED_FG, DEFAULT_FG_TIME);
    }

    public static void setTimeWhenEnteredFg(Context c, long value)  {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putLong(KEY_ENTERED_FG, value);
        editor.commit();
    }

    public static long getTimeWhenEnteredBg(Context c) {
        return getPreferences(c).getLong(KEY_ENTERED_BG, DEFAULT_BG_TIME);
    }

    public static void setTimeWhenEnteredBg(Context c, long value)  {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putLong(KEY_ENTERED_BG, value);
        editor.commit();
    }

    public static void setUserRefusedToGrantPermissions(Context c, boolean state) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(KEY_USER_REFUSED_TO_GRANT_PERMISSIONS, state);
        editor.commit();
    }

    public static boolean hasUserRefusedToGrantPermissions(Context c) {
        return getPreferences(c).getBoolean(KEY_USER_REFUSED_TO_GRANT_PERMISSIONS, false);
    }

    public static void saveParlay(Context c, Set<String> parlay) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putStringSet(KEY_PARLAY, parlay);

        editor.commit();
    }

    public static Set<String> loadParlay(Context c) {
        return getPreferences(c).getStringSet(KEY_PARLAY, null);
    }

    public static void setPushToken(Context c, String value) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_PUSH_TOKEN, value);
        editor.commit();
    }

    public static String getPushToken(Context c) {
        return getPreferences(c).getString(KEY_PUSH_TOKEN, "");
    }

}