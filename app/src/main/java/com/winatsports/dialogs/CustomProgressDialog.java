package com.winatsports.dialogs;



import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.icu.util.MeasureUnit;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.data.Constants;

import java.util.Set;


/**
 * Created by broda on 12/11/2015.
 */

public class CustomProgressDialog extends ProgressDialog {

    private Context context;
    private View vLeft;
    private View vCenter;
    private TextView tvSplashScreenText;

    public CustomProgressDialog(Context context) {
        super(context);
        this.context = context;
    }

    public CustomProgressDialog(Context context, int theme) {
        super(context, theme);
        this.context = context;
    }

    public static ProgressDialog ctor(Context context) {
        CustomProgressDialog dialog = new CustomProgressDialog(context, R.style.MyThemeForCustomProgressDialog);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);

        return dialog;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_progress_dialog);

        setCancelable(false);

        tvSplashScreenText = (TextView) findViewById(R.id.tvSplashScreenText);
        String loadingStr = Util.getString(Constants.MSG_KEY_APP_LOADING);
        tvSplashScreenText.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_LOADING));

        if (!Util.isPopulated(loadingStr))
            tvSplashScreenText.setText(context.getString(R.string.loading));
        else
            tvSplashScreenText.setText(loadingStr);
        if (Util.getDisplayDiagonalInInches((Activity) context) > 5.5) {
            tvSplashScreenText.setTextSize(14.0f);
        }
        vLeft = findViewById(R.id.vLeft);
        vCenter = findViewById(R.id.vCenter);

        vCenter.setBackgroundColor(context.getResources().getColor(R.color.tint_color));
    }

    @Override
    public void show() {
        super.show();
        animateTopLeftView(vLeft);
        animateBottomRightView(vCenter);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        vLeft.clearAnimation();
        vCenter.clearAnimation();
    }

    private void animateTopLeftView(final View v) {

        final int ANIMATION_DURATION = 450;
        final int SELF_MULTIPLIER = 8;

        // these are used for movement animation
        Animation translateRight = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, SELF_MULTIPLIER,
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0);
        translateRight.setInterpolator(new LinearInterpolator());
        translateRight.setDuration(ANIMATION_DURATION);
        translateRight.setStartOffset(0);
        translateRight.setFillAfter(true);

        Animation translateDown = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, SELF_MULTIPLIER);
        translateDown.setInterpolator(new LinearInterpolator());
        translateDown.setDuration(ANIMATION_DURATION);
        translateDown.setStartOffset(ANIMATION_DURATION * 1);
        translateDown.setFillAfter(true);

        Animation translateLeft = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, -SELF_MULTIPLIER,
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0);
        translateLeft.setInterpolator(new LinearInterpolator());
        translateLeft.setDuration(ANIMATION_DURATION);
        translateLeft.setStartOffset(ANIMATION_DURATION * 2);
        translateLeft.setFillAfter(true);

        Animation translateUp = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, -SELF_MULTIPLIER);
        translateUp.setInterpolator(new LinearInterpolator());
        translateUp.setDuration(ANIMATION_DURATION);
        translateUp.setStartOffset(ANIMATION_DURATION * 3);
        translateUp.setFillAfter(true);

        // these are used for rotation
        int [] loc = new int [2];
        v.getLocationInWindow(loc);

        Animation rotate90 = new RotateAnimation(0, 90, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate90.setDuration(ANIMATION_DURATION);
        rotate90.setInterpolator(new LinearInterpolator());
        rotate90.setStartOffset(0);
        rotate90.setFillAfter(true);

        Animation rotate180 = new RotateAnimation(90, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate180.setDuration(ANIMATION_DURATION);
        rotate180.setInterpolator(new LinearInterpolator());
        rotate180.setStartOffset(ANIMATION_DURATION);
        rotate180.setFillAfter(true);

        Animation rotate270 = new RotateAnimation(180, 270, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate270.setDuration(ANIMATION_DURATION);
        rotate270.setInterpolator(new LinearInterpolator());
        rotate270.setStartOffset(ANIMATION_DURATION * 2);
        rotate270.setFillAfter(true);

        Animation rotate0 = new RotateAnimation(270, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate0.setDuration(ANIMATION_DURATION);
        rotate0.setInterpolator(new LinearInterpolator());
        rotate0.setStartOffset(ANIMATION_DURATION * 3);
        rotate0.setFillAfter(true);

        // scaling animation
        Animation scaleToHalf1 = new ScaleAnimation(1.0f, 0.5f, 1.0f, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleToHalf1.setDuration(ANIMATION_DURATION);
        scaleToHalf1.setInterpolator(new LinearInterpolator());
        scaleToHalf1.setStartOffset(0);
        scaleToHalf1.setFillAfter(true);

        Animation scaleToFull1 = new ScaleAnimation(1.0f, 2f, 1.0f, 2f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleToFull1.setDuration(ANIMATION_DURATION);
        scaleToFull1.setInterpolator(new LinearInterpolator());
        scaleToFull1.setStartOffset(ANIMATION_DURATION);
        scaleToFull1.setFillAfter(true);

        Animation scaleToHalf2 = new ScaleAnimation(1.0f, 0.5f, 1.0f, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleToHalf2.setDuration(ANIMATION_DURATION);
        scaleToHalf2.setInterpolator(new LinearInterpolator());
        scaleToHalf2.setStartOffset(ANIMATION_DURATION*2);
        scaleToHalf2.setFillAfter(true);

        Animation scaleToFull2 = new ScaleAnimation(1.0f, 2f, 1.0f, 2f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleToFull2.setDuration(ANIMATION_DURATION);
        scaleToFull2.setInterpolator(new LinearInterpolator());
        scaleToFull2.setStartOffset(ANIMATION_DURATION*3);
        scaleToFull2.setFillAfter(true);

        // In adding animations to a set the order in which they're added DOES MATTER!!!
        // First add rotations or they won't be applied correctly.
        AnimationSet set = new AnimationSet(true);
        //set.setInterpolator(new LinearInterpolator());
        set.addAnimation(rotate90);
        set.addAnimation(rotate180);
        set.addAnimation(rotate270);
        set.addAnimation(rotate0);

        set.addAnimation(scaleToHalf1);
        set.addAnimation(scaleToFull1);
        set.addAnimation(scaleToHalf2);
        set.addAnimation(scaleToFull2);

        set.addAnimation(translateRight);
        set.addAnimation(translateDown);
        set.addAnimation(translateLeft);
        set.addAnimation(translateUp);



        set.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                animateTopLeftView(v);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        v.startAnimation(set);
    }

    private void animateBottomRightView(final View v) {
        final int ANIMATION_DURATION = 450;
        final int SELF_MULTIPLIER = 8;

        // these are used for movement animation
        Animation translateRight = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, SELF_MULTIPLIER,
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0);
        translateRight.setInterpolator(new LinearInterpolator());
        translateRight.setDuration(ANIMATION_DURATION);
        translateRight.setStartOffset(ANIMATION_DURATION * 2);
        translateRight.setFillAfter(true);

        Animation translateDown = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, SELF_MULTIPLIER);
        translateDown.setInterpolator(new LinearInterpolator());
        translateDown.setDuration(ANIMATION_DURATION);
        translateDown.setStartOffset(ANIMATION_DURATION * 3);
        translateDown.setFillAfter(true);

        Animation translateLeft = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, -SELF_MULTIPLIER,
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0);
        translateLeft.setInterpolator(new LinearInterpolator());
        translateLeft.setDuration(ANIMATION_DURATION);
        translateLeft.setStartOffset(0);
        translateLeft.setFillAfter(true);

        Animation translateUp = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, -SELF_MULTIPLIER);
        translateUp.setInterpolator(new LinearInterpolator());
        translateUp.setDuration(ANIMATION_DURATION);
        translateUp.setStartOffset(ANIMATION_DURATION * 1);
        translateUp.setFillAfter(true);

        // these are used for rotation
        int [] loc = new int [2];
        v.getLocationInWindow(loc);

        Animation rotate90 = new RotateAnimation(0, 90, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate90.setDuration(ANIMATION_DURATION);
        rotate90.setInterpolator(new LinearInterpolator());
        rotate90.setStartOffset(0);
        rotate90.setFillAfter(true);

        Animation rotate180 = new RotateAnimation(90, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate180.setDuration(ANIMATION_DURATION);
        rotate180.setInterpolator(new LinearInterpolator());
        rotate180.setStartOffset(ANIMATION_DURATION);
        rotate180.setFillAfter(true);

        Animation rotate270 = new RotateAnimation(180, 270, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate270.setDuration(ANIMATION_DURATION);
        rotate270.setInterpolator(new LinearInterpolator());
        rotate270.setStartOffset(ANIMATION_DURATION * 2);
        rotate270.setFillAfter(true);

        Animation rotate0 = new RotateAnimation(270, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate0.setDuration(ANIMATION_DURATION);
        rotate0.setInterpolator(new LinearInterpolator());
        rotate0.setStartOffset(ANIMATION_DURATION * 3);
        rotate0.setFillAfter(true);

        // scaling animation
        Animation scaleToHalf1 = new ScaleAnimation(1.0f, 0.5f, 1.0f, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleToHalf1.setDuration(ANIMATION_DURATION);
        scaleToHalf1.setInterpolator(new LinearInterpolator());
        scaleToHalf1.setStartOffset(0);
        scaleToHalf1.setFillAfter(true);

        Animation scaleToFull1 = new ScaleAnimation(1.0f, 2f, 1.0f, 2f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleToFull1.setDuration(ANIMATION_DURATION);
        scaleToFull1.setInterpolator(new LinearInterpolator());
        scaleToFull1.setStartOffset(ANIMATION_DURATION);
        scaleToFull1.setFillAfter(true);

        Animation scaleToHalf2 = new ScaleAnimation(1.0f, 0.5f, 1.0f, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleToHalf2.setDuration(ANIMATION_DURATION);
        scaleToHalf2.setInterpolator(new LinearInterpolator());
        scaleToHalf2.setStartOffset(ANIMATION_DURATION*2);
        scaleToHalf2.setFillAfter(true);

        Animation scaleToFull2 = new ScaleAnimation(1.0f, 2f, 1.0f, 2f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleToFull2.setDuration(ANIMATION_DURATION);
        scaleToFull2.setInterpolator(new LinearInterpolator());
        scaleToFull2.setStartOffset(ANIMATION_DURATION*3);
        scaleToFull2.setFillAfter(true);

        // In adding animations to a set the order in which they're added DOES MATTER!!!
        // First add rotations or they won't be applied correctly.
        AnimationSet set = new AnimationSet(true);
        //set.setInterpolator(new LinearInterpolator());
        set.addAnimation(rotate90);
        set.addAnimation(rotate180);
        set.addAnimation(rotate270);
        set.addAnimation(rotate0);

        set.addAnimation(scaleToHalf1);
        set.addAnimation(scaleToFull1);
        set.addAnimation(scaleToHalf2);
        set.addAnimation(scaleToFull2);

        set.addAnimation(translateRight);
        set.addAnimation(translateDown);
        set.addAnimation(translateLeft);
        set.addAnimation(translateUp);



        set.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                animateBottomRightView(v);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        v.startAnimation(set);
    }



}
