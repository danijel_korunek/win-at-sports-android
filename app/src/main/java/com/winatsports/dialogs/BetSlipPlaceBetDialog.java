package com.winatsports.dialogs;



import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.activities.BetSlipActivity;
import com.winatsports.custom.carousel.ArrayWheelAdapter;
import com.winatsports.custom.carousel.OnWheelClickedListener;
import com.winatsports.custom.carousel.OnWheelScrollListener;
import com.winatsports.custom.carousel.WheelView;
import com.winatsports.data.Constants;
import com.winatsports.data.elements.Teaser;
import com.winatsports.data.upload.AsyncSendBetSlip;
import com.winatsports.settings.Settings;

import java.util.List;


public class BetSlipPlaceBetDialog extends Dialog {

    private BetSlipActivity uiContext;

    private Teaser teaser;

    TextView tvTitle;

    TextView tvBetSlipWagerAmount, tvToWinLabel, tvToWinAmount;
    Button btBetSlipDismiss, btBetSlipSubmit;
    EditText etBetSlipEnterMoneyAmount;
    WheelView wheelView;
    private String totalOdds;
    double toWinAmount = 0.0;

    LinearLayout llBetSlipMainLayout, llBetSlipConfirmBet, llBetSlipInsertMoney;

    boolean isCarouselSettingOn, isConfirmBetSettingOn, isWinAmountSettingOn;
    int finalMoneyValue; // also in most of time using when carousel is ON, ACTIVATED, but maybe also for editText field
    String moneyValueWithDollarSign; // using if I have carousel ON, ACTIVATED
    private boolean showWarning = false;
    private String [] moneyValues;


    public BetSlipPlaceBetDialog(BetSlipActivity context, Teaser teaser) {
        super(context);

        getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        uiContext = context;
        this.teaser = teaser;

        List<?> values = Util.createValuesForCarousel();
        moneyValues = values.toArray(new String [ values.size() ]);

        totalOdds = Util.formatDecimalOdds(uiContext, Util.calculateTotalOdds());

        isCarouselSettingOn = Settings.isPredefinedWagerAmount(uiContext);
        isConfirmBetSettingOn = Settings.isBetConfirmation(uiContext);
        isWinAmountSettingOn = Settings.isWinAmount(uiContext);

        if( !isCarouselSettingOn) {
            setContentView(LayoutInflater.from(uiContext).inflate(R.layout.bet_slip_without_carousel, null));
            etBetSlipEnterMoneyAmount = (EditText) findViewById(R.id.etBetSlipEnterMoneyAmount);
        }
        else {
            setContentView(LayoutInflater.from(uiContext).inflate(R.layout.bet_slip_with_carousel, null));
            wheelView = (WheelView) findViewById(R.id.wheelView);
        }

        llBetSlipMainLayout = (LinearLayout) findViewById(R.id.llBetSlipMainLayout);
        llBetSlipInsertMoney = (LinearLayout) findViewById(R.id.llBetSlipInsertMoney);
        llBetSlipConfirmBet = (LinearLayout) findViewById(R.id.llBetSlipConfirmBet);

        tvTitle = (TextView) findViewById(R.id.tvBetSlipPopUpTitle);
        adjustTitleText();

        setupViews();
        setCancelable(true);
    }

    private void setupViews() {

        tvBetSlipWagerAmount = (TextView) findViewById(R.id.tvBetSlipWagerAmount);
        tvToWinLabel = (TextView) findViewById(R.id.tvToWinLabel);
        tvToWinAmount = (TextView) findViewById(R.id.tvToWinAmount);

        btBetSlipDismiss = (Button) findViewById(R.id.btBetSlipDismiss);
        btBetSlipSubmit = (Button) findViewById(R.id.btBetSlipSubmit);

        tvBetSlipWagerAmount.setText(Util.getString(Constants.MSG_KEY_PARLAYBETPOPUPRED1));
        tvToWinLabel.setText( Util.getString(Constants.MSG_KEY_PARLAYBETPOPUPRED2));
        btBetSlipDismiss.setText(Util.getString(Constants.MSG_KEY_PARLAYBETPOPUPCANCELBUTTON));
        btBetSlipSubmit.setText(Util.getString(Constants.MSG_KEY_PLACEBETPOPUPPLACEBETBUTTON));

        btBetSlipDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        btBetSlipSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // check if user has enough money
                double walletAmount = MyApplication.getInstance().getUserData().getWalletAmount();
                double enteredValue = 0.0; //Double.parseDouble(etBetSlipEnterMoneyAmount.getText().toString());

                if (!isCarouselSettingOn)
                    enteredValue = Double.parseDouble(etBetSlipEnterMoneyAmount.getText().toString());
                else {
                    enteredValue = finalMoneyValue;
                }

                if (enteredValue <= 0.0) {
                    Util.showMessageDialog(uiContext, Util.getString(Constants.MSG_KEY_MANUALENTRYERROR1),
                            new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    dialog.dismiss();
                                }
                            });
                    return;
                }

                if (enteredValue > walletAmount) {
                    Util.showMessageDialog(uiContext, Util.getString(Constants.MSG_KEY_MANUALENTRYERROR2),
                            new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    dialog.dismiss();
                                }
                            });
                    return;
                }

                if (isConfirmBetSettingOn) {
                    dismiss();
                    showBetConfirmationDialog(enteredValue, toWinAmount);
                } else {
                    dismiss();
                    new AsyncSendBetSlip(uiContext, teaser, enteredValue).execute();
                }
            }

        });

        if (!isCarouselSettingOn) {

            etBetSlipEnterMoneyAmount.setHint(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_ENTER_AMOUNT));

            if (etBetSlipEnterMoneyAmount.getText().length() <= 0)
                tvToWinAmount.setText(Util.formatMoney(0.0, false));

            if (etBetSlipEnterMoneyAmount.requestFocus())
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

            TextWatcher fieldValidatorTextWatcher = new TextWatcher() {
                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    if( etBetSlipEnterMoneyAmount.getText().toString().trim().length() > 8 )
                        return;
                        // If etEnterMoneyAmount is empty, without any number,, then in t1vCalculateWinAmount will be written ==> "$0,00"
                    else if( etBetSlipEnterMoneyAmount.getText().toString().trim().length() <= 0 )
                        tvToWinAmount.setText(Util.formatMoney(0.0, false));
                    else {
                        String stringMoneyValue = String.valueOf(s);
                        finalMoneyValue = Integer.parseInt(stringMoneyValue);

                        updateToWinText();

                    }
                }

            };
            etBetSlipEnterMoneyAmount.addTextChangedListener(fieldValidatorTextWatcher);
        } else {

            // current Money Value INDEX,, on the initialization of Place Bet View
            int positionOfCurrentMoneyValue = 0;
            // after that we are adding static money values from carousel, to String array
            // For now we know that carousel has 5 values( "$5", "$10", "$20", "$50" and "$100" ), that way is size 5

            final OnWheelScrollListener scrollListener = new OnWheelScrollListener() {
                @Override
                public void onScrollingStarted(WheelView wheel) {

                }

                @Override
                public void onScrollingFinished(WheelView wheel) {

                    int position = wheel.getCurrentItem();

                    MoneyValueWheelAdapter moneyValueAdapter = new MoneyValueWheelAdapter(uiContext, moneyValues, position);
                    wheel.setViewAdapter(moneyValueAdapter);

                    CharSequence moneyValue = moneyValueAdapter.getItemText(position);
                    moneyValueWithDollarSign = String.valueOf(moneyValue); // it can be $5, $10, $20, $50 or $100
                    String correctMoneyValue = moneyValueWithDollarSign.substring(1); // money value without dollar($) sign
                    finalMoneyValue = Integer.parseInt(correctMoneyValue); // it can be 5, 10, 20, 50, 100

                    updateToWinText();
                }
            };

            OnWheelClickedListener clickedListener = new OnWheelClickedListener() {
                @Override
                public void onItemClicked(WheelView wheel, int itemIndex) {

                    int position = wheel.getCurrentItem();

                    MoneyValueWheelAdapter moneyValueAdapter = new MoneyValueWheelAdapter(uiContext, moneyValues, position);
                    wheel.setViewAdapter(moneyValueAdapter);

                    CharSequence moneyValue = moneyValueAdapter.getItemText(position);
                    moneyValueWithDollarSign = String.valueOf(moneyValue); // it can be $5, $10, $20, $50 or $100
                    String correctMoneyValue = moneyValueWithDollarSign.substring(1); // money value without dollar($) sign
                    finalMoneyValue = Integer.parseInt(correctMoneyValue); // it can be 5, 10, 20, 50, 100

                    updateToWinText();
                }
            };

            MoneyValueWheelAdapter moneyValueWheelAdapter = new MoneyValueWheelAdapter(uiContext, moneyValues, positionOfCurrentMoneyValue);
            wheelView.setViewAdapter( moneyValueWheelAdapter );
            wheelView.setCurrentItem(positionOfCurrentMoneyValue);
            wheelView.addClickingListener(clickedListener);
            wheelView.addScrollingListener(scrollListener);

            positionOfCurrentMoneyValue = wheelView.getCurrentItem();
            CharSequence moneyValue =   moneyValueWheelAdapter.getItemText(positionOfCurrentMoneyValue);
            moneyValueWithDollarSign = String.valueOf(moneyValue); // it can be $5, $10, $20, $50 or $100
            String correctMoneyValue = moneyValueWithDollarSign.substring(1); // money value without dollar($) sign
            // on initialization, on creating "Place Bet PopUp Window" ===>> Money value is 5, but it can be 5, 10, 20, 50 or 100
            finalMoneyValue = Integer.parseInt(correctMoneyValue); // here on initialization, I always pass ==> money value = 5

            updateToWinText();
        }

    }

    /**
     * Adapter for string based wheel. Highlights the current value.
     */
    private class MoneyValueWheelAdapter extends ArrayWheelAdapter<String> {

        // Index of current item
        int currentItem;

        // Index of item to be highlighted
        int currentValue;

        /**
         * Constructor
         */
        public MoneyValueWheelAdapter(Context context, String[] items, int current) {
            super(context, items);
            this.currentValue = current;
            setTextSize(17);
            //setTextSize((int) context.getResources().getDimension(R.dimen.smaller_list_row_text_size));

        }

        @Override
        protected void configureTextView(TextView view) {
            super.configureTextView(view);
            if (currentItem == currentValue) {
                view.setTextColor(0xFF000000);
            }
            view.setTypeface(Typeface.SANS_SERIF);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            currentItem = index;
            return super.getItem(index, cachedView, parent);

        }

        public int getCurrentValue() {
            return currentValue;
        }
    }

    private void updateToWinText() {
        double totalOdds = 0.0;

        if (teaser == null) {
            totalOdds = Util.calculateTotalOdds();
        } else {
            totalOdds = Util.convertUSOddsToDecimalOdds(teaser.getOdd());
        }

        if (isWinAmountSettingOn)
            toWinAmount = (finalMoneyValue * totalOdds) - finalMoneyValue;
        else
            toWinAmount = (finalMoneyValue * totalOdds);
        tvToWinAmount.setText(Util.formatMoney(toWinAmount, false));
    }

    private void adjustTitleText() {
        if (teaser == null)
            tvTitle.setText(Util.getBetSlipTitle());
        else {
            StringBuffer sb = new StringBuffer();
            sb.append(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_PARLAY));
            sb.append(" (" + MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_TOTAL) + " ");
            sb.append(Util.formatOdds(MyApplication.getInstance(), teaser.getOdd()) + ")");
            tvTitle.setText(sb.toString());
        }
    }

    @Override
    public void show() {
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = (int) (uiContext.getResources().getDisplayMetrics().widthPixels * Util.getMultiplierForDialogWidth(uiContext));
        getWindow().setBackgroundDrawable(null);
        super.show();
        getWindow().setAttributes(lp);
    }


    private void showBetConfirmationDialog(final double amount, final double toWin) {
        dismiss();
        final Dialog betConfirmationDialog = new Dialog(uiContext);

        betConfirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        betConfirmationDialog.setCancelable(true);
        betConfirmationDialog.setContentView(R.layout.dialog_confirm_bet_slip);

        Button btnConfirm = (Button) betConfirmationDialog.findViewById(R.id.btConfirmBet);
        btnConfirm.setText(Util.getString(Constants.MSG_KEY_PARLAYPOPUPCONFIRMBUTTON));
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                betConfirmationDialog.dismiss();

                new AsyncSendBetSlip(uiContext, teaser, amount).execute();
            }
        });

        Button btnCancel = (Button) betConfirmationDialog.findViewById(R.id.btCancelBet);
        btnCancel.setText(Util.getString(Constants.MSG_KEY_PARLAYPOPUPCANCELBUTTON));
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                betConfirmationDialog.dismiss();
            }
        });

        TextView tvBetTitle = (TextView) betConfirmationDialog.findViewById(R.id.tvConfirmBetTitle);
        tvBetTitle.setText(Util.getString(Constants.MSG_KEY_BETCONFIRMTITLE));

        TextView tvBetWagerAmount = (TextView) betConfirmationDialog.findViewById(R.id.tvText1);
        tvBetWagerAmount.setText(Util.getString(Constants.MSG_KEY_PARLAYBETPOPUPRED1) + " " + Util.formatMoney(amount, false) + " ");

        TextView tvBetWinAmount = (TextView) betConfirmationDialog.findViewById(R.id.tvText2);
        tvBetWinAmount.setText(Util.getString(Constants.MSG_KEY_PARLAYBETPOPUPRED2) + " " + Util.formatMoney(toWin, false));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(betConfirmationDialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = (int) (MyApplication.getInstance().getResources().getDisplayMetrics().widthPixels * Util.getMultiplierForDialogWidth(uiContext));
        betConfirmationDialog.getWindow().setBackgroundDrawable(null);
        betConfirmationDialog.show();
        betConfirmationDialog.getWindow().setAttributes(lp);
    }


}
