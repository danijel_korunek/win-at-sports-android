package com.winatsports.dialogs;




import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.activities.GuestActivity;
import com.winatsports.data.Constants;

public class RealMoneyWarningDialog extends Dialog {

    private Activity uiContext;

    TextView tvRealMoneyTitle, tvRealMoneyDescription;
    Button btRealMoneyRegister, btRealMoneyClose;
    RelativeLayout mainLinearLayoutOddsOfGame;

    public RealMoneyWarningDialog(Activity ctx, RelativeLayout mainLinearLayoutOddsOfGame1 ) {
        super(ctx);

        uiContext = ctx;
        mainLinearLayoutOddsOfGame = mainLinearLayoutOddsOfGame1;

        getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.real_money_pop_up_window_notification);

        tvRealMoneyTitle = (TextView) findViewById(R.id.tvRealMoneyTitle);
        tvRealMoneyDescription = (TextView) findViewById(R.id.tvRealMoneyDescription);
        tvRealMoneyTitle.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_REALBETTINGLOGINPOPUPTITLE));
        tvRealMoneyDescription.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_REALBETTINGLOGINPOPUPMESSAGE));

        btRealMoneyRegister = (Button) findViewById(R.id.btRealMoneyRegister);
        btRealMoneyClose = (Button) findViewById(R.id.btRealMoneyClose);
        btRealMoneyRegister.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_REALBETTINGLOGINPOPUPCONFIRMBUTTON));
        btRealMoneyClose.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_REALBETTINGLOGINPOPUPCANCELBUTTON));

        btRealMoneyRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(uiContext, GuestActivity.class);

                uiContext.finish();
                uiContext.startActivity(intent);
                uiContext.overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                dismiss();
            }
        });

        btRealMoneyClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mainLinearLayoutOddsOfGame != null)
                    mainLinearLayoutOddsOfGame.setAlpha(1.0f);
            }
        });

    }

    @Override
    public void show() {
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = (int) (uiContext.getResources().getDisplayMetrics().widthPixels * (Util.getDisplayDiagonalInInches(uiContext) > 6.5 ? 0.5 : 0.75));
        getWindow().setBackgroundDrawable(null);
        super.show();
        getWindow().setAttributes(lp);
    }
}
