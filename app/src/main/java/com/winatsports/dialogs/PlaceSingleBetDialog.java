package com.winatsports.dialogs;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.activities.GameOddsActivity;
import com.winatsports.custom.CounterTextView;
import com.winatsports.custom.carousel.ArrayWheelAdapter;
import com.winatsports.custom.carousel.OnWheelScrollListener;
import com.winatsports.custom.carousel.WheelView;
import com.winatsports.data.Constants;
import com.winatsports.data.elements.Game;
import com.winatsports.data.elements.Odd;
import com.winatsports.data.elements.Sport;
import com.winatsports.data.elements.Team;
import com.winatsports.data.parlaytotal.ParlayOdd;
import com.winatsports.data.upload.AsyncPlaceBet;
import com.winatsports.settings.Settings;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by broda on 05/01/2016.
 */
public class PlaceSingleBetDialog extends Dialog {

    private GameOddsActivity uiContext;

    private Context context;
    private ViewFlipper vfFlipper;

    Odd finalOdds;  // common thing, using on PLACE BET VIEW (FOR STRAIGHT AND BET SLIP)
    Game finalGame; //  common thing, using on PLACE BET VIEW (FOR STRAIGHT AND BET SLIP)
    String finalTitleOdds;  //  common thing, using on PLACE BET VIEW (FOR STRAIGHT AND BET SLIP)

    TextView tvTitlePlaceBet, tvStraightPlaceBet, tvBetSlipPlaceBet; // common textView, using on PLACE BET VIEW (FOR STRAIGHT AND BET SLIP)

    private DisplayMetrics displayMetrics;  // common things,, using on PLACE BET VIEW (FOR STRAIGHT AND BET SLIP)
    int dpWidth = -1;  // common things,, using on PLACE BET VIEW (FOR STRAIGHT AND BET SLIP)
    int dpHeight = -1;  // common things,, using on PLACE BET VIEW (FOR STRAIGHT AND BET SLIP)

    private LinearLayout slidingPlaceBetLayout1, slidingPlaceBetLayout2;  // common things,, using on PLACE BET VIEW (FOR STRAIGHT AND BET SLIP)

    TextView tvPlaceBetWagerAmount, tvPlaceBetWinAmount, tvPlaceBetCalculateWinAmount; // using FOR STRAIGHT VIEW
    Button btPlaceBetDismissStraight, btPlaceBetSubmitStraight; // using FOR STRAIGHT  VIEW
    EditText etEnterMoneyAmount; // using FOR STRAIGHT VIEW
    Boolean isConfirmBetSettingOn; // using for STRAIGHT VIEW
    WheelView moneyValuesCarousel; // using for STRAIGHT VIEW
    double winProfitOrLosingProfit = 0.0; // using for STRAIGHT VIEW
    String moneyValueWithDollarSign = ""; // using for STRAIGHT VIEW
    boolean isWinAmountSettingOn;


    TextView tvTeamsNamePlaceBet, tvDateAndTimePlaceBet, tvBetSlipPicks, tvNumberOfGamesBet; // using for BET SLIP VIEW
    Button btPlaceBetDismissPopUp, btPlaceBetViewBetSlipButton; // using for BET SLIP VIEW
    ParlayOdd parlayOdd; // Most of time, in 99% using for BET SLIP VIEW

    private String[] moneyValues;

    private CounterTextView tvNumberOfParlayItemsOddsOfGame;

    private LinearLayout llPlaceBetMainLayout;
    private LinearLayout llBetSlipConfirmSingleBet; // If user has set up confirm bet dialog for single bet, then I need this linearLayout
    boolean isCarouselSettingOn;
    int finalMoneyValue; // also in most of time using when carousel is ON, ACTIVATED, but maybe also for editText field

    public PlaceSingleBetDialog(GameOddsActivity ctx, CounterTextView tvNumberOfParlayItemsOddsOfGame1, String titleOdds1, Odd finalOdds1, Game finalGame1,
                                ArrayList<Team> allTeamsNames, RelativeLayout mainLinearLayoutOddsOfGame ) {

        super(ctx);

        context = uiContext = ctx;

        getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        this.tvNumberOfParlayItemsOddsOfGame = tvNumberOfParlayItemsOddsOfGame1;

        List<String> values = Util.createValuesForCarousel();
        this.moneyValues = values.toArray(new String[values.size()]);
        finalOdds = finalOdds1;
        finalGame = finalGame1;
        finalTitleOdds = titleOdds1;
        // I need to have this variable, because on dismiss PopUp WINDOW I WANT TO SET ALPHA TO 1.0f
        // OtherWise alpha will be 0.4f. This is so set in GameOddsActivity.java  ====> mainLinearLayoutOddsOfGame.setAlpha(0.4f);
        final RelativeLayout mainLinearLayoutOddsOfGame1 = mainLinearLayoutOddsOfGame;

        isCarouselSettingOn = Settings.isPredefinedWagerAmount(uiContext);
        isConfirmBetSettingOn = Settings.isBetConfirmation(uiContext);
        isWinAmountSettingOn = Settings.isWinAmount(uiContext);

        displayMetrics = this.uiContext.getResources().getDisplayMetrics();

        dpHeight = WindowManager.LayoutParams.WRAP_CONTENT; /// displayMetrics.heightPixels;
        dpWidth = displayMetrics.widthPixels; /// displayMetrics.density;

        if( isCarouselSettingOn == false ) {

            setContentView(LayoutInflater.from(uiContext).inflate(R.layout.place_bet_view_without_carousel, null));
            etEnterMoneyAmount = (EditText) findViewById(R.id.etEnterMoneyAmount);

        }
        else {

            setContentView(LayoutInflater.from(uiContext).inflate(R.layout.place_bet_view_with_carousel, null));

            moneyValuesCarousel = (WheelView) findViewById(R.id.placeBetMoneyValuesCarousel);
        }

        llPlaceBetMainLayout = (LinearLayout) findViewById(R.id.llPlaceBetMainLayout);

        vfFlipper = (ViewFlipper) findViewById(R.id.vfFlipper);

        tvTitlePlaceBet = (TextView) findViewById(R.id.tvTitlePlaceBet);
        tvTitlePlaceBet.setText(finalTitleOdds);

        tvStraightPlaceBet = (TextView) findViewById(R.id.tvStraightPlaceBet);
        tvBetSlipPlaceBet = (TextView) findViewById(R.id.tvBetSlipPlaceBet);

        // here I set text with ID from MESSAGES_EN.XML, MESSAGES_ES.XML or MESSAGES_PL.XML  FILE..
        // ID IS ====>> placeBetPopupTab1
        tvStraightPlaceBet.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PLACEBETPOPUPTAB1));
        tvBetSlipPlaceBet.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PLACEBETPOPUPTAB2));

        tvStraightPlaceBet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vfFlipper.setDisplayedChild(Constants.C_VIEW_PLACE_BET_STRAIGHT);
                changeBackgroundColorMenu(Constants.C_VIEW_PLACE_BET_STRAIGHT);
            }
        });

        tvBetSlipPlaceBet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vfFlipper.setDisplayedChild(Constants.C_VIEW_PLACE_BET_SLIP);
                changeBackgroundColorMenu(Constants.C_VIEW_PLACE_BET_SLIP);
            }
        });


        setUpStraightView();

        setUpBetSlipView(allTeamsNames);

    }


    private void setUpStraightView() {

        tvPlaceBetWagerAmount = (TextView) findViewById(R.id.tvPlaceBetWagerAmount);
        tvPlaceBetWinAmount = (TextView) findViewById(R.id.tvPlaceBetWinAmount);
        tvPlaceBetCalculateWinAmount = (TextView) findViewById(R.id.tvPlaceBetCalculateWinAmount);

        btPlaceBetDismissStraight = (Button) findViewById(R.id.btPlaceBetDismissStraight);
        btPlaceBetSubmitStraight = (Button) findViewById(R.id.btPlaceBetSubmitStraight);

        tvPlaceBetWagerAmount.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_BETTYPE1));
        tvPlaceBetWinAmount.setText( MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PARLAYBETPOPUPRED2));
        btPlaceBetDismissStraight.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PARLAYBETPOPUPCANCELBUTTON));
        btPlaceBetSubmitStraight.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PLACEBETPOPUPPLACEBETBUTTON));


        btPlaceBetDismissStraight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        // If user has set up confirm bet dialog for single bet, then I need this linearLayout ====>> "llBetSlipConfirmSingleBet"
        llBetSlipConfirmSingleBet = (LinearLayout) findViewById(R.id.llBetSlipConfirmSingleBet);
        btPlaceBetSubmitStraight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                placeStraightBet();
            }
        });

        if( isCarouselSettingOn == false ) {

            etEnterMoneyAmount.setHint(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_ENTER_AMOUNT));

            if( etEnterMoneyAmount.getText().length() <= 0 )
                tvPlaceBetCalculateWinAmount.setText("$0,00");

            if (etEnterMoneyAmount != null) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }

            TextWatcher fieldValidatorTextWatcher = new TextWatcher() {
                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    if( etEnterMoneyAmount.getText().toString().trim().length() > 8 ) {
                        finalMoneyValue = 0;
                        return;
                    }

                    // If etEnterMoneyAmount is empty, without any number,, then in t1vCalculateWinAmount will be written ==> "$0,00"
                    else if( etEnterMoneyAmount.getText().toString().trim().length() <= 0 )
                        tvPlaceBetCalculateWinAmount.setText("$0,00");
                    else {
                        String stringMoneyValue = String.valueOf(s);
                        finalMoneyValue = Integer.parseInt(stringMoneyValue);

                        double toWinAmount = 0.0;

                        if (isWinAmountSettingOn)
                            toWinAmount = (finalMoneyValue * Util.convertUSOddsToDecimalOdds(finalOdds.getBetOdds())) - finalMoneyValue;
                        else
                            toWinAmount = (finalMoneyValue * Util.convertUSOddsToDecimalOdds(finalOdds.getBetOdds()));
                        tvPlaceBetCalculateWinAmount.setText(Util.formatMoney(toWinAmount, false));
                    }

                }


            };
            etEnterMoneyAmount.addTextChangedListener(fieldValidatorTextWatcher);

        }
        else {

            // current Money Value INDEX,, on the initialization of Place Bet View
            int positionOfCurrentMoneyValue = 0;

            final OnWheelScrollListener scrollListener = new OnWheelScrollListener() {
                @Override
                public void onScrollingStarted(WheelView wheel) {

                }

                @Override
                public void onScrollingFinished(WheelView wheel) {

                    String selectedMoneyAmount = moneyValues[ wheel.getCurrentItem() ];

                    moneyValueWithDollarSign = String.valueOf(selectedMoneyAmount); // it can be $5, $10, $20, $50 or $100
                    String correctMoneyValue = moneyValueWithDollarSign.substring(1); // money value without dollar($) sign
                    finalMoneyValue = Integer.parseInt(correctMoneyValue); // it can be 5, 10, 20, 50, 100

                    double toWinAmount = 0.0;

                    if (isWinAmountSettingOn)
                        toWinAmount = (finalMoneyValue * Util.convertUSOddsToDecimalOdds(finalOdds.getBetOdds())) - finalMoneyValue;
                    else
                        toWinAmount = (finalMoneyValue * Util.convertUSOddsToDecimalOdds(finalOdds.getBetOdds()));
                    tvPlaceBetCalculateWinAmount.setText(Util.formatMoney(toWinAmount, false));
                }
            };


            MoneyValueWheelAdapter moneyValueWheelAdapter = new MoneyValueWheelAdapter(uiContext, moneyValues, positionOfCurrentMoneyValue);
            moneyValuesCarousel.setViewAdapter(moneyValueWheelAdapter);
            moneyValuesCarousel.setCurrentItem(positionOfCurrentMoneyValue);
            moneyValuesCarousel.addScrollingListener(scrollListener);

            String selectedMoneyAmount = moneyValues[ moneyValuesCarousel.getCurrentItem() ];

            moneyValueWithDollarSign = String.valueOf(selectedMoneyAmount); // it can be $5, $10, $20, $50 or $100
            String correctMoneyValue = moneyValueWithDollarSign.substring(1); // money value without dollar($) sign
            finalMoneyValue = Integer.parseInt(correctMoneyValue); // it can be 5, 10, 20, 50, 100

            double toWinAmount = 0.0;

            if (isWinAmountSettingOn)
                toWinAmount = (finalMoneyValue * Util.convertUSOddsToDecimalOdds(finalOdds.getBetOdds())) - finalMoneyValue;
            else
                toWinAmount = (finalMoneyValue * Util.convertUSOddsToDecimalOdds(finalOdds.getBetOdds()));
            tvPlaceBetCalculateWinAmount.setText(Util.formatMoney(toWinAmount, false));
        }

    }


    /**
     * Adapter for string based wheel. Highlights the current value.
     */
    private class MoneyValueWheelAdapter extends ArrayWheelAdapter<String> {

        // Index of current item
        int currentItem;

        // Index of item to be highlighted
        int currentValue;

        /**
         * Constructor
         */
        public MoneyValueWheelAdapter(Context context, String[] items, int current) {
            super(context, items);
            this.currentValue = current;
            setTextSize(16);
        }

        @Override
        protected void configureTextView(TextView view) {
            super.configureTextView(view);
            if (currentItem == currentValue) {
                view.setTextColor(getContext().getResources().getColor(R.color.tint_color));
            }
            view.setTypeface(Typeface.SANS_SERIF);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            currentItem = index;
            return super.getItem(index, cachedView, parent);

        }

        public int getCurrentValue() {
            return currentValue;
        }

    }


    private void setUpBetSlipView(ArrayList<Team> allTeamsNames) {

        // all thing I need for drawing BET SLIP VIEW
        tvTeamsNamePlaceBet = (TextView) findViewById(R.id.tvTeamsNamePlaceBet);
        tvDateAndTimePlaceBet = (TextView) findViewById(R.id.tvDateAndTimePlaceBet);
        tvBetSlipPicks = (TextView) findViewById(R.id.tvBetSlipPicks);
        tvNumberOfGamesBet = (TextView) findViewById(R.id.tvNumberOfGamesBet);
        tvNumberOfGamesBet.setText("" + MyApplication.getInstance().getParlayArray().size());
        btPlaceBetDismissPopUp = (Button) findViewById(R.id.btPlaceBetDismissPopUp);
        btPlaceBetViewBetSlipButton = (Button) findViewById(R.id.btPlaceBetViewBetSlipButton);

        String teamsName = Util.formatHomeVsAway(finalGame, context);

        tvTeamsNamePlaceBet.setText(teamsName);
        tvDateAndTimePlaceBet.setText(finalGame.getStartDate());

        // afther that I need to write Bet Slip picks
        // and write text to this TWO BUTTONS ==> On that BET SLIP VIEW
        tvBetSlipPicks.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PLACEBETPOPUPTOTALWAGERS));
        btPlaceBetDismissPopUp.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PLACEBETPOPUPDISMISSBUTTON));
        btPlaceBetViewBetSlipButton.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PLACEBETPOPUPADDTOPARLAYBUTTON));


        btPlaceBetDismissPopUp.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        btPlaceBetViewBetSlipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                parlayOdd = new ParlayOdd();

                // First I need to check If there is any game in parlayArrayList
                // If there is no game in parlayArraylist, I can add this game to this parlayArrayList
                if( MyApplication.getInstance().getParlayArray().size() == 0 ) {
                    parlayOdd.setTitleOfOdd(finalTitleOdds);
                    parlayOdd.setGame(finalGame);
                    Sport finalSport = Util.getSport(finalGame.getSportID());
                    parlayOdd.setSport(finalSport);
                    parlayOdd.setOdd(finalOdds);
                    MyApplication.getInstance().getParlayArray().add(parlayOdd);

                    Set<String> parlaySet = new TreeSet<String>();
                    for (ParlayOdd odd : MyApplication.getInstance().getParlayArray()) {
                        parlaySet.add(odd.toString());
                    }
                    Settings.saveParlay(MyApplication.getInstance(), parlaySet);

                    // show total numbers of games the user bet, in textView
                    tvNumberOfGamesBet.setText("" + MyApplication.getInstance().getParlayArray().size());
                    tvNumberOfParlayItemsOddsOfGame.setCounter(MyApplication.getInstance().getParlayArray().size());
                    dismiss();
                }
                // Here I'm checking if user did bet on maximum allowed parlay bets
                else if( MyApplication.getInstance().getParlayArray().size() == Constants.MAXIMUM_PARLAY_TOTAL_BETS )  {
                    // It is good if I write here "uiContext" or "context"
                    Util.showMessageDialog( context, Util.getString(Constants.MSG_KEY_PLACEBETPOPUPADDMORE1),
                            new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    dialog.dismiss();
                                }
                    });
                }
                // If this is not the first game we are betting on, then we are going into this else statement
                else {

                    boolean didWeBetOnThisGame = false;
                    for( int index1=0; index1<MyApplication.getInstance().getParlayArray().size(); index1++  ) {

                        Game game = MyApplication.getInstance().getParlayArray().get(index1).getGame();
                        if (finalGame.getEventID() == game.getEventID())
                            didWeBetOnThisGame = true;
                    }
                    //  if this game is not in parlayArrayList, then we are adding this game to parlayArraylist
                    if( didWeBetOnThisGame == false ) {
                        parlayOdd.setTitleOfOdd(finalTitleOdds);
                        parlayOdd.setGame(finalGame);
                        Sport finalSport = Util.getSport(finalGame.getSportID());
                        parlayOdd.setSport(finalSport);
                        parlayOdd.setOdd(finalOdds);
                        MyApplication.getInstance().getParlayArray().add(parlayOdd);

                        Set<String> parlaySet = new TreeSet<String>();
                        for (ParlayOdd odd : MyApplication.getInstance().getParlayArray()) {
                            parlaySet.add(odd.toString());
                        }
                        Settings.saveParlay(MyApplication.getInstance(), parlaySet);

                        // show total numbers of games the user bet, in textView
                        tvNumberOfGamesBet.setText("" + MyApplication.getInstance().getParlayArray().size());
                        tvNumberOfParlayItemsOddsOfGame.setCounter(MyApplication.getInstance().getParlayArray().size());
                        dismiss();
                    }
                    // else we are showing error that only one wager is allowed per game
                    else {
                        Util.showMessageDialog( context, MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PLACEBETPOPUPADDMORE2),
                                new DialogInterface.OnCancelListener() {
                                    @Override
                                    public void onCancel(DialogInterface dialog) {
                                        dialog.dismiss();
                                    }
                                });
                    }
                }
            }
        });

    }

    @Override
    public void show() {
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.height = dpHeight;
        lp.width = (int) (displayMetrics.widthPixels * (Util.getDisplayDiagonalInInches(uiContext) > 6.5 ? 0.5 : 0.9));
        getWindow().setBackgroundDrawable(null);
        super.show();
        getWindow().setAttributes(lp);

    }

    // With help of this class I have disabled horizontalScrollView
    private class OnTouch implements View.OnTouchListener
    {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return true;
        }
    }


    public void changeBackgroundColorMenu(int index) {

        if (index == Constants.C_VIEW_PLACE_BET_STRAIGHT ) {
            // set text color of textfield
            tvStraightPlaceBet.setTextColor(uiContext.getResources().getColor(R.color.tint_color));
            tvBetSlipPlaceBet.setTextColor(uiContext.getResources().getColor(R.color.white));  // #ffffff ==> white color ,,   #000000 ==> black color
            // set background color of textfield
            tvStraightPlaceBet.setBackgroundColor(uiContext.getResources().getColor(R.color.white));
            tvBetSlipPlaceBet.setBackgroundColor(uiContext.getResources().getColor(R.color.tint_color));
        } else if (index == Constants.C_VIEW_PLACE_BET_SLIP) {

            tvStraightPlaceBet.setTextColor(uiContext.getResources().getColor(R.color.white));
            tvBetSlipPlaceBet.setTextColor(uiContext.getResources().getColor(R.color.tint_color));

            tvStraightPlaceBet.setBackgroundColor(uiContext.getResources().getColor(R.color.tint_color));
            tvBetSlipPlaceBet.setBackgroundColor(uiContext.getResources().getColor(R.color.white));
        }

    }

    private void placeStraightBet() {
        double amount = 0.0;

        if (!Settings.isPredefinedWagerAmount(context)) {
            if (etEnterMoneyAmount.getText().length() == 0)
                return; // ignore empty input

            try {
                amount = Double.parseDouble(etEnterMoneyAmount.getText().toString());
            } catch (Exception e) {
                return; // ignore wrong input
            }
        } else {
            amount = finalMoneyValue;
        }

        String errorMessage = null;
        if (amount <= 0.0) {
            errorMessage = Util.getString(Constants.MSG_KEY_MANUALENTRYERROR1);
        } else if (amount > MyApplication.getInstance().getUserData().getWalletAmount()) {
            errorMessage = Util.getString(Constants.MSG_KEY_MANUALENTRYERROR2);
        }

        if (errorMessage != null) {
            Util.showMessageDialog(context, errorMessage,
                    new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            dialog.dismiss();
                        }
                    });
            return;
        }

        if (!Settings.isBetConfirmation(context)) {

            ParlayOdd selectedParlayOdd = new ParlayOdd();

            selectedParlayOdd.setTitleOfOdd(finalTitleOdds);
            selectedParlayOdd.setGame(finalGame);
            Sport finalSport = Util.getSport(finalGame.getSportID());
            selectedParlayOdd.setSport(finalSport);
            selectedParlayOdd.setOdd(finalOdds);

            new AsyncPlaceBet(uiContext, selectedParlayOdd, amount).execute();
            dismiss();
        } else {
            dismiss();
            showBetConfirmationDialog(amount);
        }
    }


    private void showBetConfirmationDialog(final double amount) {
        final Dialog betConfirmationDialog = new Dialog(context);

        betConfirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        betConfirmationDialog.setCancelable(true);
        betConfirmationDialog.setContentView(R.layout.dialog_confirm_bet);

        Button btnConfirm = (Button) betConfirmationDialog.findViewById(R.id.btnConfirmSingleBet);
        btnConfirm.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PARLAYPOPUPCONFIRMBUTTON));
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                betConfirmationDialog.dismiss();
                ParlayOdd selectedParlayOdd = new ParlayOdd();

                selectedParlayOdd.setTitleOfOdd(finalTitleOdds);
                selectedParlayOdd.setGame(finalGame);
                Sport finalSport = Util.getSport(finalGame.getSportID());
                selectedParlayOdd.setSport(finalSport);
                selectedParlayOdd.setOdd(finalOdds);

                new AsyncPlaceBet(uiContext, selectedParlayOdd, amount).execute();
            }
        });

        Button btnCancel = (Button) betConfirmationDialog.findViewById(R.id.btnCancelSingleBet);
        btnCancel.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PARLAYPOPUPCANCELBUTTON));
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btPlaceBetSubmitStraight.setEnabled(true);
                btPlaceBetDismissStraight.setEnabled(true);
                tvStraightPlaceBet.setEnabled(true);
                tvBetSlipPlaceBet.setEnabled(true);

                if (isCarouselSettingOn)
                    moneyValuesCarousel.setEnabled(true);
                else
                    etEnterMoneyAmount.setEnabled(true);
                llPlaceBetMainLayout.setAlpha(1.0f);
                llPlaceBetMainLayout.setBackgroundColor(uiContext.getResources().getColor(R.color.tint_color));

                betConfirmationDialog.dismiss();
            }
        });

        TextView tvBetTitle = (TextView) betConfirmationDialog.findViewById(R.id.tvConfirmSingleBetTitle);
        tvBetTitle.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_BETCONFIRMTITLE));
        TextView tvBetWagerAmount = (TextView) betConfirmationDialog.findViewById(R.id.tvConfirmSingleBetWagerAmount);
        tvBetWagerAmount.setText(Util.formatMoney(amount, false) + " " + MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_TERM_ON) + " " + finalTitleOdds);
        TextView tvBetWinAmount = (TextView) betConfirmationDialog.findViewById(R.id.tvConfirmSingleBetWinAmount);
        tvBetWinAmount.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PARLAYBETPOPUPRED2) + " " + tvPlaceBetCalculateWinAmount.getText().toString());

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(betConfirmationDialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = (int) (context.getResources().getDisplayMetrics().widthPixels * Util.getMultiplierForDialogWidth(uiContext));
        betConfirmationDialog.getWindow().setBackgroundDrawable(null);
        betConfirmationDialog.show();
        betConfirmationDialog.getWindow().setAttributes(lp);
    }
}
