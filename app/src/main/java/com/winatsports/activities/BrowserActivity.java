package com.winatsports.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.http.SslError;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.data.Constants;
import com.winatsports.data.upload.RBLogoutThread;

/**
 * Created by dkorunek on 18/03/16.
 */
public class BrowserActivity extends Activity {
    private WebView webView;

    private ImageView ivArrowBack;
    private TextView tvTitle;
    private ProgressBar pbDownload;

    private String urlToShow;

    private ImageView ivPrev, ivNext, ivRefresh, ivStop;
    private int eventId;
    private boolean isError;
    private String realMoneyDownMsg;

    private boolean initialUrlLoaded;

    public final static String KEY_URL = "url";

    private BroadcastReceiver downloadStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_DOWNLOAD_STATE)) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateDownloadIndicator();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_QUITTING)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_NETWORK_STATUS_UPDATE)) {
                final boolean connected = intent.getBooleanExtra(MyApplication.KEY_NETWORK_STATUS, false);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (connected) {
                            // reset
                            isError = false;
                            webView.loadUrl(urlToShow);
                        } else {
                            webView.loadData(realMoneyDownMsg, "text/html", "utf-8");
                            Log.d(BrowserActivity.class.getSimpleName(), "connectivity lost");
                            isError = true;
                        }
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_NETWORK_FAILURE)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String str = Util.getString(Constants.MSG_KEY_APP_NOINET);
                        if (!Util.isPopulated(str))
                            str = getResources().getString(R.string.error_no_internet);

                        Util.showMessageDialog(BrowserActivity.this, str, new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                MyApplication.getInstance().quit();
                                MyApplication.getInstance().shutDownVM();
                            }
                        });
                    }
                });
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.browser_activity);

        if (getIntent() != null && getIntent().hasExtra(GameOddsActivity.KEY_EVENT_ID))
            eventId = getIntent().getIntExtra(GameOddsActivity.KEY_EVENT_ID, 0);

        initialUrlLoaded = false;

        realMoneyDownMsg = "<center><h3>" + getRealMoneyDownMessage() + "</h3></center>";
        isError = false;

        webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setSupportMultipleWindows(false);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                view.loadData(realMoneyDownMsg, "text/html", "utf-8");
                isError = true;
            }
        });

        if (getIntent() != null && getIntent().hasExtra(KEY_URL)) {
            urlToShow = getIntent().getStringExtra(KEY_URL);
        } else {
            Log.e(BrowserActivity.class.getSimpleName(), "Didn't get anything!");
        }

        ImageView ivBack = (ImageView) findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setText("");

        pbDownload = (ProgressBar) findViewById(R.id.pbDownload);

        ivPrev = (ImageView) findViewById(R.id.ivPrevious);
        ivPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (webView.canGoBack())
                    webView.goBack();
            }
        });

        ivNext = (ImageView) findViewById(R.id.ivNext);
        ivNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (webView.canGoForward())
                    webView.goForward();
            }
        });

        ivStop = (ImageView) findViewById(R.id.ivStop);
        ivStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.stopLoading();
            }
        });

        ivRefresh = (ImageView) findViewById(R.id.ivRefresh);
        ivRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.isNetworkConnected(BrowserActivity.this) && isError) {
                    isError = false;
                    webView.loadUrl(urlToShow);
                } else
                    webView.reload();
            }
        });

        ivArrowBack = (ImageView) findViewById(R.id.ivBack);
        ivArrowBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        MyApplication.getInstance().getActivityStack().push(BrowserActivity.class.getName());
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter();
        filter.addAction(MyApplication.ACTION_DOWNLOAD_STATE);
        filter.addAction(MyApplication.ACTION_NETWORK_STATUS_UPDATE);
        filter.addAction(MyApplication.ACTION_NETWORK_FAILURE);
        registerReceiver(downloadStateReceiver, filter);

        if (!initialUrlLoaded) {
            if (Util.isNetworkConnected(MyApplication.getInstance())) {
                initialUrlLoaded = true;
                webView.loadUrl(urlToShow);
            } else {
                webView.loadData(realMoneyDownMsg, "text/html", "utf-8");
                isError = true;
            }
        } else {
            if (Util.isNetworkConnected(MyApplication.getInstance())) {
                webView.loadUrl(urlToShow);
            } else {
                webView.loadData(realMoneyDownMsg, "text/html", "utf-8");
                isError = true;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(downloadStateReceiver);
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack() {
        ivArrowBack.setOnClickListener(null);

        // logout
        MyApplication.getInstance().getExecutorService().submit(new RBLogoutThread());

        MyApplication.getInstance().getActivityStack().pop(); // remove the current class

        try {
            String previousClassName = MyApplication.getInstance().getActivityStack().pop();
            Class sourceClass = Class.forName(previousClassName);

            Intent intent = new Intent(this, sourceClass);
            if (eventId > 0)
                intent.putExtra(GameOddsActivity.KEY_EVENT_ID, eventId);
            startActivity(intent);
            overridePendingTransition(R.anim.end_activity_slide_in_left, R.anim.end_activity_slide_out_right);

        } catch (Exception e) {
            Log.e(BrowserActivity.class.getSimpleName(), "", e);
        }

        finish();
    }

    private void updateDownloadIndicator() {
        ProgressBar pbDownload = (ProgressBar) findViewById(R.id.pbDownload);
        if (MyApplication.getInstance().isDownloading())
            pbDownload.setVisibility(View.VISIBLE);
        else
            pbDownload.setVisibility(View.INVISIBLE);
    }

    private String getRealMoneyDownMessage() {
        StringBuffer sb = new StringBuffer();
        sb.append(Util.getString(Constants.MSG_KEY_REALMONEYDOWN) + "<br>");
        sb.append(Util.getString(Constants.MSG_KEY_REALMONEYDOWN2) + "<br>");
        sb.append(Util.getString(Constants.MSG_KEY_REALMONEYDOWN3) + "<br>");
        sb.append(Util.getString(Constants.MSG_KEY_REALMONEYDOWN4) + "<br>");
        sb.append(Util.getString(Constants.MSG_KEY_REALMONEYDOWN5) + "<br>");
        sb.append(Util.getString(Constants.MSG_KEY_REALMONEYDOWN6) + "<br>");
        return sb.toString();
    }
}
