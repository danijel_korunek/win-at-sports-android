package com.winatsports.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.custom.CounterTextView;
import com.winatsports.data.Constants;
import com.winatsports.data.MyParlay;
import com.winatsports.data.UserData;
import com.winatsports.data.adapters.WagersDetailsAdapter;
import com.winatsports.settings.Settings;


public class WagersDetailsActivity extends Activity {

    Context context;
    private BroadcastReceiver downloadStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_DOWNLOAD_STATE)) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateDownloadIndicator();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_NETWORK_FAILURE)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String str = Util.getString(Constants.MSG_KEY_APP_NOINET);
                        if (!Util.isPopulated(str))
                            str = getResources().getString(R.string.error_no_internet);

                        Util.showMessageDialog(WagersDetailsActivity.this, str, new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                MyApplication.getInstance().quit();
                                MyApplication.getInstance().shutDownVM();
                            }
                        });
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_LAUNCH_GUEST_ACTIVITY)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent guestActivityIntent = new Intent(WagersDetailsActivity.this, GuestActivity.class);
                        startActivity(guestActivityIntent);
                        overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                        finish();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_REDOWNLOAD)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (MyApplication.getInstance().wasAppInBgMoreThanXSeconds && !Settings.getUserR(getApplicationContext()).equals(Settings.DEFAULT_USER_R)) {
                            //MyApplication.getInstance().wasAppInBgMoreThanXSeconds = false;
                            MyApplication.getInstance().downloadData(WagersDetailsActivity.this, true, true, true);
                        }
                    }
                });
            }
        }
    };

    TextView tvWagersTitle, tvWagersDescription;
    CounterTextView tvNumberOfParlayItemsWagers;
    private FrameLayout flBetSlip;
    ListView lvWagersDetailsContent;
    LinearLayout llWagersWidthHeightOfArrowBack;
    private MyParlay parlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wagers_details);

        context = this;

        this.parlay = MyApplication.getInstance().getParlayToDisplay();

        UserData userData = MyApplication.getInstance().getUserData();
        tvWagersTitle = (TextView) findViewById(R.id.tvWagersTitle);
        tvWagersTitle.setText(Util.generateWalletTitle());

        Util.showAnimationForWalletTitle(tvWagersTitle);

        tvNumberOfParlayItemsWagers = (CounterTextView) findViewById(R.id.tvNumberOfParlayItemsMainActivity);
        tvNumberOfParlayItemsWagers.setCounter(MyApplication.getInstance().getParlayArray().size());

        tvWagersDescription = (TextView) findViewById(R.id.tvWagersDescription);
        if( parlay.getTeaserOdd() == 0.0 )
            tvWagersDescription.setText(Util.getString(Constants.MSG_KEY_INPLAYPARLAY));
        else  {
            tvWagersDescription.setText(Util.getString(Constants.MSG_KEY_INPLAYTEASER));
        }

        lvWagersDetailsContent = (ListView) findViewById(R.id.lvWagersDetailsContent);

        WagersDetailsAdapter wagersDetailsAdapter = new WagersDetailsAdapter(context, parlay);
        lvWagersDetailsContent.setAdapter(wagersDetailsAdapter);

        llWagersWidthHeightOfArrowBack = (LinearLayout)findViewById(R.id.llWagersWidthHeightOfArrowBack);
        llWagersWidthHeightOfArrowBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        flBetSlip = (FrameLayout) findViewById(R.id.flBetSlip);
        flBetSlip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (MyApplication.getInstance().getParlayArray().size() > 0) {

                    Intent intent = new Intent(WagersDetailsActivity.this, BetSlipActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                    finish();
                } else {

                    Util.showMessageDialog(context, MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_BETSLIPISEMPTY), new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {

                        }
                    });
                }
            }
        });


        MyApplication.getInstance().getActivityStack().push(WagersDetailsActivity.class.getName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvNumberOfParlayItemsWagers.setCounter(MyApplication.getInstance().getParlayArray().size());

        IntentFilter filter = new IntentFilter();
        filter.addAction(MyApplication.ACTION_NETWORK_FAILURE);
        filter.addAction(MyApplication.ACTION_DOWNLOAD_STATE);
        filter.addAction(MyApplication.ACTION_LAUNCH_GUEST_ACTIVITY);
        filter.addAction(MyApplication.ACTION_REDOWNLOAD);
        registerReceiver(downloadStateReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(downloadStateReceiver);
    }


    private void updateDownloadIndicator() {
        ProgressBar pbDownload = (ProgressBar) findViewById(R.id.pbDownload);
        if (MyApplication.getInstance().isDownloading())
            pbDownload.setVisibility(View.VISIBLE);
        else
            pbDownload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack() {
        MyApplication.getInstance().getActivityStack().pop(); // remove the current class
        String previousClassName = MyApplication.getInstance().getActivityStack().pop();
        Class sourceClass = null;

        try {
            sourceClass = Class.forName(previousClassName);
        } catch (Exception e) {
            Log.e(WagersDetailsActivity.class.getSimpleName(), "", e);
        }

        Intent intent = new Intent(this, sourceClass);
        intent.putExtra(StatisticsActivity.KEY_REDOWNLOAD, false);

        startActivity(intent);
        overridePendingTransition(R.anim.end_activity_slide_in_left, R.anim.end_activity_slide_out_right);
        finish();
    }

}
