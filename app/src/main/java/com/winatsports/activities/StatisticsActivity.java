package com.winatsports.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.custom.CounterTextView;
import com.winatsports.data.Constants;
import com.winatsports.data.MyParlay;
import com.winatsports.data.adapters.NoRecordsAdapter;
import com.winatsports.data.adapters.StatBestWinsAdapter;
import com.winatsports.data.adapters.StatPieChartAdapter;
import com.winatsports.data.adapters.StatWinningsAdapter;
import com.winatsports.data.adapters.StatisticsPagerAdapter;
import com.winatsports.data.adapters.StatsWeeklyOverviewAdapter;
import com.winatsports.settings.Settings;


/**
 * Created by dkorunek on 26/08/16.
 */
public class StatisticsActivity extends Activity {

    private DisplayMetrics displayMetrics;
    private float dpWidth, dpHeight;

    private ViewPager vpPager;
    private ListView lvWinnings;
    private ListView lvWeeklySummary;
    private ListView lvPieCharts;

    private final int MENU_ITEM_WIDTH = 150;

    private final int VIEW_WINNINGS = 0;
    private final int VIEW_WEEKLY_SUMMARY = 1;
    private final int VIEW_PIE_CHARTS = 2;

    private HorizontalScrollView horizontalMenu;
    private FrameLayout flBetSlip;
    private CounterTextView counterTextView;

    public static final String KEY_REDOWNLOAD = "redownload_data";

    private TextView tvWallet;
    private BroadcastReceiver downloadStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {

            if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_DOWNLOAD_STATE)) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateDownloadIndicator();
                        counterTextView.setCounter(MyApplication.getInstance().getParlayArray().size());
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(ACTION_STATS_PARSED)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (intent.hasExtra(KEY_ERROR))
                            updateUI(intent.getExtras().getString(KEY_ERROR));
                        else
                            updateUI(null);
                    }
                });

            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_NETWORK_FAILURE)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String str = Util.getString(Constants.MSG_KEY_APP_NOINET);
                        if (!Util.isPopulated(str))
                            str = getResources().getString(R.string.error_no_internet);

                        Util.showMessageDialog(StatisticsActivity.this, str, new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                MyApplication.getInstance().quit();
                                MyApplication.getInstance().shutDownVM();
                            }
                        });
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_LAUNCH_GUEST_ACTIVITY)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent guestActivityIntent = new Intent(StatisticsActivity.this, GuestActivity.class);
                        startActivity(guestActivityIntent);
                        overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                        finish();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_REDOWNLOAD)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (MyApplication.getInstance().wasAppInBgMoreThanXSeconds && !Settings.getUserR(getApplicationContext()).equals(Settings.DEFAULT_USER_R)) {
                            //MyApplication.getInstance().wasAppInBgMoreThanXSeconds = false;
                            MyApplication.getInstance().downloadData(StatisticsActivity.this, true, true, true);
                        }
                    }
                });
            }

        }
    };

    private final String ACTION_STATS_PARSED = "com.winatsports.STATS_PARSED";
    private final String KEY_ERROR = "error";

    private ProgressBar pbStatsDownload;

    private LinearLayout llBack;

    private TextView tvWinnings;
    private TextView tvWeeklySummary;
    private TextView tvPieCharts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_NO_TITLE); // command to hide title, that is usually created at the top of the application
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

        setContentView(R.layout.activity_statistics);

        displayMetrics = this.getResources().getDisplayMetrics();

        dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        dpWidth = displayMetrics.widthPixels; // / displayMetrics.density;

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        vpPager = (ViewPager) findViewById(R.id.vpPager);
        vpPager.setAdapter(new StatisticsPagerAdapter(this));
        vpPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                scrollToTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        horizontalMenu = (HorizontalScrollView) findViewById(R.id.hsvMenu);
        tvWinnings = (TextView) findViewById(R.id.menuWinnings);
        tvWinnings.setText(Util.getString(Constants.MSG_KEY_STATS_LASTBETWINS));
        tvWinnings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollToTab(VIEW_WINNINGS);
            }
        });

        tvWeeklySummary = (TextView) findViewById(R.id.menuWeeklySummary);
        tvWeeklySummary.setText(Util.getString(Constants.MSG_KEY_STATS_WEEKLY_SUMMARY));
        tvWeeklySummary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollToTab(VIEW_WEEKLY_SUMMARY);
            }
        });

        tvPieCharts = (TextView) findViewById(R.id.menuPieCharts);
        tvPieCharts.setText(Util.getString(Constants.MSG_KEY_STATS_LEAGUE_PIECHARTS));
        tvPieCharts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollToTab(VIEW_PIE_CHARTS);
            }
        });

        flBetSlip = (FrameLayout) findViewById(R.id.flBetSlip);
        flBetSlip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( MyApplication.getInstance().getParlayArray().size() > 0 ) {

                    Intent intent = new Intent(StatisticsActivity.this, BetSlipActivity.class);

                    finish();
                    startActivity(intent);
                    overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                }
                else {

                    Util.showMessageDialog(StatisticsActivity.this, Util.getString(Constants.MSG_KEY_BETSLIPISEMPTY), new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            dialog.dismiss();
                        }
                    });
                }
            }
        });

        counterTextView = (CounterTextView) findViewById(R.id.tvBetSlipCount);
        counterTextView.setText("" + MyApplication.getInstance().getParlayArray().size());

        tvWallet = (TextView) findViewById(R.id.tvWallet);
        tvWallet.setText(Util.generateWalletTitle());
        Util.showAnimationForWalletTitle(tvWallet);

        pbStatsDownload = (ProgressBar) findViewById(R.id.pbStatsDownload);
        pbStatsDownload.setVisibility(View.VISIBLE);
        //vpPager.setVisibility(View.GONE);

        llBack = (LinearLayout) findViewById(R.id.llBack);
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        // avoid re-downloading of all data if user comes back to this screen from BetSlipActivity or WagerDetailsActivity
        if (getIntent() != null) {
            boolean redownload = getIntent().getBooleanExtra(KEY_REDOWNLOAD, false);
            if (redownload) {
                downloadAndParseData();
            } else {
                vpPager.post(new Runnable() {
                    @Override
                    public void run() {
                        updateUI(null);
                    }
                });
            }
        } else {
            Log.d(StatisticsActivity.class.getSimpleName(), "intent is null!");
        }

        MyApplication.getInstance().getActivityStack().push(StatisticsActivity.class.getName());
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter();
        filter.addAction(MyApplication.ACTION_DOWNLOAD_STATE);
        filter.addAction(MyApplication.ACTION_ARRAYS_SWITCHED);
        filter.addAction(ACTION_STATS_PARSED);
        filter.addAction(MyApplication.ACTION_NETWORK_FAILURE);
        filter.addAction(MyApplication.ACTION_LAUNCH_GUEST_ACTIVITY);
        filter.addAction(MyApplication.ACTION_REDOWNLOAD);
        registerReceiver(downloadStateReceiver, filter);

        // in case user leaves the activity and comes back, download intent might not be caught
        if (MyApplication.getInstance().getStatsCollection() != null) {
            updateUI(null);
        }
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            unregisterReceiver(downloadStateReceiver);
        } catch (Exception ignored) {}
    }

    private void updateDownloadIndicator() {
        ProgressBar pbDownload = (ProgressBar) findViewById(R.id.pbDownload);
        if (MyApplication.getInstance().isDownloading())
            pbDownload.setVisibility(View.VISIBLE);
        else
            pbDownload.setVisibility(View.INVISIBLE);
    }

    private void scrollToTab(int tab) {
        adjustMenuColors(tab);

        if (tab == VIEW_WINNINGS) {
            horizontalMenu.scrollTo(0, 0);
            setupStatWinnings();
        } else if (tab == VIEW_WEEKLY_SUMMARY) {
            horizontalMenu.scrollTo(tab * MENU_ITEM_WIDTH + MENU_ITEM_WIDTH / 2, 0);
            setupWeeklySummary();
        } else if (tab == VIEW_PIE_CHARTS) {
            horizontalMenu.scrollTo(tab * MENU_ITEM_WIDTH, 0);
            setupPieCharts();
        }
        vpPager.setCurrentItem(tab, true);
    }

    private void setupStatWinnings() {
        lvWinnings = (ListView) vpPager.findViewById(R.id.lvWinnings);

        if (lvWinnings != null && MyApplication.getInstance().getStatsCollection() != null) {
            StatWinningsAdapter adapter = new StatWinningsAdapter(this);

            if (MyApplication.getInstance().getStatsCollection().getUserStats().getBestWins().size() == 0)
                adapter.addSection(Util.getString(Constants.MSG_KEY_STATS_BEST_WINS), new NoRecordsAdapter(this, Util.getString(Constants.MSG_KEY_NO_RECORDS)));
            else
                adapter.addSection(Util.getString(Constants.MSG_KEY_STATS_BEST_WINS), new StatBestWinsAdapter(this, MyApplication.getInstance().getStatsCollection().getUserStats().getBestWins()));

            if (MyApplication.getInstance().getStatsCollection().getUserStats().getLastBets().size() == 0)
                adapter.addSection(Util.getString(Constants.MSG_KEY_STATS_LAST_WIN), new NoRecordsAdapter(this, Util.getString(Constants.MSG_KEY_NO_RECORDS)));
            else
                adapter.addSection(Util.getString(Constants.MSG_KEY_STATS_LAST_WIN), new StatBestWinsAdapter(this, MyApplication.getInstance().getStatsCollection().getUserStats().getLastBets()));

            lvWinnings.setAdapter(adapter);

            lvWinnings.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Object item = lvWinnings.getAdapter().getItem(position);
                    if (item != null) {
                        MyParlay myParlay = (MyParlay) item;
                        MyApplication.getInstance().setParlayToDisplay(myParlay);

                        if (myParlay.getMyBets().size() > 1) {
                            Intent intent = new Intent(StatisticsActivity.this, WagersDetailsActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                            finish();
                        } else {
                            // nothing
                        }
                    } else {
                        // nothing
                    }
                }
            });
        }
    }

    private void setupWeeklySummary() {
        lvWeeklySummary = (ListView) vpPager.findViewById(R.id.lvWeeklySummary);
        if (lvWeeklySummary != null) {
            lvWeeklySummary.setAdapter(new StatsWeeklyOverviewAdapter(this));
        }

    }

    private void setupPieCharts() {
        lvPieCharts = (ListView) vpPager.findViewById(R.id.lvPieCharts);
        if (lvPieCharts != null) {
            lvPieCharts.setAdapter(new StatPieChartAdapter(this));
        }


    }

    private void updateUI(String error) {
        Log.d(StatisticsActivity.class.getSimpleName(), "updating ui!");
        pbStatsDownload.setVisibility(View.GONE);

        if (error != null) {
            Util.showMessageDialog(StatisticsActivity.this, error, null);
        } else {
            // render first tab
            scrollToTab(VIEW_WINNINGS);
        }

    }

    private void adjustMenuColors(int index) {
        if (index == VIEW_WINNINGS) {
            tvWinnings.setTextColor(getResources().getColor(R.color.tint_color));
            tvWinnings.setBackgroundColor(getResources().getColor(R.color.white));

            tvWeeklySummary.setTextColor(getResources().getColor(R.color.white));
            tvWeeklySummary.setBackgroundColor(getResources().getColor(R.color.tint_color));

            tvPieCharts.setTextColor(getResources().getColor(R.color.white));
            tvPieCharts.setBackgroundColor(getResources().getColor(R.color.tint_color));
        } else if (index == VIEW_WEEKLY_SUMMARY) {
            tvWinnings.setTextColor(getResources().getColor(R.color.white));
            tvWinnings.setBackgroundColor(getResources().getColor(R.color.tint_color));

            tvWeeklySummary.setTextColor(getResources().getColor(R.color.tint_color));
            tvWeeklySummary.setBackgroundColor(getResources().getColor(R.color.white));

            tvPieCharts.setTextColor(getResources().getColor(R.color.white));
            tvPieCharts.setBackgroundColor(getResources().getColor(R.color.tint_color));
        } else {
            tvWinnings.setTextColor(getResources().getColor(R.color.white));
            tvWinnings.setBackgroundColor(getResources().getColor(R.color.tint_color));

            tvWeeklySummary.setTextColor(getResources().getColor(R.color.white));
            tvWeeklySummary.setBackgroundColor(getResources().getColor(R.color.tint_color));

            tvPieCharts.setTextColor(getResources().getColor(R.color.tint_color));
            tvPieCharts.setBackgroundColor(getResources().getColor(R.color.white));
        }
    }

    private void downloadAndParseData() {
        MyApplication.getInstance().getExecutorService().execute(new Thread(new Runnable() {
            @Override
            public void run() {
                Intent endIntent = new Intent(ACTION_STATS_PARSED);

                if (!Util.isNetworkConnected(MyApplication.getInstance())) {
                    endIntent.putExtra(KEY_ERROR, Util.getString(Constants.MSG_KEY_APP_NOINET));
                } else {
                    try {
                        MyApplication.getInstance().setStatsCollection(Util.downloadStatsOverall());
                    } catch (Exception e) {
                        MyApplication.getInstance().setStatsCollection(null);
                        Log.e(StatisticsActivity.class.getSimpleName(), "Error", e);
                        endIntent.putExtra(KEY_ERROR, Util.getString(Constants.MSG_KEY_APP_TECH_DIFF));
                    }
                }

                sendBroadcast(endIntent);
            }
        }));
    }

    private void goBack() {
        MyApplication.getInstance().getActivityStack().pop(); // remove the current class
        String previousClassName = MyApplication.getInstance().getActivityStack().pop();
        Class sourceClass = null;

        try {
            sourceClass = Class.forName(previousClassName);
        } catch (Exception e) {
            Log.e(BetSlipActivity.class.getSimpleName(), "", e);
        }

        Intent intent = new Intent(this, sourceClass);
        startActivity(intent);
        overridePendingTransition(R.anim.end_activity_slide_in_left, R.anim.end_activity_slide_out_right);
        finish();
    }
}
