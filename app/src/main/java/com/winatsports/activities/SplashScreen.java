package com.winatsports.activities;


import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;
import com.tune.Tune;
import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.data.Constants;
import com.winatsports.pushnotif.Config;
import com.winatsports.pushnotif.NotificationUtils;
import com.winatsports.settings.Settings;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.extras.Base64;
import cz.msebera.android.httpclient.message.BasicNameValuePair;


public class SplashScreen extends Activity {

    private String selectedLanguage;
    private final int REQUEST_PERMISSIONS = 101;

    private boolean alreadyAskedForPermissions;
    private TextView tvVersionName;

    private boolean reentry;

    private BroadcastReceiver broadcastReceiver = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        reentry = false;

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(Config.REGISTRATION_COMPLETE)){
                    Log.d(SplashScreen.class.getSimpleName(), "Push registration completed");
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                }else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String message = intent.getStringExtra("message");
                    Log.d(SplashScreen.class.getSimpleName(), "New push message received: " + message);
                }
            }
        };

        tvVersionName = (TextView) findViewById(R.id.tvVersionName);
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;

            tvVersionName.setText("v. " + version);
        } catch (Exception e) {
            tvVersionName.setText("");
        }

        Settings.setTimesRun(this, Settings.getTimesRun(this) + 1);
        Log.d(SplashScreen.class.getSimpleName(), "Language: " + Settings.getLanguage(this));
        //alreadyAskedForPermissions = false;

/* TODO leave this commented
        gcmClientManager = new GCMClientManager(this, getString(R.string.gcm_sender_id));
        gcmClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler(){

            @Override
            public void onSuccess(String registrationId, boolean isNewRegistration) {
                Log.d(SplashScreen.class.getSimpleName(), "GCM registration success");
                new GCMRegistration().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, registrationId, Util.isNotificationEnabled(getApplicationContext()) ? "7" : "0");
            }

            @Override
            public void onFailure(String ex) {
                super.onFailure(ex);
            }
        });
*/
        // LEAVE THIS COMMENTED! DON'T ERASE IT BECAUSE IT MAY BE NEEDED AGAIN LATER!
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    getPackageName(),
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//            Log.e(SplashScreen.class.getSimpleName(), "", e);
//        } catch (NoSuchAlgorithmException e) {
//            Log.e(SplashScreen.class.getSimpleName(), "", e);
//        }


    }

    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());

        Settings.setTimeWhenEnteredBg(this, System.currentTimeMillis());
        Settings.setTimeWhenEnteredFg(this, System.currentTimeMillis());

/* TODO leave commented
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!alreadyAskedForPermissions) {
                if (!MyApplication.getInstance().areAllPermissionsGranted()) {
                    if (!Settings.hasUserRefusedToGrantPermissions(this))
                        handleNotAllGranted();
                    else {
                        Util.showMessageDialog(this, getString(R.string.app_permisison_refused), new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                finish();
                                android.os.Process.killProcess(android.os.Process.myPid());
                                System.exit(0);
                            }
                        });
                    }
                } else {
                    proceed();
                }
            }
        } else {
            proceed();
        } */
        proceed();
    }
/* TODO leave this commented
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSIONS) {
            boolean allGranted = true;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    allGranted = false;
                    break;
                }
            }

            if (!allGranted) {
                Settings.setUserRefusedToGrantPermissions(this, true);
                Util.showMessageDialog(this, getString(R.string.app_permisison_refused), new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        finish();
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(0);
                    }
                });
            } else {
                proceed();
            }

        }
    } */

    private void proceed() {
        if (!Util.isNetworkConnected(this)) {
            Util.showMessageDialog(this, Util.getString(Constants.MSG_KEY_APP_NOINET), new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    MyApplication.getInstance().quit();
                    finish();
                }
            });
        }

        MyApplication.getInstance().adjustCertainAppFeatures();

        // Get source of open for app re-engagement
        // Tune.getInstance().setReferralSources(this);
        // Attribution Analytics will not function unless the measureSession call is included
        Tune.getInstance().measureSessionInternal();

        if (MyApplication.getInstance().isDownloading() && !MyApplication.getInstance().isAllowedRedownload()) {
            if (MyApplication.getInstance().getDownloadTask() != null)
                MyApplication.getInstance().getDownloadTask().showProgressDialog();
            else
                MyApplication.getInstance().downloadData(this, true, true, false);
        } else if (!MyApplication.getInstance().isDownloading() && MyApplication.getInstance().isAllowedRedownload()) {
            // proceed to main activity
            Intent nextActivityIntent = null;
            if (MyApplication.getInstance().getHeaderMainClass().isShowingAppTutorial()
                    && (Settings.getTimesRun(this) == (Settings.DEFAULT_TIMES_RUN+1)) )
                nextActivityIntent = new Intent(this, TutorialActivity.class);
            else if (Settings.getUserR(this).equals(Settings.DEFAULT_USER_R)) {
                nextActivityIntent = new Intent(this, GuestActivity.class);
            } else {
                nextActivityIntent = new Intent(this, MainActivity.class);
            }
            startActivity(nextActivityIntent);
            finish();
        } else if (!MyApplication.getInstance().isDownloading() && !MyApplication.getInstance().isAllowedRedownload()) {
            MyApplication.getInstance().downloadData(this, true, false, false);
        }
    }

    /*
        TODO: leave this commented!
    public void showLanguageDialog() {

        final Dialog langDialog = new Dialog(this);

        langDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        langDialog.setContentView(R.layout.initial_language_view);

        langDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });

        final Button btnSelectLanguage = (Button) langDialog.findViewById(R.id.btnSelectLanguage);
        btnSelectLanguage.setText(getString(R.string.button_select_langauge));
        btnSelectLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnSelectLanguage.getText().toString().equalsIgnoreCase(getString(R.string.button_select_langauge)))
                    return;
                Settings.setLanguage(SplashScreen.this, selectedLanguage);
                Settings.setFirstTimeLanguageSetup(SplashScreen.this, true);
                // submit settings
                langDialog.dismiss();
                new AsyncChangeLanguage(SplashScreen.this, selectedLanguage).execute();
            }
        });

        ListView lvLanguages = (ListView) langDialog.findViewById(R.id.lvLanguages);
        final LanguageAdapter adapter = new LanguageAdapter(MyApplication.getInstance().getUserData().getSupportLangs());
        lvLanguages.setAdapter(adapter);
        lvLanguages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LanguageAdapter.Language lItem = (LanguageAdapter.Language) adapter.getItem(position);
                adapter.setSelected(position);
                btnSelectLanguage.setText(lItem.lang.getLangName());
                selectedLanguage = lItem.lang.getLangID();
                adapter.notifyDataSetChanged();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(langDialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = (int) (getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
        langDialog.show();
        langDialog.getWindow().setAttributes(lp);
    } */

    private void handleNotAllGranted() {
        // handle the problem with this
        /*showNoPermissionDialog(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();*/
                ActivityCompat.requestPermissions(SplashScreen.this,
                        new String[] {
                                Manifest.permission.READ_PHONE_STATE/*,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE */},
                        REQUEST_PERMISSIONS);
         /*   }
        });*/
    }

//    public void showNoPermissionDialog(DialogInterface.OnCancelListener cancelListener) {
//
//        final Dialog dialog = new Dialog(this);
//
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.no_permission_dialog);
//
//        TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
//        tvTitle.setText(getString(R.string.app_permission_title));
//
//        TextView tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
//        tvMessage.setText(getString(R.string.app_permission_text));
//
//        Button btnProceed = (Button) dialog.findViewById(R.id.btnProceed);
//        btnProceed.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.cancel();
//            }
//        });
//
//        dialog.setOnCancelListener(cancelListener);
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        lp.width = (int) (getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
//        dialog.show();
//        dialog.getWindow().setAttributes(lp);
//
//        alreadyAskedForPermissions = true;
//    }



    @Override
    protected void onPause() {
        super.onPause();

        if (MyApplication.getInstance().getDownloadTask() != null) {
            MyApplication.getInstance().getDownloadTask().hideProgressDialog();
        }

        reentry = true;
    }
}
