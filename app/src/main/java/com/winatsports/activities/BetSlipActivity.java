package com.winatsports.activities;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.custom.BetSlipView;
import com.winatsports.data.Constants;
import com.winatsports.dialogs.BetSlipPlaceBetDialog;
import com.winatsports.dialogs.CustomProgressDialog;
import com.winatsports.settings.Settings;


public class BetSlipActivity extends Activity {

    private BroadcastReceiver downloadStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_DOWNLOAD_STATE)) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateDownloadIndicator();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_LAUNCH_GUEST_ACTIVITY)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent guestActivityIntent = new Intent(BetSlipActivity.this, GuestActivity.class);
                        startActivity(guestActivityIntent);
                        overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                        finish();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_NETWORK_FAILURE)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String str = Util.getString(Constants.MSG_KEY_APP_NOINET);
                        if (!Util.isPopulated(str))
                            str = getResources().getString(R.string.error_no_internet);

                        Util.showMessageDialog(BetSlipActivity.this, str, new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                MyApplication.getInstance().quit();
                                MyApplication.getInstance().shutDownVM();
                            }
                        });
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_REDOWNLOAD)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (MyApplication.getInstance().wasAppInBgMoreThanXSeconds && !Settings.getUserR(getApplicationContext()).equals(Settings.DEFAULT_USER_R)) {
                            //MyApplication.getInstance().wasAppInBgMoreThanXSeconds = false;
                            MyApplication.getInstance().downloadData(BetSlipActivity.this, true, true, true);
                        }
                    }
                });
            }

        }
    };

    private int eventId;
    private int sportId;
    private int leagueId;

    private ImageView ivArrowBack;
    private TextView tvWallet;
    private ImageView ivEdit;

    private Button btnSubmitParlay;

    private BetSlipView betSlipView;

    private BetSlipPlaceBetDialog betSlipPlaceBetDialog;
    private CustomProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_bet_slip);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        if (getIntent() != null) {
            eventId = getIntent().getIntExtra("EVENT_ID", 0);
            sportId = getIntent().getIntExtra("SPORT_ID", 0);
            leagueId = getIntent().getIntExtra("LEAGUE_ID", 0);
        }

        ivArrowBack = (ImageView) findViewById(R.id.ivArrowBack);
        ivArrowBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        tvWallet = (TextView) findViewById(R.id.tvWallet);
        Util.showAnimationForWalletTitle(tvWallet);

        ivEdit = (ImageView) findViewById(R.id.ivEdit);
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().setBetSlipInEditMode( !MyApplication.getInstance().isBetSlipInEditMode() );

                betSlipView.refresh();
            }
        });

        betSlipView = (BetSlipView) findViewById(R.id.betSlipView);

        btnSubmitParlay = (Button) findViewById(R.id.btnSubmitParlay);
        btnSubmitParlay.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PARLAYSUBMITBUTTON));
        btnSubmitParlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MyApplication.getInstance().getParlayArray().size() < 2) {
                    Util.showMessageDialog(BetSlipActivity.this, Util.getString(Constants.MSG_KEY_PARLAYWARNING), null);
                    return;
                }

                betSlipPlaceBetDialog = new BetSlipPlaceBetDialog(BetSlipActivity.this, betSlipView.getSelectedTeaser());
                betSlipPlaceBetDialog.show();
            }
        });


        MyApplication.getInstance().getActivityStack().push(BetSlipActivity.class.getName());
    }

    @Override
    public void onBackPressed() {
        goBack();
    }


    public void goBack() {
        ivArrowBack.setOnClickListener(null);
        MyApplication.getInstance().getActivityStack().pop(); // remove the current class
        String previousClassName = MyApplication.getInstance().getActivityStack().pop();
        Class sourceClass = null;

        try {
            sourceClass = Class.forName(previousClassName);
        } catch (Exception e) {
            Log.e(BetSlipActivity.class.getSimpleName(), "", e);
        }

        Intent intent = new Intent(BetSlipActivity.this, sourceClass);
        intent.putExtra("EVENT_ID", eventId);
        intent.putExtra("SPORT_ID", sportId);
        intent.putExtra("LEAGUE_ID", leagueId);
        intent.putExtra(StatisticsActivity.KEY_REDOWNLOAD, false);

        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.end_activity_slide_in_left, R.anim.end_activity_slide_out_right);
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MyApplication.ACTION_DOWNLOAD_STATE);
        intentFilter.addAction(MyApplication.ACTION_LAUNCH_GUEST_ACTIVITY);
        intentFilter.addAction(MyApplication.ACTION_NETWORK_FAILURE);
        intentFilter.addAction(MyApplication.ACTION_REDOWNLOAD);

        registerReceiver(downloadStateReceiver, intentFilter);
    }


    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(downloadStateReceiver);
        if (betSlipPlaceBetDialog != null && betSlipPlaceBetDialog.isShowing())
            betSlipPlaceBetDialog.dismiss();

        dismissProgressDialog();
    }

    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void showProgressDialog() {
        dismissProgressDialog();
        progressDialog = new CustomProgressDialog(this);
        progressDialog.show();
    }

    private void updateDownloadIndicator() {
        ProgressBar pbDownload = (ProgressBar) findViewById(R.id.pbDownload);
        if (MyApplication.getInstance().isDownloading())
            pbDownload.setVisibility(View.VISIBLE);
        else
            pbDownload.setVisibility(View.INVISIBLE);
    }
}
