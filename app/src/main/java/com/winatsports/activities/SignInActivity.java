package com.winatsports.activities;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.custom.MyFixedEditText;
import com.winatsports.data.Constants;
import com.winatsports.data.upload.AsyncLogin;
import com.winatsports.dialogs.CustomProgressDialog;


public class SignInActivity extends Activity {

    public static String KEY_EMAIL = "email";
    public static String KEY_PASSWORD = "password";

    private BroadcastReceiver downloadStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_DOWNLOAD_STATE)) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateDownloadIndicator();
                    }
                });
            }
        }
    };

    TextView tvLoginEmailTitle;
    MyFixedEditText etEmailUsername, etEmailPassword;
    Button btnLogin, btnForgotPassword;
    ImageView ivArrowBackLoginEmail;

    private AsyncLogin loginTask;
    private boolean isTaskWorking;
    private CustomProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_confirmation);

        tvLoginEmailTitle = (TextView) findViewById(R.id.tvLoginEmailTitle);
        btnForgotPassword = (Button) findViewById(R.id.btnForgotPassword);

        tvLoginEmailTitle.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_LOGIN));
        btnForgotPassword.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_LOGINFORGOTYOURPASSWORD));
        btnForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, PasswordRecoveryActivity.class);

                finish();
                startActivity(intent);
                overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
            }
        });

        etEmailUsername = (MyFixedEditText) findViewById(R.id.etEmailUsername);
        etEmailPassword = (MyFixedEditText) findViewById(R.id.etEmailPassword);

        etEmailUsername.setHint(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_OTHER_EMAIL));
        etEmailPassword.setHint(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_PASSWORD));

        String email = getIntent() != null && getIntent().hasExtra(KEY_EMAIL) ? getIntent().getStringExtra(KEY_EMAIL) : "";
        String pwd = getIntent() != null && getIntent().hasExtra(KEY_PASSWORD) ? getIntent().getStringExtra(KEY_PASSWORD) : "";

        etEmailUsername.setText(email);
        etEmailPassword.setText(pwd);

        addToKeyBoardDoneButton();

        btnLogin = (Button) findViewById(R.id.btLogin);
        btnLogin.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_LOGIN));
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSubmit();
            }
        });

        ivArrowBackLoginEmail = (ImageView) findViewById(R.id.ivArrowBackLoginEmail);
        ivArrowBackLoginEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

    }


    private void addToKeyBoardDoneButton() {

        etEmailPassword.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etEmailPassword.setSingleLine(true);
        etEmailPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
    }

    private void handleSubmit() {
        if (etEmailPassword.getText().length() == 0 || etEmailUsername.getText().length() == 0) {
            // skip when empty
            Util.showMessageDialog(this, Util.getString(Constants.MSG_KEY_ERR_MANDATORY), null);
            return;
        }

        btnLogin.setOnClickListener(null);

        if (loginTask == null && !isTaskWorking) {
            isTaskWorking = true;
            showProgressDialog();
            loginTask = new AsyncLogin(SignInActivity.this, etEmailUsername.getText().toString(), etEmailPassword.getText().toString());
            loginTask.execute();
        } else {
            // do nothing
        }
    }


    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack() {
        ivArrowBackLoginEmail.setOnClickListener(null);
        Intent intent = new Intent(this, GuestActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.end_activity_slide_in_left, R.anim.end_activity_slide_out_right);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(downloadStateReceiver, new IntentFilter(MyApplication.ACTION_DOWNLOAD_STATE));
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(downloadStateReceiver);
    }


    private void updateDownloadIndicator() {
        ProgressBar pbDownload = (ProgressBar) findViewById(R.id.pbDownload);
        if (MyApplication.getInstance().isDownloading())
            pbDownload.setVisibility(View.VISIBLE);
        else
            pbDownload.setVisibility(View.INVISIBLE);
    }


    public void finishTask() {
        loginTask = null;
        isTaskWorking = false;
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSubmit();
            }
        });
    }

    public void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new CustomProgressDialog(this);
            progressDialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

}
