package com.winatsports.activities;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.custom.CounterTextView;
import com.winatsports.data.Constants;
import com.winatsports.data.UserData;
import com.winatsports.data.adapters.LeagueAdapter;
import com.winatsports.data.elements.League;
import com.winatsports.data.elements.Sport;
import com.winatsports.settings.Settings;

import java.util.Collections;
import java.util.Comparator;


public class LeaguesActivity extends Activity {

    private Context context;

    private BroadcastReceiver downloadStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_DOWNLOAD_STATE)) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateDownloadIndicator();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_DOWNLOAD_STATE)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initializeUI();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_LAUNCH_GUEST_ACTIVITY)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent guestActivityIntent = new Intent(LeaguesActivity.this, GuestActivity.class);
                        startActivity(guestActivityIntent);
                        overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                        finish();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_NETWORK_FAILURE)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String str = Util.getString(Constants.MSG_KEY_APP_NOINET);
                        if (!Util.isPopulated(str))
                            str = getResources().getString(R.string.error_no_internet);

                        Util.showMessageDialog(LeaguesActivity.this, str, new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                MyApplication.getInstance().quit();
                                MyApplication.getInstance().shutDownVM();
                            }
                        });
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_REDOWNLOAD)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (MyApplication.getInstance().wasAppInBgMoreThanXSeconds && !Settings.getUserR(getApplicationContext()).equals(Settings.DEFAULT_USER_R)) {
                            //MyApplication.getInstance().wasAppInBgMoreThanXSeconds = false;
                            MyApplication.getInstance().downloadData(LeaguesActivity.this, true, true, true);
                        }
                    }
                });
            }
        }
    };

    private LinearLayout llSizeOfArrowBack;
	private TextView tvTitle, tvLeagueTitle;
	private ListView lvLeagues;

    private LeagueAdapter leagueAdapter;

    private int sportID = -1;
    private Sport sport;

    private CounterTextView tvNumberOfParlayItemsLeague;

    private FrameLayout flBetSlip;

    @Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_leagues);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        context = this;

        sportID = getIntent().getIntExtra("SPORT_ID", 0);

        UserData userData = MyApplication.getInstance().getUserData();
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setText( Util.generateWalletTitle() );

        Util.showAnimationForWalletTitle(tvTitle);

        tvNumberOfParlayItemsLeague = (CounterTextView) findViewById(R.id.tvNumberOfParlayItemsMainActivity);
        tvNumberOfParlayItemsLeague.setCounter(MyApplication.getInstance().getParlayArray().size());

        llSizeOfArrowBack = (LinearLayout)findViewById(R.id.llWidthHeightOfArrowBack);
        llSizeOfArrowBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();

            }
        });

        flBetSlip = (FrameLayout) findViewById(R.id.flBetSlip);
        flBetSlip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( MyApplication.getInstance().getParlayArray().size() > 0 ) {

                    Intent intent = new Intent(LeaguesActivity.this, BetSlipActivity.class);
                    intent.putExtra("SPORT_ID", sportID);

                    finish();
                    startActivity(intent);
                    overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                }
                else {

                    Util.showMessageDialog(context, MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_BETSLIPISEMPTY), new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            dialog.dismiss();
                        }
                    });
                }
            }
        });

        initializeUI();

        MyApplication.getInstance().getActivityStack().push(LeaguesActivity.class.getName());
	}

    private void initializeUI() {
        sport = Util.getSport(sportID);

        if (sport == null)
            return;

        Log.d(LeaguesActivity.class.getSimpleName(), "Showing " + sport.getLeagues().size() + " leagues for sport " + sport.getSportName());

        tvLeagueTitle = (TextView)findViewById(R.id.tvLeagueTitle);
        tvLeagueTitle.setText( sport.getSportName() );

        lvLeagues = (ListView)findViewById(R.id.lvLeagues);
        View footer = new View(this);

        //footer.setBackgroundColor(getResources().getColor(R.color.list_divider_color));
        lvLeagues.addFooterView(footer);
        Collections.sort(sport.getLeagues(), new Comparator<League>() {
            @Override
            public int compare(League o1, League o2) {
                return o1.getLeagueName().compareTo(o2.getLeagueName());
            }
        });

        leagueAdapter = new LeagueAdapter( this, sport.getLeagues(), sport.getScaledIcon() );
        lvLeagues.setAdapter(leagueAdapter);
        lvLeagues.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Object item = leagueAdapter.getItem(position);
                League league = (League) item;
                if (league.getGameArrayList().size() > 0) {

                    Intent intent = new Intent(LeaguesActivity.this, GamesActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("SPORT_ID", sport.getSportID());
                    bundle.putInt("LEAGUE_ID", sport.getLeagues().get(position).getLeagueID());

                    finish();
                    intent.putExtras(bundle);
                    startActivity(intent);
                    overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                } else {

                    Util.showMessageDialog( context, MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_NO_GAMES_LEAGUE),
                            new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    dialog.dismiss();
                                }
                            });
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        tvNumberOfParlayItemsLeague.setCounter(MyApplication.getInstance().getParlayArray().size());

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MyApplication.ACTION_ARRAYS_SWITCHED);
        intentFilter.addAction(MyApplication.ACTION_DOWNLOAD_STATE);
        intentFilter.addAction(MyApplication.ACTION_LAUNCH_GUEST_ACTIVITY);
        intentFilter.addAction(MyApplication.ACTION_NETWORK_FAILURE);
        intentFilter.addAction(MyApplication.ACTION_REDOWNLOAD);
        registerReceiver(downloadStateReceiver, intentFilter);
    }


    @Override
    public void onBackPressed() {
        goBack();
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(downloadStateReceiver);
    }


    private void updateDownloadIndicator() {
        ProgressBar pbDownload = (ProgressBar) findViewById(R.id.pbDownload);
        if (MyApplication.getInstance().isDownloading())
            pbDownload.setVisibility(View.VISIBLE);
        else
            pbDownload.setVisibility(View.INVISIBLE);
    }

    private void goBack() {
        llSizeOfArrowBack.setOnClickListener(null);
        MyApplication.getInstance().getActivityStack().pop(); // remove the current class
        String previousClassName = MyApplication.getInstance().getActivityStack().pop();
        Class sourceClass = null;

        try {
            sourceClass = Class.forName(previousClassName);
        } catch (Exception e) {
            Log.e(BetSlipActivity.class.getSimpleName(), "", e);
        }

        Intent intent = new Intent(this, sourceClass);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.end_activity_slide_in_left, R.anim.end_activity_slide_out_right);
    }


}
