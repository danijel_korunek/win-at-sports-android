package com.winatsports.activities;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.custom.CounterTextView;
import com.winatsports.data.Constants;
import com.winatsports.data.UserData;
import com.winatsports.data.adapters.GamesAdapter;
import com.winatsports.data.adapters.SeparatedGamesListAdapter;
import com.winatsports.data.elements.Game;
import com.winatsports.data.elements.League;
import com.winatsports.data.elements.Sport;
import com.winatsports.settings.Settings;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * Created by broda on 18/11/2015.
 */

public class GamesActivity extends Activity {

    Bundle bundleGames;

    private BroadcastReceiver downloadStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_DOWNLOAD_STATE)) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateDownloadIndicator();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_DOWNLOAD_STATE)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initializeUI();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_LAUNCH_GUEST_ACTIVITY)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent guestActivityIntent = new Intent(GamesActivity.this, GuestActivity.class);
                        startActivity(guestActivityIntent);
                        overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                        finish();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_NETWORK_FAILURE)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String str = Util.getString(Constants.MSG_KEY_APP_NOINET);
                        if (!Util.isPopulated(str))
                            str = getResources().getString(R.string.error_no_internet);

                        Util.showMessageDialog(GamesActivity.this, str, new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                MyApplication.getInstance().quit();
                                MyApplication.getInstance().shutDownVM();
                            }
                        });
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_REDOWNLOAD)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (MyApplication.getInstance().wasAppInBgMoreThanXSeconds && !Settings.getUserR(getApplicationContext()).equals(Settings.DEFAULT_USER_R)) {
                            //MyApplication.getInstance().wasAppInBgMoreThanXSeconds = false;
                            MyApplication.getInstance().downloadData(GamesActivity.this, true, true, true);
                        }
                    }
                });
            }
        }
    };

    private int leagueID = -1;
    private int sportID = -1;

    private ImageView ivHome;

    League league;
    Sport sport;

    // Adapter for ListView Contents
    private SeparatedGamesListAdapter separatedGamesAdapter;
    // ListView Contents
    private ListView lvGames;

    private LinearLayout llSizeOfArrowBack;
    private TextView tvTitle;
    private FrameLayout flBetSlip;
    private CounterTextView tvNumberOfParlayItemsGames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_games);

        MyApplication.getInstance().getActivityStack().push(GamesActivity.class.getName());

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ivHome = (ImageView) findViewById(R.id.ivHome);
        ivHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GamesActivity.this, MainActivity.class);

                startActivity(intent);
                overridePendingTransition(R.anim.end_activity_slide_in_left, R.anim.end_activity_slide_out_right);
                finish();
            }
        });

        tvNumberOfParlayItemsGames = (CounterTextView) findViewById(R.id.tvNumberOfParlayItemsMainActivity);
        tvNumberOfParlayItemsGames.setCounter(MyApplication.getInstance().getParlayArray().size());

        Intent intent = getIntent();
        bundleGames = intent.getExtras();

        sportID = bundleGames.getInt("SPORT_ID", 0);
        leagueID = bundleGames.getInt("LEAGUE_ID", 0);

        final UserData userData = MyApplication.getInstance().getUserData();
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setText( Util.generateWalletTitle() );

        Util.showAnimationForWalletTitle(tvTitle);

        // Get a reference to the ListView holder
        lvGames = (ListView) this.findViewById(R.id.lvGames);
        View footer = new View(this);

        footer.setBackgroundColor(getResources().getColor(R.color.list_divider_color));
        lvGames.addFooterView(footer);

        llSizeOfArrowBack = (LinearLayout)findViewById(R.id.llWidthHeightOfArrowBack);
        llSizeOfArrowBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        flBetSlip = (FrameLayout) findViewById(R.id.flBetSlip);
        flBetSlip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( MyApplication.getInstance().getParlayArray().size() > 0 ) {

                    Intent intent = new Intent(GamesActivity.this, BetSlipActivity.class);
                    intent.putExtra("SPORT_ID", sportID);
                    intent.putExtra("LEAGUE_ID", leagueID);

                    finish();
                    startActivity(intent);
                    overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                }
                else {

                    Util.showMessageDialog(GamesActivity.this, MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_BETSLIPISEMPTY), new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            dialog.dismiss();
                        }
                    });
                }
            }
        });

        initializeUI();

    }

    private void initializeUI() {
        // fill UI controls with data
        // Create the ListView Adapter
        separatedGamesAdapter = new SeparatedGamesListAdapter(this);

        sport = Util.getSport(sportID);
        if (sport == null) {
            Log.e(GamesActivity.class.getSimpleName(), "Sport is null");
            return;
        }

        league = Util.getLeague(leagueID);
        if (league == null) {
            Log.e(GamesActivity.class.getSimpleName(), "League is null");
            return;
        }

        Collections.sort(league.getGameArrayList(), new Comparator<Game>() {
            @Override
            public int compare(Game lhs, Game rhs) {

                String lDateString = lhs.getStartDate();
                String rDateString = rhs.getStartDate();

                SimpleDateFormat format = new SimpleDateFormat("EEEE, MMMM d, yyyy 'at' kk:mm");
                try {
                    Date lDate = format.parse(lDateString);
                    Date rDate = format.parse(rDateString);

                    return lDate.compareTo(rDate);

                } catch (ParseException e) {
                    Log.e(GamesActivity.class.getSimpleName(), "Date format error: ", e);
                    return -1;
                }
            }
        });

        if( league.getGameArrayList().size() > 0 ) {

            Map<String, ArrayList<Game>> displayMap = new LinkedHashMap<String, ArrayList<Game>>();
            ArrayList<Game> gamesOnSameDay = new ArrayList<Game>();
            ArrayList<Game> allGamesInLeague = new ArrayList<Game>();
            allGamesInLeague.addAll(league.getGameArrayList());

            while (allGamesInLeague.size() > 0) {
                Game anchor = allGamesInLeague.remove(0);
                Date anchorDate = getOnlyDate(anchor.getStartDate());

                gamesOnSameDay.add(anchor);

                for (Game g : allGamesInLeague) {
                    Date gameDate = getOnlyDate(g.getStartDate());

                    if (Util.onSameDay(anchorDate, gameDate))
                        gamesOnSameDay.add(g);
                }

                allGamesInLeague.removeAll(gamesOnSameDay);

                String sectionTitle = generateSectionName(anchorDate);
                ArrayList<Game> gamesForAdapter = new ArrayList<Game>();
                gamesForAdapter.addAll(gamesOnSameDay);
                Sport tempSport = Util.getSport(anchor.getSportID());
                GamesAdapter adapter = new GamesAdapter(this, tempSport.getScaledIcon(), gamesForAdapter);
                separatedGamesAdapter.addSection(sectionTitle, adapter);

                gamesOnSameDay.clear();
            }
        }

        lvGames.setAdapter(separatedGamesAdapter);
        lvGames.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long duration) {

                Object item = separatedGamesAdapter.getItem(position);
                Game finalGame = (Game) item;

                if (finalGame.getOddArrayList().size() > 0) {
                    Intent intent = new Intent(GamesActivity.this, GameOddsActivity.class);
                    Bundle oddsFromGame = new Bundle();
                    oddsFromGame.putInt("EVENT_ID", finalGame.getEventID()); // Pass Id from GAME => For example 49773

                    finish();
                    intent.putExtras(oddsFromGame); //Put your id to your next Intent
                    startActivity(intent);
                    overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                } else {

                    Util.showMessageDialog(GamesActivity.this, MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_NO_ODDS),
                            new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    dialog.dismiss();
                                }
                            });
                }
            }
        });
    }

    private Date getOnlyDate( String startDate ) {

        final SimpleDateFormat formatDate = new SimpleDateFormat("EEEE, MMMM d, yyyy 'at' kk:mm");
        final SimpleDateFormat newFormatDate = new SimpleDateFormat("EEEE, MMMM d, yyyy");
        Date date = null;
        try {
            date = formatDate.parse(startDate);
            return newFormatDate.parse(newFormatDate.format(date));
        } catch (ParseException e) {
            Log.e(MainActivity.class.getSimpleName(), "Date format error: ", e);
        }
        return null;
    }


    @Override
    protected void onResume() {
        super.onResume();

        tvNumberOfParlayItemsGames.setCounter(MyApplication.getInstance().getParlayArray().size());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MyApplication.ACTION_ARRAYS_SWITCHED);
        intentFilter.addAction(MyApplication.ACTION_DOWNLOAD_STATE);
        intentFilter.addAction(MyApplication.ACTION_LAUNCH_GUEST_ACTIVITY);
        intentFilter.addAction(MyApplication.ACTION_NETWORK_FAILURE);
        intentFilter.addAction(MyApplication.ACTION_REDOWNLOAD);
        registerReceiver(downloadStateReceiver, intentFilter);
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(downloadStateReceiver);
    }

    private void updateDownloadIndicator() {
        ProgressBar pbDownload = (ProgressBar) findViewById(R.id.pbDownload);
        if (MyApplication.getInstance().isDownloading())
            pbDownload.setVisibility(View.VISIBLE);
        else
            pbDownload.setVisibility(View.INVISIBLE);
    }

    private void goBack() {
        llSizeOfArrowBack.setOnClickListener(null);

        MyApplication.getInstance().getActivityStack().pop(); // remove the current class
        String previousClassName = MyApplication.getInstance().getActivityStack().pop();
        Class sourceClass = null;

        try {
            sourceClass = Class.forName(previousClassName);
        } catch (Exception e) {
            Log.e(GameOddsActivity.class.getSimpleName(), "", e);
        }

        if (sourceClass != null && previousClassName.equals(MainActivity.class.getName())) {
            Intent intent = new Intent(this, sourceClass);

            startActivity(intent);
            overridePendingTransition(R.anim.end_activity_slide_in_left, R.anim.end_activity_slide_out_right);
        } else if (sourceClass != null && previousClassName.equals(LeaguesActivity.class.getName())) {
            Intent intent = new Intent(GamesActivity.this, sourceClass);
            Bundle bundle = new Bundle();
            bundle.putInt("SPORT_ID", sport.getSportID());
            intent.putExtras(bundle);

            startActivity(intent);
            overridePendingTransition(R.anim.end_activity_slide_in_left, R.anim.end_activity_slide_out_right);
        } else {
            Log.d(GamesActivity.class.getName(), "Previous class: " + previousClassName);
        }

        finish();
    }

    private String generateSectionName(Date date) {
        final SimpleDateFormat newFormatDate = new SimpleDateFormat("EEEE, MMMM d, yyyy");

        try {
            return newFormatDate.format(date);
        } catch (Exception e) {
            Log.e(MainActivity.class.getSimpleName(), "Date format error: ", e);
        }
        return null;
    }

}