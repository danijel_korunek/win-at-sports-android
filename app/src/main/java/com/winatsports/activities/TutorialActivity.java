package com.winatsports.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.data.adapters.TutorialAdapter;
import com.winatsports.dialogs.CustomProgressDialog;
import com.winatsports.settings.Settings;


public class TutorialActivity extends Activity {

    TutorialAdapter viewPagerAdapter;
    ViewPager viewPager;
    LinearLayout llDots;
    private boolean startedFromProfile;
    public static final String KEY_FROM_PROFILE = "from_profile";

    boolean isMainActivityStarted = true;

    private CustomProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);


        if (getIntent() != null && getIntent().hasExtra(KEY_FROM_PROFILE))
            startedFromProfile = getIntent().getBooleanExtra(KEY_FROM_PROFILE, false);

        viewPager = (ViewPager) findViewById(R.id.viewPager);

        viewPagerAdapter = new TutorialAdapter(this, startedFromProfile);
        viewPager.setAdapter(viewPagerAdapter);

        llDots=(LinearLayout) findViewById(R.id.llDots);

        for ( int i = 0; i < viewPagerAdapter.getCount(); i++ )
        {
            ImageButton imgDot = new ImageButton(this);
            imgDot.setTag(i);
            imgDot.setImageResource(R.drawable.view_pager_tutorial_dot_selector);
            imgDot.setBackgroundResource(0);
            // Here I set last dot to be aplha 0.0f, because I don't want to see last dot on screen
            // And just for any case I have set backgroundColor ===> Transparent
            // Otherwise I will have here 5 dots
            if( i == 4 ) {
                imgDot.setAlpha(0.0f);
                imgDot.setBackgroundColor(Color.TRANSPARENT);
            }
            imgDot.setPadding(5, 5, 5, 5);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(25, 25);
            imgDot.setLayoutParams(params);
            if(i == 0)
                imgDot.setSelected(true);

            llDots.addView(imgDot);
        }


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int pos) {
                Log.e("", "Page Selected is ===> " + pos);
                for (int i = 0; i < viewPagerAdapter.getCount(); i++) {
                    if (i != pos) {
                        llDots.findViewWithTag(i).setSelected(false);
                    }
                }
                llDots.findViewWithTag(pos).setSelected(true);
            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {
                // Here I need to set 3, because we have 4 screen in tutorial
                // And the last screen has position, index = 3
                // If user scroll to end, then the MainActivity.java need to start

                // I have here also this boolean variable so that MainActivity will only be started once
                if (pos == 3 && arg1 > 0.1f && isMainActivityStarted == true) {

                    viewPager.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            //show dialog here
                            if (isMainActivityStarted == true) {

                                int X = (int) event.getX();
                                int Y = (int) event.getY();

                                int eventaction = event.getAction();

                                switch (eventaction) {

                                    case MotionEvent.ACTION_DOWN:
                                        nextActivity();
                                        isMainActivityStarted = false;
                                        break;

                                    case MotionEvent.ACTION_MOVE:
                                        nextActivity();
                                        isMainActivityStarted = false;
                                        break;

                                    case MotionEvent.ACTION_UP:
                                        nextActivity();
                                        isMainActivityStarted = false;
                                        break;
                                }
                            }

                            return true;
                        }
                    });
                }
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {


            }


        });

    }

    @Override
    public void onBackPressed() {

        nextActivity();
    }


    private void nextActivity() {
        Intent nextActivityIntent = null;
        if (startedFromProfile)
            nextActivityIntent = new Intent(this, MainActivity.class);
        else if (Settings.getUserR(this).equals(Settings.DEFAULT_USER_R))
            nextActivityIntent = new Intent(this, GuestActivity.class);
        else
            nextActivityIntent = new Intent(this, MainActivity.class);
        startActivity(nextActivityIntent);
        finish();
        overridePendingTransition(R.anim.end_activity_slide_in_left, R.anim.end_activity_slide_out_right);
    }

    public void showProgressDialog() {
        progressDialog = new CustomProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
