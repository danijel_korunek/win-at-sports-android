package com.winatsports.activities;


import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.custom.CounterTextView;
import com.winatsports.data.Constants;
import com.winatsports.data.MyParlay;
import com.winatsports.data.adapters.MainPagerAdapter;
import com.winatsports.data.adapters.ProfileAdapter;
import com.winatsports.data.adapters.SearchGamesAdapter;
import com.winatsports.data.adapters.SearchLeaguesAdapter;
import com.winatsports.data.adapters.SeparatedFirstPageAdapter;
import com.winatsports.data.adapters.SeparatedLiveScoresAdapter;
import com.winatsports.data.adapters.SeparatedSearchAdapter;
import com.winatsports.data.adapters.SeparatedWagersAdapter;
import com.winatsports.data.adapters.SportsBookAdapter;
import com.winatsports.data.download.DownloadHistoryThread;
import com.winatsports.data.download.DownloadLiveScoresThread;
import com.winatsports.data.firstpage.HomeNotification;
import com.winatsports.data.elements.Game;
import com.winatsports.data.elements.League;
import com.winatsports.data.elements.Sport;
import com.winatsports.data.elements.Team;
import com.winatsports.data.upload.AsyncRemoveNotification;
import com.winatsports.dialogs.CustomProgressDialog;
import com.winatsports.dialogs.RealMoneyWarningDialog;
import com.winatsports.settings.Settings;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.message.BasicNameValuePair;


// all classes begin with UpperCase without any underscore
public class MainActivity extends Activity {

    Context _context;
    private CustomProgressDialog progressDialog;

    private class GCMRegistration extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String...params) {
            try {
                sendToken(params[0]);
            } catch (Exception e) {
                Log.e(GCMRegistration.class.getSimpleName(), "", e);
            }
            try {
                beginPush(params[1]);
            } catch (Exception e) {
                Log.e(GCMRegistration.class.getSimpleName(), "", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String o) {
            super.onPostExecute(o);
        }
    }

    private BroadcastReceiver downloadStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_DOWNLOAD_STATE)) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateDownloadIndicator();
                        tvNumberOfParlayItemsMainActivity.setCounter(MyApplication.getInstance().getParlayArray().size());
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_ARRAYS_SWITCHED)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (MyApplication.getInstance().wasAppInBgMoreThanXSeconds) {
                            dismissProgressDialog();
                            MyApplication.getInstance().wasAppInBgMoreThanXSeconds = false;
                            Util.showRealBettingDialog(MainActivity.this);
                        }
                        refreshViews();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_SHOW_RB_DIALOG)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Util.showRealBettingDialog(MainActivity.this);
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_REDOWNLOAD)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (MyApplication.getInstance().wasAppInBgMoreThanXSeconds && !Settings.getUserR(getApplicationContext()).equals(Settings.DEFAULT_USER_R)) {
                            //MyApplication.getInstance().wasAppInBgMoreThanXSeconds = false;
                            MyApplication.getInstance().downloadData(MainActivity.this, true, true, true);
                        }
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(DownloadLiveScoresThread.ACTION_REFRESH_LIVE_SCORES)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refreshLiveScores();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_REFRESH_WAGERS)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setupWagersView();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_ADD_WAGERS_HISTORY)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setupWagersView();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_LAUNCH_GUEST_ACTIVITY)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent guestActivityIntent = new Intent(MainActivity.this, GuestActivity.class);
                        guestActivityIntent.putExtra(MyApplication.KEY_REENTERED, true);
                        startActivity(guestActivityIntent);
                        overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                        finish();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_NETWORK_FAILURE)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String str = Util.getString(Constants.MSG_KEY_APP_NOINET);
                        if (!Util.isPopulated(str))
                            str = getResources().getString(R.string.error_no_internet);

                        Util.showMessageDialog(MainActivity.this, str, new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                MyApplication.getInstance().quit();
                                MyApplication.getInstance().shutDownVM();
                            }
                        });
                    }
                });
            }
        }
    };

    // all variable required that sliding(between layouts), fling event work well, good
    // and also when I click on one item in Menu, that this work well, good
    public static HorizontalScrollView horizontalScrollMenu;

    private DisplayMetrics displayMetrics;
    int numberOfItems = 6; // I use this integer for number of menu

    private LinearLayout menuFirstScreen, menuSportsBookScreen, menuBetScreen,
                         menuLiveScoreScreen, menuSearchScreen, menuProfilescreen;
    private TextView tvFirstScreen, tvSportsBookScreen, tvBetScreen,
                     tvLiveScoreScreen, tvSearchScreen, tvProfilescreen;
    private ImageView ivFirstScreen, ivSportsBookScreen, ivBetScreen,
            ivLiveScoreScreen, ivSearchScreen, ivProfilescreen;

    private CounterTextView tvNumberOfParlayItemsMainActivity;

    private final int SEARCH_VIEW_CHAR_LIMIT = 3;
    private boolean rbShownAtStart;

    TextView tvMainActivityWallet;

    float dpHeight = -1.0f;
    float dpWidth = -1.0f;
    // and also when I click on one item in Menu, that this will work well
    // all variable required that sliding(between layouts), fling event will work well

    // almost all listView required for filling up ListView-s, only without this listView ===>> lvFirstPage
    ListView lvSportsBook, lvLiveScores, lvAccount, lvSearch;

    // all variable I need to show data ==> ( games, leagues ) on home screen
    ListView lvFirstPage;

    // variable to show data on SPORTSBOOK VIEW ------------------------------------------------------- //
    TextView tvSportsBookTitle;
    // variable to show data on SPORTSBOOK VIEW ------------------------------------------------------- //

    /* ---------- ----------------  variable for showing WAGERS VIEW -------------------------------------------------- ------ */
    ListView lvWagersInPlay;
    /* ---------- ----------------  variable for showing WAGERS VIEW -------------------------------------------------- ------ */

    /* ------ ---- variable for showing SEARCH VIEW ---------------------- ------------------------ ------------------- */
    SearchView searchView;
    TextView tvSearchNoResultsFound;
    SeparatedSearchAdapter separatedSearchAdapter;
    /* ------ ---- variable for showing SEARCH VIEW ---------------------- ------------------------ ------------------- */

    /* ------ ---- variable for showing ACCOUNT VIEW ---------------------- ------------------------ ------------------- */
    TextView tvProfileTitle;
    /* ------ ---- variable for showing ACCOUNT VIEW ---------------------- ------------------------ ------------------- */

    private FrameLayout flBetSlip;

    private ViewPager vpPager;

    private DownloadHistoryThread historyThread;

    private int wagersHistoryPosition;
    private boolean rbStarted;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_NO_TITLE); // command to hide title, that is usually created at the top of the application
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

        _context = this;
        historyThread = null;
        wagersHistoryPosition = 0;

        displayMetrics = this.getResources().getDisplayMetrics();

        dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        dpWidth = displayMetrics.widthPixels; // / displayMetrics.density;

        new GCMRegistration().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Settings.getPushToken(this), Util.isNotificationEnabled(getApplicationContext()) ? "7" : "0");

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // for galaxy tab

        setUpMainActivityScreenLayout();
        rbShownAtStart = false;
    }

    public void setUpMainActivityScreenLayout( ) {

        setContentView(R.layout.activity_main);

        vpPager = (ViewPager) findViewById(R.id.vpPager);
        MainPagerAdapter mpa = new MainPagerAdapter(this);
        vpPager.setAdapter(mpa);

        flBetSlip = (FrameLayout) findViewById(R.id.flBetSlip);
        flBetSlip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( MyApplication.getInstance().getParlayArray().size() > 0 ) {

                    Intent intent = new Intent(MainActivity.this, BetSlipActivity.class);

                    finish();
                    startActivity(intent);
                    overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                }
                else {

                    Util.showMessageDialog(_context, MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_BETSLIPISEMPTY), new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            dialog.dismiss();
                        }
                    });
                }
            }
        });

        Log.d(MainActivity.class.getSimpleName(), "for guests on? : " + Settings.isRealBettingGuestsOn(this) + " runs: " + Settings.getTimesRun(this) + " rbGuest runs: " + Settings.getRealBettingNumOfGuestRuns(this));
        horizontalScrollMenu = (HorizontalScrollView) findViewById(R.id.horizontal_Menu);
        //horizontalScrollMenu.setVerticalScrollBarEnabled(false); // command to hide scrollbar of HorizontalScrollView
        horizontalScrollMenu.setHorizontalScrollBarEnabled(false);  // command to hide scrollbar of HorizontalScrollView

        tvNumberOfParlayItemsMainActivity = (CounterTextView) findViewById(R.id.tvNumberOfParlayItemsMainActivity);
        tvNumberOfParlayItemsMainActivity.setCounter(MyApplication.getInstance().getParlayArray().size());

        menuFirstScreen = (LinearLayout) findViewById(R.id.menuFirstScreen);
        menuSportsBookScreen = (LinearLayout) findViewById(R.id.menuSportsBookScreen);
        menuBetScreen = (LinearLayout) findViewById(R.id.menuBetScreen);
        menuLiveScoreScreen = (LinearLayout) findViewById(R.id.menuLiveScoreScreen);
        menuSearchScreen = (LinearLayout) findViewById(R.id.menuSearchScreen);
        menuProfilescreen = (LinearLayout) findViewById(R.id.menuProfilescreen);

        ivFirstScreen = (ImageView) findViewById(R.id.ivHomeScreen);
        ivSportsBookScreen = (ImageView) findViewById(R.id.ivSportsbookScreen);
        ivBetScreen = (ImageView) findViewById(R.id.ivBetScreen);
        ivLiveScoreScreen = (ImageView) findViewById(R.id.ivLiveScoresScreen);
        ivSearchScreen = (ImageView) findViewById(R.id.ivSearchScreen);
        ivProfilescreen = (ImageView) findViewById(R.id.ivProfileScreen);

        tvFirstScreen = (TextView) findViewById(R.id.tvFirstScreen);
        tvSportsBookScreen = (TextView) findViewById(R.id.tvSportsBookScreen);
        tvBetScreen = (TextView) findViewById(R.id.tvBetScreen);
        tvLiveScoreScreen = (TextView) findViewById(R.id.tvLiveScoreScreen);
        tvSearchScreen = (TextView) findViewById(R.id.tvSearchScreen);
        tvProfilescreen = (TextView) findViewById(R.id.tvProfilescreen);

        setMenuTitle(); // here we will set up title for every menu
        defaultScreen();  // here we will set up background color and text color for menu

        vpPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                scrollToTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        menuFirstScreen.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                scrollToTab(Constants.C_VIEW_HOME_SCREEN);
            }
        });

        menuSportsBookScreen.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                scrollToTab(Constants.C_VIEW_SPORTSBOOK_SCREEN);
            }
        });

        menuBetScreen.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                scrollToTab(Constants.C_VIEW_BET_SCREEN);
            }
        });

        menuLiveScoreScreen.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                scrollToTab(Constants.C_VIEW_LIVESCORE_SCREEN);
            }
        });

        menuSearchScreen.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                scrollToTab(Constants.C_VIEW_SEARCH_SCREEN);
            }
        });

        menuProfilescreen.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                scrollToTab(Constants.C_VIEW_PROFILE_SCREEN);
            }
        });

        tvMainActivityWallet = (TextView) findViewById(R.id.tvMainActivityWallet);
        tvMainActivityWallet.setText(Util.generateWalletTitle());

        Util.showAnimationForWalletTitle(tvMainActivityWallet);


    }

    private void updateMenuTitles() {
        setMenuTitle();
        tvMainActivityWallet.setText(Util.generateWalletTitle());
    }


    public void scrollToTab(int index) {
        if (index < Constants.C_VIEW_HOME_SCREEN && index > Constants.C_VIEW_PROFILE_SCREEN)
            return;
        if (index == Constants.C_VIEW_LIVESCORE_SCREEN) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
        changeBackgroundColorMenu(index);

        vpPager.setCurrentItem(index, true);
        MyApplication.getInstance().setLastActiveTab(index);

        int leftPixelX = 0;

        if (index > Constants.C_VIEW_HOME_SCREEN)
            leftPixelX = determineMenuScrollX(displayMetrics.widthPixels, numberOfItems, index);
        if (leftPixelX >= 0)
            horizontalScrollMenu.smoothScrollTo(leftPixelX, 0);

        switch (index) {
            case Constants.C_VIEW_HOME_SCREEN:
                setupHomeView();
                break;
            case Constants.C_VIEW_SPORTSBOOK_SCREEN:
                setupSportsView();
                break;
            case Constants.C_VIEW_BET_SCREEN:
                setupWagersView();
                break;
            case Constants.C_VIEW_LIVESCORE_SCREEN:
                refreshLiveScores();
                break;
            case Constants.C_VIEW_SEARCH_SCREEN:
                setUpSearchView();
                break;
            default:
                setupProfileView();
                break;
        }

        if (searchView != null)
            searchView.clearFocus();
    }

    public void setupHomeView() {
        lvFirstPage = (ListView) vpPager.findViewById(R.id.lvFirstPage);

        if (lvFirstPage != null) {
            lvFirstPage.setAdapter(new SeparatedFirstPageAdapter(this));
            lvFirstPage.requestLayout();

            lvFirstPage.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Object item = lvFirstPage.getAdapter().getItem(position);

                    if (item instanceof Game) {

                        Intent intent = new Intent(MainActivity.this, GameOddsActivity.class);
                        Bundle oddsFromGame = new Bundle();

                        oddsFromGame.putBoolean("ACTIVITY_STARTED_FROM_MAIN_ACTIVITY", true);
                        oddsFromGame.putInt("EVENT_ID", ((Game) item).getEventID());

                        intent.putExtras(oddsFromGame); //Put your id to your next Intent
                        finish();
                        startActivity(intent);
                        overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                    } else if (item instanceof League) {

                        League finalLeague = (League) item;

                        Intent intent = new Intent(MainActivity.this, GamesActivity.class);
                        Bundle oddsFromGame = new Bundle();
                        oddsFromGame.putBoolean("ACTIVITY_STARTED_FROM_MAIN_ACTIVITY", true);
                        oddsFromGame.putInt("SPORT_ID", finalLeague.getSportID());
                        oddsFromGame.putInt("LEAGUE_ID", finalLeague.getLeagueID());

                        intent.putExtras(oddsFromGame); //Put your id to your next Intent
                        finish();
                        startActivity(intent);
                        overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                    } else if (item instanceof HomeNotification) {
                        HomeNotification notification = (HomeNotification) item;
                        if (notification.isDismissible()) {
                            try {
                                MyApplication.getInstance().getUserData().getHomeNotifications().remove(position - 1);
                                if (notification.getNoteID() != -1) {
                                    new AsyncRemoveNotification(notification).execute();
                                }

                                if (notification.isMarchMadness()) {
                                    // TODO: implement March Madness handling here - business contract not signed
                                    //Log.d(MainActivity.class.getSimpleName(), "Notification to open March Madness real betting not handled completely!");
                                }
                                setupHomeView();
                            } catch (Exception e) {
                                Log.e(MainActivity.class.getSimpleName(), "", e);
                            }
                        }

                        if (notification.isOpensRealBetting()) {
                            // for guests - show a warning dialog
                            String userR = Settings.getUserR(getApplicationContext());
                            if (Util.isPopulated(userR) && !userR.equalsIgnoreCase(Settings.DEFAULT_USER_R)) {
                                if (Util.isRealBettingEnabled(MyApplication.getInstance()))
                                    MyApplication.getInstance().startRealBetting(false, MainActivity.this, 0);
                            } else {
                                RealMoneyWarningDialog realMoney = new RealMoneyWarningDialog(MainActivity.this, null);
                                realMoney.show();
                            }
                        }

                        if (Util.isPopulated(notification.getUrl())) {
                            Util.showUrlInBrowser(notification.getUrl());
                        }
                    } else
                        Log.d(MainActivity.class.getSimpleName(), "Something went wrong on first page(Under FEATURED GAMES, or FEATURED LEAGUES or UP NEXT)");

                }
            });
        }
    }

    public void setupSportsView() {
        lvSportsBook = (ListView) vpPager.findViewById(R.id.lvSportsBook);
        if (lvSportsBook != null) {
            tvSportsBookTitle = (TextView) vpPager.findViewById(R.id.tvSportsBookTitle);

            tvSportsBookTitle.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_TOOL_SPORTS));

            lvSportsBook.setAdapter(new SportsBookAdapter(this));
            lvSportsBook.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Intent intent = new Intent(_context, LeaguesActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("SPORT_ID", ((Sport) lvSportsBook.getAdapter().getItem(position)).getSportID());
                    intent.putExtras(bundle);
                    //intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    finish();
                    _context.startActivity(intent);

                    ((Activity) _context).overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                }
            });
        }
    }

    public void setupWagersView() {
        lvWagersInPlay = (ListView) vpPager.findViewById(R.id.lvWagersInPlay);

        if (lvWagersInPlay != null) {

            lvWagersInPlay.setAdapter(new SeparatedWagersAdapter(this));
            lvWagersInPlay.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Object item = lvWagersInPlay.getAdapter().getItem(position);
                    if (item != null) {
                        MyParlay myParlay = (MyParlay) item;
                        MyApplication.getInstance().setParlayToDisplay(myParlay);

                        if (myParlay.getMyBets().size() > 1) {
                            Intent intent = new Intent(MainActivity.this, WagersDetailsActivity.class);
                            finish();
                            startActivity(intent);
                            overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                        } else {
                            // nothing
                        }
                    } else {
                        // nothing
                    }
                }
            });


            lvWagersInPlay.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    if (firstVisibleItem + visibleItemCount == totalItemCount) {
                        if (historyThread == null || (historyThread != null && !historyThread.running)) {
                            wagersHistoryPosition = firstVisibleItem;
                            historyThread = new DownloadHistoryThread();
                            historyThread.start();
                        }
                    } else {
                        if (historyThread != null && !historyThread.running)
                            historyThread = null;
                    }
                }
            });

            if (wagersHistoryPosition > 0 && wagersHistoryPosition < MyApplication.getInstance().getHistoryWagers().size()) {
                lvWagersInPlay.setSelection(wagersHistoryPosition);
            }
            lvWagersInPlay.setEnabled(true);
        }
    }

    public void setupProfileView() {
        lvAccount = (ListView) vpPager.findViewById(R.id.lvAccount);
        if( lvAccount != null ) {
            tvProfileTitle = (TextView) vpPager.findViewById(R.id.tvProfileTitle);
            if (tvProfileTitle != null)
                tvProfileTitle.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_TOOL_PROFILE));

            // THAT ARE ALL VARIABLE  TO SAVE TEXT FROM MESSAGES.XML FILE
            ArrayList<String> profileTextOnTop = new ArrayList<String>();
            if (Settings.getUserR(MainActivity.this).equals(Settings.DEFAULT_USER_R))
                profileTextOnTop.add( MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PROFILESIGNUPTEXT1));
            else
                profileTextOnTop.add( MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PROFILELOGOUTTEXT1));
            profileTextOnTop.add(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_STATS_TITLE));
            profileTextOnTop.add(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PROFILESETTINGSTEXT1));
            profileTextOnTop.add(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PROFILETUTORIALTEXT1));
            profileTextOnTop.add(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PROFILECONTACTUSTEXT1));

            ArrayList<String> profileTextOnBottom = new ArrayList<String>();
            if (Settings.getUserR(MainActivity.this).equals(Settings.DEFAULT_USER_R))
                profileTextOnBottom.add(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PROFILESIGNUPTEXT2));
            else
                profileTextOnBottom.add(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PROFILELOGOUTTEXT2));
            profileTextOnBottom.add(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_STATS_TITLE_DESC));
            profileTextOnBottom.add(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PROFILESETTINGSTEXT2));
            profileTextOnBottom.add(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PROFILETUTORIALTEXT2));
            profileTextOnBottom.add(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PROFILECONTACTUSTEXT2));

            ArrayList<Integer> profilePicture = new ArrayList<Integer>();
            if (Settings.getUserR(MainActivity.this).equals(Settings.DEFAULT_USER_R))
                profilePicture.add(R.drawable.glpyh_sign_in);
            else
                profilePicture.add(R.drawable.glyph_logout);
            profilePicture.add(R.drawable.glyph_stats);
            profilePicture.add(R.drawable.glyph_settings);
            profilePicture.add(R.drawable.glyph_tutorial);
            profilePicture.add(R.drawable.glyph_contact_us);

            ProfileAdapter custom_sportsBookAdapter = new ProfileAdapter( _context , profilePicture, profileTextOnTop, profileTextOnBottom);
            lvAccount.setAdapter(custom_sportsBookAdapter);
            lvAccount.setOnItemClickListener( new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    // I think here I will not need Bundle(), because not to one activity from profile, I don't need to pass anything
                    Intent intent= null;
                    //Bundle bundle  = new Bundle();
                    if( position == 0 ) {
                        lvAccount.setOnItemClickListener(null);
                        if (Settings.getUserR(MainActivity.this).equals(Settings.DEFAULT_USER_R))
                            intent = new Intent( MainActivity.this, GuestActivity.class );
                        else {
                            Util.showLogoutDialog(MainActivity.this);
                        }
                    }
                    else if( position == 1 ) {
                        lvAccount.setOnItemClickListener(null);
                        intent = new Intent(MainActivity.this, StatisticsActivity.class);
                        intent.putExtra(StatisticsActivity.KEY_REDOWNLOAD, true);
                    }
                    else if( position == 2 ) {
                        lvAccount.setOnItemClickListener(null);
                        intent = new Intent(MainActivity.this, SettingsActivity.class);
                    } else if( position == 3 ) {
                        lvAccount.setOnItemClickListener(null);
                        intent = new Intent(MainActivity.this, TutorialActivity.class);
                        intent.putExtra(TutorialActivity.KEY_FROM_PROFILE, true);
                    } else if (position == 4) {
                        try {
                            intent = new Intent(Intent.ACTION_SEND);
                            intent.setType("application/octet-stream");
                            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{Util.getString("profileEmailAddres")});
                            intent.putExtra(Intent.EXTRA_SUBJECT, Util.getString("profileEmailTitle"));
                            intent.putExtra(Intent.EXTRA_TEXT, Util.getString("profileEmailBody"));
                            startActivity(Intent.createChooser(intent, Util.getString("ProfileContEmail")));
                            return;
                        } catch (Exception e) {
                            Log.e(MainActivity.class.getSimpleName(), "Error sending email", e);
                            Util.showMessageDialog(MainActivity.this, Util.getString("profileEmailError"), null);
                            return;
                        }
                    }

                    //intent.putExtras(bundle);
                    //intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    if( intent != null ) {
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                    }
                }
            });
        }

    }

    private void setUpSearchView() {
        searchView = (SearchView) vpPager.findViewById(R.id.searchView);

        if (searchView != null) {
            tvSearchNoResultsFound = (TextView) vpPager.findViewById(R.id.tvSearchNoResultsFound);

            searchView.setQueryHint(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SEARCHPLACEHOLDER));

            tvSearchNoResultsFound.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SEARCHNORESULTS));

            searchView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (searchView.getQuery().toString().length() >= SEARCH_VIEW_CHAR_LIMIT) {
                        separatedSearchAdapter = generateSearchAdapter(searchView.getQuery().toString());
                        if (separatedSearchAdapter != null) {
                            lvSearch.setVisibility(View.VISIBLE);
                            tvSearchNoResultsFound.setVisibility(View.GONE);
                            lvSearch.setAdapter(separatedSearchAdapter);
                        } else {
                            lvSearch.setVisibility(View.GONE);
                            tvSearchNoResultsFound.setVisibility(View.VISIBLE);
                        }
                    } else {
                        lvSearch.setVisibility(View.GONE);
                        tvSearchNoResultsFound.setVisibility(View.VISIBLE);
                    }
                }
            });

            SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
                public boolean onQueryTextChange(String newText) {

                    //if (newText.length() >= SEARCH_VIEW_CHAR_LIMIT && displayAllGamesAndLeaguesThatMatchedSearchedText() == true) {
                    if (newText.length() >= SEARCH_VIEW_CHAR_LIMIT) {
                        separatedSearchAdapter = generateSearchAdapter(newText);
                        if (separatedSearchAdapter != null) {
                            lvSearch.setVisibility(View.VISIBLE);
                            tvSearchNoResultsFound.setVisibility(View.GONE);
                            lvSearch.setAdapter(separatedSearchAdapter);
                        } else {
                            lvSearch.setVisibility(View.GONE);
                            tvSearchNoResultsFound.setVisibility(View.VISIBLE);
                        }
                    } else {
                        lvSearch.setVisibility(View.GONE);
                        tvSearchNoResultsFound.setVisibility(View.VISIBLE);
                    }

                    MyApplication.getInstance().setSearchString(newText);

                    return true;
                }

                public boolean onQueryTextSubmit(String query) {
                    //Here u can get the value "query" which is entered in the search box.

                    return false;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
            lvSearch = (ListView) vpPager.findViewById(R.id.lvSearch);
            lvSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (separatedSearchAdapter == null)
                        return;

                    Object item = separatedSearchAdapter.getItem(position);
                    if (item == null)
                        return;

                    if (item instanceof Game) {

                        Game game = (Game) item;
                        if (game.getOddArrayList().size() > 0) {

                            Intent intent = new Intent(MainActivity.this, GameOddsActivity.class);
                            Bundle oddsFromGame = new Bundle();
                            oddsFromGame.putBoolean("ACTIVITY_STARTED_FROM_MAIN_ACTIVITY", true);
                            oddsFromGame.putInt("EVENT_ID", ((Game) item).getEventID());

                            intent.putExtras(oddsFromGame); //Put your id to your next Intent
                            finish();
                            startActivity(intent);
                            overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                        } else {

                            Util.showMessageDialog(_context, MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_NO_ODDS), new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    dialog.dismiss();
                                }
                            });
                        }

                    } else if (item instanceof League) {

                        League finalLeague = (League) item;

                        if (finalLeague.getGameArrayList().size() > 0) {
                            Intent intent = new Intent(MainActivity.this, GamesActivity.class);
                            Bundle oddsFromGame = new Bundle();
                            oddsFromGame.putBoolean("ACTIVITY_STARTED_FROM_MAIN_ACTIVITY", true);
                            oddsFromGame.putInt("SPORT_ID", finalLeague.getSportID());
                            oddsFromGame.putInt("LEAGUE_ID", finalLeague.getLeagueID());

                            intent.putExtras(oddsFromGame); //Put your id to your next Intent
                            finish();
                            startActivity(intent);
                            overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                        } else {

                            Util.showMessageDialog(_context, MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_NO_GAMES_LEAGUE), new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    dialog.dismiss();
                                }
                            });
                        }
                    }

                }
            });

            String searchString = MyApplication.getInstance().getSearchString();
            searchView.setQuery(Util.isPopulated(searchString) ? searchString : "", true);
        }
    }

    private SeparatedSearchAdapter generateSearchAdapter(String searchText) {
        ArrayList<League> leagues = new ArrayList<>();
        ArrayList<Game> games = new ArrayList<>();

        for (Sport s : MyApplication.getInstance().getAllSports()) {
            for (League league : s.getLeagues()) {
                if (league.getLeagueName().toLowerCase().contains(searchText.toLowerCase()))
                    leagues.add(league);

                for (Game g : league.getGameArrayList()) {
                    for (Team t : g.getTeamArrayList()) {
                        if (Util.isPopulated(t.getName()) && t.getName().toLowerCase().contains(searchText.toLowerCase())) {
                            games.add(g);
                        }
                    }
                }
            }
        }

        String gamesTitleHeader =  MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SEARCHTEAMS);
        String leaguesTitleHeader = MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SEARCHLANGUAGES);
        SeparatedSearchAdapter searchAdapter = new SeparatedSearchAdapter(this);
        if (!leagues.isEmpty()) {
            Collections.sort(leagues, new Comparator<League>() {
                @Override
                public int compare(League leftItem, League rightItem) {

                    int firstLeague = leftItem.getSportID();
                    int secondLeague = rightItem.getSportID();
                    return firstLeague < secondLeague ? 1 : (firstLeague == secondLeague ? 0 : -1);
                }
            });

            SearchLeaguesAdapter leaguesAdapter = new SearchLeaguesAdapter(this, leagues);
            searchAdapter.addSection(leaguesTitleHeader, leaguesAdapter);
        }

        if (!games.isEmpty()) {
            SearchGamesAdapter gamesAdapter = new SearchGamesAdapter(this, games);
            searchAdapter.addSection(gamesTitleHeader, gamesAdapter);
        }

        if (searchAdapter.isEmpty())
            return null;
        return searchAdapter;
    }

    private void setMenuTitle() {
        tvFirstScreen.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_TOOL_HOME));
        tvSportsBookScreen.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_TOOL_SPORTS));
        tvBetScreen.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_TOOL_WAGERS));
        tvLiveScoreScreen.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_TOOL_LIVE));
        tvSearchScreen.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_TOOL_SEARCH));
        tvProfilescreen.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_TOOL_PROFILE));
    }

    public void centerMenuItem(int menuItemIndex) {
        if (menuItemIndex == Constants.C_VIEW_LIVESCORE_SCREEN) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
        int leftPixelX = 0;

        if (menuItemIndex > Constants.C_VIEW_HOME_SCREEN)
            leftPixelX = determineMenuScrollX(displayMetrics.widthPixels, numberOfItems, menuItemIndex);

        if (leftPixelX >= 0)
            horizontalScrollMenu.scrollTo(leftPixelX, 0);
    }


    public int determineMenuScrollX(int screenWidth, int numberOfItems, int itemToBeSelected) throws IllegalArgumentException {

        int oneItemWidth = menuFirstScreen.getMeasuredWidth(); // getWidth() returns size in pixel
        int horizonalScrollViewWidth = numberOfItems * oneItemWidth; //

        if (screenWidth < oneItemWidth)
            throw new IllegalArgumentException(" screenWidth < oneItemWidth ");

        if (itemToBeSelected >= numberOfItems)
            throw new IllegalArgumentException(" itemToBeSelected >= numberOfItems ");

        //float numberOfItemsWhichFitScreen = (float) screenWidth / (float)oneItemWidth;
        //float offset = (float)( screenWidth  / (numberOfItemsWhichFitScreen - 1.0) );

        float centredPixelX = ((itemToBeSelected * oneItemWidth) + ((itemToBeSelected + 1) * oneItemWidth)) / 2;
        float offset = screenWidth / 2;

        if ((centredPixelX + offset) > horizonalScrollViewWidth)
            return horizonalScrollViewWidth - screenWidth;

        int returnValue = (int) (centredPixelX - offset);
        if (returnValue < 0)
            returnValue = 0;

        return returnValue;
    }


    public void changeBackgroundColorMenu(int index) {

        if (index == Constants.C_VIEW_HOME_SCREEN ) {
            // set text color of textfield
            tvFirstScreen.setTextColor(getResources().getColor(R.color.tint_color));
            tvSportsBookScreen.setTextColor(getResources().getColor(R.color.white));
            tvBetScreen.setTextColor(getResources().getColor(R.color.white));
            tvLiveScoreScreen.setTextColor(getResources().getColor(R.color.white));
            tvSearchScreen.setTextColor(getResources().getColor(R.color.white));
            tvProfilescreen.setTextColor(getResources().getColor(R.color.white));

            ivFirstScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_home));
            ivSportsBookScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_sportsbook_white));
            ivBetScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_wagers_white));
            ivLiveScoreScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_livescores_white));
            ivSearchScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_search_white));
            ivProfilescreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_profile_white));

            // set background color of linear layouts
            menuFirstScreen.setBackgroundColor(getResources().getColor(R.color.white));
            menuSportsBookScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuBetScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuLiveScoreScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuSearchScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuProfilescreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
        } else if (index == Constants.C_VIEW_SPORTSBOOK_SCREEN) {
            tvFirstScreen.setTextColor(getResources().getColor(R.color.white));
            tvSportsBookScreen.setTextColor(getResources().getColor(R.color.tint_color));
            tvBetScreen.setTextColor(getResources().getColor(R.color.white));
            tvLiveScoreScreen.setTextColor(getResources().getColor(R.color.white));
            tvSearchScreen.setTextColor(getResources().getColor(R.color.white));
            tvProfilescreen.setTextColor(getResources().getColor(R.color.white));

            ivFirstScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_home_white));
            ivSportsBookScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_sportsbook));
            ivBetScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_wagers_white));
            ivLiveScoreScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_livescores_white));
            ivSearchScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_search_white));
            ivProfilescreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_profile_white));

            menuFirstScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuSportsBookScreen.setBackgroundColor(getResources().getColor(R.color.white));
            menuBetScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuLiveScoreScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuSearchScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuProfilescreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
        } else if (index == Constants.C_VIEW_BET_SCREEN) {
            tvFirstScreen.setTextColor(getResources().getColor(R.color.white));
            tvSportsBookScreen.setTextColor(getResources().getColor(R.color.white));
            tvBetScreen.setTextColor(getResources().getColor(R.color.tint_color));
            tvLiveScoreScreen.setTextColor(getResources().getColor(R.color.white));
            tvSearchScreen.setTextColor(getResources().getColor(R.color.white));
            tvProfilescreen.setTextColor(getResources().getColor(R.color.white));

            ivFirstScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_home_white));
            ivSportsBookScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_sportsbook_white));
            ivBetScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_wagers));
            ivLiveScoreScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_livescores_white));
            ivSearchScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_search_white));
            ivProfilescreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_profile_white));

            menuFirstScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuSportsBookScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuBetScreen.setBackgroundColor(getResources().getColor(R.color.white));
            menuLiveScoreScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuSearchScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuProfilescreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
        } else if (index == Constants.C_VIEW_LIVESCORE_SCREEN) {

            tvFirstScreen.setTextColor(getResources().getColor(R.color.white));
            tvSportsBookScreen.setTextColor(getResources().getColor(R.color.white));
            tvBetScreen.setTextColor(getResources().getColor(R.color.white));
            tvLiveScoreScreen.setTextColor(getResources().getColor(R.color.tint_color));
            tvSearchScreen.setTextColor(getResources().getColor(R.color.white));
            tvProfilescreen.setTextColor(getResources().getColor(R.color.white));

            ivFirstScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_home_white));
            ivSportsBookScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_sportsbook_white));
            ivBetScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_wagers_white));
            ivLiveScoreScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_livescores));
            ivSearchScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_search_white));
            ivProfilescreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_profile_white));

            menuFirstScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuSportsBookScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuBetScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuLiveScoreScreen.setBackgroundColor(getResources().getColor(R.color.white));
            menuSearchScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuProfilescreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
        } else if (index == Constants.C_VIEW_SEARCH_SCREEN) {
            tvFirstScreen.setTextColor(getResources().getColor(R.color.white));
            tvSportsBookScreen.setTextColor(getResources().getColor(R.color.white));
            tvBetScreen.setTextColor(getResources().getColor(R.color.white));
            tvLiveScoreScreen.setTextColor(getResources().getColor(R.color.white));
            tvSearchScreen.setTextColor(getResources().getColor(R.color.tint_color));
            tvProfilescreen.setTextColor(getResources().getColor(R.color.white));

            ivFirstScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_home_white));
            ivSportsBookScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_sportsbook_white));
            ivBetScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_wagers_white));
            ivLiveScoreScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_livescores_white));
            ivSearchScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_search));
            ivProfilescreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_profile_white));

            menuFirstScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuSportsBookScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuBetScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuLiveScoreScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuSearchScreen.setBackgroundColor(getResources().getColor(R.color.white));
            menuProfilescreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
        } else if (index == Constants.C_VIEW_PROFILE_SCREEN) {
            tvFirstScreen.setTextColor(getResources().getColor(R.color.white));
            tvSportsBookScreen.setTextColor(getResources().getColor(R.color.white));
            tvBetScreen.setTextColor(getResources().getColor(R.color.white));
            tvLiveScoreScreen.setTextColor(getResources().getColor(R.color.white));
            tvSearchScreen.setTextColor(getResources().getColor(R.color.white));
            tvProfilescreen.setTextColor(getResources().getColor(R.color.tint_color));

            ivFirstScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_home_white));
            ivSportsBookScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_sportsbook_white));
            ivBetScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_wagers_white));
            ivLiveScoreScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_livescores_white));
            ivSearchScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_search_white));
            ivProfilescreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_profile));

            menuFirstScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuSportsBookScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuBetScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuLiveScoreScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuSearchScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
            menuProfilescreen.setBackgroundColor(getResources().getColor(R.color.white));
        }

    }


    private void defaultScreen() {

        tvFirstScreen.setTextColor(getResources().getColor(R.color.tint_color));
        tvSportsBookScreen.setTextColor(getResources().getColor(R.color.white));
        tvBetScreen.setTextColor(getResources().getColor(R.color.white));
        tvLiveScoreScreen.setTextColor(getResources().getColor(R.color.white));
        tvSearchScreen.setTextColor(getResources().getColor(R.color.white));
        tvProfilescreen.setTextColor(getResources().getColor(R.color.white));

        ivFirstScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_home));
        ivSportsBookScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_sportsbook_white));
        ivBetScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_wagers_white));
        ivLiveScoreScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_livescores_white));
        ivSearchScreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_search_white));
        ivProfilescreen.setImageDrawable(getResources().getDrawable(R.drawable.menu_profile_white));

        // set background color of linear layouts
        menuFirstScreen.setBackgroundColor(getResources().getColor(R.color.white));
        menuSportsBookScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
        menuBetScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
        menuLiveScoreScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
        menuSearchScreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
        menuProfilescreen.setBackgroundColor(getResources().getColor(R.color.tint_color));
    }


    @Override
    protected void onResume() {
        super.onResume();

        rbStarted = false;

        // reset the activity stack
        MyApplication.getInstance().getActivityStack().clear();
        MyApplication.getInstance().getActivityStack().push(MainActivity.class.getName());

        tvNumberOfParlayItemsMainActivity.setCounter(MyApplication.getInstance().getParlayArray().size());

        horizontalScrollMenu.postDelayed(new Runnable() {
            @Override
            public void run() {
                centerMenuItem(MyApplication.getInstance().getLastActiveTab());
                changeBackgroundColorMenu(MyApplication.getInstance().getLastActiveTab());
                scrollToTab(MyApplication.getInstance().getLastActiveTab());
            }
        }, 0);

        Log.d(MainActivity.class.getSimpleName(), "last active tab = " + MyApplication.getInstance().getLastActiveTab());

        IntentFilter filter = new IntentFilter();
        filter.addAction(MyApplication.ACTION_DOWNLOAD_STATE);
        filter.addAction(MyApplication.ACTION_ARRAYS_SWITCHED);
        filter.addAction(DownloadLiveScoresThread.ACTION_REFRESH_LIVE_SCORES);
        filter.addAction(MyApplication.ACTION_REFRESH_WAGERS);
        filter.addAction(MyApplication.ACTION_SHOW_RB_DIALOG);
        filter.addAction(MyApplication.ACTION_ADD_WAGERS_HISTORY);
        filter.addAction(MyApplication.ACTION_LAUNCH_GUEST_ACTIVITY);
        filter.addAction(MyApplication.ACTION_NETWORK_FAILURE);
        filter.addAction(MyApplication.ACTION_REDOWNLOAD);
        registerReceiver(downloadStateReceiver, filter);

        if (lvLiveScores != null)
            refreshLiveScores();

        if (!MyApplication.getInstance().isRbShownAtStart()) {
            MyApplication.getInstance().setRbShownAtStart(true);
            Util.showRealBettingDialog(this);
        } else if (getIntent() != null && getIntent().getBooleanExtra(MyApplication.KEY_REENTERED, false)
                && MyApplication.getInstance().wasAppInBgMoreThanXSeconds) {
            MyApplication.getInstance().downloadData(this, true, true, true);
        }

        Log.d(MainActivity.class.getSimpleName(), "onresume end");
    }

    @Override
    protected void onPause() {
        super.onPause();

        dismissProgressDialog();


        if (searchView != null) {
            MyApplication.getInstance().setSearchString(searchView.getQuery().toString());
        }

        MyApplication.getInstance().setTimeMyActivityPaused(System.currentTimeMillis());
        try {
            unregisterReceiver(downloadStateReceiver);
        } catch (Exception ignored) {}

        if (MyApplication.getInstance().isQuitting()) {
            MyApplication.getInstance().quit();
        }


        Log.d(MainActivity.class.getSimpleName(), "onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (MyApplication.getInstance().isQuitting()) {
            System.exit(0);
        }
    }

    private void updateDownloadIndicator() {
        ProgressBar pbDownload = (ProgressBar) findViewById(R.id.pbDownload);
        if (MyApplication.getInstance().isDownloading())
            pbDownload.setVisibility(View.VISIBLE);
        else
            pbDownload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onBackPressed() {
        Util.showQuitDialog(this);
    }

    public void refreshViews() {

        switch (vpPager.getCurrentItem()) {
            case Constants.C_VIEW_HOME_SCREEN:
                setupHomeView();
                break;
            case Constants.C_VIEW_SPORTSBOOK_SCREEN:
                setupSportsView();
                break;
            case Constants.C_VIEW_BET_SCREEN:
                setupWagersView();
                break;
            case Constants.C_VIEW_LIVESCORE_SCREEN:
                refreshLiveScores();
                break;
            case Constants.C_VIEW_SEARCH_SCREEN:
                setUpSearchView();
                break;
            default:
                setupProfileView();
                break;
        }

        updateMenuTitles();
    }


    private void refreshLiveScores() {
        if (vpPager.getCurrentItem() == Constants.C_VIEW_LIVESCORE_SCREEN) {
            lvLiveScores = (ListView) vpPager.findViewById(R.id.lvLiveScores);
            if (lvLiveScores != null) {
                final int savedPosition = lvLiveScores.getFirstVisiblePosition();
                lvLiveScores.setAdapter(null);
                lvLiveScores.setAdapter(new SeparatedLiveScoresAdapter(this));
                lvLiveScores.requestLayout();

                lvLiveScores.post(new Runnable() {
                    @Override
                    public void run() {
                        //lvLiveScores.smoothScrollToPositionFromTop(savedPosition, 0);
                        lvLiveScores.setSelection(savedPosition);
                    }
                });
            }
        }
    }

    public void showProgressDialog() {
        progressDialog = new CustomProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private void sendToken(String token) throws Exception {

        URL obj = new URL(Constants.URL_USER_COMMON);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setReadTimeout(10000);
        con.setConnectTimeout(15000);

        con.setRequestMethod("POST");

        con.setRequestProperty("http.agent", "Unknown");

        con.setDoOutput(true);
        con.setDoInput(true);

        List<NameValuePair> params =new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("key", Constants.USER_COMMON_REQUEST_HEADER_KEY));
        params.add(new BasicNameValuePair("p", "token"));
        //params.add(new BasicNameValuePair("app_id", "1"));
        params.add(new BasicNameValuePair("device_id", Settings.getDeviceId(this)));
        params.add(new BasicNameValuePair("token", token));
        con.connect();
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, "UTF-8");
        OutputStream post = con.getOutputStream();
        entity.writeTo(post);
        post.flush();


        StringBuffer buffer = new StringBuffer();
        InputStream is = con.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line = null;
        while ((line= br.readLine())!=null){
            buffer.append(line);

        }
        is.close();
        con.disconnect();
    }

    private void beginPush(String value)throws Exception{
        URL obj = new URL(Constants.URL_USER_COMMON);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setReadTimeout(10000);
        con.setConnectTimeout(15000);

        con.setRequestMethod("POST");

        con.setRequestProperty("http.agent", "Unknown");

        con.setDoOutput(true);
        con.setDoInput(true);

        List<NameValuePair> params =new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("key", Constants.USER_COMMON_REQUEST_HEADER_KEY));
        params.add(new BasicNameValuePair("p", "beginpush"));
        params.add(new BasicNameValuePair("app_id", "1"));
        params.add(new BasicNameValuePair("val", value));
        params.add(new BasicNameValuePair("device_id", Settings.getDeviceId(this)));

        con.connect();
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, "UTF-8");
        OutputStream post = con.getOutputStream();
        entity.writeTo(post);
        post.flush();

        StringBuffer buffer = new StringBuffer();
        InputStream is = con.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line = null;
        while ((line= br.readLine())!=null){
            buffer.append(line);

        }
        is.close();
        con.disconnect();
    }
}
