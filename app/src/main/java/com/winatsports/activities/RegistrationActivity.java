package com.winatsports.activities;



import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.custom.MyFixedEditText;
import com.winatsports.data.Constants;
import com.winatsports.data.upload.AsyncRegisterAccount;



public class RegistrationActivity extends Activity {
    private BroadcastReceiver downloadStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_DOWNLOAD_STATE)) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateDownloadIndicator();
                    }
                });
            }
        }
    };

    private EditText etRegistrationEmailUsername;
    MyFixedEditText etRegistrationEmailPassword;
    MyFixedEditText etRegistrationEmailFirstName;
    MyFixedEditText etRegistrationEmailLastName;
    MyFixedEditText etRegistrationEmailMobile;
    TextView tvRegistrationEmailTitle, tvRegistrationEmailOptional, tvRegistrationEmailSignIn;
    ImageView ivArrowBackRegistrationEmail;
    Button btRegistrationEmailCreateAccount;

    private AsyncRegisterAccount registrationTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_registration_email);

        etRegistrationEmailUsername = (MyFixedEditText) findViewById(R.id.etRegistrationEmailUsername);
        etRegistrationEmailPassword = (MyFixedEditText) findViewById(R.id.etRegistrationEmailPassword);
        etRegistrationEmailFirstName = (MyFixedEditText) findViewById(R.id.etRegistrationEmailFirstName);
        etRegistrationEmailLastName = (MyFixedEditText) findViewById(R.id.etRegistrationEmailLastName);
        etRegistrationEmailMobile = (MyFixedEditText) findViewById(R.id.etRegistrationEmailMobile);

        etRegistrationEmailUsername.setHint(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_OTHER_EMAIL));
        etRegistrationEmailPassword.setHint(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_PASSWORD));
        etRegistrationEmailFirstName.setHint(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_FIRST_NAME));
        etRegistrationEmailLastName.setHint(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_LAST_NAME));
        etRegistrationEmailMobile.setHint(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_MOBILE));

        addToKeyBoardDoneButton();

        tvRegistrationEmailTitle = (TextView) findViewById(R.id.tvRegistrationEmailTitle);
        tvRegistrationEmailOptional = (TextView) findViewById(R.id.tvRegistrationEmailOptional);
        tvRegistrationEmailSignIn = (TextView) findViewById(R.id.tvRegistrationEmailSignIn);

        tvRegistrationEmailTitle.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_REGISTER));
        tvRegistrationEmailOptional.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SIGNUPOPTIONAL));
        String incorrectText = MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SIGNUPLOGINBUTTON);
        String[] separatedText = incorrectText.split("\\*\\*");
        String correctText = separatedText[0] + " \n " + separatedText[1];
        tvRegistrationEmailSignIn.setText(correctText);
        tvRegistrationEmailSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(RegistrationActivity.this, SignInActivity.class);
                finish();
                startActivity(intent);
                overridePendingTransition(R.anim.end_activity_slide_in_left, R.anim.end_activity_slide_out_right);
            }
        });

        btRegistrationEmailCreateAccount = (Button) findViewById(R.id.btRegistrationEmailCreateAccount);
        btRegistrationEmailCreateAccount.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SIGNUPCREATEACCOUNT));
        btRegistrationEmailCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etRegistrationEmailUsername.getText().length() == 0 || etRegistrationEmailPassword.getText().length() == 0
                    || etRegistrationEmailFirstName.getText().length() == 0 || etRegistrationEmailLastName.getText().length() == 0) {
                    Util.showMessageDialog(RegistrationActivity.this, Util.getString(Constants.MSG_KEY_ERR_MANDATORY), null);
                    return;
                }

                if (registrationTask == null) {
                    registrationTask = new AsyncRegisterAccount(RegistrationActivity.this,
                            etRegistrationEmailUsername.getText().toString(),
                            etRegistrationEmailPassword.getText().toString(),
                            etRegistrationEmailFirstName.getText().toString(),
                            etRegistrationEmailLastName.getText().toString(),
                            etRegistrationEmailMobile.getText().toString());
                    registrationTask.execute();
                } else {
                    // still executing... do nothing
                }

            }
        });

        ivArrowBackRegistrationEmail = (ImageView) findViewById(R.id.ivArrowBackRegistrationEmail);
        ivArrowBackRegistrationEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
    }


    private void addToKeyBoardDoneButton() {

        etRegistrationEmailPassword.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etRegistrationEmailPassword.setSingleLine(true);
        //etRegistrationEmailPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        etRegistrationEmailFirstName.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etRegistrationEmailFirstName.setSingleLine(true);
        etRegistrationEmailLastName.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etRegistrationEmailLastName.setSingleLine(true);
        etRegistrationEmailMobile.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etRegistrationEmailMobile.setSingleLine(true);
    }


    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack() {
        ivArrowBackRegistrationEmail.setOnClickListener(null);
        Intent intent = new Intent(this, GuestActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.end_activity_slide_in_left, R.anim.end_activity_slide_out_right);
        finish();
    }

    public void setRegistrationTask(AsyncRegisterAccount registrationTask) {
        this.registrationTask = registrationTask;
    }


    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(downloadStateReceiver, new IntentFilter(MyApplication.ACTION_DOWNLOAD_STATE));
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(downloadStateReceiver);
    }


    private void updateDownloadIndicator() {
        ProgressBar pbDownload = (ProgressBar) findViewById(R.id.pbDownload);
        if (MyApplication.getInstance().isDownloading())
            pbDownload.setVisibility(View.VISIBLE);
        else
            pbDownload.setVisibility(View.INVISIBLE);
    }
}
