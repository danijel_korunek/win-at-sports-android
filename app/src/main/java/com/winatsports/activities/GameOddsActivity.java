package com.winatsports.activities;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.custom.CounterTextView;
import com.winatsports.data.Constants;
import com.winatsports.data.UserData;
import com.winatsports.data.adapters.GameOddsAdapter;
import com.winatsports.data.adapters.NoRecordsAdapter;
import com.winatsports.data.adapters.SeparatedOddsAdapter;
import com.winatsports.data.elements.Game;
import com.winatsports.data.elements.League;
import com.winatsports.data.elements.Odd;
import com.winatsports.data.elements.OddType;
import com.winatsports.data.elements.Team;
import com.winatsports.dialogs.CustomProgressDialog;
import com.winatsports.dialogs.PlaceSingleBetDialog;
import com.winatsports.dialogs.RealMoneyWarningDialog;
import com.winatsports.settings.Settings;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


/**
 * Created by broda on 09/02/2016.
 *
 * modified by Danijel
 */


public class GameOddsActivity extends Activity {

    Context context;
    public static final String KEY_EVENT_ID = "EVENT_ID";

    private BroadcastReceiver downloadStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_DOWNLOAD_STATE)) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateDownloadIndicator();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_ARRAYS_SWITCHED)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initializeUI();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_LAUNCH_GUEST_ACTIVITY)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent guestActivityIntent = new Intent(GameOddsActivity.this, GuestActivity.class);
                        startActivity(guestActivityIntent);
                        overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                        finish();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_NETWORK_FAILURE)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String str = Util.getString(Constants.MSG_KEY_APP_NOINET);
                        if (!Util.isPopulated(str))
                            str = getResources().getString(R.string.error_no_internet);

                        Util.showMessageDialog(GameOddsActivity.this, str, new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                MyApplication.getInstance().quit();
                                MyApplication.getInstance().shutDownVM();
                            }
                        });
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_REDOWNLOAD)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (MyApplication.getInstance().wasAppInBgMoreThanXSeconds && !Settings.getUserR(getApplicationContext()).equals(Settings.DEFAULT_USER_R)) {
                            //MyApplication.getInstance().wasAppInBgMoreThanXSeconds = false;
                            MyApplication.getInstance().downloadData(GameOddsActivity.this, true, true, true);
                        }
                    }
                });
            }
        }
    };

    private Game game;
    private int eventID = 0;

    SeparatedOddsAdapter separatedOddsAdapter;
    ListView lvOdds;

    private ImageView ivHome;
    private CustomProgressDialog progressDialog;

    ArrayList<Team> allTeamsNames;

    private LinearLayout llSizeOfArrowBack;

    PlaceSingleBetDialog popup = null;
    private RelativeLayout rlMain;
    private TextView tvOddActivityWallet, tvLeagueAndTeamNameOddsActivity, tvDateOfGameOddsActivity, tvGameModeOddsActivity, tvRealMoneyOddsActivity;

    private FrameLayout flBetSlip;
    private CounterTextView tvNumberOfParlayItemsOddsOfGame;

    private LinearLayout llRealBetBar;

    DisplayMetrics metrics = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;

        metrics = getResources().getDisplayMetrics();

        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_odds_of_game);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        llRealBetBar = (LinearLayout) findViewById(R.id.llRealBetBar);
        ivHome = (ImageView) findViewById(R.id.ivHome);
        tvNumberOfParlayItemsOddsOfGame = (CounterTextView) findViewById(R.id.tvNumberOfParlayItemsMainActivity);
        rlMain = (RelativeLayout) findViewById(R.id.rlMain);
        tvOddActivityWallet = (TextView) findViewById(R.id.tvOddActivityWallet);
        tvGameModeOddsActivity = (TextView) findViewById(R.id.tvGameModeOddsActivity);
        tvRealMoneyOddsActivity = (TextView) findViewById(R.id.tvRealMoneyOddsActivity);
        lvOdds = (ListView) findViewById(R.id.lvOdds);
        tvLeagueAndTeamNameOddsActivity = (TextView) findViewById(R.id.tvLeagueAndTeamNameOddsActivity);
        tvDateOfGameOddsActivity = (TextView) findViewById(R.id.tvDateOfGameOddsActivity);
        llSizeOfArrowBack = (LinearLayout)findViewById(R.id.llWidthHeightOfArrowBack);

        llSizeOfArrowBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        flBetSlip = (FrameLayout) findViewById(R.id.flBetSlip);
        flBetSlip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( MyApplication.getInstance().getParlayArray().size() > 0 ) {

                    Intent intent = new Intent(GameOddsActivity.this, BetSlipActivity.class);
                    intent.putExtra("EVENT_ID", eventID);

                    finish();
                    startActivity(intent);
                    overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                }
                else {

                    Util.showMessageDialog(context, MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_BETSLIPISEMPTY), new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            dialog.dismiss();
                        }
                    });
                }
            }
        });

        tvNumberOfParlayItemsOddsOfGame.setCounter(MyApplication.getInstance().getParlayArray().size());

        if (getIntent() != null && getIntent().hasExtra(KEY_EVENT_ID))
            eventID = getIntent().getIntExtra(KEY_EVENT_ID, 0);

        initializeUI();

        MyApplication.getInstance().getActivityStack().push(GameOddsActivity.class.getName());
    }

    private String generateScrollingMarqueeText(Game g) {
        String homeAway = Util.formatHomeVsAway(g, this);
        String date = g.getStartDate();
        League league = Util.getLeague(g.getLeagueID());
        String leagueName = league != null && league.getLeagueName() != null ? league.getLeagueName() : "";

        if (g.getStatus() != 0) {
            String statusText = Util.getString(Constants.MSG_KEY_ODDSGAMEINPROGRESS);
            if (!Util.isPopulated(g.getGameDesc()))
                return leagueName + " * " + statusText + " * " + homeAway + " * " + statusText + " * " + date + " * " + statusText;
            else
                return leagueName + " * " + statusText + " * " + homeAway + " * " + statusText + " * " + g.getGameDesc() + " * " + date + " * " + statusText;
        } else {
            if (!Util.isPopulated(g.getGameDesc()))
                return leagueName + " * " + homeAway + " * " + date;
            else
                return leagueName + " * " + homeAway + " * " + g.getGameDesc() + " * " + date;
        }
    }
//
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            if (popup != null && popup.isShowing())
//                popup.dismiss();
//            goBack();
//        }
//        return false; //to prevent this event from being propagated further.
//    }

    @Override
    protected void onResume() {
        super.onResume();

        tvNumberOfParlayItemsOddsOfGame.setCounter(MyApplication.getInstance().getParlayArray().size());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MyApplication.ACTION_ARRAYS_SWITCHED);
        intentFilter.addAction(MyApplication.ACTION_DOWNLOAD_STATE);
        intentFilter.addAction(MyApplication.ACTION_LAUNCH_GUEST_ACTIVITY);
        intentFilter.addAction(MyApplication.ACTION_NETWORK_FAILURE);
        intentFilter.addAction(MyApplication.ACTION_REDOWNLOAD);
        registerReceiver(downloadStateReceiver, intentFilter);
    }


    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(downloadStateReceiver);
        dismissProgressDialog();
    }

    public void showProgressDialog() {
        progressDialog = new CustomProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private void updateDownloadIndicator() {
        ProgressBar pbDownload = (ProgressBar) findViewById(R.id.pbDownload);
        if (MyApplication.getInstance().isDownloading())
            pbDownload.setVisibility(View.VISIBLE);
        else
            pbDownload.setVisibility(View.INVISIBLE);
    }


    @Override
    public void onBackPressed() {
        goBack();
    }

    public void goBack() {
        Log.d(GameOddsActivity.class.getSimpleName(), "Going back");
        llSizeOfArrowBack.setOnClickListener(null);

        if (MyApplication.getInstance() != null && MyApplication.getInstance().getActivityStack() != null &&
                !MyApplication.getInstance().getActivityStack().empty() && MyApplication.getInstance().getActivityStack().size() != 1) {

            MyApplication.getInstance().getActivityStack().pop(); // remove the current class
            String previousClassName = MyApplication.getInstance().getActivityStack().pop();
            Class sourceClass = null;

            try {
                sourceClass = Class.forName(previousClassName);

                Intent intent = new Intent(GameOddsActivity.this, sourceClass);
                Bundle bundle = new Bundle();
                if (game != null) {
                    bundle.putInt("SPORT_ID", game.getSportID());
                    bundle.putInt("LEAGUE_ID", game.getLeagueID());
                }
                intent.putExtras(bundle);

                finish();
                startActivity(intent);
                overridePendingTransition(R.anim.end_activity_slide_in_left, R.anim.end_activity_slide_out_right);
            } catch (Exception e) {
                Log.e(GameOddsActivity.class.getSimpleName(), "", e);
            }
        } else {
            Log.d(GameOddsActivity.class.getSimpleName(), "Just finished...");
            finish();
        }
    }

    public void initializeUI() {
        // init
        Log.e(GameOddsActivity.class.getSimpleName(), "Event ID: " + eventID);
        game = Util.getGame(eventID);
        if (game == null) {
            // don't initialize UI at all
            Log.e(GameOddsActivity.class.getSimpleName(), "Game null... UI initialization postponed until arrays are switched.");
            return;
        }

        if (MyApplication.getInstance().getActivityStack().size() >= 2) {
            ivHome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ivHome.setOnClickListener(null);
                    Intent intent = new Intent(GameOddsActivity.this, MainActivity.class);

                    startActivity(intent);
                    overridePendingTransition(R.anim.end_activity_slide_in_left, R.anim.end_activity_slide_out_right);
                    finish();
                }
            });
        } else {
            ivHome.setVisibility(View.GONE);
        }

        final UserData userData = MyApplication.getInstance().getUserData();
        tvOddActivityWallet.setText(Util.generateWalletTitle());

        Util.showAnimationForWalletTitle(tvOddActivityWallet);

        tvGameModeOddsActivity.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_REALBETTINGTAB1));
        tvRealMoneyOddsActivity.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_REALBETTINGTAB2));

        tvRealMoneyOddsActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user_r = Settings.getUserR(context);

                if( user_r.equals(Settings.DEFAULT_USER_R) ) {
                    RealMoneyWarningDialog realMoney = new RealMoneyWarningDialog(GameOddsActivity.this, rlMain);
                    realMoney.show();
                } else {
                    if (MyApplication.getInstance().getHeaderMainClass().isShowingRealBettingTutorial()
                            && !Settings.wasRealBettingTutorialShown(GameOddsActivity.this)) {
                        Settings.setRealBettingTutorialShown(GameOddsActivity.this, true);
                        Util.showRealBettingTutorial(GameOddsActivity.this, eventID);
                    } else {
                        MyApplication.getInstance().startRealBetting(false, GameOddsActivity.this, eventID);
                    }
                }
            }
        });

        if (!Util.isRealBettingEnabled(this))
            llRealBetBar.setVisibility(View.GONE);
        else
            llRealBetBar.setVisibility(View.VISIBLE);

        separatedOddsAdapter = new SeparatedOddsAdapter(this);

        View footer = new View(this);

        footer.setBackgroundColor(getResources().getColor(R.color.list_divider_color));
        lvOdds.addFooterView(footer);

        // after that I want to find teams name of this GAME
        // This arrayList "allTeamsNames" is most of times,, size of 2 items
        allTeamsNames = new ArrayList<Team>();

        if (game.getTeamArrayList() != null) {
            for (int index1 = 0; index1 < game.getTeamArrayList().size(); index1++) {

                Team team1 = game.getTeamArrayList().get(index1);
                // I need to put here "game.getSportID()-1", because sportID are starting counting from 1 - 10,
                // and arrayList is counting from 0 - 9
                for (int index2 = 0; index2 < MyApplication.getInstance().getAllSports().get(game.getSportID() - 1).getTeams().size(); index2++) {
                    Team team2 = MyApplication.getInstance().getAllSports().get(game.getSportID() - 1).getTeams().get(index2);
                    if (team1.getTeamID() == team2.getTeamID()) {
                        allTeamsNames.add(team2);
                        break;
                    }
                }
            }
        }


        tvLeagueAndTeamNameOddsActivity.setSelected(true);
        tvLeagueAndTeamNameOddsActivity.setText(generateScrollingMarqueeText(game));

        tvDateOfGameOddsActivity.setText(game.getStartDate());
        tvDateOfGameOddsActivity.setSelected(true);

        if (!game.getOddArrayList().isEmpty()) {
            Collections.sort(game.getOddArrayList(), new Comparator<Odd>() {
                @Override
                public int compare(Odd o1, Odd o2) {
                    if (o1.getSortId() > o2.getSortId()) {
                        return 1;
                    } else if (o1.getSortId() < o2.getSortId())
                        return -1;
                    else
                        return 0;
                }
            });

            ArrayList<OddType> oddTypes = new ArrayList<>();

            for (Odd o : game.getOddArrayList()) {

                OddType ot = new OddType();
                ot.setTypeName(o.getBetTypeName());
                ot.setBetTypeId(o.getBetTypeId());

                // don't create duplicates
                boolean found = false;
                for (OddType tmpOddType : oddTypes) {
                    if (tmpOddType.getBetTypeId() == ot.getBetTypeId()
                            && Util.isPopulated(tmpOddType.getTypeName())
                            && tmpOddType.getTypeName().equalsIgnoreCase(ot.getTypeName())) {
                        found = true;
                        break;
                    }
                }

                if (!found)
                    oddTypes.add(ot);

            }

            for (Odd o : game.getOddArrayList()) {
                for (OddType ot : oddTypes) {
                    if (ot.getBetTypeId() == o.getBetTypeId()
                            && Util.isPopulated(ot.getTypeName())
                            && Util.isPopulated(o.getBetTypeName())
                            && ot.getTypeName().equalsIgnoreCase(o.getBetTypeName())) {
                        ot.getOdds().add(o);
                    }
                }
            }

            // generate odd adapters
            for (OddType oddType : oddTypes) {

                GameOddsAdapter gameOddsAdapter = new GameOddsAdapter(GameOddsActivity.this, game, allTeamsNames, oddType);
                separatedOddsAdapter.addSection(oddType.getTypeName(), gameOddsAdapter);
            }

        } else {
            NoRecordsAdapter noRecordsAdapter = new NoRecordsAdapter(GameOddsActivity.this, Util.getString(Constants.MSG_KEY_NO_RECORDS));
            separatedOddsAdapter.addSection(Util.getString(Constants.MSG_KEY_NO_RECORDS), noRecordsAdapter);
        }

        // on the end I fill up listView with all data from ===>> separatedOddsAdapter
        lvOdds.setAdapter(separatedOddsAdapter);
        lvOdds.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long duration) {

                Object item = separatedOddsAdapter.getItem(position);

                TextView descriptionOdds = (TextView) view.findViewById(R.id.tvDescriptionOdds);
                TextView valueBets = (TextView) view.findViewById(R.id.tvValueOdds);

                String sendTitleOdds = descriptionOdds.getText().toString() + " (" + valueBets.getText().toString() + ")";

                if (item instanceof Odd) {

                    Odd finalOdd = (Odd) item;

                    // If status is equal to 0, that means, that user CAN bet on this game, event
                    if (game.getStatus() == 0) {
                        if (popup != null)
                            popup.dismiss();

                        popup = new PlaceSingleBetDialog(GameOddsActivity.this, tvNumberOfParlayItemsOddsOfGame, sendTitleOdds, finalOdd, game, allTeamsNames, rlMain);
                        popup.show();
                    }
                    // If status is not equal to 0, that means, that user CAN NOT bet on this game, event
                    else {
                        Util.showMessageDialog(context, MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_ODDSGAMEINPROGRESS), new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                dialog.dismiss();
                            }
                        });
                    }
                }
            }
        });

    }

}
