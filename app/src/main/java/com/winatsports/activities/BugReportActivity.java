package com.winatsports.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Process;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.data.Constants;

import java.util.regex.Pattern;


/**
 * Created by dkorunek on 18/05/16.
 */
public class BugReportActivity extends Activity {
    private Button btnSend;
    private Button btnCancel;

    private TextView tvBugReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_bug_report);

        tvBugReport = (TextView) findViewById(R.id.tvBugReport);
        String bugReport = getIntent().getStringExtra("bug_report");

        // filter out what should not be shown
        bugReport = bugReport.replaceAll("(?i)" + Pattern.quote("http://www.eclecticasoft.com/betting3/"), "");
        bugReport = bugReport.replaceAll("(?i)" + Pattern.quote("eclectica"), "");
        bugReport = bugReport.replaceAll("(?i)" + Pattern.quote("eclecticasoft.com"), "");

        tvBugReport.setText(bugReport);

        btnSend = (Button) findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("plain/text");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "sb.crash.reports@gmail.com" });
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Sports Betting CRASH REPORT");
                    //intent.putExtra (Intent.EXTRA_STREAM, Uri.parse ("file://" + fullName));
                    intent.putExtra(Intent.EXTRA_TEXT, tvBugReport.getText().toString());
                    startActivity(intent);
                    finish();
                    android.os.Process.killProcess(Process.myPid());
                    System.exit(0);
                } catch (Exception e) {
                    Util.showMessageDialog(BugReportActivity.this, getString(R.string.no_email_client), new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            android.os.Process.killProcess(Process.myPid());
                            System.exit(0);
                        }
                    });
                }
            }
        });

        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                MyApplication.getInstance().quit();
            }
        });
    }
}
