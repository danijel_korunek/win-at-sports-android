package com.winatsports.activities;


import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.custom.CounterTextView;
import com.winatsports.custom.CustomSingleSelectionLayout;
import com.winatsports.data.Constants;
import com.winatsports.data.SupportLang;
import com.winatsports.data.UserData;
import com.winatsports.data.adapters.LanguageAdapter;
import com.winatsports.data.adapters.SettingsAdapter;
import com.winatsports.dialogs.CustomProgressDialog;
import com.winatsports.settings.AsyncChangeSettings;
import com.winatsports.settings.Settings;

import java.util.ArrayList;


public class SettingsActivity extends Activity {

    Context context;
    private BroadcastReceiver downloadStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_DOWNLOAD_STATE)) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateDownloadIndicator();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_LAUNCH_GUEST_ACTIVITY)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent guestActivityIntent = new Intent(SettingsActivity.this, GuestActivity.class);
                        startActivity(guestActivityIntent);
                        overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                        finish();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_REDOWNLOAD)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (MyApplication.getInstance().wasAppInBgMoreThanXSeconds && !Settings.getUserR(getApplicationContext()).equals(Settings.DEFAULT_USER_R)) {
                            //MyApplication.getInstance().wasAppInBgMoreThanXSeconds = false;
                            MyApplication.getInstance().downloadData(SettingsActivity.this, true, true, true);
                        }
                    }
                });
            }
        }
    };

    private LinearLayout llSizeOfArrowBack;

    ListView lvSettings;

    private CustomProgressDialog progressDialog;

    TextView tvLanguage, tvChangeLanguage, tvSetLanguage, tvSettingsActivityWallet, tvSettingsMainText;
    LinearLayout llLanguage;

    private String selectedLanguage;
    private FrameLayout flBetSlip;

    int selectedIndex = -1;

    public Handler notificationHandler;

    private CounterTextView tvNumberOfParlayItemsSettings;
    private AsyncChangeSettings task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        context = this;

        notificationHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);

                Util.showMessageDialog(context, MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_NOINET),
                        new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                dialog.dismiss();
                                MyApplication.getInstance().setQuitting(true);
                                MyApplication.getInstance().quit();
                                MyApplication.getInstance().shutDownVM();
                            }
                        });
            }
        };

        setupViews();

        MyApplication.getInstance().getActivityStack().push(SettingsActivity.class.getName());
    }

    private void setUpCustomSettingsAdapter() {

        ArrayList<String> settingsTextOnTop = new ArrayList<String>();
        settingsTextOnTop.add( MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SETTINGSSTARTGAMETEXT1));
        settingsTextOnTop.add( MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SETTINGSENDGAMETEXT1));
        settingsTextOnTop.add( MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SETTINGSBETCONFIRMATIONTEXT1));
        settingsTextOnTop.add( MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SETTINGSBETAMOUNTTEXT1));
        settingsTextOnTop.add( MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SETTINGSSHOWEMPTY1));
        settingsTextOnTop.add( MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SETTINGSBETAMOUNTMODE1));

        ArrayList<String> settingsTextOnBottom = new ArrayList<String>();
        settingsTextOnBottom.add(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SETTINGSSTARTGAMETEXT2));
        settingsTextOnBottom.add(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SETTINGSENDGAMETEXT2));
        settingsTextOnBottom.add( MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SETTINGSBETCONFIRMATIONTEXT2));
        settingsTextOnBottom.add( MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SETTINGSBETAMOUNTTEXT2));
        settingsTextOnBottom.add(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SETTINGSSHOWEMPTY2));
        settingsTextOnBottom.add(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SETTINGSBETAMOUNTMODE2));

        if (lvSettings != null ) {
            SettingsAdapter settingsAdapter = new SettingsAdapter( SettingsActivity.this, context, settingsTextOnTop, settingsTextOnBottom );
            lvSettings.setAdapter(settingsAdapter);
            lvSettings.setEnabled(true);
        }

    }
    // TODO don't erase this!
    private void showLanguageDialog() {
        final Dialog langDialog = new Dialog(this);

        langDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        langDialog.setContentView(R.layout.settings_language_view);

        String selectedLanguageId = Settings.getLanguage(this);

        SupportLang temp = null;
        int tmpPos = -1;

        for (int index = 0; index < MyApplication.getInstance().getUserData().getSupportLangs().size(); index++) {
            SupportLang supportLang = MyApplication.getInstance().getUserData().getSupportLangs().get(index);
            if (supportLang.getLangID().equalsIgnoreCase(selectedLanguageId)) {
                selectedIndex = index;
                temp = supportLang;
                tmpPos = index;
                break;
            }
        }

        // set up list adapter
        final SupportLang selectedLanguage = temp;
        final int oldSelectedIndex = tmpPos;
        final ListView lvLanguages = (ListView) langDialog.findViewById(R.id.lvLanguages);
        final LanguageAdapter adapter = new LanguageAdapter(MyApplication.getInstance().getUserData().getSupportLangs());
        adapter.setSelected(oldSelectedIndex);
        lvLanguages.setAdapter(adapter);
        lvLanguages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LanguageAdapter.Language lItem = (LanguageAdapter.Language) adapter.getItem(position);
                adapter.setSelected(position);
                selectedIndex = position;
                adapter.notifyDataSetChanged();
            }
        });

        // set up button for language selection
        final Button btnSelectLanguage = (Button) langDialog.findViewById(R.id.btnOk);
        btnSelectLanguage.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_OTHER_LANG_CHANGE));
        btnSelectLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedIndex == oldSelectedIndex)
                    return;
                SupportLang selectedLang = MyApplication.getInstance().getUserData().getSupportLangs().get(selectedIndex);

                String newLanguageId = selectedLang.getLangID();
                String newLanguageName = selectedLang.getLangName();


                if (task == null || (task != null && (task.getStatus() == AsyncTask.Status.FINISHED))) {
                    task = new AsyncChangeSettings(SettingsActivity.this, tvSetLanguage, newLanguageName, newLanguageId);
                    task.execute();
                } else {
                    Toast.makeText(SettingsActivity.this, getString(R.string.task_in_progress), Toast.LENGTH_LONG).show();
                }
                langDialog.dismiss();
            }
        });


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(langDialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = (int) (getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
        langDialog.show();
        langDialog.getWindow().setAttributes(lp);
    }


    @Override
    public void onBackPressed() {
        goBack();
    }


    @Override
    protected void onResume() {
        super.onResume();

        tvNumberOfParlayItemsSettings.setCounter(MyApplication.getInstance().getParlayArray().size());
        IntentFilter filter = new IntentFilter();
        filter.addAction(MyApplication.ACTION_DOWNLOAD_STATE);
        filter.addAction(MyApplication.ACTION_LAUNCH_GUEST_ACTIVITY);
        filter.addAction(MyApplication.ACTION_REDOWNLOAD);
        registerReceiver(downloadStateReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(downloadStateReceiver);
        dismissProgressDialog();
    }


    private void updateDownloadIndicator() {
        ProgressBar pbDownload = (ProgressBar) findViewById(R.id.pbDownload);
        if (MyApplication.getInstance().isDownloading())
            pbDownload.setVisibility(View.VISIBLE);
        else
            pbDownload.setVisibility(View.INVISIBLE);
        setupViews();
    }

    private void setupViews() {
        tvNumberOfParlayItemsSettings = (CounterTextView) findViewById(R.id.tvNumberOfParlayItemsMainActivity);
        tvNumberOfParlayItemsSettings.setCounter(MyApplication.getInstance().getParlayArray().size());

        tvSettingsMainText = (TextView) findViewById(R.id.tvSettingsMainText);
        tvSettingsMainText.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PROFILESETTINGSTEXT1));

        UserData userData = MyApplication.getInstance().getUserData();
        tvSettingsActivityWallet = (TextView) findViewById(R.id.tvSettingsActivityWallet);
        tvSettingsActivityWallet.setText(Util.generateWalletTitle());

        Util.showAnimationForWalletTitle(tvSettingsActivityWallet);

        CustomSingleSelectionLayout oddTypesLayout = (CustomSingleSelectionLayout)findViewById(R.id.customOddTypeLayout);
        ArrayList<String> oddTypesOptions = new ArrayList<String>();

        String oddDecimalText = MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SETTINGSODDDECIMAL);
        String oddFractionText = MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SETTINGSODDFRACTIONAL);
        String oddUSText = MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SETTINGSODDUS);

        // set title and pass activity for showing notification, when user want to change settings if there is no internet
        oddTypesLayout.setTitle(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SETTINGSODDTITLE));

        oddTypesOptions.add(oddUSText);   // on first index in arrayList will be oddUSID because I need to display them in that order
        oddTypesOptions.add(oddDecimalText);  // on second index in arrayList will be decimals ODD because I need to display them in that order
        oddTypesOptions.add(oddFractionText);

        oddTypesLayout.passActivity(SettingsActivity.this);
        oddTypesLayout.setOptionKey(Settings.KEY_ODD_TYPE);
        oddTypesLayout.setOptions(oddTypesOptions);

        CustomSingleSelectionLayout teamOrderLayout = (CustomSingleSelectionLayout)findViewById(R.id.customTeamOrderLayout);
        ArrayList<String> teamOrderOptions = new ArrayList<String>();

        String teamOrderAwayVsHomeText = MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SETTINGSTEAMORDERAWAYVSHOME);
        String teamOrderHomeVsAwayText = MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SETTINGSTEAMORDERHOMEVSAWAY);
        teamOrderLayout.setTitle(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SETTINGSTEAMORDERTITLE));

        teamOrderOptions.add(teamOrderHomeVsAwayText);  // on first index in arrayList will be HOME_VS_AWAY because I need to display them in that order
        teamOrderOptions.add(teamOrderAwayVsHomeText);  // on second index in arrayList will be AWAY_VS_HOME because I need to display them in that order

        teamOrderLayout.passActivity(SettingsActivity.this);
        teamOrderLayout.setOptionKey(Settings.KEY_TEAM_ORDER);
        teamOrderLayout.setOptions(teamOrderOptions);

//      TODO: keep this commented. It might be needed in the future
//        llLanguage = (LinearLayout) findViewById(R.id.llLanguage);
//        tvLanguage = (TextView)findViewById(R.id.tvLanguage);
//        tvLanguage.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_OTHER_LANG));
//        tvChangeLanguage = (TextView)findViewById(R.id.tvChangeLanguage);
//        tvChangeLanguage.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_OTHER_LANG_CHANGE));
//
//        tvSetLanguage = (TextView)findViewById(R.id.tvSetLanguage);
//        String langID = Settings.getLanguage(this);
//
//        for( int index=0; index < MyApplication.getInstance().getUserData().getSupportLangs().size(); index++  ) {
//
//            SupportLang supportLang = MyApplication.getInstance().getUserData().getSupportLangs().get(index);
//            if( supportLang.getLangID().equalsIgnoreCase(langID) ) {
//                tvSetLanguage.setText(supportLang.getLangName());
//                break;
//            }
//        }

//        llLanguage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showLanguageDialog();
//            }
//        });

        lvSettings = (ListView)findViewById(R.id.lvSettings);
        View footer = new View(this);

        footer.setBackgroundColor(getResources().getColor(R.color.list_divider_color));
        lvSettings.addFooterView(footer);
        setUpCustomSettingsAdapter();

        llSizeOfArrowBack = (LinearLayout)findViewById(R.id.llWidthHeightOfArrowBack);
        llSizeOfArrowBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        flBetSlip = (FrameLayout) findViewById(R.id.flBetSlip);
        flBetSlip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( MyApplication.getInstance().getParlayArray().size() > 0 ) {
                    Intent intent = new Intent(SettingsActivity.this, BetSlipActivity.class);
                    finish();
                    startActivity(intent);
                    overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                }
                else {

                    Util.showMessageDialog(context, MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_BETSLIPISEMPTY),
                            new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    dialog.dismiss();
                                }
                            });
                }
            }
        });
    }

    private void goBack() {
        llSizeOfArrowBack.setOnClickListener(null);
        MyApplication.getInstance().getActivityStack().pop(); // remove the current class
        String previousClassName = MyApplication.getInstance().getActivityStack().pop();
        Class sourceClass = null;

        try {
            sourceClass = Class.forName(previousClassName);
        } catch (Exception e) {
            Log.e(BetSlipActivity.class.getSimpleName(), "", e);
        }

        Intent intent = new Intent(this, sourceClass);
        startActivity(intent);
        overridePendingTransition(R.anim.end_activity_slide_in_left, R.anim.end_activity_slide_out_right);
        finish();
    }

    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void showProgressDialog() {
        dismissProgressDialog();
        progressDialog = new CustomProgressDialog(this);
        progressDialog.show();
    }

}
