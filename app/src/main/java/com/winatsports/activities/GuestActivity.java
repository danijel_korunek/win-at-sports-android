package com.winatsports.activities;



import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tune.TuneEvent;
import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.data.Constants;
import com.winatsports.data.upload.AsyncUploadFacebookInfo;
import com.winatsports.dialogs.CustomProgressDialog;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.tune.Tune;

import org.json.JSONObject;

import java.util.Arrays;


public class GuestActivity extends Activity {

    private BroadcastReceiver downloadStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_DOWNLOAD_STATE)) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateDownloadIndicator();
                    }
                });
            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_NETWORK_FAILURE)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String str = Util.getString(Constants.MSG_KEY_APP_NOINET);
                        if (!Util.isPopulated(str))
                            str = getResources().getString(R.string.error_no_internet);

                        Util.showMessageDialog(GuestActivity.this, str, new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                MyApplication.getInstance().quit();
                                MyApplication.getInstance().shutDownVM();
                            }
                        });
                    }
                });
            }
        }
    };

    TextView tvLoginTitle, tvOpenFreeAccount, tvSignUpNow, tvSignWithEmail, tvSignWithFacebook, tvContinueAsGuest, tvSignInNow;
    ImageView ivArrowBackLogin;
    LinearLayout llSignUpWithEmail, llSignInNow;

    private CallbackManager callbackManager;
    private CustomProgressDialog progressDialog;
    private boolean showRbDialogAgain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MyApplication.getInstance().getActivityStack().clear();

        setContentView(R.layout.activity_login);

        progressDialog = new CustomProgressDialog(this);

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d(GuestActivity.class.getSimpleName(), "Facebook login success");

                        if (progressDialog == null) {
                            progressDialog = new CustomProgressDialog(GuestActivity.this);
                            progressDialog.show();
                        }

                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        if (response.getError() == null) {
                                            // parse facebook response
                                            try {
                                                /*{"id":"101719493556835","first_name":"Danijel","birthday":"04\/04\/1983","hometown":{"id":"111949768822901","name":"Nedelisce"},
                                                "location":{"id":"110838155611027","name":"Varazdin"},"email":"danijel.korunek@meritumsoft.hr","locale":"en_US","last_name":"Korunek","gender":"male"}*/
                                                JSONObject json = response.getJSONObject();
                                                Log.d(GuestActivity.class.getSimpleName(), json.toString());
                                                AsyncUploadFacebookInfo.FBData fbData = new AsyncUploadFacebookInfo.FBData();
                                                fbData.id = json.has("id") ? json.getString("id") : "";
                                                fbData.birthDate = json.has("birthday") ? json.getString("birthday") : "";
                                                if (json.has("location")) {
                                                    JSONObject loc = json.getJSONObject("location");
                                                    if (loc.has("name"))
                                                        fbData.city = loc.getString("name");
                                                    else
                                                        fbData.city = "";
                                                } else if (json.has("hometown")) {
                                                    JSONObject hometown = json.getJSONObject("hometown");
                                                    if (hometown.has("name"))
                                                        fbData.city = hometown.getString("name");
                                                    else
                                                        fbData.city = "";
                                                } else {
                                                    fbData.city = "";
                                                }

                                                fbData.firstName = json.has("first_name") ? json.getString("first_name") : "";
                                                fbData.lastName = json.has("last_name") ? json.getString("last_name") : "";
                                                fbData.localLanguage = json.has("locale") ? json.getString("locale") : "";
                                                fbData.gender = json.has("gender") ? json.getString("gender") : "";
                                                fbData.email = json.has("email") ? json.getString("email") : "";

                                                Tune.getInstance().measureEvent("Register");

                                                new AsyncUploadFacebookInfo(GuestActivity.this, fbData).execute();
                                            } catch (Exception e) {
                                                Log.e(GuestActivity.class.getSimpleName(), "Error parsing FB JSON response", e);
                                                dismissProgressDialog();
                                                Util.showMessageDialog(GuestActivity.this, getString(R.string.error_retrieving_fb_data), null);
                                            }
                                        } else {
                                            // process error
                                            dismissProgressDialog();
                                            Util.showMessageDialog(GuestActivity.this, getString(R.string.error_retrieving_fb_data) + "\n" + response.getError().getErrorMessage(), null);
                                        }
                                    }

                                });

                        Bundle requestBundle = new Bundle();
                        requestBundle.putString("fields", "email, birthday, locale, first_name, last_name, gender, location, hometown");
                        request.setParameters(requestBundle);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Log.d(GuestActivity.class.getSimpleName(), "Facebook login cancelled");
                        dismissProgressDialog();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        String message = Util.getString(Constants.MSG_KEY_ERR_TRY_AGAIN); //R.string.error_logging_into_fb); //: exception.getLocalizedMessage();

                        dismissProgressDialog();
                        Util.showMessageDialog(GuestActivity.this, message, null);
                        Log.e(GuestActivity.class.getSimpleName(), "Facebook error", exception);
                    }
                });



        tvLoginTitle = (TextView) findViewById(R.id.tvLoginTitle);
        tvOpenFreeAccount = (TextView) findViewById(R.id.tvOpenFreeAccount);
        tvSignUpNow = (TextView) findViewById(R.id.tvSignUpNow);
        tvSignWithEmail = (TextView) findViewById(R.id.tvSignWithEmail);
        tvSignWithFacebook = (TextView) findViewById(R.id.tvSignWithFacebook);
        tvContinueAsGuest = (TextView) findViewById(R.id.tvContinueAsGuest);
        tvSignInNow = (TextView) findViewById(R.id.tvSignInNow);
        llSignInNow = (LinearLayout) findViewById(R.id.llSignInNow);

        tvLoginTitle.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_PROFILESIGNUPTEXT2));
        tvOpenFreeAccount.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_SIGNUP));
        tvSignUpNow.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_LOGINSIGNUPNOW));
        tvSignWithEmail.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_LOGINEMAIL));
        tvSignWithFacebook.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_LOGINFACEBOOK));
        tvContinueAsGuest.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_LOGINGUEST));

        tvSignWithFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (progressDialog != null && !progressDialog.isShowing())
                    progressDialog.show();
                else {
                    progressDialog = new CustomProgressDialog(GuestActivity.this);
                    progressDialog.show();
                }

                LoginManager.getInstance().logInWithReadPermissions(GuestActivity.this,
                        Arrays.asList("user_hometown", "user_location", "user_about_me", "user_birthday", "email", "user_photos"));
            }
        });

        tvSignInNow.setText(Util.getString(Constants.MSG_KEY_LOGINALREADYHAVE));
        llSignInNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (progressDialog != null && !progressDialog.isShowing())
                    progressDialog.show();
                else {
                    progressDialog = new CustomProgressDialog(GuestActivity.this);
                    progressDialog.show();
                }

                Intent intent = new Intent(GuestActivity.this, SignInActivity.class);

                finish();
                startActivity(intent);
                overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
            }
        });

        ivArrowBackLogin = (ImageView) findViewById(R.id.ivArrowBackLogin);
        ivArrowBackLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        llSignUpWithEmail = (LinearLayout)findViewById(R.id.llSignUpWithEmail);
        llSignUpWithEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GuestActivity.this, RegistrationActivity.class);

                startActivity(intent);
                overridePendingTransition(R.anim.start_activity_slide_in_right, R.anim.start_activity_slide_out_left);
                finish();
            }
        });

        tvContinueAsGuest = (TextView) findViewById(R.id.tvContinueAsGuest);
        tvContinueAsGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack() {
        ivArrowBackLogin.setOnClickListener(null);

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(MyApplication.KEY_REENTERED, MyApplication.getInstance().wasAppInBgMoreThanXSeconds);
        startActivity(intent);
        overridePendingTransition(R.anim.end_activity_slide_in_left, R.anim.end_activity_slide_out_right);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(MyApplication.ACTION_DOWNLOAD_STATE);
        filter.addAction(MyApplication.ACTION_NETWORK_FAILURE);
        registerReceiver(downloadStateReceiver, filter);

    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(downloadStateReceiver);
        dismissProgressDialog();
    }


    private void updateDownloadIndicator() {
        ProgressBar pbDownload = (ProgressBar) findViewById(R.id.pbDownload);
        if (MyApplication.getInstance().isDownloading())
            pbDownload.setVisibility(View.VISIBLE);
        else
            pbDownload.setVisibility(View.INVISIBLE);
    }

    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}

