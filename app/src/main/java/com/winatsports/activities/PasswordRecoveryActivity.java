package com.winatsports.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.winatsports.MyApplication;
import com.winatsports.R;
import com.winatsports.Util;
import com.winatsports.data.Constants;
import com.winatsports.data.upload.AsyncForgotPassword;
import com.winatsports.dialogs.CustomProgressDialog;


public class PasswordRecoveryActivity extends Activity {

    private BroadcastReceiver downloadStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(MyApplication.ACTION_DOWNLOAD_STATE)) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateDownloadIndicator();
                    }
                });
            }
        }
    };

    TextView tvLoginPasswordTitle, tvEnterValidEmailAddress;
    ImageView ivArrowBackLoginPasswordRecovery;
    EditText etLoginPasswordRecovery;
    Button btLoginPassword;

    private CustomProgressDialog progressDialog;

    private AsyncForgotPassword task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_password_recovery);

        tvLoginPasswordTitle = (TextView) findViewById(R.id.tvLoginPasswordTitle);
        tvEnterValidEmailAddress = (TextView) findViewById(R.id.tvEnterValidEmailAddress);

        tvLoginPasswordTitle.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_APP_PASS_RECOVER));
        tvEnterValidEmailAddress.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_RECOVERPASSWORDTEXT));

        etLoginPasswordRecovery = (EditText) findViewById(R.id.etLoginPasswordRecovery);
        etLoginPasswordRecovery.setHint(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_OTHER_EMAIL));

        btLoginPassword = (Button) findViewById(R.id.btLoginPassword);
        btLoginPassword.setText(MyApplication.getInstance().getAllMessages().get(Constants.MSG_KEY_RECOVERPASSWORDBUTTON));
        btLoginPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handlePasswordRecovery();
            }
        });

        ivArrowBackLoginPasswordRecovery = (ImageView) findViewById(R.id.ivArrowBackLoginPasswordRecovery);
        ivArrowBackLoginPasswordRecovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(downloadStateReceiver, new IntentFilter(MyApplication.ACTION_DOWNLOAD_STATE));
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(downloadStateReceiver);
    }

    private void goBack() {
        ivArrowBackLoginPasswordRecovery.setOnClickListener(null);
        Intent intent = new Intent(PasswordRecoveryActivity.this, SignInActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.end_activity_slide_in_left, R.anim.end_activity_slide_out_right);
        finish();
    }

    private void updateDownloadIndicator() {
        ProgressBar pbDownload = (ProgressBar) findViewById(R.id.pbDownload);
        if (MyApplication.getInstance().isDownloading())
            pbDownload.setVisibility(View.VISIBLE);
        else
            pbDownload.setVisibility(View.INVISIBLE);
    }

    public void handlePasswordRecovery() {
        if (task != null) {
            Log.d(PasswordRecoveryActivity.class.getSimpleName(), "task != null");
            return;
        } else {
            String emailAddress = etLoginPasswordRecovery.getText().toString();
            if (emailAddress.isEmpty()) {
                Util.showMessageDialog(this, Util.getString(Constants.MSG_KEY_ERR_MANDATORY), null);
                return;
            }
            showProgressDialog();
            task = new AsyncForgotPassword(PasswordRecoveryActivity.this, etLoginPasswordRecovery.getText().toString());
            task.execute();
        }
    }
    public void resetTask() {
        task = null;
    }

    public void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new CustomProgressDialog(this);
            progressDialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

}
